#include "head.h"
#include <assert.h>

namespace LibTypes {

class Checker {
	Checker() {
		assert( sizeof(ubyte)==1 && "ubyte must contain one byte" );
		assert( sizeof(uint16)==2 && "uint16 must contain two bytes" );
		assert( sizeof(uint32)==4 && "uint32 must contain four bytes" );
		assert( sizeof(ubyte)==sizeof(sbyte) && "ubyte and sbyte must contain equal byte count" );
		assert( sizeof(uint16)==sizeof(sint16) && "uint16 and sint16 must contain equal byte count" );
		assert( sizeof(uint32)==sizeof(sint32) && "uint32 and sint32 must contain equal byte count" );
		assert( sizeof(usize)==sizeof(ssize) && "usize and ssize must contain equal byte count" );
	};
	static Checker checker;
};

Checker Checker :: checker;

}; // end LibTypes
