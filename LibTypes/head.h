#pragma once
#include <stddef.h>

/*************************************************************************************
*************************************************************************************/

namespace LibTypes {

typedef unsigned int uint32;
typedef signed int sint32;

typedef unsigned short uint16;
typedef signed short sint16;

typedef unsigned char ubyte;
typedef signed char sbyte;

typedef unsigned long usize; // platform size (unsigned)
typedef signed long ssize; // platform size (signed)
// ssize must be == signed usize

const ssize NNMLIB_SSIZE_MIN = ssize(1L << ((sizeof(ssize)*8)-1));
const ssize NNMLIB_SSIZE_MAX = ssize(size_t(-1) >> 1);
const usize NNMLIB_USIZE_MAX = size_t(-1);

// non-copyable empty class to be inherited to protect object from copying
class NonCopyable {
	NonCopyable(const NonCopyable&);
	void operator =(const NonCopyable&);
protected:
	inline NonCopyable() {};
};

}; // end LibTypes
