#include "head.h"
#include <iostream>
#include <stdexcept>

// implementation

namespace TestsUtils {

const char* test_src_dir = "../_TestData/src/";
const char* test_tmp_dir = "../_TestData/tmp/";

// log

Log :: Log(const char* log_file_name) {
	if(! init_done) {
		init_done = true;
		to_file = false;
		if(log_file_name) { // to file
			log_file.open(log_file_name);
			if(! log_file.is_open()) {
				throw std::runtime_error("Can't open log file for writing");
			}
			to_file = true;
		}
	}
};

void Log :: flush() {
	get() << std::endl;
	get() << "success_count: " << success_count << std::endl;
	get() << "fail_count: " << fail_count << std::endl;
	if(! to_file) { // console
		get() << "press any key..";
		std::cin.ignore(1);
	} else {
		log_file.close();
	}
};

std::ostream& Log :: get() {
	if(to_file) {
		return log_file;
	} else {
		return std::cout;
	}
};

void Log :: success() {
	++success_count;
};

void Log :: fail() {
	++fail_count;
};

std::ofstream Log :: log_file;
bool Log :: init_done = false;
bool Log :: to_file = false;
long Log :: success_count = 0;
long Log :: fail_count = 0;

Log log_instance(NULL);

// test base

TestBase :: TestBase(const char* test_name)
		: name(test_name) {

	result = 0; // unmarked
};

void TestBase :: success() {
	if(! result) {
		result = 1;
		Log::success();
	}
};

void TestBase :: fail() {
	if(! result) {
		result = -1;
		Log::fail();
	}
};

std::ostream& TestBase :: log() const {
	return Log::get();
};

const char* TestBase :: getResult() const {
	static const char *strs[] = {"fail", "ok (auto)", "ok"};
	return strs[result+1];
};

// GetRunners

RunnersType& GetRunners() {
	static RunnersType runners;
	return runners;
};

}; // end TestsUtils

// empty main

int main() {
	TestsUtils::RunnersType& runners = TestsUtils::GetRunners();
	for(auto runner : runners) {
		try {
			runner->run();
			runner->success();
			TestsUtils::Log::get() << runner->name << " result : " << runner->getResult() << "\n";
		} catch(...) {
			runner->fail();
			TestsUtils::Log::get() << "* " << runner->name << " : An unhandled exception has occurred.\n";
		}
	}
	TestsUtils::Log::flush();
	return 0;
};
