#pragma once
#include <fstream>
#include <vector>

namespace TestsUtils {

class Log {
	static std::ofstream log_file;
	static bool init_done;
	static bool to_file;
	static long success_count;
	static long fail_count;
public:
	Log(const char* log_file_name);
	static void flush();
	static std::ostream& get();
	static void success();
	static void fail();
};

class TestBase {
	int result;
protected:
	TestBase(const char* test_name);
public:
	const char* const name;
	void success();
	void fail();
	std::ostream& log() const;
	const char* getResult() const;
	virtual void run() = 0;
};

typedef std::vector<TestBase*> RunnersType;
RunnersType& GetRunners();

extern const char* test_src_dir;
extern const char* test_tmp_dir;

}; // end TestsUtils

#define NTEST(classname) \
	struct classname : public TestsUtils::TestBase { \
		classname() : TestsUtils::TestBase(#classname) { \
			TestsUtils::GetRunners().push_back(this); \
		}; \
		void run(); \
	}; \
	static classname classname##_instance; \
	void classname :: run()

#define ASSERT(expr) \
	if(!(expr)) \
		fail();

#define SRC_FNAME(var_name, file_name) \
	const std::string var_name = std::string(TestsUtils::test_src_dir) + file_name;
#define TMP_FNAME(var_name, file_name) \
	const std::string var_name = std::string(TestsUtils::test_tmp_dir) + file_name;
