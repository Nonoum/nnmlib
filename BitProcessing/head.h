#pragma once
#include "../LibTypes/head.h"

/*************************************************************************************
	Bit Processing library
*************************************************************************************/

namespace BitProcessing {

using namespace LibTypes;

namespace Hidden {
extern const uint32 readwrite_bit_masks32[];
};

inline uint32 ReadBits(const void* src, usize* src_byte, ubyte* src_bit, ubyte bits_to_read) {
	const uint32* dptr;
	uint32 dbuf;
	dptr = (uint32*)(((ubyte*)src)+(*src_byte));
	dbuf = *dptr;
	dbuf >>= (*src_bit);
	dbuf &= Hidden::readwrite_bit_masks32[bits_to_read];
	(*src_bit) += bits_to_read;
	(*src_byte) += ((*src_bit) >> 3);
	(*src_bit) &= 0x07;
	return dbuf;
}; // end read bits
/*	'ReadBits' returns (bits_to_read) bits readed from (src) from position (byte = *src_byte, bit = *src_bit),
	and corrects this variables respectively, return value is expanded with zeroes.
	MAXIMUM guaranteed bit count is 25.
*/

// for WriteBits output must be clean (zeroes).
inline void WriteBits(void* dst, usize* dst_byte, ubyte* dst_bit, ubyte bits_to_write, uint32 value) {
	uint32 *dptr;
	dptr = (uint32*)(((ubyte*)dst)+(*dst_byte));
	value &= Hidden::readwrite_bit_masks32[bits_to_write];
	value <<= (*dst_bit);
	(*dptr) |= value;
	(*dst_bit) += bits_to_write;
	(*dst_byte) += ((*dst_bit) >> 3);
	(*dst_bit) &= 0x07;
}; // end write bits
/*	'WriteBits' writes (bits_to_write) lower bits of (value) to (dst) from position (byte = *dst_byte, bit = *dst_bit),
	and corrects this variables respectively.
	MAXIMUM guaranteed bit count is 25.
*/

}; // end BitProcessing
