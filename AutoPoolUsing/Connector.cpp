#include "Connector.h"
#include "../Helpers/ScopedVarLock.h"
#include <assert.h>

namespace AutoPoolUsing {

ConnectorBase :: ConnectorBase()
		: operation_processing(false) {
};

ConnectorBase :: ~ConnectorBase() {
	disconnect();
};

void ConnectorBase :: connect(ConnectorBase* _client) {
	assert((_client != NULL) && "operand should not be NULL");
	Helpers::ScopedVarLock<bool> svm(operation_processing, true, false);
	if((! _client) || (! svm)) {
		return;
	}
	bool already_connected = false;
	for(ClientsType::LockIterator iter(clients); ! iter.isDone(); ++iter) {
		if(*iter == _client) {
			already_connected = true;
			assert(false && "_client is already connected in call of 'connect' method");
			break;
		}
	}
	if(! already_connected) {
		clients.addFirst(_client); // registering client for (this)
	}
	_client->connect(this); // registering (this) for client
};

void ConnectorBase :: disconnect() {
	Helpers::ScopedVarLock<bool> svm(operation_processing, true, false);
	if(! svm) {
		return;
	}
	for(ClientsType::LockIterator iter(clients); ! iter.isDone(); ) {
		(*iter)->disconnect(this);
		iter.remove();
	}
};

void ConnectorBase :: disconnect(ConnectorBase* who) {
	assert((who != NULL) && "operand can't be NULL");
	Helpers::ScopedVarLock<bool> svm(operation_processing, true, false);
	if((! who) || (! svm)) {
		return;
	}
	for(ClientsType::LockIterator iter(clients); ! iter.isDone(); ++iter) {
		if(*iter == who) {
			(*iter)->disconnect(this);
			iter.remove();
			return;
		}
	}
	assert(false && "client wasn't found");
};

size_t ConnectorBase :: connectionsCount() const {
	return clients.getTotalCount();
};

}; // end AutoPoolUsing
