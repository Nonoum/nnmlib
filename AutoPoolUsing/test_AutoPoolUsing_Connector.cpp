#include "Connector.h"
#include "../TestsUtils/head.h"

static int value = 0;

struct A : public AutoPoolUsing::Connector<int> {
	const int multipler;
	A(int m) : multipler(m) {
	};
	void notificationHandler(int x) {
		value += x * multipler;
	};
};

NTEST(AutoPoolUsing_Connector) {
	A server(1000);
	A c1(1), c2(10), c3(100);
	server.connect(&c1);
	server.connect(&c2);
	c3.connect(&server);
	server.notify(5);
	c1.notify(3);
	c3.notify(4);
	ASSERT( value == 7555 )
};
