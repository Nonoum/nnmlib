#include "List.h"
#include "../TestsUtils/head.h"

typedef AutoPoolUsing::List<int> List;

NTEST(AutoPoolUsing_List_empty) {
	List ls;
	List::LockIterator it(ls);
	ASSERT(it.isDone())
};

NTEST(AutoPoolUsing_List_remove_first) {
	List ls;
	ls.addFirst(4);
	List::LockIterator it(ls);
	it.remove();
	ASSERT(it.isDone())
};

NTEST(AutoPoolUsing_List_remove_multiple) {
	List ls;
	ls.addFirst(1);
	ls.addFirst(5);
	ls.addFirst(2);
	int cnt = 0;
	for(List::LockIterator it(ls); ! it.isDone(); ) {
		it.remove();
		++cnt;
	}
	ASSERT(cnt == 3)
};

NTEST(AutoPoolUsing_List_remove_cascade) {
	List ls;
	ls.addFirst(100);
	ls.addFirst(50);
	{
		List::LockIterator it1(ls);
		++it1;
		{
			List::LockIterator it2(ls);
			it2.remove();
		}
		it1.remove();
	}
	ASSERT(ls.getTotalCount() == 0)
};

NTEST(AutoPoolUsing_List_remove_with_add) {
	List ls;
	ls.addFirst(10);
	ls.addFirst(20);
	ls.addFirst(30);
	ls.addFirst(40);
	int i = 0;
	for(List::LockIterator it(ls); i < 2; ++i) {
		ls.addFirst(123);
		it.remove();
		ls.addFirst(456);
	}
	ASSERT(ls.getTotalCount() == 6)
};

NTEST(AutoPoolUsing_List_values_are_correct) {
	List ls;
	int arr[5] = {20,21,22,23,24};
	for(int i = 0; i < 5; ++i) {
		ls.addFirst(arr[i]);
	}
	int i = 4;
	for(List::LockIterator it(ls); ! it.isDone(); --i) {
		ASSERT(arr[i] == *it)
		it.remove();
	}
};