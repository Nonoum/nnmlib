#pragma once
#include "../AutoPoolGeneration/head.h"
#include "../LibTypes/head.h" // NonCopyable

namespace AutoPoolUsing {

namespace Hidden {

template <class T, size_t autopool_reserve = 256> class ListNode : public AutoPoolGeneration::ObjectBase {
	typedef ListNode<T, autopool_reserve> Node;
	Node* next;
	void* extra;
	T value;
public:
	ListNode() : next(NULL), extra(NULL) {
	};
	ListNode(Node* _next) : next(_next), extra(NULL) {
	};
	const T& get() const {
		return next->value;
	};
	T& get() {
		return next->value;
	};
	bool hasValue() const {
		return next != NULL;
	};
	static Node* autoPoolNode(Node* _next) {
		return AutoPoolGeneration::ObjectBase::create(Node(_next));
	};
	Node* addFirst(const T& new_value) {
		value = new_value;
		return autoPoolNode(this); // this as next
	};
	void remove() {
		// must be non-empty. removes current Node, then *this contains next Node
		Node* kill = next;
		next = kill->next;
		kill->removeFromPool();
	};
	inline void removeThis() {
		removeFromPool();
	};
	inline Node* getNext() const {
		return next;
	};
	inline void* getExtra() const {
		return extra;
	};
	inline void setExtra(void* _extra) {
		extra = _extra;
	};
	size_t reserveRecommendation() const {
		return autopool_reserve;
	};
};

}; // end Hidden

template <class T, size_t autopool_reserve = 256> class List : private LibTypes::NonCopyable {
protected:
	typedef Hidden::ListNode<T, autopool_reserve> Node;
	Node* first;
	size_t total_count;
	//
	T* getLastAdded() {
		if(! first) {
			return NULL;
		}
		if(first->getNext() == NULL) {
			return NULL;
		}
		return &(first->get());
	};
public:
	List() : total_count(0), first(NULL) {
	};
	~List() {
		while(first) {
			Node* next = first->getNext();
			first->removeThis();
			first = next;
		}
		first = NULL;
		total_count = 0;
	};
	void addFirst(const T& new_value) {
		if(! first) {
			first = Node::autoPoolNode(NULL);
		}
		first = first->addFirst(new_value);
		++total_count;
	};
	size_t getTotalCount() const { // count of locked and unlocked values (not recommended to use)
		return total_count;
	};
	size_t calcLockedCount() const {
		size_t count = 0;
		for(Node* current = first; current != NULL; current = current->getNext()) {
			if(current->getExtra() != NULL) {
				++count;
			}
		}
		return 0;
	};
	/*
	* LockIterator is iterator that locks Node that he points to, so nested LockIterators
	* will skip it in their passage. All iterators can remove objects safely with the only
	* condition that 'remove' method moves iterator to the next value or makes it invalid
	* after removing the last available Node.
	* LockIterator unlocks last Node he was locked after it's destructing, so all nested
	* iterators must be declared in local blocks.
	* Usage:
	*    List<int> list;
	*    for(List<int>::LockIterator it1(list); ! it1.isDone(); ) {
	*        int value = *it1;
	*        {...} // might be nested iterator cycle here
	*        if(...) {
	*            it1.remove();
	*        } else {
	*            ++it1;
	*        }
	*    }
	*/
	class LockIterator : private LibTypes::NonCopyable {
		Node* current;
		Node* next;
		size_t* total_count_ptr;
		bool invalid; // true if current is NULL or locked by other iterator
	private:
		void unlock() {
			current->setExtra(NULL); // free from this (and any) iterator
		};
		void lock() {
			current->setExtra(this); // locked by this iterator
		};
		static bool isLocked(const Node* node) {
			return node->getExtra() != NULL;
		};
		bool findAndSetNext() {
			Node* node = current;
			while(1) {
				node = node->getNext();
				if(! node) {
					next = NULL;
					return false; // not found
				}
				if(! isLocked(node)) { // free from iterators
					next = node;
					return true; // found
				}
			}
		};
		void setCurrent(Node* node) {
			current = node;
		};
	public:
		LockIterator(List<T, autopool_reserve>& list) : invalid(false) {
			total_count_ptr = & list.total_count; // storing pointer to count to modify in remove method
			current = list.first;
			if(! current) {
				invalid = true;
				return; // empty collection, nothing to do here
			}
			if(isLocked(current)) { // first is locked
				if(findAndSetNext()) { // have free and 'next' points to it
					current = next; // first of free is found
					lock(); // locking current Node
				} else { // not found
					invalid = true; // current is locked by other iterator
				}
			} else {
				lock(); // locking current Node
			}
		};
		~LockIterator() {
			if(! invalid) { // points on Node, locked by this iterator
				unlock();
				invalid = true; // manual destructing protection
			}
		};
		bool isDone() {
			return invalid || ( ! findAndSetNext() );
		};
		void operator ++() {
			// must have at least one more free Node!
			unlock();
			if(findAndSetNext()) {
				current = next;
				lock();
			} else { // after 'isDone' call next Node might be locked by other iterator
				invalid = true;
			}
		};
		T& operator *() {
			return current->get();
		};
		const T& operator *() const {
			return current->get();
		};
		void remove() {
			// after this method call '++' is done one time automatically
			if(isLocked(current->getNext())) {
				LockIterator* iter = static_cast<LockIterator*>(current->getNext()->getExtra());
				current->remove();
				iter->setCurrent(current);
				unlock();
				iter->lock();
				if(findAndSetNext()) {
					current = next;
					lock();
				} else {
					invalid = true;
				}
			} else { // free
				// unlock isn't needed
				current->remove();
			}
			--(*total_count_ptr); // notifying List that Node was removed
		};
	}; // end LockIterator
};

}; // end AutoPoolUsing
