#pragma once
#include "List.h"

namespace AutoPoolUsing {

/*
	Base interface for connecting group of objects to other object.
*/
class ConnectorBase {
	bool operation_processing;
	//
	void disconnect(ConnectorBase* who);
protected:
	ConnectorBase(const ConnectorBase& other) : operation_processing(false) {}; // empty copying
	ConnectorBase& operator =(const ConnectorBase& other) { return *this; }; // same here
	typedef AutoPoolUsing::List<ConnectorBase*, 8> ClientsType;
	ClientsType clients;
	//
	ConnectorBase();
	virtual ~ConnectorBase();
	void connect(ConnectorBase* _client);
public:
	void disconnect();
	size_t connectionsCount() const;
};

/*
	Template interface to allow notifications between connected objects.
	On destructing, object is automatically disconnecting from all connected objects.

	Feature: notifications are using LockIterator so scheme like:
	"a -> notify b -> notify a" may have unexpected result.

	Usage:
		class Par {
		};
		class A : public Connector<Par&> {
			void notificationHandler(Par& x) {
				// we got notification here
			}
		};
		class B : public Connector<Par&> {
			void notificationHandler(Par& x) {
				// we got notification here
			}
		};
		A a1, a2;
		B b1;
		b1.connect(&a1);
		b1.connect(&a2);
		a1.notify(Par()); // b1 will take notification here
		b1.notify(Par()); // a1 and a2 will take notification here
		a1.connect(&a2);
*/
template <class Params> struct Connector : public ConnectorBase {
	void connect(Connector* other) { // [1]
		ConnectorBase::connect(other);
	};
	void notify(Params params) {
		for(ClientsType::LockIterator iter(clients); ! iter.isDone(); ++iter) {
			(static_cast<Connector*>(*iter))->notificationHandler(params); // we can make static_cast here cause we put correct pointer in [1]
		}
	};
protected:
	virtual void notificationHandler(Params) = 0; // implement this in inherited classes
};

}; // end AutoPoolUsing
