#include "ListArray.h"
#include "../TestsUtils/head.h"

typedef AutoPoolUsing::ListArray<int> Arr;

#define STATIC_SIZE(arr) (sizeof(arr)/sizeof(*arr))

NTEST(AutoPoolUsing_ListArray) {
	Arr a;
	const int vals[] = {5,7,17,41};
	for(int i = 0; i < STATIC_SIZE(vals); ++i)
		a.addFirst(vals[i]);
	int i = 0;
	for(Arr::Iterator it(a); ! it.isDone(); ++it, ++i) {
		ASSERT( *it == vals[STATIC_SIZE(vals) - 1 - i] )
	}
};
