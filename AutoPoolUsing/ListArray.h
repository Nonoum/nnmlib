#pragma once
#include "List.h"

namespace AutoPoolUsing {

namespace Hidden {

template <class T, size_t block_size> struct ListArrayElement {
	T values[block_size];
};

}; // end Hidden

template <class T, size_t block_size = 64, size_t autopool_reserve = 8> class ListArray
		: private List< Hidden::ListArrayElement<T, block_size>, autopool_reserve> {

	typedef List< Hidden::ListArrayElement<T, block_size>, autopool_reserve> Parent;

	typedef Hidden::ListArrayElement<T, block_size> BlockType;
	BlockType* current_block_ptr;
	size_t count;

	inline bool isFirstBlock(const BlockType* block) const {
		return current_block_ptr == block;
	};
	inline size_t size() const {
		return count;
	};
public:
	ListArray() : count(0), current_block_ptr(NULL) {
	};
	void addFirst(const T& value) {
		const size_t index = count % block_size;
		if(index == 0) { // doesn't have place to add in current block, creating new block
			Parent::addFirst(BlockType());
			current_block_ptr = Parent::getLastAdded();
		}
		current_block_ptr->values[index] = value;
		++count;
	};
	/*
		Iterator Usage:
			ListArray<int> list;
			list.addFirst(123);
			// ...
			for(ListArray<int>::Iterator iter(list); ! iter.isDone(); ++iter) {
				int current_value = *iter;
				// ...
			}
	*/
	class Iterator : private LibTypes::NonCopyable {
		typename List<BlockType, autopool_reserve>::Node* block_node;
		size_t local_index;
		bool done;
		BlockType* current;
	public:
		Iterator(ListArray& list) : block_node(list.first), done(false) {
			if(block_node == NULL) {
				done = true;
				current = NULL;
				local_index = 0; // zero index to not crash on some possibly cases with incorrect using of iterator
			} else {
				current = &(block_node->get());
				if(! list.isFirstBlock(current)) { // block is locked by other iterator
					local_index = block_size - 1;
				} else { // the block is first and most likely not full
					local_index = (list.size() - 1) % block_size;
				}
			}
		};
		inline bool isDone() {
			return done;
		};
		inline void operator ++() {
			if(local_index != 0) {
				--local_index;
				return;
			}
			block_node = block_node->getNext(); // moving to next block
			if(! block_node->hasValue()) { // if it's done - set (done) and out
				done = true;
				return;
			}
			local_index = block_size - 1;
			current = &(block_node->get());
		};
		inline T& operator *() {
			return current->values[local_index];
		};
	};
};

}; // end AutoPoolUsing
