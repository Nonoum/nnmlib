#pragma once

namespace Algorithms {

static inline bool LexicographicalCompare() { return false; };

/*
	Variadic template version of std::lexicographical_compare algorithm.
	Usage:
		struct A { int higher; char middle; char lower; };
		A a1, a2; ...
		bool a1_less = LexicographicalCompare(a1.higher, a2.higher, a1.middle, a2.middle, a1.lower, a2.lower);
*/
template <class T, class... Args> bool LexicographicalCompare(const T& left, const T& right, Args... args) {
	if(left < right) {
		return true;
	} else if(right < left) {
		return false;
	}
	return LexicographicalCompare(args...);
};

}; // end Algorithms
