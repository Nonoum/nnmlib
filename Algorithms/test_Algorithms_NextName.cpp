#include "head.h"
#include "../TestsUtils/head.h"

using Algorithms::NextName;

NTEST(Algorithms_NextName) {
	std::string str("zxYkm0r8");
	str = NextName(str);
	ASSERT( str == "zxYkm0r9" )
	str = NextName(str);
	ASSERT( str == "zxYkm1r0" )
};
