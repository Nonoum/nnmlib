#include "LexicographicalCompare.h"
#include "../TestsUtils/head.h"

NTEST(Algorithms_LexicographicalCompare) {
	int x1 = 1, x2 = 2;
	char c40 = 40, c50 = 50, c60 = 60;
	ASSERT(Algorithms::LexicographicalCompare(x1, x2, c40, c50) == true)
	ASSERT(Algorithms::LexicographicalCompare(x1, x2, c50, c40) == true)
	ASSERT(Algorithms::LexicographicalCompare(x2, x1, c40, c50) == false)
	ASSERT(Algorithms::LexicographicalCompare(x2, x1, c50, c40) == false)
	ASSERT(Algorithms::LexicographicalCompare(x1, x1, x2, x2, c40, c60) == true)
	ASSERT(Algorithms::LexicographicalCompare(x1, x1, x2, x2, c60, c40) == false)
};
