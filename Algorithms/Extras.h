#pragma once
#include <algorithm>
#include <functional>

namespace Algorithms {

//// std::'s missed extensions

template <class C, class P> bool allOf(const C& c, P predicate) { return std::all_of(std::begin(c), std::end(c), predicate); };
template <class C, class P> bool anyOf(const C& c, P predicate) { return std::any_of(std::begin(c), std::end(c), predicate); };
template <class C, class P> bool noneOf(const C& c, P predicate) { return std::none_of(std::begin(c), std::end(c), predicate); };

template <class C, class T> bool allIs(const C& c, const T& value) { return std::all_of(std::begin(c), std::end(c), [&value](const T& v) { return v == value; }); };
template <class C, class T> bool anyIs(const C& c, const T& value) { return std::any_of(std::begin(c), std::end(c), [&value](const T& v) { return v == value; }); };
template <class C, class T> bool noneIs(const C& c, const T& value) { return std::none_of(std::begin(c), std::end(c), [&value](const T& v) { return v == value; }); };

template <class C, class T> void fill(C& c, const T& value) { std::fill(std::begin(c), std::end(c), value); };

//// some useful variadic stuff

// helper functions
template <class Rule, class T>
bool allAdjacentObeyRule(Rule rule, const T&) { return true; };

template <class Rule, class T, typename... Args>
bool allAdjacentObeyRule(Rule rule, const T& first, const T& second, Args... args) { return rule(first, second) && allAdjacentObeyRule(rule, second, args...); };

// matchers
template <class T, typename... Args>
bool matchAll(const T& matcher, Args... args) { return allAdjacentObeyRule(std::equal_to<T>(), matcher, args...); };

template <class T>
bool matchAny(const T&) { return false; };

template <class T, typename... Args>
bool matchAny(const T& matcher, const T& second, Args... args) { return (matcher == second) || matchAny(matcher, args...); };

template <class T, typename... Args>
bool matchNone(const T& matcher, Args... args) { return ! matchAny(matcher, args...); };

// chain comparator
template <class T, typename... Args>
bool lessOrEqual(const T& matcher, Args... args) { return allAdjacentObeyRule(std::less_equal<T>(), matcher, args...); };

}; // end Algorithms
