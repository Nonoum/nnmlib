#pragma once
#include "Variants.h"
#include "Gradient.h"
#include "LexicographicalCompare.h"
#include "Extras.h"
#include <string>

namespace Algorithms {

/*
	Returns string with incremented number of digits through source string
	(example: "abc012" -> "abc013", "qw5rty" -> "qw6rty").
*/
std::string NextName(const std::string& string_with_number);

};
