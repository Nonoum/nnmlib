#include "head.h"

namespace Algorithms {

std::string NextName(const std::string& string_with_number) {
	static const char* numbers = "0123456789";
	std::string result = string_with_number;
	size_t prev_index = std::string::npos;
	while(1) {
		const size_t index = result.find_last_of(numbers, prev_index);
		if(index == std::string::npos) {
			break;
		}
		if(result[index] != '9') {
			++result[index];
			break;
		}
		result[index] = '0';
		prev_index = index - 1;
	}
	return result;
};

}; // end Algorithms
