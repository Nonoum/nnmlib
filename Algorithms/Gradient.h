#pragma once

namespace Algorithms {

template <class Iter, class T> void Gradient(Iter first, Iter last, T init_value, T appender) {
	for(; first != last; ++first) {
		*first = init_value;
		init_value += appender;
	}
};

}; // end Algorithms
