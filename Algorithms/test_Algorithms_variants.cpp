#include "Variants.h"
#include "../TestsUtils/head.h"
#include <algorithm>
#include <iterator>

typedef size_t TestType;

class Checker {
	int iteration;
	int errors;
public:
	Checker() : iteration(0), errors(0) {
	};
	void operator ()(TestType* data, int length) {
		static TestType expects[] = {1,2,3, 1,3,2, 2,1,3, 2,3,1, 3,2,1, 3,1,2};
		int start = iteration * length;
		int end = start + length;
		for(int i = start; i < end; ++i) {
			if(expects[i] != data[i-start]) {
				++errors;
			}
		}
		++iteration;
	};
	int getErrors() {
		return errors;
	};
};

NTEST(Algorithms_Variants) {
	TestType data[3] = {1,2,3};
	Checker checker = Checker();
	Algorithms::Variants(data, 3, checker);
	ASSERT( checker.getErrors() == 0 )
};
