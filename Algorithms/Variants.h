#pragma once
#include "Extras.h"
#include <assert.h>

namespace Algorithms {

/*
	Hardcore function to enumerate all variants of permutation of (data) sequence with (length) elements;
	calls (functor)<data, length> for each permutation: totally (length)! times.
*/
template <class T, class Functor> void Variants(T* data, int length, Functor functor) {
	const int N_MAX = 20; // 20 is critically hard for processors in near future.
	assert(length > 0 && "length of sequence supposed to be > 0.");
	assert(length < N_MAX && "too big length..");
	int step_shifts[N_MAX];
	bool flags[N_MAX];
	fill(step_shifts, 0);
	fill(flags, true);

	int i = 0;
	while(i >= 0) {
		if(i == length-1) { // pair reached
			functor(data, length);
			i--;
		}
		if(i >= 0) {
			if(flags[i]) {
				flags[i] = false;
				++i;
			}
			else {
				std::swap(data[i], data[i+step_shifts[i]]);
				++(step_shifts[i]);
				if((step_shifts[i] + i) >= length) {
					step_shifts[i] = 0;
					flags[i] = true;
					--i;
				} else {
					std::swap(data[i], data[i+step_shifts[i]]);
					++i;
				}
			}
		}
	}
};

}; // end Algorithms
