#include "head.h"
#include "../TestsUtils/head.h"
#include <cstdlib>

using LibTypes::ubyte;

static int TestCompressDecompress(ubyte* data, int length) {
	char hdr = 0;
	ubyte *decoded = new ubyte[length];
	int errors = 0;

	int est_bits = SequenceLevelsAnalyzer::Analyze(data, length, &hdr);
	int bytes = (est_bits / 8) + 4;
	ubyte *tmp = new ubyte[bytes];
	for(int i = 0; i < bytes; ++i) {
		tmp[i] = 0;
	}

	LibTypes::usize tbyte = 0;
	LibTypes::ubyte tbit = 0;
	SequenceLevelsAnalyzer::Compress(data, length, hdr, tmp, &tbyte, &tbit);

	tbyte = 0;
	tbit = 0;
	SequenceLevelsAnalyzer::Decompress(tmp, &tbyte, &tbit, decoded, length);
	for(int i = 0; i < length; ++i) {
		if(data[i] != decoded[i]) {
			++errors;
		}
	}
	delete [] tmp;
	delete [] decoded;
	return errors;
};

NTEST(SequenceLevelsAnalyzer_random_values) {
	ubyte arr[] = {7,235,234,35,10,22,91};
	int len = sizeof(arr)/sizeof(*arr);
	ASSERT(TestCompressDecompress(arr, len) == 0)
};

NTEST(SequenceLevelsAnalyzer_equal_values) {
	ubyte arr[] = {1, 1, 1, 1, 1};
	int len = sizeof(arr)/sizeof(*arr);
	ASSERT(TestCompressDecompress(arr, len) == 0)
};

NTEST(SequenceLevelsAnalyzer_values_with_repeats) {
	ubyte arr[] = {7,235,234,35,10,10,10,22,91,91};
	int len = sizeof(arr)/sizeof(*arr);
	ASSERT(TestCompressDecompress(arr, len) == 0)
};

NTEST(SequenceLevelsAnalyzer_zeroes) {
	ubyte arr[] = {0,0,0,0};
	int len = sizeof(arr)/sizeof(*arr);
	ASSERT(TestCompressDecompress(arr, len) == 0)
};

NTEST(SequenceLevelsAnalyzer_random_values_generic) {
	const int n_tests = 1000;
	const int max_length = 2000;
	ubyte* arr = new ubyte[max_length];
	int n_fails = 0;
	int n_suberrors = 0;
	for(int i = 0; i < n_tests; ++i) {
		const int count = rand() % max_length;
		for(int j = 0; j < count; ++j) {
			arr[j] = ubyte(rand() & 0x0ff);
		}
		const int cur_errors = TestCompressDecompress(arr, count);
		n_suberrors += cur_errors;
		n_fails += cur_errors ? 1 : 0;
	}
	ASSERT( n_fails == 0 )
	if(n_fails) {
		log() << name << ":" << std::endl <<
			n_tests << " sub tests;" << std::endl <<
			n_fails << " fails;" << std::endl <<
			n_suberrors << " sub errors;" << std::endl;
	}

	delete [] arr;
};
