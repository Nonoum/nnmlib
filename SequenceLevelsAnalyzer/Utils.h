#pragma once
#include "../BitProcessing/head.h"

namespace SequenceLevelsAnalyzer {

using namespace BitProcessing;

ubyte GetLog2s(uint16 x); // returns binary logarithm of (x)

}; // end SequenceLevelsAnalyzer
