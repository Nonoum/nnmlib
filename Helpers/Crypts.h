#pragma once
#include <string>

namespace Helpers {

std::string SimpleEncrypt(void* data, size_t length, const char* phrase = NULL);
void* SimpleDecrypt(const std::string& str, size_t& out_length, const char* phrase = NULL);

}; // end Helpers
