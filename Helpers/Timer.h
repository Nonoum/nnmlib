#pragma once

namespace Helpers {

class Timer {
	size_t value;
public:
	inline Timer() : value(0) {
	};
	inline bool live() {
		if(value) {
			--value;
			return !value;
		}
		return false;
	};
	inline void set(size_t _value) {
		value = _value;
	};
	inline operator size_t() const {
		return value;
	};
};

}; // end Helpers
