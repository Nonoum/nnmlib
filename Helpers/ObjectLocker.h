#pragma once
#include "../LibTypes/head.h"

namespace Helpers {

/*
	Class-locker for protecting operations that can't cross each other.
	uses counter instead of bool flag.
*/
class ObjectLocker {
	size_t lock_counter;
public:
	ObjectLocker();
	~ObjectLocker();
	ObjectLocker(const ObjectLocker& other); // no actual copying
	ObjectLocker& operator =(const ObjectLocker& other); // no actual copying
	inline bool isLocked() const {
		return lock_counter != 0;
	};
	inline void lockObject() {
		++lock_counter;
	};
	void unlockObject();
	virtual void onFullUnlock() = 0;
};

}; // end Helpers
