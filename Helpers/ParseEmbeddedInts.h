#pragma once

namespace Helpers {

/*
	Parses custom string that contains numbers.
	Returns pointer to character after processed format in specified string, or NULL on errors.
	Usage:
		int a, b, c, d;
		char src[] = "rect(25:12,37:44)QWertY";
		void* outputs[] = {&a, &b, &c, &d};
		const char* pos = ParseEmbeddedInts(src, "rect(0:0,0:0)", outputs);
		// now pos points at "QWErtY"; a == 25; b == 12; c == 37; d == 44.
*/
const char* ParseEmbeddedInts(const char* str, const char* format, void* outputs[]);

}; // end Helpers
