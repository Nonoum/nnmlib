#pragma once
#include <algorithm>

namespace Helpers {

using std::min;
using std::max;

template <class T> inline const T& min2(const T& x1, const T& x2) {
	return min(x1, x2);
};

template <class T> inline const T& max2(const T& x1, const T& x2) {
	return max(x1, x2);
};

template <class T> inline const T& min4(const T& x1, const T& x2, const T& x3, const T& x4) {
	return min2( min2(x1, x2), min2(x3, x4) );
};

template <class T> inline const T& max4(const T& x1, const T& x2, const T& x3, const T& x4) {
	return max2( max2(x1, x2), max2(x3, x4) );
};

}; // end Helpers
