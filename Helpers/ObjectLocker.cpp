#include "ObjectLocker.h"
#include <assert.h>
#include <stdexcept>

namespace Helpers {

ObjectLocker :: ObjectLocker()
		: lock_counter(0) {
};

ObjectLocker :: ~ObjectLocker() {
	assert((lock_counter == 0) && "error : destructing locked object");
};

ObjectLocker :: ObjectLocker(const ObjectLocker& other)
		: lock_counter(0) {
};

ObjectLocker& ObjectLocker :: operator =(const ObjectLocker& other) {
	if(lock_counter || other.lock_counter) {
		throw std::runtime_error("ObjectLocker :: operator= error : some of objects is locked");
	}
	// nothing to do here
	return *this;
};

void ObjectLocker :: unlockObject() {
	assert((lock_counter != 0) && "can't be unlocked before locking");
	--lock_counter;
	if(! lock_counter) {
		onFullUnlock();
	}
};

}; // end Helpers
