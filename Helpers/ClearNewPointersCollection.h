#pragma once

namespace Helpers {

// cleaner of collection of pointers to objects that were allocated with (operator new)
template <class Collection> void ClearNewPointersCollection(Collection& c) {
	for(auto ptr : c) {
		delete ptr;
	}
	c.clear();
};

}; // end Helpers
