#pragma once
#include "../LibTypes/head.h" // NonCopyable
#include "ObjectLocker.h"
#include "Requests.h"

namespace Helpers {

/*
	Class wrapper for ObjectLocker instance to unlock on destruction.
	If lock was successful then conversion to bool will return true.
	If lock isn't successful, request will be toggled on.
	Request will be toggled off on destructing AutoLocker if lock was successful.
*/
class AutoLocker : private LibTypes::NonCopyable {
	const bool was_locked;
	ObjectLocker& locker;
	Trigger& request;
public:
	inline AutoLocker(ObjectLocker& _locker, Trigger& _request)
			: locker(_locker), request(_request), was_locked(_locker.isLocked()) {

		if(! was_locked) {
			locker.lockObject();
		} else {
			request.toggleOn();
		}
	};
	~AutoLocker() {
		if(! was_locked) {
			request.toggleOff();
			locker.unlockObject();
		}
	};
	inline operator bool() const {
		return ! was_locked;
	};
};

}; // end Helpers
