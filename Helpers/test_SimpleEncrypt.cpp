#include "Crypts.h"
#include "../TestsUtils/head.h"
#include <algorithm>
#include <stdexcept>
#include <iterator>
#include <memory>

using namespace Helpers;

const int cnt = 10;
int vals[cnt];

std::string str(const char* pswd) {
	for(int i = 0; i < cnt; ++i) {
		vals[i] = i*i*i*i*i*i;
	}
	return SimpleEncrypt(vals, cnt*sizeof(int), pswd);
};

NTEST(SimpleEncrypt_small) {
	std::string s = str("vsd");
	size_t length = 0;
	std::unique_ptr<int[]> out( (int*)SimpleDecrypt(s, length, "vsd") );
	ASSERT( out.get() != NULL )
	ASSERT( length == cnt*sizeof(int) )
	if(out.get()) {
		ASSERT( std::equal(out.get(), out.get()+cnt, vals) )
	}
};

NTEST(SimpleEncrypt_wrong_passwords) {
	std::string s = str("vsd");
	size_t length = 0;
	const char* phrases[] = {"vsa","asd","vsada","asdf","1234567","","vsd ","VSA"};
	for(size_t i = 0; i < sizeof(phrases)/sizeof(*phrases); ++i) {
		std::unique_ptr<int[]> out( (int*)SimpleDecrypt(s, length, phrases[i]) );
		ASSERT( out.get() == NULL )
	}
};

NTEST(SimpleEncrypt_exception_length) {
	std::string s = str("vsd");
	s[0]++; // increasing length of data expected to be decompressed
	size_t length = 0;
	bool failed = false;
	try {
		std::unique_ptr<int[]> out( (int*)SimpleDecrypt(s, length, "vsd") );
	} catch (std::out_of_range& oor) {
		failed = true;
	}
	ASSERT( failed )
};

NTEST(SimpleEncrypt_empty_string) {
	std::string s = "";
	size_t length = 0;
	std::unique_ptr<int[]> out( (int*)SimpleDecrypt(s, length, "vsd") );
	ASSERT( out.get() == NULL )
};
