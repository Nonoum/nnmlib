#pragma once
#include <string>

namespace Helpers {

namespace Filesystem {

// forms path from it's parts (like os.path.join in python)
std::string Join(const std::string& path, const char* path_or_file);

};

}; // end Helpers
