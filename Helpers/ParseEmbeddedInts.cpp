#include "ParseEmbeddedInts.h"
#include <string.h>
#include <stdio.h>
#include <ctype.h>

namespace Helpers {

const char* ParseEmbeddedInts(const char* str, const char* format, void* outputs[]) {
	if((! str) || (! format) || (! outputs)) {
		return NULL;
	}
	char buffer[32];
	size_t si = 0, di = 0;
	for(size_t i = 0; ; ++i) {
		while(str[si] == format[di] && format[di] != '0' && format[di]) {
			++si; ++di;
		}
		if(! format[di]) {
			return str + si; // done successfully
		}
		// getting bounds of number in string
		size_t end = si;
		if(str[end] == '-' || str[end] == '+') {
			++end;
		}
		while(isdigit(str[end])) {
			++end;
		}
		if((end == si) || (end - si >= sizeof(buffer))) {
			return NULL; // empty or enormous sequence
		}
		// copying numeric string to buffer
		strncpy(buffer, str + si, end - si);
		buffer[end - si] = 0;
		// parsing number
		if( sscanf(buffer, "%d", (int*)outputs[i]) != 1 ) {
			return NULL; // can't parse number somehow
		}
		++di;
		si = end;
	}
	return NULL;
};

}; // end Helpers
