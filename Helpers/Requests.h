#pragma once

namespace Helpers {

class Trigger {
	bool state;
public:
	Trigger() : state(false) {
	};
	~Trigger() {
		state = false;
	};
	inline void toggleOn() {
		state = true;
	};
	inline void toggleOff() {
		state = false;
	};
	inline bool isSet() const {
		return state;
	};
};

template <class RequestParam> struct Request : public Trigger {
	RequestParam param;
};

template <> struct Request<void> : public Trigger {
};

}; // end Helpers
