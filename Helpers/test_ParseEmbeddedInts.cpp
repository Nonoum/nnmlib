#include "ParseEmbeddedInts.h"
#include "../TestsUtils/head.h"

int a, b, c, d;
void* outputs[] = {&a, &b, &c, &d};

using Helpers::ParseEmbeddedInts;

NTEST(ParseEmbeddedInts_ok) {
	const char* str = "rect(1.20;300:4000)";
	ASSERT( ParseEmbeddedInts(str, "rect(0.0;0:0)", outputs) != NULL )
	ASSERT( a+b+c+d == 4321 )
	str = "rect(1000*200'30(4)";
	ASSERT( ParseEmbeddedInts(str, "rect(0*0'0(0)", outputs) != NULL )
	ASSERT( a+b+c+d == 1234 )
};

NTEST(ParseEmbeddedInts_ok_with_signs) {
	const char* str = "[-1:+20:-17]QWE";
	ASSERT( ParseEmbeddedInts(str, "[0:0:0]", outputs) == str+12 )
	ASSERT( (a == -1) && (b == 20) && (c == -17) )
};

NTEST(ParseEmbeddedInts_non_conformant_format) {
	const char* str = "recAt(1111)";
	ASSERT( ParseEmbeddedInts(str, "rect(0)", outputs) == NULL )
	ASSERT( ParseEmbeddedInts(str, "recat(0)", outputs) == NULL )
};

NTEST(ParseEmbeddedInts_insufficient_ints) {
	const char* str = "rect(";
	ASSERT( ParseEmbeddedInts(str, "rect(0", outputs) == NULL )
};
