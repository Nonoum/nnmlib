#pragma once

namespace Helpers {

/*
	Class storage for callback. callback supposes to call method 'action',
	which takes 'CbackParams' and returns bool.
*/
template <class CbackBase, class CbackParams> class CallbackStorage {
	CbackBase* cback;
public:
	CallbackStorage() : cback(NULL) {
	};
	~CallbackStorage() {
		setCallback(NULL);
	};
	void setCallback(CbackBase* new_cback) {
		if(cback) {
			delete cback;
		}
		cback = new_cback;
	};
	bool sendCallback(CbackParams params) const {
		if(! cback) {
			return false;
		}
		return cback->action(params);
	};
	inline bool haveCallback() const {
		return cback != NULL;
	};
};

}; // end Helpers
