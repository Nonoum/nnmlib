#include "Crypts.h"
#include "../HuffmanCodec/head.h"
#include <sstream>
#include <vector>
#include <iterator>
#include <memory>
#include <assert.h>

namespace Helpers {

using namespace LibTypes;

void XorCycled(char* data, size_t length, const char* phrase) {
	if(! phrase) {
		return;
	}
	for(size_t i = 0, j = 0; i < length; ++i, ++j) {
		if(phrase[j] == 0) {
			j = 0;
		}
		data[i] ^= phrase[j];
	}
};

class CRC32Impl : private NonCopyable {
	static const uint32 polynomial = 0x04C11DB7;
	static const uint32 initial = 0xFFFFFFFF;
	static const uint32 final_xor = 0xFFFFFFFF;
	uint32* table32;
	CRC32Impl() {
		table32 = new uint32[256];
		for(size_t i = 0; i < 256; ++i) {
			uint32 value = i << 24;
			for(size_t j = 0; j < 8; ++j) {
				value = (value & 0x80000000) ? (value << 1) ^ polynomial : value << 1;
			}
			table32[i] = value;
		}
	};
	~CRC32Impl() {
		delete [] table32;
		table32 = NULL;
	};
	static const uint32* GetTable() {
		static CRC32Impl impl;
		return impl.table32;
	};
public:
	// non-reflected algorithm version (non-standard)
	static uint32 CRC32(void* data, size_t length) {
		const uint32* table = GetTable();
		ubyte* ptr = (ubyte*)data;
		uint32 res = initial;
		for(size_t i = 0; i < length; ++i) {
			ubyte idx = ptr[i] ^ (res >> 24);
			res = table[idx] ^ (res << 8);
		}
		return res ^ final_xor;
	};
};

std::string SimpleEncrypt(void* data, size_t length, const char* phrase) {
	assert(data != NULL);
	if(! data) {
		return "";
	}
	// +4 bytes needed to round up (1 byte) and to have 3 more bytes for accessing by uint32's
	const size_t n_bytes = (HuffmanCodec::Compress((const ubyte*)data, length, 1, NULL, NULL, NULL, false) / 8) + 4;
	std::unique_ptr<ubyte[]> d(new ubyte[n_bytes]);
	std::fill(d.get(), d.get()+n_bytes, 0);
	ubyte bit = 0;
	usize byte = 0;
	HuffmanCodec::Compress((const ubyte*)data, length, 1, d.get(), &byte, &bit, true);
	uint32 cc = CRC32Impl::CRC32(d.get(), n_bytes);
	// applying password
	XorCycled((char*)d.get(), n_bytes, phrase);
	XorCycled((char*)&cc, sizeof(uint32), phrase);
	std::ostringstream oss;
	oss << std::hex << length << ' ' << cc;
	// slipping last 3 bytes since it always zeroes
	for(size_t i = 0; i < n_bytes - 3; ++i) {
		oss << ' ' << std::hex << uint32(d.get()[i]);
	}
	return oss.str();
};

void* SimpleDecrypt(const std::string& str, size_t& out_length, const char* phrase) {
	std::vector<char> data;
	data.reserve(64);
	std::istringstream iss(str);
	uint32 cc;
	iss >> std::hex >> out_length >> cc;
	if(! iss) {
		return NULL;
	}
	std::copy(std::istream_iterator<int>(iss >> std::hex), std::istream_iterator<int>(), std::back_inserter(data));
	// processing password
	XorCycled((char*)&cc, sizeof(uint32), phrase);
	XorCycled(&data[0], data.size(), phrase);
	// recovering 3 zero-bytes
	for(size_t i = 0; i < 3; ++i) {
		data.push_back(0);
	}
	if(cc != CRC32Impl::CRC32(&data[0], data.size())) {
		return NULL; // control code check failed
	}
	std::unique_ptr<ubyte[]> raw(new ubyte[out_length]);
	ubyte bit = 0;
	usize byte = 0;
	HuffmanCodec::Decompress(&data[0], &byte, &bit, raw.get(), out_length, 1, data.size()-3);
	return raw.release();
};

}; // end Helpers
