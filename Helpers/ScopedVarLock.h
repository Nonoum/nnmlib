#pragma once
#include <assert.h>

namespace Helpers {

template <class T> class ScopedVarLock {
	T& var;
	const bool valid;
	const T unlocked_state;
public:
	ScopedVarLock(T& _var, T _locked_state, T _unlocked_state)
			: var(_var), valid(_locked_state != _var), unlocked_state(_unlocked_state) {

		if(valid) {
			var = _locked_state;
		}
		assert(_locked_state != _unlocked_state);
	};
	~ScopedVarLock() {
		if(valid) {
			var = unlocked_state;
		}
	};
	operator bool() const { return valid; };
};

};
