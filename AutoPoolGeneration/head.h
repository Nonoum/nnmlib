#pragma once
#include "Common.h"

/*************************************************************************************
CONTRACTS FROM AutoPoolGeneration:
	1. This namespace supplies class to be inherited for buffering
	allocation of objects. For each class that inherits 'AutoPoolGeneration::ObjectBase',
	a static object pool is being generated automatically.

	2. Class 'AutoPoolGeneration::ObjectBase' have const method 'reserveRecommendation' that returns
	count of objects to be reserved in one pool. This method
	can be overloaded in user class. If reserved memory is devastated, then an
	additional pool is being created.

	3. After finishing main function of program, destructors of pools call
	destructors of objects that weren't destroyed manually.

CONTRACTS FROM USER:
	1. If class 'T' have dynamic components, then assigning operator have to be overloaded correctly.

	2. Overloaded method 'reserveRecommendation' must return 'size_t' value
	greater than zero, and be 'const'.

USAGE:
	using AutoPoolGeneration::ObjectBase;

	class MyClass : public ObjectBase {
		// ....
	};

	MyClass* obj = ObjectBase::create(MyClass());
	// ....
	obj->removeFromPool();

*************************************************************************************/
