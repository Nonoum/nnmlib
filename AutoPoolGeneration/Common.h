#pragma once
#include "../Algorithms/Gradient.h"
#include <stdlib.h> // declaration of NULL
#include <new>

/*************************************************************************************
*************************************************************************************/

namespace AutoPoolGeneration {

class ObjectPoolInterface {
protected:
	size_t count;
	size_t capacity;
	size_t object_size;
	char* data;
	size_t* links;
	// methods
	ObjectPoolInterface();
	void exclude(void* object_ptr); // excludes object with specified address (address doesn't have to be distinct, multiple inheritance is allowed)
	friend class ObjectBase;
}; // -----------------------------------------------------

template <class T> class ObjectPool;

class ObjectBase {
	ObjectPoolInterface* object_pool_pointer;
public:
	ObjectBase() : object_pool_pointer(NULL) {
	};
	ObjectBase(const ObjectBase&) : object_pool_pointer(NULL) {
	}; // empty. no actual copying
	ObjectBase& operator =(const ObjectBase&) {
		return *this;
	}; // no actual copying
	virtual ~ObjectBase() {
	};
	size_t reserveRecommendation() const; // count of instances of class to be allocated in one pool
	void removeFromPool(); // kills object and excludes it from pool
	template <class T> static T* create(const T& init_value);
private:
	template <class T> friend class ObjectPool;
	void assign(ObjectPoolInterface* pool) {
		object_pool_pointer = pool;
	};
}; // -----------------------------------------------------

template <class T> class ObjectPool : public ObjectPoolInterface {
	ObjectPool<T>* best_to_work;
	ObjectPool<T>* next;
public:
	ObjectPool() : next(NULL) {
		object_size = sizeof(T);
		best_to_work = this;
		capacity = T().reserveRecommendation();
		data = new char[capacity * object_size];
		links = new size_t[capacity];
		Algorithms::Gradient(links, links + capacity, 0, 1);
	};
	~ObjectPool() {
		while( count ) { // destructing objects that left
			--count;
			const size_t index = links[count];
			T* object = reinterpret_cast<T*>(&data[object_size * index]);
			object->~T();
		}
		delete [] data; // data array
		data = NULL;
		delete [] links; // links array
		links = NULL;
		delete next; // next pool
		next = NULL;
	};
	T* create(const T& init_value) { // this method is always called for first link of chain
		if(! best_to_work->countOfFree()) {
			best_to_work = bestFromThis(0, NULL);
		}
		return best_to_work->createHere(init_value);
	};
private:
	size_t countOfFree() const {
		return capacity - count;
	};
	ObjectPool<T>* bestFromThis(size_t best_count, ObjectPool<T>* best) {
		// returns best pool to work (most empty), can create new pool if all is full
		size_t n_free = countOfFree();
		if(n_free > best_count) {
			best_count = n_free;
			best = this;
		}
		if(! next) { // end of chain
			if(! best_count) { // all is full, add new
				next = new ObjectPool<T>();
				return next->bestFromThis(best_count, best);
			}
			// else - best result was found
			return best;
		}
		// else - moving further
		return next->bestFromThis(best_count, best);
	};
	T* createHere(const T& init_value) {
		const size_t index = links[count];
		T* object = new (&data[object_size * index]) T();
		*object = init_value;
		object->assign(this); // designating manager information
		++count; // object was added
		return object; // returning pointer
	};
}; // -----------------------------------------------------

template <class T> T* ObjectBase :: create(const T& init_value) {
	static ObjectPool<T> pool; // declaring personal pool for each class
	return pool.create(init_value); // including an element and initializing it
};

}; // end AutoPoolGeneration
