#include "Common.h"
#include <assert.h>

namespace AutoPoolGeneration {

ObjectPoolInterface :: ObjectPoolInterface()
		: count(0), capacity(0), object_size(0), data(NULL), links(NULL) {
};

void ObjectPoolInterface :: exclude(void* object_ptr) {
	assert((count != 0) && "count can't be 0 here");
	const size_t index = (((char*)object_ptr) - data) / object_size;
	size_t double_index;
	for(double_index = 0; double_index < count; ++double_index) {
		if(links[double_index] == index) {
			break;
		}
	}
	assert((double_index < count) && "object to exclude wasn't found");
	--count;
	for(size_t i = double_index; i < count; ++i) {
		links[i] = links[i+1];
	}
	links[count] = index;
};

// -------------------------------------------------------------------------

size_t ObjectBase :: reserveRecommendation() const {
	return 128;
};

void ObjectBase :: removeFromPool() {
	assert((object_pool_pointer != NULL) && "can't be NULL");
	object_pool_pointer->exclude(this); // excluding from pool
	this->~ObjectBase(); // destructing the object
};

}; // end AutoPoolGeneration
