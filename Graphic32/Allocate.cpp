#include "functions.h"

namespace Graphic32 {

void Allocate(uint32** dst, usize* dst_width, usize* dst_height, usize new_width, usize new_height) {
	if((*dst == NULL) || (*dst_width != new_width) || (*dst_height != new_height)) { // allocating
		delete [] *dst;
		*dst = NULL;
		*dst_width = 0;
		*dst_height = 0;
		*dst = new uint32[new_width * new_height];
		*dst_width = new_width;
		*dst_height = new_height;
	} // else - already allocated needed size
};

}; // end Graphic32
