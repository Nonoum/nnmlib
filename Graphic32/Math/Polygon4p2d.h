#pragma once
#include "../Utils.h"
#include "LineEquation2d.h"

namespace Graphic32 {

template <class T> class Polygon4p2d {
	LineEquation2d <T> lines[4];
public:
	Polygon4p2d(T xA, T yA, T xB, T yB, T xC, T yC, T xD, T yD) {
		lines[0].setEquation(xA, yA, xB, yB); // ab
		lines[1].setEquation(xB, yB, xC, yC); // bc
		lines[2].setEquation(xC, yC, xD, yD); // cd
		lines[3].setEquation(xD, yD, xA, yA); // da
	};
	bool getXRange(T y, T* xstart, T* xend) const {
		T x[4];
		int i, cnt = 0;
		for(i = 0; i < 4; ++i) {
			if(lines[i].calcX(y, &x[cnt])) {
				++cnt;
			}
		}
		if(cnt < 2) {
			return false;
		}
		for(i = cnt; i < 4; ++i) {
			x[i] = x[i-1];
		}
		*xstart = min4(x[0], x[1], x[2], x[3]);
		*xend = max4(x[0], x[1], x[2], x[3]);
		return true;
	};
}; // end polygon 4 points 2d

}; // end Graphic32
