#pragma once
#include "../Utils.h"
#include <math.h>

namespace Graphic32 {

template <class T> class LineEquation2d {
	T xmin, xmax, ymin, ymax;
	T dx, dy;
	T yk, yb; // y = yk * x + yb
	T xk, xb; // x = xk * y + xb
	bool dx_troubles;
	bool dy_troubles;
	inline void checkAndCorrectDX() {
		if(fabs(dx) < 0.0001)  {
			dx = T(0.0001);
			dx_troubles = true;
		}
	};
	inline void checkAndCorrectDY() {
		if(fabs(dy) < 0.0001)  {
			dy = T(0.0001);
			dy_troubles = true;
		}
	};
public:
	LineEquation2d() : xk(0), xb(0), yk(0), yb(0), dx_troubles(false), dy_troubles(false) {
	};
	inline T calcX(T y) const {
		return (xk * y + xb);
	};
	inline T calcY(T x) const {
		return (yk * x + yb);
	};
	inline bool calcX(T y, T* x) const {
		if((y < ymin) || (y > ymax)) {
			return false;
		}
		if(dy_troubles) {
			*x = xmin;
			return true;
		}
		*x = calcX(y);
		return true;
	};
	inline bool calcY(T x, T* y) const {
		if((x < xmin) || (x > xmax)) {
			return false;
		}
		if(dx_troubles) {
			*y = ymin;
			return true;
		}
		*y = calcY(x);
		return true;
	};
	void setEquation(T xA, T yA, T xB, T yB) {
		xmin = min2(xA, xB);
		xmax = max2(xA, xB);
		ymin = min2(yA, yB);
		ymax = max2(yA, yB);
		dx = xB - xA;
		dy = yB - yA;
		checkAndCorrectDX();
		checkAndCorrectDY();
		yk = dy / dx;
		xk = dx / dy;
		yb = 0;
		xb = 0;
		yb = yA - calcY(xA);
		xb = xA - calcX(yA);
	};
}; // end LineEquation2d

}; // end Graphic32
