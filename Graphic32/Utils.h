#pragma once
#include "../LibTypes/head.h"
#include "../Helpers/MinMax.h"

namespace Graphic32 {

using namespace LibTypes;

using Helpers::min2;
using Helpers::max2;
using Helpers::min4;
using Helpers::max4;

const uint32 COLOR_MASK_RED_BLUE = 0x00ff00ff;

enum Drawers { // pixel output methods
	D_SOLID, // copying pixels
	D_ALPHA, // alpha output, in result picture alpha channel is NOT correct
	D_THRESHOLD, // thresholding mask and alpha output, lower or equal than threshold = 0, higher = 1
	D_ALPHAKEEP, // alpha output, in result picture alpha channel IS correct
	D_MULTIPLEDALPHA, // alpha output with transparancy level
};

enum Resamplers { // resampling methods
	R_BILINEAR, // bilinear filtration
	R_ANISOTROPIC // anisotropic filtration
};

enum Operation { // operation types
	O_CLEAR,
	O_SET,
	O_NEGATE
};

bool BoundsCheck(ssize width, ssize height, ssize &xXS, ssize &yYS, ssize &cxXE, ssize &cyYE);
// 'BoundsCheck' returns false if area is out of bounds (not even crossing);
// x will become xs (x_start) , y -> ys , cx -> xe (x_end) , cy -> ye.

}; // end Graphic32
