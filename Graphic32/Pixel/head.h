#pragma once
#include "Merges.h"

namespace Graphic32 {

uint32 BilinearPixel(const uint32* src, ssize width, ssize height,
		ssize x8, ssize y8, uint32 background_color = 0x00000000);

uint32 AnisotropicPixel(const uint32* src, usize width, usize height,
		ssize x16, ssize y16, ssize estm_sx16, ssize estm_sy16, uint32 background_color = 0x00000000);

}; // end Graphic32
