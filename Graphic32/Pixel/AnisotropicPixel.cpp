#include "../Utils.h"

namespace Graphic32 {

const ssize ANISOTROPIC_PIXEL_MAXSIZE = 8;
const ssize ANISOTROPIC_PIXEL_MAX_ESTM16 = ((ANISOTROPIC_PIXEL_MAXSIZE - 2) << 16);

uint32 AnisotropicPixel(const uint32* src, usize width, usize height, ssize x16, ssize y16, ssize estm_sx16, ssize estm_sy16, uint32 background_color) {
	if(x16 < (-ANISOTROPIC_PIXEL_MAXSIZE * 32768)) { // div2 , X16
		return background_color; // out of contur color
	}
	if(y16 < (-ANISOTROPIC_PIXEL_MAXSIZE * 32768)) { // div2 , X16
		return background_color; // out of contur color
	}
	if(estm_sx16 > ANISOTROPIC_PIXEL_MAX_ESTM16) {
		x16 += (ANISOTROPIC_PIXEL_MAX_ESTM16 - estm_sx16) >> 1;
		estm_sx16 = ANISOTROPIC_PIXEL_MAX_ESTM16;
	}
	if(estm_sy16 > ANISOTROPIC_PIXEL_MAX_ESTM16) {
		y16 += (ANISOTROPIC_PIXEL_MAX_ESTM16 - estm_sy16) >> 1;
		estm_sy16 = ANISOTROPIC_PIXEL_MAX_ESTM16;
	}
	uint32 dPixels[4][ANISOTROPIC_PIXEL_MAXSIZE * ANISOTROPIC_PIXEL_MAXSIZE];
	uint32 pixel;
	ssize i, j, k, xs, ys, xe, ye, w, h, ofs1, ofs2, ofs;
	usize cf_up, cf_dn, cf_lt, cf_rt, x, y;
	usize c_ld, c_lu, c_ru, c_rd;
	usize square16;
	uint32 _r, _g, _b, _a;
	square16 = ((estm_sx16 >> 3) * (estm_sy16 >> 3)) >> 10; // max size by coordinate is 7.999
	estm_sx16 >>= 1;
	estm_sy16 >>= 1;
	xs = x16 - estm_sx16;
	xe = x16 + estm_sx16;
	ys = y16 - estm_sy16;
	ye = y16 + estm_sy16;
	cf_lt = 65536 - (xs & 0x0ffffL);
	cf_rt = xe & 0x0ffffL;
	cf_dn = 65536 - (ys & 0x0ffffL);
	cf_up = ye & 0x0ffffL;
	//-coefficients done-//
	xe |= 0x0ffffL;
	ye |= 0x0ffffL;
	xe++;
	ye++;
	xs >>= 16;
	xe >>= 16;
	ys >>= 16;
	ye >>= 16;
	w = xe - xs;
	h = ye - ys;
	//-sizes done-//
	ofs1 = 0;
	for(i = 0, y = ys; i < h; ++i, ++y, ofs1 += w) {
		if(y >= height) {
			for(j = 0; j < w; ++j) {
				ofs = ofs1 + j;
				dPixels[0][ofs] = (background_color & 0x0ffL);
				dPixels[1][ofs] = ((background_color >> 8) & 0x0ffL);
				dPixels[2][ofs] = ((background_color >> 16) & 0x0ffL);
				dPixels[3][ofs] = ((background_color >> 24) & 0x0ffL);
			}
		} else {
			ofs2 = y * width;
			for(j = 0, x = xs; j < w; ++j, ++x) {
				if(x >= width) {
					ofs = ofs1 + j;
					dPixels[0][ofs] = (background_color & 0x0ffL);
					dPixels[1][ofs] = ((background_color >> 8) & 0x0ffL);
					dPixels[2][ofs] = ((background_color >> 16) & 0x0ffL);
					dPixels[3][ofs] = ((background_color >> 24) & 0x0ffL);
				} else {
					pixel = src[ofs2 + x];
					ofs = ofs1 + j;
					dPixels[0][ofs] = (pixel & 0x0ffL);
					dPixels[1][ofs] = ((pixel >> 8) & 0x0ffL);
					dPixels[2][ofs] = ((pixel >> 16) & 0x0ffL);
					dPixels[3][ofs] = ((pixel >> 24) & 0x0ffL);
				}
			}
		}
	}
	if((w == 1)&&(h == 1)) {
		pixel = (dPixels[0][0] & 0x0ffL);
		pixel |= (dPixels[1][0] & 0x0ffL) << 8;
		pixel |= (dPixels[2][0] & 0x0ffL) << 16;
		pixel |= (dPixels[3][0] & 0x0ffL) << 24;
		return pixel;
	}
	_r = 0; _g = 0; _b = 0; _a = 0; // result is reseted.
	// oly 'w' or 'h' can be 1
	if(w == 1) { // h > 1
		for(ofs = 1; ofs < h - 1; ++ofs) {
			_b += dPixels[0][ofs];
			_g += dPixels[1][ofs];
			_r += dPixels[2][ofs];
			_a += dPixels[3][ofs];
		}
		_b <<= 16;
		_g <<= 16;
		_r <<= 16;
		_a <<= 16;
		estm_sy16 <<= 1;
		// + down
		_b += dPixels[0][0] * cf_dn;
		_g += dPixels[1][0] * cf_dn;
		_r += dPixels[2][0] * cf_dn;
		_a += dPixels[3][0] * cf_dn;
		// + up
		ofs = h - 1;
		_b += dPixels[0][ofs] * cf_up;
		_g += dPixels[1][ofs] * cf_up;
		_r += dPixels[2][ofs] * cf_up;
		_a += dPixels[3][ofs] * cf_up;
		_b /= estm_sy16;
		_g /= estm_sy16;
		_r /= estm_sy16;
		_a /= estm_sy16;
		pixel = _b;
		pixel |= _g << 8;
		pixel |= _r << 16;
		pixel |= _a << 24;
		return pixel; // -- //
	} // end 'w' = 1, h > 1
	if(h == 1) { // w > 1
		for(ofs = 1; ofs < w - 1; ++ofs) {
			_b += dPixels[0][ofs];
			_g += dPixels[1][ofs];
			_r += dPixels[2][ofs];
			_a += dPixels[3][ofs];
		}
		_b <<= 16;
		_g <<= 16;
		_r <<= 16;
		_a <<= 16;
		estm_sx16 <<= 1;
		// + left
		_b += dPixels[0][0] * cf_lt;
		_g += dPixels[1][0] * cf_lt;
		_r += dPixels[2][0] * cf_lt;
		_a += dPixels[3][0] * cf_lt;
		// + right
		ofs = w - 1;
		_b += dPixels[0][ofs] * cf_rt;
		_g += dPixels[1][ofs] * cf_rt;
		_r += dPixels[2][ofs] * cf_rt;
		_a += dPixels[3][ofs] * cf_rt;
		_b /= estm_sx16;
		_g /= estm_sx16;
		_r /= estm_sx16;
		_a /= estm_sx16;
		pixel = _b;
		pixel |= _g << 8;
		pixel |= _r << 16;
		pixel |= _a << 24;
		return pixel; // -- //
	} // end 'h' = 1, w > 1
	// main contribution
	ofs1 = w;
	for(j = 1; j < h - 1; ++j) {
		for(k = 1; k < w-1; ++k) {
			ofs = ofs1 + k;
			_b += dPixels[0][ofs];
			_g += dPixels[1][ofs];
			_r += dPixels[2][ofs];
			_a += dPixels[3][ofs];
		}
		ofs1 += w;
	}
	_b <<= 16;
	_g <<= 16;
	_r <<= 16;
	_a <<= 16;
	// edge coefficients
	c_ld = ((cf_lt >> 1) * (cf_dn)) >> 15;
	c_lu = ((cf_lt >> 1) * (cf_up)) >> 15;
	c_rd = ((cf_rt >> 1) * (cf_dn)) >> 15;
	c_ru = ((cf_rt >> 1) * (cf_up)) >> 15;
	// left down pixel
	_b += dPixels[0][0] * c_ld;
	_g += dPixels[1][0] * c_ld;
	_r += dPixels[2][0] * c_ld;
	_a += dPixels[3][0] * c_ld;
	// right down pixel
	ofs = w - 1;
	_b += dPixels[0][ofs] * c_rd;
	_g += dPixels[1][ofs] * c_rd;
	_r += dPixels[2][ofs] * c_rd;
	_a += dPixels[3][ofs] * c_rd;
	// left up pixel
	ofs = (h - 1) * w;
	_b += dPixels[0][ofs] * c_lu;
	_g += dPixels[1][ofs] * c_lu;
	_r += dPixels[2][ofs] * c_lu;
	_a += dPixels[3][ofs] * c_lu;
	// right up pixel
	ofs = h * w - 1;
	_b += dPixels[0][ofs] * c_ru;
	_g += dPixels[1][ofs] * c_ru;
	_r += dPixels[2][ofs] * c_ru;
	_a += dPixels[3][ofs] * c_ru;
	// edges done, now sides
	for(j = 1; j < h - 1; ++j) {
		// left side
		ofs = j * w;
		_b += dPixels[0][ofs] * cf_lt;
		_g += dPixels[1][ofs] * cf_lt;
		_r += dPixels[2][ofs] * cf_lt;
		_a += dPixels[3][ofs] * cf_lt;
		// right side
		ofs += w - 1;
		_b += dPixels[0][ofs] * cf_rt;
		_g += dPixels[1][ofs] * cf_rt;
		_r += dPixels[2][ofs] * cf_rt;
		_a += dPixels[3][ofs] * cf_rt;
	}
	// left, right done
	ofs1 = w * (h - 1);
	for(j = 1; j < w - 1; ++j) {
		// down side
		ofs = j;
		_b += dPixels[0][ofs] * cf_dn;
		_g += dPixels[1][ofs] * cf_dn;
		_r += dPixels[2][ofs] * cf_dn;
		_a += dPixels[3][ofs] * cf_dn;
		// up side
		ofs += ofs1;
		_b += dPixels[0][ofs] * cf_up;
		_g += dPixels[1][ofs] * cf_up;
		_r += dPixels[2][ofs] * cf_up;
		_a += dPixels[3][ofs] * cf_up;
	}
	// up, down done
	_r /= square16;
	_g /= square16;
	_b /= square16;
	_a /= square16;
	pixel = _b;
	pixel |= _g << 8;
	pixel |= _r << 16;
	pixel |= _a << 24;
	return pixel;
}; // end anisotropic pixel

}; // end Graphic32
