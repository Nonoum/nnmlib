#pragma once
#include "../Utils.h"

namespace Graphic32 {

inline void AlphaMergeKeep(uint32& dst, uint32 adding) {
	uint32 buf1, buf2, buf3;
	uint32 alpha2, alpha1 = (adding >> 24);
	if(! alpha1) {
		return;
	}
	alpha2 = alpha1 + 1;
	alpha1 = alpha1 ^ 0x000000ff;
	buf1 = dst & COLOR_MASK_RED_BLUE;
	buf2 = adding & COLOR_MASK_RED_BLUE;
	buf1 *= alpha1; // red, blue /dst
	buf2 *= alpha2; // red, blue /input
	buf3 = ((buf1 + buf2) >> 8) & COLOR_MASK_RED_BLUE; // result red, blue
	buf1 = (dst >> 8) & COLOR_MASK_RED_BLUE;
	buf2 = (adding >> 8) & COLOR_MASK_RED_BLUE;
	buf1 *= alpha1; // alpha, green /dst
	buf2 *= alpha2; // alpha, green /input
	dst = buf3; // red,blue
	dst |= ((buf1 + buf2) & 0xff00ff00); // alpha, green
};

inline void AlphaMerge(uint32& dst, uint32 adding) {
	uint32 buf1, buf2, buf3;
	uint32 alpha2, alpha1 = (adding >> 24);
	if(! alpha1) {
		return;
	}
	alpha2 = alpha1 + 1;
	alpha1 = alpha1 ^ 0x000000ff;
	buf1 = dst & COLOR_MASK_RED_BLUE;
	buf2 = adding & COLOR_MASK_RED_BLUE;
	buf1 *= alpha1; // red, blue /dst
	buf2 *= alpha2; // red, blue /input
	buf3 = ((buf1 + buf2) >> 8) & COLOR_MASK_RED_BLUE; // result red, blue
	buf1 = dst & 0x0000ff00;
	buf2 = adding  & 0x0000ff00;
	buf1 *= alpha1; // alpha, green /dst
	buf2 *= alpha2; // alpha, green /input
	dst = buf3; // red,blue
	dst |= (((buf1 + buf2) >> 8) & 0x0000ff00); // alpha, green
};

inline void PixelsMergeKeep(uint32& dst, uint32 adding, uint32 alpha256) {
	uint32 buf1, buf2, buf3;
	uint32 alpha1;
	alpha1 = 256L - alpha256;
	buf1 = dst & COLOR_MASK_RED_BLUE;
	buf2 = adding & COLOR_MASK_RED_BLUE;
	buf1 *= alpha1; // red, blue /dst
	buf2 *= alpha256; // red, blue /input
	buf3 = ((buf1 + buf2) >> 8) & COLOR_MASK_RED_BLUE; // result red, blue
	buf1 = (dst >> 8) & COLOR_MASK_RED_BLUE;
	buf2 = (adding >> 8) & COLOR_MASK_RED_BLUE;
	buf1 *= alpha1; // alpha, green /dst
	buf2 *= alpha256; // alpha, green /input
	dst = buf3; // red,blue
	dst |= ((buf1 + buf2) & 0xff00ff00); // alpha, green
};

}; // end Graphic32
