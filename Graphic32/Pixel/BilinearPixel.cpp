#include "../Utils.h"

namespace Graphic32 {

uint32 BilinearPixel(const uint32* src, ssize width, ssize height, ssize x8, ssize y8, uint32 background_color) {
	uint32 pixel, PLD, PLU, PRD, PRU;
	uint32 buf1, buf2, buf3, buf4;
	ssize px, py;
	ssize dxl, dxr, dyd, dyu, mld, mlu, mrd, mru;
	px = (x8 >> 8);
	py = (y8 >> 8);
	if(px >= width - 1) {
		if(px > width - 1) {
			return background_color;
		}
		// px == wi-1
		if(py >= height - 1) {
			if(py > height - 1) {
				return background_color;
			}
			// py == he-1
			PLU = background_color;
			PLD = src[py * width + px];
			PRU = background_color;
			PRD = background_color;
			dxr = x8 & 0x0ff;
			dyu = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyd = 256 - dyu;
		} else if(py <= -1) {
			if(py < -1) {
				return background_color;
			}
			// py == -1
			PLU = src[px];
			PLD = background_color;
			PRU = background_color;
			PRD = background_color;
			dxr = x8 & 0x0ff; // same 4 lines
			dyd = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyu = 256 - dyd;
		} else {
			// py == normal
			src += py * width;
			PLU = src[px + width]; // px,py+1
			PLD = src[px]; // px,py
			PRU = background_color;
			PRD = background_color;
			dxr = x8 & 0x0ff; // same 4 lines
			dyu = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyd = 256 - dyu;
		}
	} else if(px <= -1) { //// px < wi-1
		if(px < -1) {
			return background_color;
		}
		// px == -1
		if(py >= height - 1) {
			if(py > height - 1) {
				return background_color;
			}
			// py == he-1
			PLU = background_color;
			PLD = background_color;
			PRU = background_color;
			PRD = src[py * width]; // 0,py
			dxr = x8 & 0x0ff; // same 4 lines
			dyu = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyd = 256 - dyu;
		} else if(py <= -1) {
			if(py < -1) {
				return background_color;
			}
			// py == -1
			PLU = background_color;
			PLD = background_color;
			PRU = *src; // 0,0
			PRD = background_color;
			dxr = x8 & 0x0ff; // same 4 lines
			dyd = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyu = 256 - dyd;
		} else {
			// py == normal
			PLU = background_color;
			PLD = background_color;
			src += py * width;
			PRD = *src; // 0,py
			PRU = src[width]; // 0,py+1
			dxr = x8 & 0x0ff; // same 4 lines
			dyu = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyd = 256 - dyu;
		}
	} else {
		// px == normal
		if(py >= height - 1) {
			if(py > height - 1) {
				return background_color;
			}
			// py == he-1
			src += py * width + px;
			PLU = background_color;
			PLD = *src; // px,py
			PRU = background_color;
			PRD = src[1]; // px+1,py
			dxr = x8 & 0x0ff; // same 4 lines
			dyu = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyd = 256 - dyu;
		} else if(py <= -1) {
			if(py < -1) {
				return background_color;
			}
			// py == -1
			src += px;
			PLU = *src; // px,0
			PLD = background_color;
			PRU = src[1]; // px+1,0
			PRD = background_color;
			dxr = x8 & 0x0ff; // same 4 lines
			dyu = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyd = 256 - dyu;
		} else {
			// py == normal
			src += py * width + px;
			PLU = src[width]; // px,py+1
			PLD = *src; // px,py
			PRU = src[width + 1]; // px+1,py+1
			PRD = src[1]; // px+1,py
			dxr = x8 & 0x0ff; // same 4 lines
			dyu = y8 & 0x0ff;
			dxl = 256 - dxr;
			dyd = 256 - dyu;
		}
	}
	dxr = x8 & 0x0ff;
	dyu = y8 & 0x0ff;
	dxl = 256 - dxr;
	dyd = 256 - dyu;
	mld = (dxl * dyd) >> 8;
	mlu = (dxl * dyu) >> 8;
	mrd = (dxr * dyd) >> 8;
	mru = (dxr * dyu) >> 8;
	buf1 = PLD & COLOR_MASK_RED_BLUE;
	buf2 = PLU & COLOR_MASK_RED_BLUE;
	buf3 = PRD & COLOR_MASK_RED_BLUE;
	buf4 = PRU & COLOR_MASK_RED_BLUE;
	buf1 *= mld;
	buf2 *= mlu;
	buf3 *= mrd;
	buf4 *= mru;
	buf1 += buf2;
	buf3 += buf4;
	buf1 += buf3; // red, blue
	buf1 |= 0x007F007F; // rounding
	buf1 += 0x00010001; // ..
	pixel = (buf1 >> 8) & COLOR_MASK_RED_BLUE;
	buf1 = (PLD >> 8) & COLOR_MASK_RED_BLUE;
	buf2 = (PLU >> 8) & COLOR_MASK_RED_BLUE;
	buf3 = (PRD >> 8) & COLOR_MASK_RED_BLUE;
	buf4 = (PRU >> 8) & COLOR_MASK_RED_BLUE;
	buf1 *= mld;
	buf2 *= mlu;
	buf3 *= mrd;
	buf4 *= mru;
	buf1 += buf2;
	buf3 += buf4;
	buf1 += buf3; // alpha, green
	buf1 |= 0x007F007F; // rounding
	buf1 += 0x00010001; // ..
	pixel |= (buf1 & 0xff00ff00);
	return pixel;
}; // end bilinear pixel

}; // end Graphic32
