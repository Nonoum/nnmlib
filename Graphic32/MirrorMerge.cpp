#include "functions.h"
#include "Pixel/Merges.h"

namespace Graphic32 {

bool MirrorMerge(uint32* dst, ssize width, ssize height, bool xmirror, bool ymirror, ssize x_start, ssize y_start) {
	if(! dst) {
		return false;
	}
	uint32* ptr;
	uint32 alpha256;
	ssize x, y, offset_between, dist, limit;
	if(xmirror) {
		x_start = max2(x_start, 1 + (width >> 1));
		x_start = min2(x_start, width - 1);
		dist = width - x_start;
		offset_between = -(width - 1);
		if(dist == 1) {
			limit = 128;
		} else {
			limit = (256 * (dist - 1)) / dist;
		}
		for(x = width - 1; x >= x_start; --x) {
			alpha256 = uint32(((x - x_start + 1) * limit) / dist);
			ptr = dst + x;
			for(y = 0; y < height; ++y) {
				PixelsMergeKeep(ptr[0], ptr[offset_between], alpha256);
				ptr += width;
			}
			offset_between += 2;
		}
	}
	if(ymirror) {
		y_start = max2(y_start, 1 + (height >> 1));
		y_start = min2(y_start, height - 1);
		dist = height - y_start;
		offset_between = -((height - 1) * width);
		if(dist == 1) {
			limit = 128;
		} else {
			limit = (256 * (dist - 1)) / dist;
		}
		for(y = height - 1; y >= y_start; --y) {
			alpha256 = uint32(((y - y_start + 1) * limit) / dist);
			ptr = dst + (y * width);
			for(x = 0; x < width; ++x) {
				PixelsMergeKeep(ptr[0], ptr[offset_between], alpha256);
				++ptr;
			}
			offset_between += (width<<1);
		}
	}
	return true;
}; // end mirror merge

}; // end Graphic32
