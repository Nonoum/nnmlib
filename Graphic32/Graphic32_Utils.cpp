#include "Utils.h"

namespace Graphic32 {

bool BoundsCheck(ssize width, ssize height, ssize &xXS, ssize &yYS, ssize &cxXE, ssize &cyYE) {
	cyYE += yYS;
	cxXE += xXS;
	if((cyYE <= 0) || (cxXE <= 0)) {
		return false;
	}
	if((xXS >= width) || (yYS >= height)) {
		return false;
	}
	if(xXS < 0) {
		xXS = 0;
	}
	if(yYS < 0) {
		yYS = 0;
	}
	if(cxXE > width) {
		cxXE = width;
	}
	if(cyYE > height) {
		cyYE = height;
	}
	return true; // normal
}; // end bounds check

}; // end Graphic32
