#pragma once
#include "Utils.h"

namespace Graphic32 {

/* DrawRect: (level) is Threshold Level for mode (drawer) = D_THRESHOLD,
	or Alpha multiplier level for mode (drawer) = D_MULTIPLEDALPHA */
bool DrawRect(uint32* dst, ssize dst_w, ssize dst_h, ssize x, ssize y, usize cx, usize cy,
		const uint32* src, ssize src_w, ssize src_h, ssize toffsx, ssize toffsy, Drawers drawer, ubyte level);

bool FillPolygon4p(uint32* dst, ssize width, ssize height,
		float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD, uint32 color);

bool DrawTexture(uint32* dst, ssize width, ssize height, float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD,
		const uint32* src, ssize src_width, ssize src_height, bool full_square, Resamplers method, uint32 background_color = 0x00000000);

bool DrawLine(uint32* dst, ssize width, ssize height, ssize x1, ssize y1, ssize x2, ssize y2, uint32 color);

bool MirrorMerge(uint32* dst, ssize width, ssize height, bool xmirror, bool ymirror, ssize x_start, ssize y_start);

bool ResizeAnisotropic(const uint32* src, usize width, usize height,
		uint32** dst, usize* dst_width, usize* dst_height, usize new_w, usize new_h);

bool ResizeBilinear(const uint32* src, usize width, usize height,
		uint32** dst, usize* dst_width, usize* dst_height, usize new_w, usize new_h);

bool Rotate(const uint32* src, usize width, usize height, double degrees, bool same_size,
		uint32** dst, usize* dst_width, usize* dst_height, uint32 background_color = 0x00000000);

bool FillWithPixel(uint32* dst, usize width, usize height,
		ssize x, ssize y, ssize cx, ssize cy, uint32 pixel = 0xFF000000, bool keep_alpha = false);

bool XorWithPixel(uint32* dst, usize width, usize height,
		ssize x, ssize y, ssize cx, ssize cy, uint32 pixel = 0xFFFFFFFF);

bool Cut(const uint32* src, usize width, usize height,
		ssize c_left, ssize c_right, ssize c_up, ssize c_down, uint32** dst, usize* dst_width, usize* dst_height);

bool Cycle(const uint32* src, usize width, usize height,
		uint32** dst, usize* dst_width, usize* dst_height, usize new_w, usize new_h);

void Allocate(uint32** dst, usize* dst_width, usize* dst_height, usize new_width, usize new_height);

}; // end Graphic32
