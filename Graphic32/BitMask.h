#pragma once
#include "Img32.h"

namespace Graphic32 {

class BitMask : private NonCopyable {
	ubyte* data;
	usize _width;
	usize _height;
public:
	BitMask();
	inline operator bool() const {
		return data != NULL;
	};
	inline usize width() const {
		return _width;
	};
	inline usize height() const {
		return _height;
	};
	inline void outputPixelUnchecked(uint32& dst, usize x, usize y, uint32 color) const {
		if(testUnchecked(x, y)) {
			dst = color;
		}
	};
	void loadAlpha(const Img32& src, ubyte positive_from);
private:
	inline bool testUnchecked(usize x, usize y) const {
		return testUnchecked(y*_width + x);
	};
	void alloc(usize new_width, usize new_height);
	inline void setUnchecked(usize num, bool val = true) {
		static const ubyte t_bits[] = {1,2,4,8,16,32,64,128};
		static const ubyte f_bits[] = {0xFE,0xFD,0xFB,0xF7,0xEF,0xDF,0xBF,0x7F};
		if(val) {
			data[num >> 3] |= t_bits[num & 0x07];
		} else {
			data[num >> 3] &= f_bits[num & 0x07];
		}
	};
	inline bool testUnchecked(usize num) const {
		static const ubyte bits[] = {1,2,4,8,16,32,64,128};
		return (data[num >> 3] & bits[num & 0x07]) != 0;
	};
};

}; // end Graphic32
