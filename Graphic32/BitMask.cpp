#include "BitMask.h"

namespace Graphic32 {

BitMask :: BitMask()
		: data(NULL), _width(0), _height(0) {
};

void BitMask :: loadAlpha(const Img32& src, ubyte positive_from) {
	if(! src) {
		return;
	}
	alloc(src.width(), src.height());
	for(usize y = 0; y < _height; ++y) {
		for(usize x = 0; x < _width; ++x) {
			bool value = (src.pixelUnchecked(x, y) >> 24) >= positive_from;
			setUnchecked(y*_width + x, value);
		}
	}
};

void BitMask :: alloc(usize new_width, usize new_height) {
	usize cnt = (((new_width * new_height - 1) | 0x7) + 1) >> 3; // round up and to bytes cnt
	if((!data) || (_width != new_width) || (_height != new_height)) {
		delete [] data;
		_width = 0;
		_height = 0;
		data = NULL;
		data = new ubyte[cnt];
		_width = new_width;
		_height = new_height;
	}
};

}; // end Graphic32
