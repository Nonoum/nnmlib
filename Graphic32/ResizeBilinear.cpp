#include "functions.h"
#include "Pixel/head.h"

namespace Graphic32 {

bool ResizeBilinear(const uint32* src, usize width, usize height, uint32** dst, usize* dst_width, usize* dst_height, usize new_w, usize new_h) {
	if((! src) || (! dst)) {
		return false; // NULL pointer
	}
	if((! new_w) || (! new_h)) {
		return false; // BAD sizes
	}
	if(sizeof(usize) == 4) {
		if((width > 32000) || (height > 32000)) {
			return false; // too large
		}
	}
	Allocate(dst, dst_width, dst_height, new_w, new_h);
	usize i, j; // dst cycles
	usize x16, y16, dx16, dy16; // +16 bits after point
	dx16 = (width << 16) / new_w;
	dy16 = (height << 16) / new_h;
	uint32* ptr = *dst;
	for(i = 0, y16 = 0; i < new_h; ++i, y16 += dy16) {
		for(j = 0, x16 = 0; j < new_w; ++j, x16 += dx16) {
			*ptr = BilinearPixel(src, width, height, x16 >> 8, y16 >> 8);
			++ptr;
		}
	}
	return true;
}; // end resize anisotropic

}; // end Graphic32
