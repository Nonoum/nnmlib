#include "functions.h"

namespace Graphic32 {

void DrawLineSectionUnsafe(uint32 *dst, usize width, ssize x, ssize y, ssize xRight, ssize y2, ssize dx, ssize dy, uint32 color) {
	ssize d, delta1, delta2;
	dst += width*y;
	dst[x] = color;
	if(dy >= 0) {
		if(dx >= dy) {
			delta1 = (dy << 1);
			delta2 = ((dy - dx) << 1);
			d = (dy << 1) - dx;
			for(; x < xRight; ) {
				d >= 0 ? dst += width, d += delta2 : d += delta1;
				dst[++x] = color;
			}
		} else {
			delta1 = (dx << 1);
			delta2 = ((dx - dy) << 1);
			d = (dx << 1) - dy;
			dst += x;
			for(; y < y2; ++y) {
				d >= 0 ? ++dst, d += delta2 : d += delta1;
				dst += width; // y++
				*dst = color;
			}
		}
	} else {
		dy = -dy;
		if(dx >= dy) {
			delta1 = (dy << 1);
			delta2 = ((dy - dx) << 1);
			d = (dy << 1) - dx;
			for(; x < xRight; ) {
				d >= 0 ? dst -= width, d += delta2 : d += delta1;
				dst[++x] = color;
			}
		} else {
			delta1 = (dx << 1);
			delta2 = ((dx - dy) << 1);
			d = (dx << 1) - dy;
			dst += x;
			for(; y > y2; --y) {
				d >= 0 ? ++dst, d += delta2 : d += delta1;
				dst -= width; // y--
				*dst = color;
			}
		}
	}
}; // end draw line section

void LineBrezenghamUnsafe(uint32 *dst, ssize width, ssize x1, ssize y1, ssize x2, ssize y2, uint32 color) {
	ssize dx,dy;
	dx = x2 - x1;
	dy = y2 - y1;
	if(dx >= 0) {
		DrawLineSectionUnsafe(dst, width, x1, y1, x2, y2, dx, dy, color);
	} else {
		DrawLineSectionUnsafe(dst, width, x2, y2, x1, y1, -dx, -dy, color);
	}
}; // end line brezengham unsafe

void DrawVerticalLineUnsafe(uint32 *dst, ssize width, ssize x, ssize y, ssize dy, uint32 color) {
	dst += (width * y + x);
	*dst = color;
	if(dy < 0) {
		dy = -dy;
		width = -width;
	}
	for(y = 0; y <= dy; ++y, dst += width) {
		*dst = color;
	}
}; // end draw vertical line unsafe

void DrawHorizontalLineUnsafe(uint32 *dst, ssize width, ssize x, ssize y, ssize dx, uint32 color) {
	dst += (width * y + x);
	if(dx < 0) {
		std::fill(dst + dx, dst + 1, color);
	} else {
		std::fill(dst, dst + dx + 1, color);
	}
}; // end draw vertical line unsafe

bool DrawLine(uint32* dst, ssize width, ssize height, ssize x1, ssize y1, ssize x2, ssize y2, uint32 color) {
	if(! dst) {
		return false;
	}
	if(min2(x1, x2) >= width) {
		return true; // out of contur (right)
	}
	if(min2(y1, y2) >= height) {
		return true; // out of contur (up)
	}
	if(max2(x1, x2) < 0) {
		return true; // out of contur (left)
	}
	if(max2(y1, y2) < 0) {
		return true; // out of contur (down)
	}
	// line is somewhere in contur or crossing it
	ssize dx, dy, ofsx, ofsy;
	if(x1 > x2) {
		std::swap(x1, x2);
		std::swap(y1, y2);
	}
	dx = x2 - x1;
	// dx >= 0 (!)
	if(! dx) { // vertical line
		if(y2 < y1) {
			std::swap(y1, y2);
		}
		if(y1 < 0) {
			y1 = 0;
		}
		if(y2 >= height) {
			y2 = height - 1;
		}
		DrawVerticalLineUnsafe(dst, width, x1, y1, y2-y1, color);
		return true;
	}
	dy = y2 - y1;
	if(! dy) { // horizontal line
		if(x1 < 0) {
			x1 = 0;
		}
		if(x2 >= width) {
			x2 = width - 1;
		}
		DrawHorizontalLineUnsafe(dst, width, x1, y1, x2-x1, color);
		return true;
	}
	// tilt line
	ofsy = y1 - ((x1 * dy) / dx);
	ofsx = x1 - ((y1 * dx) / dy);
	if(x1 < 0) { // left point - out of contur
		x1 = 0;
		y1 = ofsy;
	}
	if(x2 >= width) { // right point - out of contur
		x2 = width - 1;
		y2 = ((x2 * dy) / dx) + ofsy;
	}
	if(y1 < 0) {
		y1 = 0;
		x1 = ofsx;
	} else if(y1 >= height) {
		y1 = height - 1;
		x1 = ((y1 * dx) / dy) + ofsx;
	}
	if(y2 < 0) {
		y2 = 0;
		x2 = ofsx;
	} else if(y2 >= height) {
		y2 = height - 1;
		x2 = ((y2 * dx) / dy) + ofsx;
	}
	if(x2 < x1) {
		std::swap(x1, x2);
		std::swap(y1, y2);
	}
	if((x1 >= 0)&&(x2 < width)) {
		DrawLineSectionUnsafe(dst, width, x1, y1, x2, y2, x2-x1, y2-y1, color);
	}
	return true;
}; // end draw line safe

}; // end Graphic32
