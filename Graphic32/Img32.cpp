#include "functions.h"
#include "Img32.h"
#include "BitMask.h"
#include "AlphaMask.h"
#include "../Helpers/MinMax.h"
#include "../TargaImageCodec/head.h"
#include <algorithm> // std::swap
#include <fstream>
#include <assert.h>

namespace Graphic32 {

// constructors e.t.c -------------------------------------------------------

Img32 :: Img32() {
	reset();
};

Img32 :: Img32(const Img32& other) {
	reset();
	other.clone(*this);
};

Img32 :: Img32(Img32& other, bool to_swap) {
	reset();
	if(to_swap) {
		swap(other);
	} else {
		other.clone(*this);
	}
};

Img32 :: Img32(usize width, usize height) {
	reset();
	alloc(width, height);
};

Img32 :: Img32(usize width, usize height, uint32 color) {
	reset();
	alloc(width, height);
	fillRect(color);
};

Img32& Img32 :: operator=(const Img32& other) {
	other.clone(*this);
	return *this;
};

Img32 :: ~Img32() {
	dealloc();
};

// simple processing -------------------------------------------------------

void Img32 :: setActiveChannels(ubyte channels_count) {
	if((data) && (channels_count) && (channels_count <= 4)) {
		nchannels = channels_count;
	}
};

void Img32 :: processChannels(ubyte rgba_flags, Operation op, uint32 pixel) {
	if((! data) || (! _height)) {
		return;
	}
	assert( rgba_flags > 0 && "No channels specified for processing" );
	assert( rgba_flags < 16 && "Invalid channels specified for processing" );
	const uint32 mask = (rgba_flags & 1 ? 0xFF000000 : 0)
						| (rgba_flags & 2 ? 0xFF : 0)
						| (rgba_flags & 4 ? 0xFF00 : 0)
						| (rgba_flags & 8 ? 0xFF0000 : 0);
	const uint32 rmask = ~mask;
	pixel &= mask;
	assert( (_width*_height)/_height == _width && "Too big image, optimization error" );
	const usize cnt = _width * _height;
	if(op == O_CLEAR) {
		for(usize i = 0; i < cnt; ++i) {
			data[i] &= rmask;
		}
	} else if(op == O_SET) {
		for(usize i = 0; i < cnt; ++i) {
			data[i] &= rmask;
			data[i] |= pixel;
		}
	} else if(op == O_NEGATE) {
		for(usize i = 0; i < cnt; ++i) {
			data[i] ^= mask;
		}
	}
};

void Img32 :: subtract(const Img32& right, ubyte rgba_flags) {
	assert( rgba_flags > 0 && "No channels specified for subtracting" );
	assert( rgba_flags < 16 && "Invalid channels specified for subtracting" );
	if((! data) || (! right.data)) {
		return;
	}
	usize xa, ya, xb, yb;
	const bool flags[4] = {bool(rgba_flags & 2), bool(rgba_flags & 4), bool(rgba_flags & 8), bool(rgba_flags & 1)};
	// TODO: not optimized cycle
	for(ya = 0; ya < _height; ++ya) {
		yb = ya % right._height;
		for(xa = 0, xb = 0; xa < _width; ++xa, ++xb) {
			if(xb >= right._width) {
				xb = 0;
			}
			union {
				uint32 pixel;
				ubyte rgba[4];
			} lp, rp;
			lp.pixel = pixelUnchecked(xa, ya);
			rp.pixel = right.pixelUnchecked(xb, yb);
			for(int i = 0; i < 4; ++i) {
				if(flags[i]) {
					if(lp.rgba[i] <= rp.rgba[i]) {
						lp.rgba[i] = 0;
					} else {
						lp.rgba[i] -= rp.rgba[i];
					}
				}
			}
			data[ya*_width + xa] = lp.pixel;
		}
	}
};

bool Img32 :: moveAverageRGBToAlpha(bool keep_rgb) {
	if(! data) {
		return false;
	}
	size_t cnt = _width * _height;
	for(size_t i = 0; i < cnt; ++i) {
		uint32 pixel = data[i];
		usize avg = pixel & 0x0FF;
		avg += (pixel >> 8) & 0x0FF;
		avg += (pixel >> 16) & 0x0FF;
		avg /= 3;
		if(keep_rgb) {
			pixel &= 0x00FFFFFF;
			pixel |= (avg << 24);
		} else {
			pixel = uint32(avg << 24);
		}
		data[i] = pixel;
	}
	return true;
};

bool Img32 :: moveAlphaToRGB() {
	if(! data) {
		return false;
	}
	uint32 table[256];
	for(uint32 i = 0; i < 256; ++i) {
		table[i] = i | (i << 8);
		table[i] |= table[i] << 16;
	}
	size_t cnt = _width * _height;
	for(size_t i = 0; i < cnt; ++i) {
		data[i] = table[data[i] >> 24];
	}
	return true;
};

bool Img32 :: channelPower(ubyte power, ubyte ichannel) {
	assert(data != NULL && ichannel <= 3);
	if((! data) || (ichannel > 3)) {
		return false;
	}
	union {
		uint32 value;
		ubyte channels[4];
	} pixel;
	ubyte table[256];
	const uint32 level = 255 - power;
	std::fill(table + level, table + 256, 255);
	for(uint32 i = 0; i < level; ++i) {
		table[i] = (i << 8) / level;
	}
	size_t cnt = _width * _height;
	for(size_t i = 0; i < cnt; ++i) {
		pixel.value = data[i];
		pixel.channels[ichannel] = table[(uint32)pixel.channels[ichannel]];
		data[i] = pixel.value;
	}
	return true;
};

void Img32 :: alloc(usize new__width, usize new__height) {
	Allocate(&data, &_width, &_height, new__width, new__height);
	if(data) {
		nchannels = 4; // 32 bit image (4 channels) allocated
	}
};

void Img32 :: dealloc() {
	delete [] data;
	data = NULL;
	_width = 0;
	_height = 0;
	nchannels = 4;
};

bool Img32 :: setXorOf(const Img32& src1, const Img32& src2, uint32 background_color) {	
	if((! src1) || (! src2)) {
		return false;
	}
	if((&src1 == this) || (&src2 == this)) {
		assert(false && "Img32::setXorOf error: sources must be different from destination!");
		return false;
	}
	const usize new_w = Helpers::max2(src1.width(), src2.width());
	const usize new_h = Helpers::max2(src1.height(), src2.height());
	alloc(new_w, new_h);
	// cleaning background
	fillRect(background_color);
	// filling with src1
	const usize h1 = src1.height();
	const usize w1 = src1.width();
	const uint32* src_ptr = src1.data;
	uint32* dst_ptr = data;
	for(usize y = 0; y < h1; ++y) {
		for(usize x = 0; x < w1; ++x) {
			dst_ptr[x] = src_ptr[x];
		}
		src_ptr += w1;
		dst_ptr += new_w;
	}
	// XORing with src2
	const usize w2 = src2.width();
	const usize h2 = src2.height();
	src_ptr = src2.data;
	dst_ptr = data;
	for(usize y = 0; y < h2; ++y) {
		for(usize x = 0; x < w2; ++x) {
			dst_ptr[x] ^= src_ptr[x];
		}
		src_ptr += w2;
		dst_ptr += new_w;
	}
	return true;
};

bool Img32 :: fillRect(uint32 color, ssize x, ssize y, ssize cx, ssize cy, bool keep_alpha) {
	return FillWithPixel(data, _width, _height, x, y, cx, cy, color, keep_alpha);
};

bool Img32 :: xorWith(uint32 color, ssize x, ssize y, ssize cx, ssize cy) {
	return XorWithPixel(data, _width, _height, x, y, cx, cy, color);
};

bool Img32 :: cycle(Img32& dst, usize new_width, usize new_height) const {
	return Cycle(data, _width, _height, &dst.data, &dst._width, &dst._height, new_width, new_height);
};

bool Img32 :: resizeAnisotropic(Img32& dst, usize new_width, usize new_height) const {
	return ResizeAnisotropic(data, _width, _height, &dst.data, &dst._width, &dst._height, new_width, new_height);
};

bool Img32 :: resizeBilinear(Img32& dst, usize new_width, usize new_height) const {
	return ResizeBilinear(data, _width, _height, &dst.data, &dst._width, &dst._height, new_width, new_height);
};

bool Img32 :: rotate(Img32& dst, double degrees, bool same_size, uint32 background_color) const {
	return Rotate(data, _width, _height, degrees, same_size, &dst.data, &dst._width, &dst._height, background_color);
};

bool Img32 :: drawRect(ssize x, ssize y, usize cx, usize cy, const Img32& src,
		ssize xoffs, ssize yoffs, Drawers mode, ubyte level) {

	return DrawRect(data, _width, _height, x, y, cx, cy, src.data, src._width, src._height, xoffs, yoffs, mode, level);
};

bool Img32 :: mirrorMerge(bool by_x, bool by_y, usize x_start, usize y_start) {
	return MirrorMerge(data, _width, _height, by_x, by_y, x_start, y_start);
};

bool Img32 :: drawLine(ssize x1, ssize y1, ssize x2, ssize y2, uint32 color) {
	return DrawLine(data, _width, _height, x1, y1, x2, y2, color);
};

bool Img32 :: drawPolygon(float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD, uint32 color) {
	return FillPolygon4p(data, _width, _height, xA, yA, xB, yB, xC, yC, xD, yD, color);
};

bool Img32 :: drawTexture(const Img32& src, float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD,
		bool full_square, Resamplers method) {

	return DrawTexture(data, _width, _height, xA, yA, xB, yB, xC, yC, xD, yD, src.data, src._width, src._height, full_square, method);
};

bool Img32 :: drawRectContur(ssize x, ssize y, ssize width_included, ssize height_included, uint32 color) {
	bool result = false;
	result |= drawLine(x, y, x, y + height_included, color);
	result |= drawLine(x, y, x + width_included, y, color);
	result |= drawLine(x + width_included, y, x + width_included, y + height_included, color);
	result |= drawLine(x, y + height_included, x + width_included, y + height_included, color);
	return result;
};

bool Img32 :: drawGrid(ssize x_disp, ssize y_disp, ssize vline_width, ssize hline_height, ssize x_step, ssize y_step, uint32 color, const Geometry::Rect* allowed_area) {
	assert(x_step != 0 && y_step != 0);
	assert(hline_height < y_step && vline_width < x_step);
	if(! data) {
		return false;
	}
	if(vline_width >= x_step) {
		vline_width = x_step - 1;
	}
	if(hline_height >= y_step) {
		hline_height = y_step - 1;
	}
	bool result = false;
	x_disp %= x_step;
	y_disp %= y_step;
	if(x_disp < 0) {
		x_disp += x_step;
	}
	if(y_disp < 0) {
		y_disp += y_step;
	}
	// initial positions
	ssize x = x_disp;
	ssize y = y_disp;
	// counts of vertical and horizontal lines
	ssize x_times = 0;
	ssize y_times = 0;
	// bounds of allowed area
	ssize x_min = 0, y_min = 0;
	ssize max_width = _width, max_height = _height;
	if(allowed_area) {
		const ssize ix_start = (allowed_area->x() + x_step - 1 - (x + vline_width)) / x_step;
		const ssize ix_end = (allowed_area->getXEnd() + x_step - 1 - x) / x_step;
		x_times = ix_end - ix_start;
		x += ix_start * x_step;
		const ssize iy_start = (allowed_area->y() + y_step - 1 - (y + hline_height)) / y_step;
		const ssize iy_end = (allowed_area->getYEnd() + y_step - 1 - y) / y_step;
		y_times = iy_end - iy_start;
		y += iy_start * y_step;
		// bounds
		x_min = allowed_area->x();
		y_min = allowed_area->y();
		max_width = allowed_area->width();
		max_height = allowed_area->height();
	} else {
		x_times = _width / x_step;
		y_times = _height / y_step;
	}
	// vertical lines (cycle by X)
	for(ssize i = 0; i < x_times; ++i) {
		const ssize xx = Helpers::max2(x, x_min);
		const ssize w = Helpers::min2(vline_width + x - xx, x_min + max_width - xx);
		result |= fillRect(color, xx, y_min, w, max_height, false);
		x += x_step;
	}
	// horizontal lines (cycle by Y)
	for(ssize i = 0; i < y_times; ++i) {
		const ssize yy = Helpers::max2(y, y_min);
		const ssize h = Helpers::min2(hline_height + y - yy, y_min + max_height - yy);
		result |= fillRect(color, x_min, yy, max_width, h, false);
		y += y_step;
	}
	return result;
};

template <class SourceType> bool DrawByMask(uint32* data, usize width, usize height,
		ssize x, ssize y, usize cx, usize cy, const SourceType& src, ssize xoffs, ssize yoffs, uint32 color) {

	if((! data) || (! src)) {
		return false;
	}
	ssize xs = x;
	ssize ys = y;
	ssize xe = x + cx;
	ssize ye = y + cy;
	if(xs < 0) {
		xoffs -= xs;
		xs = 0;
	}
	if(ys < 0) {
		yoffs -= ys;
		ys = 0;
	}
	if(xoffs < 0) { // expected offsets are in range [0 ; src.get...()). clipping range
		xoffs = 0;
	}
	if(yoffs < 0) { // same here
		yoffs = 0;
	}
	if((xs >= ssize(width)) || (ys >= ssize(height))) {
		return false;
	}
	xe = min2(xe, ssize(width));
	ye = min2(ye, ssize(height));
	if(xe - xs > ssize(src.width() - xoffs)) { // don't have enough source to draw, clipping ranges
		xe -= ((xe-xs) - (src.width()-xoffs));
	}
	if(ye - ys > ssize(src.height() - yoffs)) { // same here
		ye -= ((ye-ys) - (src.height()-yoffs));
	}
	ssize xsrc, ysrc;
	for(y = ys, ysrc = yoffs; y < ye; ++y, ++ysrc) {
		for(x = xs, xsrc = xoffs; x < xe; ++x, ++xsrc) {
			src.outputPixelUnchecked(data[y*width + x], xsrc, ysrc, color);
		}
	}
	return true;
};

bool Img32 :: drawByMask(ssize x, ssize y, usize cx, usize cy, const BitMask& src, ssize xoffs, ssize yoffs, uint32 color) {
	return DrawByMask(data, _width, _height, x, y, cx, cy, src, xoffs, yoffs, color);
};

bool Img32 :: drawByMask(ssize x, ssize y, usize cx, usize cy, const AlphaMask& src, ssize xoffs, ssize yoffs, uint32 color) {
	return DrawByMask(data, _width, _height, x, y, cx, cy, src, xoffs, yoffs, color);
};

// TGA METHODS ----------------------------------------------------------------------------------------------
uint32 Img32 :: readTGA(const char* file_name) {
	std::ifstream ifs(file_name, std::ios::binary);
	TargaImageCodec::TARGA_HEADER hdr;
	const bool result = TargaImageCodec::ReadTGA(ifs, &data, &_width, &_height, &hdr);
	if(result) {
		nchannels = (hdr.bpp == 24 ? 3 : 4);
	}
	return result;
};

uint32 Img32 :: writeTGA(const char* file_name, bool rle) const {
	std::ofstream ofs(file_name, std::ios::binary);
	return TargaImageCodec::WriteTGA32(ofs, rle, data, _width, _height);
};

// CIMG METHODS ----------------------------------------------------------------------------------------------
uint16 Img32 :: readCimgBatch(const char* file_name, uint16 count_from_this, std::vector<CompressedImageCodec::CimgInfo>* dst_info) {
	// if 'count_from_this' is > 0, returns count of images really read,
	// if 'count_from_this' == 0, returns count of images in file.
	std::ifstream src(file_name, std::ios::binary);
	if(! src.is_open()) {
		return 0;
	}
	uint16 count;
	if(! CompressedImageCodec::Codec::getDefault().readBatchHeader(src, &count)) {
		return 0;
	}
	if(! count_from_this) {
		return count;
	}
	if(count > count_from_this) { // not enough space
		count = count_from_this;
	}
	if(dst_info) {
		dst_info->clear();
		dst_info->resize(count);
	}
	for(uint16 i = 0; i < count; ++i) {
		CompressedImageCodec::CimgInfo* param = dst_info ? &(*dst_info)[i] : NULL;
		if(! this[i].readCimg(src, param)) {
			return i;
		}
	}
	return count;
};

usize Img32 :: readCimg(const char* file_name, CompressedImageCodec::CimgInfo* dst_info) {
	std::ifstream ifs(file_name, std::ios::binary);
	return readCimg(ifs, dst_info);
};

usize Img32 :: readCimg(std::istream& src_file, CompressedImageCodec::CimgInfo* dst_info) {
	CompressedImageCodec::CimgInfo info(false); // partial info for setting real channels count
	if(! dst_info) {
		dst_info = &info;
	}
	const usize result = CompressedImageCodec::Codec::getDefault().decompress(src_file, &data, &_width, &_height, dst_info);
	if(result) {
		nchannels = dst_info->nchannels;
	}
	return result;
};

uint16 Img32 :: writeCimgBatch(const char* file_name, uint16 count_from_this, const CompressedImageCodec::Codec& configured) const {
	// returns count of images really written
	std::ofstream dst(file_name, std::ios::binary);
	if(! dst.is_open()) {
		return 0;
	}
	if(! configured.writeBatchHeader(dst, count_from_this)) {
		return 0;
	}
	for(uint16 i = 0; i < count_from_this; ++i) {
		if(! this[i].writeCimg(dst, configured)) {
			return i;
		}
	}
	return count_from_this;
};

bool Img32 :: writeCimg(const char* file_name, const CompressedImageCodec::Codec& configured) const {
	std::ofstream ofs(file_name, std::ios::binary);
	return writeCimg(ofs, configured);
};

bool Img32 :: writeCimg(std::ostream& dst_file, const CompressedImageCodec::Codec& configured) const {
	return configured.compress(data, _width, _height, nchannels, dst_file);
};

// ----------------------------------------------------------------------------------------------

void Img32 :: swap(Img32& img) {
	std::swap(data, img.data);
	std::swap(_width, img._width);
	std::swap(_height, img._height);
	std::swap(nchannels, img.nchannels);
};

void Img32 :: clone(Img32& dst) const {
	if(! *this) {
		return;
	}
	dst.alloc(_width, _height);
	for(size_t i = 0; i < _width * _height; ++i) {
		dst.data[i] = data[i];
	}
	dst.nchannels = nchannels;
};

bool Img32 :: isEqual(const Img32& img, ubyte max_allowed_diff) const {
	if((_width != img._width) || (_height != img._height)){
		return false;
	}
	if(data && img.data) {
		if(! max_allowed_diff) {
			for(size_t i = 0; i < _width * _height; ++i) {
				if(data[i] != img.data[i]) {
					return false;
				}
			}
		} else {
			union {
				uint32 value;
				ubyte b[];
			} p1, p2;
			for(size_t i = 0; i < _width * _height; ++i) {
				p1.value = data[i];
				p2.value = img.data[i];
				for(int j = 0; j < sizeof(p1); ++j) {
					const ubyte diff = (p1.b[j] > p2.b[j]) ? p1.b[j] - p2.b[j] : p2.b[j] - p1.b[j];
					if(diff > max_allowed_diff) {
						return false;
					}
				}
			}
		}
		return true;
	}
	return false;
};

void Img32 :: rotate(int side, Img32& dst) const {
	if(! data) {
		return;
	}
	usize x, y, xx, yy;
	switch (side) {
	case 0: // 0*, just make copy
		clone(dst);
		break;
	case 1: // 90* by clock
		dst.alloc(_height, _width);
		for(y = 0, xx = _width - 1; y < _width; ++y, --xx) {
			for(x = 0, yy = 0; x < _height; ++x, ++yy) {
				dst.data[y * dst._width + x] = data[yy * _width + xx];
			}
		}
		break;
	case 2: // 180* by clock
		dst.alloc(_width, _height);
		for(y = 0, yy = _height - 1; y < _height; ++y, --yy) {
			for(x = 0, xx = _width - 1; x < _width; ++x, --xx) {
				dst.data[y * dst._width + x] = data[yy * _width + xx];
			}
		}
		break;
	case 3: // 270* by clock
		dst.alloc(_height, _width);
		for(y = 0, xx = 0; y < _width; ++y, ++xx) {
			for(x = 0, yy = _height - 1; x < _height; ++x, --yy) {
				dst.data[y * dst._width + x] = data[yy * _width + xx];
			}
		}
		break;
	};
};

void Img32 :: reset() {
	data = NULL;
	_width = 0;
	_height = 0;
	nchannels = 4;
};

}; // end Graphic32
