#pragma once
#include "Img32.h"
#include "Pixel/Merges.h" // PixelsMergeKeep

namespace Graphic32 {

class AlphaMask : private NonCopyable {
	ubyte* data;
	usize _width;
	usize _height;
public:
	AlphaMask();
	inline operator bool() const {
		return data != NULL;
	};
	inline usize width() const {
		return _width;
	};
	inline usize height() const {
		return _height;
	};
	inline void outputPixelUnchecked(uint32& dst, usize x, usize y, uint32 color) const {
		PixelsMergeKeep(dst, color, getUnchecked(x, y));
	};
	void loadChannel(const Img32& src, ubyte ichannel);
private:
	inline const ubyte& getUnchecked(usize x, usize y) const {
		return data[y*_width + x];
	};
	inline ubyte& getUnchecked(usize x, usize y) {
		return data[y*_width + x];
	};
	void alloc(usize new_width, usize new_height);
};

}; // end Graphic32
