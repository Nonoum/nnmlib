#include "../Algorithms/LexicographicalCompare.h"
#include "TextPrinter.h"
#include <assert.h>
#include <stdexcept>

namespace Graphic32 {

FontInfo :: FontInfo(ssize _width, ssize _height, ubyte _power)
		: width(_width), height(_height), power(_power) {
};

bool FontInfo :: operator <(const FontInfo& right) const {
	return Algorithms::LexicographicalCompare(width, right.width, height, right.height, power, right.power);
};

Img32 TextPrinter :: font_original;

static inline bool newlineMarkerBegins(char c) {
	return (c == '\n') || (c == '\r');
};

ssize TextPrinter :: getCHeight() const {
	return c_height;
};

bool TextPrinter :: initFonts(const char* cimg_fonts_file_name) {
	if(! font_original) { // not loaded yet
		if(! font_original.readCimgBatch(cimg_fonts_file_name, 1)) {
			throw std::runtime_error("TextPrinter::initFonts error: can't open resources file");
		}
	}
	return true; // success
};

bool TextPrinter :: create(const FontInfo& info) {
	if(! font_original) { // no resources
		// TextPrinter::initFonts() haven't been done
		throw std::runtime_error("TextPrinter::create error: no source for font.");
	}
	if((info.width <= 2) || (info.height <= 2)) {
		assert(false && "too small font size");
		return false; // just too small font
	}
	Img32 buf;
	ssize f_width = info.width * 16;
	ssize f_height = info.height * 16;
	if(! font_original.resizeAnisotropic(buf, f_width, f_height)) {
		if(! font_original.resizeBilinear(buf, f_width, f_height)) {
			return false; // probably memory allocation problems
		}
	}
	c_width = info.width;
	c_height = info.height;
	buf.channelPower(info.power, 0);
	font.loadChannel(buf, 0);
	return true;
};

bool TextPrinter :: printChar(Img32& dst, ssize x, ssize y, ssize width, ssize height,
		ssize xoffs, ssize yoffs, uint32 color, char c) const {

	ssize xtable = (c % 16) * c_width; // x position in table of font
	ssize ytable = (c / 16) * c_height; // y -:-
	return dst.drawByMask(x, y, width, height, font, xtable + xoffs, ytable + yoffs, color);
};

const char* TextPrinter :: passNewlineMarker(const char* str) {
	assert(str != NULL);
	char c = *str;
	if(! newlineMarkerBegins(c)) { // unexpected
		return str;
	}
	++str;
	if((*str != '\n') && (*str != '\r')) { // not newline marker
		return str;
	} // else = may be continuation of newline marker
	if(*str != c) { // continuation
		return str+1;
	} // else - start of new line
	return str;
};

const char* TextPrinter :: reachLineEnd(const char* str) {
	if(! str) {
		assert(false && "str can't be NULL here");
		return NULL;
	}
	for(; *str; ++str) {
		if(newlineMarkerBegins(*str)) {
			return str;
		}
	}
	return str;
};

size_t TextPrinter :: processString(Img32* dst, ssize x, ssize y, uint32 color, const char* str,
		size_t* nl, size_t* nc, const TextPrinter* pr, const Geometry::Rect* zone, std::vector<Geometry::Rect>* char_areas) {

	if(! str) {
		assert(false && "str can't be NULL here");
		return false;
	}
	size_t printed = 0; // return result
	const bool to_print = pr && dst; // checking for NULL pointers
	size_t current_line_length = 0, n_lines = 1, n_columns = 0;
	ssize x_start = x;
	ssize c_width = 0, c_height = 0;
	if(pr) {
		c_width = pr->c_width;
		c_height = pr->c_height;
	} // else - don't care
	ssize x_left, x_right, y_bottom, y_top;
	if(zone) { // have drawing restrictions
		x_left = zone->x();
		x_right = x_left + zone->width();
		y_bottom = zone->y();
		y_top = y_bottom + zone->height();
	}
	if(char_areas) {
		char_areas->clear(); // cleaning areas array
	}
	ssize last_output_height = c_height;
	for( ; *str ; ) {
		if(newlineMarkerBegins(*str)) {
			if(current_line_length > n_columns) {
				n_columns = current_line_length;
			}
			const char* previous = str;
			str = passNewlineMarker(str); // to next character
			if(char_areas) {
				for( ; previous < str; ++previous ) { // can be several chars for moving to newline
					char_areas->push_back(Geometry::Rect(x, y, 0, last_output_height)); // inserting empty rect
				}
			}
			y -= c_height;
			x = x_start;
			++n_lines; // lines count ++
			current_line_length = 0;
		} else { // printable character
			if(to_print || char_areas) {
				if(! zone) { // default drawing (no restriction)
					if(to_print) {
						if(pr->printChar(*dst, x, y, c_width, c_height, 0, 0, color, *str)) {
							++printed;
						}
					}
					if(char_areas) { // add area of character to output array
						char_areas->push_back(Geometry::Rect(x, y, c_width, c_height)); // always full sized area
					}
					last_output_height = c_height;
				} else { // with restrictions
					if((x + c_width > x_left) // TODO: this hard checks can be optimized for cycle
						&&(x < x_right)
						&&(y < y_top)
						&&(y + c_height > y_bottom)) { // character is in allowed zone

						ssize x_draw = x;
						ssize y_draw = y;
						ssize w_draw = c_width;
						ssize h_draw = c_height;
						ssize x_offs = 0;
						ssize y_offs = 0;
						if(x_draw < x_left) { // checking left bound
							x_offs = x_left - x_draw;
							w_draw -= (x_left - x_draw);
							x_draw = x_left;
						}
						if(x_draw + w_draw > x_right) { // checking right bound
							w_draw -= (x_draw + w_draw - x_right);
						}
						if(y_draw + h_draw > y_top) { // checking top bound
							h_draw -= (y_draw + h_draw - y_top);
						}
						if(y_draw < y_bottom) { // checking bottom bound
							y_offs = y_bottom - y_draw;
							h_draw -= (y_bottom - y_draw);
							y_draw = y_bottom;
						}
						if(to_print) {
							if(pr->printChar(*dst, x_draw, y_draw, w_draw, h_draw, x_offs, y_offs, color, *str)) {
								++printed;
							}
						}
						if(char_areas) {
							char_areas->push_back(Geometry::Rect(x_draw, y_draw, w_draw, h_draw)); // possibly, cutted area
						}
						last_output_height = h_draw;
					} else { // area is out of allowed zone, add empty
						if(char_areas) {
							char_areas->push_back(Geometry::Rect(x, y, 0, last_output_height)); // inserting empty rect
						}
					}
				}
				x += c_width;
			}
			++current_line_length; // columns count ++
			++str; // to next character
		}
	}
	if(current_line_length > n_columns) { // check last line
		n_columns = current_line_length;
	}
	if(nl) { // called to calc number of lines
		*nl = n_lines; // output number of lines
	}
	if(nc) { // called to calc number of columns
		*nc = n_columns; // output number of columns
	}
	return printed;
};

size_t TextPrinter :: print(Img32& dst, ssize x, ssize y, uint32 color, const char* str, const Geometry::Rect* zone) const {
	return processString(&dst, x, y, color, str, NULL, NULL, this, zone);
};

size_t TextPrinter :: nLines(const char* str) {
	size_t nl = 0;
	processString(NULL, 0, 0, 0, str, &nl, NULL, NULL);
	return nl;
};

size_t TextPrinter :: nColumns(const char* str) {
	size_t nc = 0;
	processString(NULL, 0, 0, 0, str, NULL, &nc, NULL);
	return nc;
};

Geometry::Size TextPrinter :: getFitSize(const char* str) const {
	size_t nlines = 0, ncolumns = 0;
	processString(NULL, 0, 0, 0, str, &nlines, &ncolumns, NULL);
	ssize full_w = ncolumns * c_width;
	ssize full_h = nlines * c_height;
	return Geometry::Size(full_w, full_h);
};

size_t TextPrinter :: getCharAreas(const char* str, std::vector<Geometry::Rect>* char_areas,
		const Geometry::Rect* zone) const {

	if((! char_areas) || (! str)){
		assert(false && "str can't be NULL here");
		return 0;
	}
	processString(NULL, 0, 0, 0, str, NULL, NULL, this, zone, char_areas);
	return char_areas->size();
};

}; // end Graphic32
