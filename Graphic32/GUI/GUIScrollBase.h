#pragma once
#include "GUIObject.h"

namespace Graphic32 {

namespace GUI {

/*
	Protected class with all scroll bar logic implementation.
*/
class GUIScrollBase : public GUIObject {
	GUIObject* const client;

	const Scrolls s_type;
	const ssize divider; // movement distance divider (for mouse right)
	const bool need_self_background;
	GUIState self_background_colors;

	// dynamic parameters
	bool m_left_down;
	bool m_right_down;
	ssize last_pos; // last position for mouse_right controls
public:
	void setBackgroundColor(uint32 color, State s);
protected:
	GUIScrollBase(const Geometry::Rect& rect, DrawType _type, GUIObject* _client, Scrolls scroll_type, ssize _divider, bool _need_self_background = false);

	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
	bool controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) override;

	uint32 sliderColor() const;
	static usize calcScrollMarkerSize(usize maximum);
private:
	ssize moveAndUpdate(ssize new_pos);
	bool autoScroll(Geometry::Point local_pos);
	bool manualScroll(bool key_down, Geometry::Point local_pos);
};

}; // end GUI

}; // end Graphic32
