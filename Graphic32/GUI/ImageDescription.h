#pragma once
#include "gui_utils.h"
#include <string.h> // strcmp

namespace Graphic32 {

namespace GUI {

typedef std::shared_ptr<Img32> SharedImage;

/*
	Comparable description for image (by specified name).
*/
class ImageDescription {
	const char* name;
	const bool can_clean; // to make some instances unable to be automatically cleaned
public:
	ImageDescription(const char* static_string_name, bool cleanable = false)
			: can_clean(cleanable), name(static_string_name) {

		assert(name != NULL);
		if(! name) { // unexpected, but won't lead to crash
			name = "null";
		}
	};
	bool operator <(const ImageDescription& right) const {
		return strcmp(name, right.name) < 0;
	};
	static bool cleanable(const ImageDescription& desc, const SharedImage& img) {
		return desc.can_clean && img.unique();
	};
};

}; // end GUI

}; // end Graphic32
