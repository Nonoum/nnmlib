#include "GUITextZone.h"

namespace Graphic32 {

namespace GUI {

static const Geometry::Size MIN_OBJECT_SIZE(1, 1);

GUITextZone :: GUITextZone(const Geometry::Rect& rect, const FontInfo& info,
	DrawType _type, bool _auto_sized, size_t _id, const char* cached_image_name, Geometry::Alignment _x_align, Geometry::Alignment _y_align)
		: GUIObject(rect, _type, _id, cached_image_name), auto_sized(_auto_sized), x_align(_x_align), y_align(_y_align), fit_controller(NULL) {

	if((info.width <= 0) || (info.height <= 0)) {
		if((info.width < 0) || (info.height < 0)) {
			assert(false && "font suppose to have positive size");
		}
		return;
	}
	if(! auto_sized) {
		fit_controller = new GUIObject(Geometry::Rect(rect.getSize()), DRAW_NONE);
		Surface::push(fit_controller);
		fit_controller->activate(false); // this should be inactive to not grab internal input focus
	}
	align_on_settext = true;

	textColors.setDefaultColors(SET_TEXT); // setting text set of default colors for printing

	const PrinterDescription printer_desc(info);
	if(Printers().has(printer_desc)) { // matching printer is already created in cache
		setPrinter( Printers().find(printer_desc) );
	} else { // not found, creating new printer
		printer = SharedPrinter(new TextPrinter());
		printer.get()->create(info);
		Printers().set(printer_desc, printer);
	}
};

bool GUITextZone :: setText(const char* src) {
	assert(src != NULL);
	if(! src) {
		return false;
	}
	bool result = prepareTextPlacement(src);
	beforeTextSet(src);
	text.assign(src);
	return result;
};

const char* GUITextZone :: getText() const {
	return text.c_str();
};

void GUITextZone :: setTextColor(uint32 color, State s) {
	textColors.setColor(color, s);
	needRedraw();
};

bool GUITextZone :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	bool result = GUIObject::renderArea(dst, displacement, local_area);
	return result | printWithParams(dst, displacement, local_area);
};

bool GUITextZone :: controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) {
	const uint32 prev_color = textColors.getColor(getState()) & 0x00FFFFFF;
	bool result = GUIObject::controlHandler(key, key_down, local_pos, in_valid_zone);
	const uint32 cur_color = textColors.getColor(getState()) & 0x00FFFFFF;
	if(text.length() != 0) { // have text
		if(prev_color != cur_color) { // text color is changed
			result |= true;
			needRedraw();
		}
	}
	return result;
};

Geometry::Size GUITextZone :: getFitSize() const {
	return fit_size;
};

bool GUITextZone :: printWithParams(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	if(! printer.get()) {
		// printer could be NULL and it's ok, since this class is inherited by GUIButton that
		// may not have text, and have image instead.
		return false;
	}
	const Geometry::Point pt = displacement + printing_offset;
	const Geometry::Rect bounds = local_area + displacement;
	return printer.get()->print(dst, pt.x(), pt.y(),
		textColors.getColor(getState()), text.c_str(), &bounds) > 0; // true if any characters were printed
};

Geometry::Point GUITextZone :: getPrintingOffset() const {
	return printing_offset;
};

const SharedPrinter& GUITextZone :: getPrinter() const {
	return printer;
};

void GUITextZone :: beforeTextSet(const char* text_to_set) {
	// empty
};

void GUITextZone :: setPrinter(const SharedPrinter& _printer) {
	if(printer.get() != _printer.get()) { // if printer is changed then redraw is needed
		needRedraw();
	}
	printer = _printer;
	if(printer.get()) {
		std::string buf = text; // using the temporary string to not break text string refreshing
		setText(buf.c_str()); // setting text to calculate parameters for new printer
	} else {
		fit_size = MIN_OBJECT_SIZE;
		if(auto_sized) {
			setSize(fit_size);
		} else {
			surface(fit_controller)->setSize(fit_size);
		}
	}
};

void GUITextZone :: alignOnSetText(bool auto_align) {
	align_on_settext = auto_align;
};

void GUITextZone :: processAlignments(Geometry::Size size, Geometry::Size fit_size) {
	const ssize x_scroll_limit = getScrollLimit(SCROLL_X);
	const ssize y_scroll_limit = getScrollLimit(SCROLL_Y);
	ssize x_print = 0; // start position
	if(size.width() > fit_size.width()) {
		switch (x_align) {
			case Geometry::ALIGN_BEGIN: break;
			case Geometry::ALIGN_CENTER: x_print += (size.width() - fit_size.width()) / 2; break;
			case Geometry::ALIGN_END: x_print += size.width() - fit_size.width(); break;
		};
	}
	ssize y_print = size.height() - printer.get()->getCHeight(); // start position
	if(size.height() > fit_size.height()) {
		switch (y_align) {
			case Geometry::ALIGN_BEGIN: break;
			case Geometry::ALIGN_CENTER: y_print -= (size.height() - fit_size.height()) / 2; break;
			case Geometry::ALIGN_END: y_print -= size.height() - fit_size.height(); break;
		};
	}
	if(align_on_settext) {
		ssize x_scroll = 0, y_scroll = 0;
		if(x_align != Geometry::ALIGN_BEGIN) {
			x_scroll = (x_align == Geometry::ALIGN_END) ? x_scroll_limit : x_scroll_limit / 2;
		}
		if(y_align != Geometry::ALIGN_END) {
			y_scroll = (y_align == Geometry::ALIGN_BEGIN) ? y_scroll_limit : y_scroll_limit / 2;
		}
		setScrollValue(SCROLL_X, x_scroll);
		setScrollValue(SCROLL_Y, y_scroll);
	}
	printing_offset.setPoint(x_print, y_print);
};

bool GUITextZone :: prepareTextPlacement(const char* src) {
	assert(printer.get() != NULL && src != NULL);
	if((! printer.get()) || (! src)) {
		return false;
	}
	needRedraw();
	Geometry::Size size = getPlacement().getSize();
	fit_size = printer.get()->getFitSize(src).getMaximums(MIN_OBJECT_SIZE);

	if(auto_sized) {
		const Geometry::Point pos = getPlacement().getPoint();
		const Geometry::Point aligned_pos(pos.x(), pos.y() + size.height() - fit_size.height()); // aligned to top
		setPlacement(Geometry::Rect(aligned_pos, fit_size));
		size = fit_size;
	} else {
		const Geometry::Point pos(0, Helpers::min2<ssize>(size.height() - fit_size.height(), 0));
		surface(fit_controller)->setPlacement(Geometry::Rect(pos, fit_size));
	}
	processAlignments(size, fit_size);
	return true;
};

}; // end GUI

}; // end Graphic32
