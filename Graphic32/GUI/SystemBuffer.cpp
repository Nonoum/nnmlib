#include "SystemBuffer.h"

namespace Graphic32 {

namespace GUI {

static std::string& buffer() {
	static std::string buff;
	return buff;
};

static SystemBuffer::Callback*& callback() {
	static SystemBuffer::Callback* cback = NULL;
	return cback;
};

void SystemBuffer :: setCallback(Callback* new_callback) {
	if(callback() != NULL) {
		delete callback();
	}
	callback() = new_callback;
};

void SystemBuffer :: put(const char* str) {
	buffer().assign(str);
	if(callback()) {
		callback()->copy(buffer(), false);
	}
};

std::string SystemBuffer :: get() {
	if(callback()) {
		callback()->copy(buffer(), true);
	}
	return buffer();
};

}; // end GUI

}; // end Graphic32
