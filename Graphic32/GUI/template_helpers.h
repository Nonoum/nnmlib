#pragma once
#include "GUIState.h"

namespace Graphic32 {

namespace GUI {

// Function for setting text colors for all states of custom GUI item;
// takes pointer to item and an array of colors with indexes appropriating to item states.
template <class Item> void SetTextColors(Item* item, const uint32 colors[GUI::GUIState::STATE_COUNT]) {
	for(int i = GUI::GUIState::STATE_ITERATOR; i < GUI::GUIState::STATE_COUNT; ++i) {
		item->setTextColor(colors[i], GUI::GUIState::State(i));
	}
};

// Function for setting colors for all states of custom GUI item;
// takes pointer to item and an array of colors with indexes appropriating to item states.
template <class Item> void SetColors(Item* item, const uint32 colors[GUI::GUIState::STATE_COUNT]) {
	for(int i = GUI::GUIState::STATE_ITERATOR; i < GUI::GUIState::STATE_COUNT; ++i) {
		item->setColor(colors[i], GUI::GUIState::State(i));
	}
};

}; // end GUI

}; // end Graphic32
