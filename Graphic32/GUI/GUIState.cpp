#include "GUIState.h"

namespace Graphic32 {

namespace GUI {

uint32 GUIState :: default_colors_sets[SET_SETS_COUNT][STATE_COUNT] = {
	{0xFF80A0A0, 0xFF70D8B8, 0xA080B0B0}, // background
	{0xFF000000, 0xFF202020, 0xFF101010}, // text
	{0xFF2000FF, 0xFFA000FF, 0xFF6000FF}, // scroll
	{0xFFD0C0B0, 0xFFD0C0B0, 0xFFD0C0B0} // window
	};

GUIState :: GUIState()
		: default_set(default_colors_sets[SET_BACKGROUND]), state(STATE_DEFAULT) {

	Algorithms::fill(personal_color, false);
};

void GUIState :: needRedraw() { // empty
};

void GUIState :: setColor(uint32 color, State s) {
	personal_color[s] = true;
	colors[s] = color;
	if(s == state) {
		needRedraw();
	}
};

uint32 GUIState :: getColor(State s) const {
	if(personal_color[s]) {
		return colors[s];
	}
	return default_set[s];
};

bool GUIState :: isUserColor(State s) const {
	return personal_color[s];
};

void GUIState :: setState(State s) {
	state = s;
	needRedraw();
};

GUIState::State GUIState :: getState() const {
	return state;
};

void GUIState :: setDefaultColors(ColorsSet colors_set) {
	default_set = default_colors_sets[colors_set];
	needRedraw();
};

void GUIState :: setDefault(ColorsSet colors_set, State state, uint32 color) {
	default_colors_sets[colors_set][state] = color;
};

}; // end GUI

}; // end Graphic32
