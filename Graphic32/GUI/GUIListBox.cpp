#include "GUIListBox.h"
#include <algorithm>

namespace Graphic32 {

namespace GUI {

GUIListBox :: GUIListBox(const Geometry::Rect& rect, const FontInfo& _font, DrawType _type, size_t _id,
	Geometry::Alignment _x_align)
		: GUIObject(rect, _type, _id, NULL), users_background(_type != DRAW_NONE),
		x_align(_x_align), font(_font), x_scroll(NULL), y_scroll(NULL), container(NULL), max_width(0) {

	setContur(true, 0x00000000);
	container = surface(new GUIObject(Geometry::Rect(rect.getSize()), DRAW_NONE));
	Surface::push(container);
};

void GUIListBox :: appendFields(size_t count) {
	size_t idx = fields.size();
	const DrawType d_type = users_background ? DRAW_NONE : DRAW_SOLID_COLOR;

	ssize y = getPlacement().height() - (idx + 1) * itemSize();
	for(size_t i = 0; i < count; ++i, ++idx) {
		const Geometry::Rect rect(0, y, 1, itemSize());
		y -= itemSize();

		GUITextZone* field = new GUITextZone(rect, font, d_type, false, idx, NULL, x_align);
		container->push(surface(field));
		field->setCallback( new FieldCallback(this) );
		fields.push_back(field);
		field->activate(false);
	}
	refreshFieldsParameters();
};

void GUIListBox :: updateFieldWidths() {
	const Geometry::Size size(Helpers::max2(max_width, container->getPlacement().width()), itemSize());
	for(size_t i = 0; i < fields.size(); ++i) {
		surface(fields[i])->setSize(size);
	}
};

void GUIListBox :: updateScrolls() {
	// setting original size and placement (full area of *this)
	container->setPlacement(Geometry::Rect(getPlacement().getSize()));
	updateFieldWidths();

	bool x_needed = false, y_needed = false;
	if( container->getScrollLimit(SCROLL_X) ) {
		x_needed = true;
		container->setSize(Geometry::Size(getPlacement().width(), getPlacement().height() - itemSize()));
	}
	if( container->getPlacement().height() < (strings.size() * itemSize()) ) {
		y_needed = true;
		container->setSize(Geometry::Size(getPlacement().width() - itemSize(), container->getPlacement().height()));
		updateFieldWidths();
		if((! x_needed) && (container->getScrollLimit(SCROLL_X))) {
			x_needed = true;
			container->setSize(Geometry::Size(container->getPlacement().width(), getPlacement().height() - itemSize()));
		}
	}
	container->setPlacementPos(Geometry::Point(0, x_needed ? itemSize() : 0));

	const Geometry::Rect x_scroll_rect(0, 0, getPlacement().width(), itemSize());
	const Geometry::Rect y_scroll_rect(getPlacement().width() - itemSize(), x_needed ? itemSize() : 0,
										itemSize(), getPlacement().height() - (x_needed ? itemSize() : 0));
	// X
	if(x_needed ^ (x_scroll != NULL)) {
		if(x_needed) {
			x_scroll = new GUIXScroll(x_scroll_rect, static_cast<GUIObject*>(container), 1, ! users_background);
			Surface::push(surface(x_scroll));
			x_scroll->setContur(true, 0);
		} else {
			Surface::exclude(surface(x_scroll));
			delete x_scroll;
			x_scroll = NULL;
		}
	}
	if(x_needed) {
		surface(x_scroll)->setSize(x_scroll_rect.getSize());
	}
	// Y
	if(y_needed ^ (y_scroll != NULL)) {
		if(y_needed) {
			y_scroll = new GUIYScroll(y_scroll_rect, static_cast<GUIObject*>(container), 1, ! users_background);
			Surface::push(surface(y_scroll));
			y_scroll->setContur(true, 0);
		} else {
			Surface::exclude(surface(y_scroll));
			delete y_scroll;
			y_scroll = NULL;
		}
	}
	if(y_needed) {
		surface(y_scroll)->setPlacement(y_scroll_rect);
	} else {
		container->setScrollValue(SCROLL_Y, container->getScrollLimit(SCROLL_Y));
	}
	refreshScrollParameters();
};

// content interface -------------------------------------------------------------------------------
void GUIListBox :: pushString(const char* str) {
	strings.push_back(str);
	const size_t idx = strings.size() - 1;
	const size_t n_needed = Helpers::max2<size_t>(minItems(), strings.size());
	if(n_needed > fields.size()) {
		appendFields(n_needed - fields.size());
	}

	GUITextZone* const fld = fields[idx];
	fld->activate(true);
	fld->setText(strings[idx].c_str());

	surface(fld)->setSize(Geometry::Size(1, itemSize()));
	const ssize text_width = 1 + surface(fld)->getScrollLimit(SCROLL_X);
	widths.push_back(text_width);
	max_width = Helpers::max2(text_width, max_width);

	updateScrolls();
	if(y_scroll) {
		container->setScrollValue(SCROLL_Y, 0); // an item added, scroll to it
	}
};

bool GUIListBox :: killString(size_t index) {
	if(index >= strings.size()) { // incorrect string index
		return false; // out
	}
	strings.erase(strings.begin() + index);
	widths.erase(widths.begin() + index);

	GUITextZone* const fld = fields[index];
	const Geometry::Point last_pos = fields[fields.size() - 1]->getPlacement().getPoint();
	for(size_t i = fields.size() - 1; i > index; --i) {
		surface(fields[i])->setPlacementPos(fields[i-1]->getPlacement().getPoint());
		fields[i]->setID(i-1);
	}
	// removing from list
	fields.erase(fields.begin() + index);
	if(fields.size() > minItems()) { // can remove
		container->exclude(surface(fld));
		delete fld;
	} else { // can't remove, clear and move down
		fields.push_back(fld);
		surface(fld)->setPlacementPos(last_pos);
		fld->setText("");
		fld->setID(fields.size() - 1);
		fld->activate(false);
	}
	max_width = widths.size() ? *std::max_element(widths.begin(), widths.end()) : 0;
	updateScrolls();
	return true;
};

size_t GUIListBox :: count() const {
	return strings.size();
};

const char* GUIListBox :: getString(size_t index) const {
	if(index < strings.size()) {
		return strings[index].c_str();
	}
	return ""; // returning empty string if index is incorrect
};

// other interface -------------------------------------------------------------------------------
void GUIListBox :: setFieldColor(uint32 color, State s) {
	fieldColors.setColor(color, s);
	refreshFieldsParameters();
	refreshScrollParameters();
};

void GUIListBox :: setTextColor(uint32 color, State s) {
	textColors.setColor(color, s);
	refreshFieldsParameters();
};

void GUIListBox :: setScrollColor(uint32 color, State s) {
	scrollColors.setColor(color, s);
	refreshScrollParameters();
};

// content support -------------------------------------------------------------------------------

void GUIListBox :: refreshFieldsParameters() {
	needRedraw();
	for(int s = STATE_ITERATOR; s != STATE_COUNT; ++s) {
		const State st = State(s);
		for(size_t i = 0; i < fields.size(); ++i) {
			if(textColors.isUserColor(st)) {
				fields[i]->setTextColor(textColors.getColor(st), st);
			}
			if(fieldColors.isUserColor(st)) {
				fields[i]->setColor(fieldColors.getColor(st), st);
			}
		}
	}
};

// scrolling support -------------------------------------------------------------------------------

void GUIListBox :: refreshScrollParameters() {
	if((! x_scroll) && (! y_scroll)) { // no scrolls created
		return;
	}
	needRedraw();
	GUIScrollBase* const scrls[2] = {x_scroll, y_scroll};
	for(int s = STATE_ITERATOR; s != STATE_COUNT; ++s) { // checking all states
		const State st = State(s);
		for(int i = 0; i < 2; ++i) {
			if(scrls[i]) {
				if(scrollColors.isUserColor(st)) {
					scrls[i]->setColor(scrollColors.getColor(st), st);
				}
				if(fieldColors.isUserColor(st)) {
					scrls[i]->setBackgroundColor(fieldColors.getColor(st), st);
				}
			}
		}
	}
};

ssize GUIListBox :: itemSize() const {
	return font.height;
};

ssize GUIListBox :: minItems() const {
	return (getPlacement().height() + itemSize() - 1) / itemSize();
};

// internal field callback -------------------------------------------------------------------------------
GUIListBox::FieldCallback :: FieldCallback(GUIListBox* _list)
		: list(_list) {
};

bool GUIListBox::FieldCallback :: action(CallbackParams& params) {
	if((params.action == KEY_MOUSE_LEFT) || (params.action == KEY_MOUSE_DOUBLE)) {
		if(list->haveCallback() && list->isActive()) {
			CallbackParams par(list, params.action, params.key_down);
			par.hit_area = params.hit_area;
			par.index = params.caller->getID(); // string index in array
			par.length = list->strings.size(); // count of strings in array

			Geometry::Point offset(0, 0);
			list->container->scrollLocalPoint(offset);
			par.local_pos = params.local_pos
							+ params.caller->getPlacement().getPoint()
							+ list->container->getPlacement().getPoint()
							- offset;
			return list->sendCallback(par);
		}
	}
	return false;
};

}; // end GUI

}; // end Graphic32
