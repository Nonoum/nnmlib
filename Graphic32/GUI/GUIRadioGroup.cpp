#include "GUIRadioGroup.h"
#include "../../Helpers/ClearNewPointersCollection.h"

namespace Graphic32 {

namespace GUI {

GUIRadioGroup :: GUIRadioGroup(const Geometry::Rect& rect, const FontInfo& info, size_t radios_count, DrawType t, size_t _id,
		Geometry::Alignment x_text_align, Geometry::Alignment y_text_align)
		: GUIObject(getCorrectedRect(rect, radios_count), t, _id), items_count(radios_count) {

	i_active = 0;
	i_pretender = 0;
	for(size_t i = 0; i < radios_count; ++i) {
		GUITextZone* text = new GUITextZone(getTextRect(rect, radios_count, i), info, DRAW_NONE, false, 0, NULL, x_text_align, y_text_align);
		Surface::push(surface(text));
		texts.push_back( text );

		GUIButton* button = new GUIButton(getButtonRect(rect, radios_count, i), i == i_active ? DRAW_RADIO_ON_MASK : DRAW_RADIO_OFF_MASK, i);
		Surface::push(surface(button));
		buttons.push_back( button );
		button->setCallback( new ButtonCallback(*this) );
	}
	setContur(true, 0);
};

void GUIRadioGroup :: setActiveItem(size_t i_active_radio) {
	i_active_radio = items_count - i_active_radio - 1;
	setActiveItem(i_active_radio, true);
	setActiveItem(i_active_radio, false);
};

size_t GUIRadioGroup :: getActiveItem() const {
	return items_count - i_active - 1;
};

size_t GUIRadioGroup :: getItemsCount() const {
	return items_count;
};

bool GUIRadioGroup :: setText(size_t i_item, const char* src) {
	assert( i_item < items_count );
	if(i_item < items_count) {
		return texts[items_count - i_item - 1]->setText(src);
	}
	return false;
};

void GUIRadioGroup :: setTextColor(uint32 color, State s) {
	for(size_t i = 0; i < items_count; ++i) {
		texts[i]->setTextColor(color, s);
	}
};

void GUIRadioGroup :: setColor(uint32 color, State s) {
	GUIObject::setColor(color, s);
	for(size_t i = 0; i < items_count; ++i) {
		buttons[i]->setColor(color, s);
	}
};

bool GUIRadioGroup :: setActiveItem(size_t i_active_radio, bool begin_pretend) {
	assert( i_active_radio < items_count );
	if(i_active_radio >= items_count) {
		return false; // error, wrong index
	}
	if(begin_pretend) {
		i_pretender = i_active_radio;
		return false;
	} else if(i_pretender != i_active_radio) {
		return false;
	}
	if(i_active_radio != i_active) {
		buttons[i_active]->setMask( getMaskFor(DRAW_RADIO_OFF_MASK, buttons[i_active]->getPlacement().getSize()) );
		i_active = i_active_radio;
		buttons[i_active]->setMask( getMaskFor(DRAW_RADIO_ON_MASK, buttons[i_active]->getPlacement().getSize()) );
	}
	return true;
};

static const ssize double_border_size = 2; // left+right (or top+bottom) border size

Geometry::Rect GUIRadioGroup :: getCorrectedRect(const Geometry::Rect& total_rect, size_t items_count) {
	assert( items_count != 0 );
	const ssize internal_width = total_rect.width() - double_border_size;
	const ssize item_height = Helpers::min2<ssize>(internal_width, (total_rect.height() - double_border_size) / items_count);

	return Geometry::Rect(total_rect.getPoint(),
						Geometry::Size(total_rect.width(), (item_height * items_count) + double_border_size));
};

Geometry::Rect GUIRadioGroup :: getButtonRect(const Geometry::Rect& total_rect, size_t items_count, size_t i_item) {
	const Geometry::Rect corrected = getCorrectedRect(total_rect, items_count);
	const ssize item_size = (corrected.height() - double_border_size) / items_count;

	return Geometry::Rect(Geometry::Point(1, 1 + item_size * i_item),
						Geometry::Size(item_size, item_size));
};

Geometry::Rect GUIRadioGroup :: getTextRect(const Geometry::Rect& total_rect, size_t items_count, size_t i_item) {
	const Geometry::Rect corrected = getCorrectedRect(total_rect, items_count);
	const ssize item_size = (corrected.height() - double_border_size) / items_count;

	return Geometry::Rect(Geometry::Point(1 + item_size, 1 + item_size * i_item),
						Geometry::Size(corrected.width() - double_border_size - item_size, item_size));
};

// internal button callback -------------------------------------------------------------------------------

GUIRadioGroup::ButtonCallback :: ButtonCallback(GUIRadioGroup& _owner)
		: owner(_owner) {
};

bool GUIRadioGroup::ButtonCallback :: action(CallbackParams& params) {
	if(params.action == KEY_MOUSE_LEFT && params.hit_area) {
		const bool item_changed = owner.setActiveItem(params.caller->getID(), params.key_down);
		if(item_changed && owner.haveCallback() && owner.isActive()) {
			CallbackParams par(&owner, params.action, params.key_down);
			par.length = owner.getItemsCount();
			par.index = owner.getActiveItem();
			par.hit_area = true;
			return owner.sendCallback(par);
		}
	}
	return false;
};

}; // end GUI

}; // end Graphic32
