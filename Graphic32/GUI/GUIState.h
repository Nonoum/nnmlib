#pragma once
#include "gui_utils.h"

namespace Graphic32 {

namespace GUI {

class GUIState {
public:
	enum State {
		STATE_ITERATOR = 0,
		STATE_DEFAULT = STATE_ITERATOR, // not selected object
		STATE_SELECTED, // selected object
		STATE_HOVER, // mouse over object
		STATE_COUNT // count of states
	};
	enum ColorsSet {
		SET_BACKGROUND, // set of default colors for background
		SET_TEXT, // set of default colors for text
		SET_SCROLL, // set of default colors for scroll bars
		SET_WINDOW, // set of default colors for window background
		SET_SETS_COUNT // count of color sets
	};
private:
	static uint32 default_colors_sets[SET_SETS_COUNT][STATE_COUNT]; // default colors sets per every state
	uint32 colors[STATE_COUNT]; // personal colors of corresponding state
	bool personal_color[STATE_COUNT]; // flag of personal colors for corresponding states
	State state; // current object state
	const uint32* default_set; // pointer to set of default colors
protected:
	virtual void needRedraw();
public:
	GUIState();
	void setColor(uint32 color, State s);
	uint32 getColor(State s) const;
	bool isUserColor(State s) const;
	void setState(State s);
	State getState() const;
	void setDefaultColors(ColorsSet colors_set);
	static void setDefault(ColorsSet colors_set, State state, uint32 color);
};

}; // end GUI

}; // end Graphic32
