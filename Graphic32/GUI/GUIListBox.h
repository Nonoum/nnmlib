#pragma once
#include "GUITextZone.h"
#include "GUIXScroll.h"
#include "GUIYScroll.h"

namespace Graphic32 {

namespace GUI {

/*
	List Box implementation.
	On KEY_MOUSE_LEFT and KEY_MOUSE_DOUBLE events callback is called twice:
	1) by background object:
		params.length == 0;
		params.index == 0; (default values).
	2) by field object which got cursor:
		params.length == count of strings in listbox;
		params.index == index of string that appropriated field contains;
*/
class GUIListBox : public GUIObject {
	const FontInfo font;
	Geometry::Alignment x_align;
	const bool users_background;
	//
	typedef std::vector<std::string> CollectionType;
	typedef std::vector<GUITextZone*> FieldsType;
	typedef std::vector<ssize> WidthsType;
	CollectionType strings; // collection of string data
	FieldsType fields; // collection of fields
	WidthsType widths; // cached widths of fields
	ssize max_width;

	GUIXScroll* x_scroll;
	GUIYScroll* y_scroll;
	Surface* container;
	// configurations
	GUIState fieldColors; // colors for field background
	GUIState textColors; // storage for user text colors
	GUIState scrollColors; // storage for user scroll masks colors
public:
	GUIListBox(const Geometry::Rect& rect, const FontInfo& _font, DrawType _type = DRAW_NONE, size_t _id = 0,
		Geometry::Alignment _x_align = Geometry::ALIGN_BEGIN);
	// content interface -------------------------------------------------------------------------------
	void pushString(const char* str); // adds (str) as last string.
	bool killString(size_t index); // removes string with (index) if (index) is correct.
	size_t count() const; // returns count of strings in list.
	const char* getString(size_t index) const; // returns string with (index) if (index) is correct and empty string if not.
	// other interface -------------------------------------------------------------------------------
	void setFieldColor(uint32 color, State s); // sets color for fields and scrolls background.
	void setTextColor(uint32 color, State s); // sets text color for corresponding state.
	void setScrollColor(uint32 color, State s); // sets scroll color for corresponding state.
private:
	void appendFields(size_t count);
	void updateFieldWidths();
	void updateScrolls();

	void refreshFieldsParameters();
	void refreshScrollParameters();
	ssize itemSize() const; // returns height for X scroll and field (and width for Y scroll).
	ssize minItems() const;
	// internal field callback -------------------------------------------------------------------------------
	class FieldCallback : public CallbackInterface {
		GUIListBox* list;
	public:
		FieldCallback(GUIListBox* _list);
		bool action(CallbackParams& params) override;
	};
};

}; // end GUI

}; // end Graphic32
