#pragma once
#include "GUITextZone.h"

namespace Graphic32 {

namespace GUI {

/*
	Class for displaying, editing and selecting text in object's area.
*/
class GUIEdit : public GUITextZone {
	typedef Geometry::Range<size_t> Range;

	typedef std::vector<Geometry::Rect> AreasType;
	typedef std::vector<Range> RangesType;
	AreasType char_areas;
	RangesType row_ranges;
	Range cur_sel; // current selection range
	Range prev_sel; // previous selection range
	bool m_left_down;
	bool capture_on;
	size_t cursor;
	ssize proposed_char_width;
public:
	GUIEdit(const Geometry::Rect& rect, const FontInfo& info, DrawType _type, size_t _id = 0,
		Geometry::Alignment _x_align = Geometry::ALIGN_CENTER, Geometry::Alignment _y_align = Geometry::ALIGN_CENTER);
	std::string getSelectedText() const; // returns selected text or empty string if no selection
protected:
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
	bool controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) override;
	bool charHandler(char c, bool key_down) override;
private:
	struct RectYComparator { // compares rect areas by X coordinate like LESS. areas are equal if one inside other by X.
		bool operator ()(const Geometry::Rect& first, const Geometry::Rect& second) const;
	};
	struct RectXComparator { // compares rect areas by Y coordinate like LESS. areas are equal if one inside other by Y.
		bool operator ()(const Geometry::Rect& first, const Geometry::Rect& second) const;
	};
	size_t findCharFor(ssize x, ssize y) const; // returns index of char that exactly (or more than others) corresponds to (x, y) position in pixels.
	struct RangeComparator { // compares ranges by their positioning like LESS. ranges are equal if one inside other.
		bool operator ()(const Range& first, const Range& second) const;
	};
	size_t findRowFor(size_t char_index) const; // returns index of row in text, that appropriates to character with (char_index).
	bool updateSelection(ssize x, ssize y, bool key_down); // updates selection on mouse events.
	bool keyboardSubHandler(Controls key, bool key_down, bool& was_keyboard_event); // handles keyboard events
	bool mouseSubHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone); // handles mouse events
	void beforeTextSet(const char* text_to_set) override; // calculates ranges and areas before (text) changes to (text_to_set) in lower level functions.
	bool drawCursor(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area, size_t cursor_pos);

	template <class Functor> bool processRectRange(Range range, Functor functor);
	Geometry::Rect getCursorRect(size_t cursor_pos) const;
	Range getSelectionChanges() const;
};

}; // end GUI

}; // end Graphic32
