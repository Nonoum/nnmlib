#pragma once
#include "GUIScrollBase.h"

namespace Graphic32 {

namespace GUI {

class GUIXScroll : public GUIScrollBase {
public:
	GUIXScroll(const Geometry::Rect& rect, GUIObject* _client, ssize divider, bool _need_self_background = false)
			: GUIScrollBase(rect, DRAW_XSCROLL_MASK, _client, SCROLL_X, divider, _need_self_background) {
	};
};

}; // end GUI

}; // end Graphic32
