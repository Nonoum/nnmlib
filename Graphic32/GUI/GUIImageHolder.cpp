#include "GUIImageHolder.h"

namespace Graphic32 {

namespace GUI {

static inline ssize offsetFor(Geometry::Alignment align, ssize max) {
	if(align == Geometry::ALIGN_BEGIN) {
		return 0;
	}
	return (align == Geometry::ALIGN_END) ? max : max / 2;
};

GUIImageHolder :: GUIImageHolder(const Geometry::Rect& rect, size_t _id, bool is_solid, Geometry::Alignment _x_align, Geometry::Alignment _y_align)
		: GUIObject(rect, DRAW_NONE, _id), area_controller(NULL), x_align(_x_align), y_align(_y_align) {

	area_controller = new GUIObject(Geometry::Rect(), is_solid ? DRAW_IMAGE_SOLID : DRAW_IMAGE_ALPHA);
	Surface::push(area_controller);
	area_controller->activate(false);
};

void GUIImageHolder :: setImage(const SharedImage& image) {
	area_controller->setImage(image);
	setXAlignment(x_align);
	setYAlignment(y_align);
};

void GUIImageHolder :: setCachedImage(const char* cached_image_name) {
	area_controller->setCachedImage(cached_image_name);
	setXAlignment(x_align);
	setYAlignment(y_align);
};

void GUIImageHolder :: setXAlignment(Geometry::Alignment _x_align) {
	x_align = _x_align;
	const ssize y = area_controller->getPlacement().y();
	const ssize unfit = area_controller->getPlacement().width() - getPlacement().width();
	if(unfit < 0) { // fits fully
		surface(area_controller)->setPlacementPos(Geometry::Point(offsetFor(x_align, -unfit), y));
	} else {
		surface(area_controller)->setPlacementPos(Geometry::Point(-unfit / 2, y));
		setScrollValue( SCROLL_X, offsetFor(x_align, unfit) );
	}
};

void GUIImageHolder :: setYAlignment(Geometry::Alignment _y_align) {
	y_align = _y_align;
	const ssize x = area_controller->getPlacement().x();
	const ssize unfit = area_controller->getPlacement().height() - getPlacement().height();
	if(unfit < 0) { // fits fully
		surface(area_controller)->setPlacementPos(Geometry::Point(x, -unfit - offsetFor(y_align, -unfit)));
	} else {
		surface(area_controller)->setPlacementPos(Geometry::Point(x, -unfit / 2));
		setScrollValue( SCROLL_Y, unfit - offsetFor(y_align, unfit) );
	}
};

}; // end GUI

}; // end Graphic32
