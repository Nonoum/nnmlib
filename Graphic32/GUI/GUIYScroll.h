#pragma once
#include "GUIScrollBase.h"

namespace Graphic32 {

namespace GUI {

class GUIYScroll : public GUIScrollBase {
public:
	GUIYScroll(const Geometry::Rect& rect, GUIObject* _client, ssize divider, bool _need_self_background = false)
			: GUIScrollBase(rect, DRAW_YSCROLL_MASK, _client, SCROLL_Y, divider, _need_self_background) {
	};
};

}; // end GUI

}; // end Graphic32
