#include "GUIWindow.h"
#include "../../Helpers/ClearNewPointersCollection.h"
#include "../../Helpers/AutoLocker.h"
#include <algorithm>
#include <iterator>

namespace Graphic32 {

namespace GUI {

GUIWindow :: GUIWindow(Geometry::Size size, DrawType t, size_t _id)
		: GUIObject(Geometry::Rect(Geometry::Point(0, 0), size), t, _id) {

	if(size) {
		screen.alloc(size.width(), size.height());
	}
	setDefaultColors(SET_WINDOW);
	initially_rendered = false;
};

GUIWindow :: ~GUIWindow() {
	clearObjects(false);
};

void GUIWindow :: clearObjects(bool redraw) {
	Helpers::AutoLocker auto_locker(*this, cleaning_request);
	if(! auto_locker) {
		return;
	}
	Surface::clear();
	initially_rendered = false;
	if(redraw) {
		render();
	}
};

bool GUIWindow :: registerObject(GUIObject* new_object) {
	Surface::push(new_object);
	return new_object != NULL;
};

bool GUIWindow :: render() {
	if(! screen) { // no output to render
		return false;
	}
	Helpers::Trigger empty_request; // required parameter in auto_locker, but there is no request
	Helpers::AutoLocker auto_locker(*this, empty_request);
	if(! auto_locker) {
		return false;
	}
	refreshed_areas.clear();
	refreshed_areas.reserve( getDirt().size() );
	std::copy(getDirt().begin(), getDirt().end(), std::back_inserter(refreshed_areas));
	if(! refreshed_areas.size() && ! initially_rendered) {
		initially_rendered = 0;
		refreshed_areas.push_back(getPlacement());
	}

	Surface::render(screen);
	if(haveCallback()) {
		CallbackParams params(this, KEY_RENDER_ACTION, true);
		// refreshing by parts
		for(size_t i = 0; i < refreshed_areas.size(); ++i) {
			params.screen = &screen;
			params.area = refreshed_areas[i];
			params.length = refreshed_areas.size(); // count of callbacks call for this render
			params.index = i; // number of call in set
			sendCallback(params);
		}
	}
	return true;
};

bool GUIWindow :: handleInput(Controls key, bool key_down, Geometry::Point local_pos) {
	if(GUIObject::handleInput(key, key_down, local_pos, true)) {
		render();
	}
	return false;
};

bool GUIWindow :: handleInput(char c, bool key_down) {
	if(GUIObject::handleInput(c, key_down)) {
		render();
	}
	return false;
};

bool GUIWindow :: setRenderSize(Geometry::Size size) {
	Helpers::AutoLocker auto_locker(*this, resizing_request);
	if(! auto_locker) { // object is locked, probably by 'render'
		resizing_request.param = size;
		return false;
	}
	if(size == screen.size()) {
		return true;
	}
	if(! size) { // killing render image
		screen.dealloc();
	} else {
		screen.alloc(size.width(), size.height());
	}
	setSize(size); // size of canvas for painting
	return true;
};

void GUIWindow :: onFullUnlock() {
	if(resizing_request.isSet()) {
		setRenderSize(resizing_request.param);
	}
	if(cleaning_request.isSet()) {
		clearObjects();
	}
};

}; // end GUI

}; // end Graphic32
