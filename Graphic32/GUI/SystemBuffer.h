#pragma once
#include "gui_utils.h"

namespace Graphic32 {

namespace GUI {

/*
	Text storage for copy-pasting.
	Allows synchronizing with system storage by callback.
*/
class SystemBuffer {
	SystemBuffer(); // disallowed creating object to avoid confusing with 'multiple buffers'
public:
	struct Callback {
		virtual void copy(std::string& src_or_dst, bool direction_to_user) = 0;
		virtual ~Callback() {};
	};
	static void setCallback(Callback* new_callback);
	static void put(const char* str);
	static std::string get();
};

}; // end GUI

}; // end Graphic32
