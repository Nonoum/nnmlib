#include "GUICheckBox.h"
#include <algorithm>

namespace Graphic32 {

namespace GUI {

GUICheckBox :: GUICheckBox(const Geometry::Rect& rect, const FontInfo& info, DrawType t, size_t _id,
		Geometry::Alignment x_text_align, Geometry::Alignment y_text_align)
		: GUIObject(rect, t, _id), button(NULL), text(NULL) {

	is_checked = false;

	button = new GUIButton(getButtonRect(rect), DRAW_CHECK_OFF_MASK);
	Surface::push(surface(button));
	button->setCallback( new ButtonCallback(*this) );

	text = new GUITextZone(getTextRect(rect), info, DRAW_NONE, false, 0, NULL, x_text_align, y_text_align);
	Surface::push(surface(text));
};

void GUICheckBox :: setChecked(bool checked) {
	if(is_checked != checked) {
		is_checked = checked;
		button->setMask( getMaskFor(checked ? DRAW_CHECK_ON_MASK : DRAW_CHECK_OFF_MASK, button->getPlacement().getSize()) );
	}
};

bool GUICheckBox :: isChecked() const {
	return is_checked;
};

bool GUICheckBox :: setText(const char* src) {
	return text->setText(src);
};

void GUICheckBox :: setTextColor(uint32 color, State s) {
	text->setTextColor(color, s);
};

void GUICheckBox :: setColor(uint32 color, State s) {
	GUIObject::setColor(color, s);
	button->setColor(color, s);
};

Geometry::Rect GUICheckBox :: getButtonRect(const Geometry::Rect& total_rect) {
	const ssize width = Helpers::min2(total_rect.height(), total_rect.width()) - 2;
	assert( width > 0 );
	assert( total_rect.height() > 2 );
	return Geometry::Rect(Geometry::Point(1, 1),
						Geometry::Size(width, total_rect.height() - 2));
};

Geometry::Rect GUICheckBox :: getTextRect(const Geometry::Rect& total_rect) {
	const ssize width = total_rect.width() - total_rect.height() - 1;
	assert( width > 0 );
	assert( total_rect.height() > 2 );
	if(width <= 0) {
		return Geometry::Rect();
	}
	return Geometry::Rect(Geometry::Point(total_rect.height(), 1),
						Geometry::Size(width, total_rect.height() - 2));
};

// internal button callback -------------------------------------------------------------------------------

GUICheckBox::ButtonCallback :: ButtonCallback(GUICheckBox& _owner)
		: owner(_owner) {
};

bool GUICheckBox::ButtonCallback :: action(CallbackParams& params) {
	if(params.action == KEY_MOUSE_LEFT && (! params.key_down) && params.hit_area) {
		const bool was = owner.isChecked();
		owner.setChecked(! was);
		if(owner.haveCallback() && owner.isActive()) {
			CallbackParams par(&owner, params.action, params.key_down);
			par.state = was ? 0 : 1;
			par.length = 1;
			par.hit_area = true;
			return owner.sendCallback(par);
		}
	}
	return false;
};

}; // end GUI

}; // end Graphic32
