#pragma once
#include "caches.h"
#include "GUIState.h"
#include "../../Helpers/CallbackStorage.h"
#include "../Composition/Surface.h"

namespace Graphic32 {

namespace GUI {

/*
	Base GUI Object class with different styles support except text drawing.
	Object can have only the single callback instance (not chain of callbacks).

	Drawing commonly depends on COLOR which corresponds to state: COLOR == getColor(getState()).
	Drawing features:
		1) for types DRAW_*_MASK appropriate mask is being draw with RGB part of COLOR (3 lower bytes);
		2) for type DRAW_IMAGE_SOLID - COLOR doesn't affect drawing;
		3) for type DRAW_IMAGE_ALPHA - on drawing alpha channel of source image is being pre-multiplied
			with alpha channel of COLOR (higher byte);
		4) for type DRAW_NONE drawing is being skipped;
		5) for type DRAW_SOLID_COLOR - background is being draw by copying ALL channels of COLOR to output;

	Background setting features:
		1) setMask, setImage can take Shared object with NULL pointer, it will be applied as empty image/mask;
		2) same with setCachedImage if no specified cached image is found.
*/
class GUIObject : protected Composition::Surface,
					public GUIState,
					public Helpers::CallbackStorage<CallbackInterface, CallbackParams&>,
					private NonCopyable {
public:
	enum DrawType { // type of drawing mask for object
		DRAW_CIRCLE_MASK = 0, // circle-mask (with color, that corresponds to State)
		DRAW_RECTANGLE_MASK, // rect-mask -:-
		DRAW_CIRCLE_GRADIENT_MASK,
		DRAW_RECTANGLE_GRADIENT_MASK,
		DRAW_CIRCLE_CONTUR_MASK,
		DRAW_RECTANGLE_CONTUR_MASK,
		DRAW_ARROW_DOWN_MASK,
		DRAW_ARROW_LEFT_MASK,
		DRAW_ARROW_UP_MASK,
		DRAW_ARROW_RIGHT_MASK,
		DRAW_XSCROLL_MASK,
		DRAW_YSCROLL_MASK,
		DRAW_ROUNDED_RECT_MASK,
		DRAW_CHECK_ON_MASK,
		DRAW_CHECK_OFF_MASK,
		DRAW_RADIO_ON_MASK,
		DRAW_RADIO_OFF_MASK,
		DRAW_MASKS_COUNT, // count of types with real masks
		//--------------
		DRAW_NONE = DRAW_MASKS_COUNT, // no drawing for object
		DRAW_IMAGE_SOLID, // solid image background
		DRAW_IMAGE_ALPHA, // alpha-output image background
		DRAW_SOLID_COLOR // solid background color (color that corresponds to State)
	};
private:
	static Img32 original_masks[DRAW_MASKS_COUNT]; // original masks as source to resized versions
	SharedMask mask; // mask to draw
	SharedImage image; // image to draw
	DrawType type; // type of drawing
	bool auto_sized; // if auto sized on setImage method
	bool active; // false if object ignoring the controls in handling
	size_t id; // id for parent window (if needed)
	bool skip_need_redraw; // optimization
public:
	GUIObject(const Geometry::Rect& rect, DrawType t, size_t _id = 0, const char* image_cached_name = NULL);

	// getters
	inline const Geometry::Rect& getPlacement() const { return Surface::getPlacement(); };
	size_t getID() const;
	DrawType getType() const;
	bool isActive() const;

	// setters
	bool setMask(const SharedMask& _mask); // sets background mask if any mask background mode is set.
	bool setImage(const SharedImage& _image); // sets background image if any image background mode is set.
	bool setCachedImage(const char* cached_name); // like 'setImage' but looks for image with (cached_name) in (Images()) cache.
	void activate(bool _active = true); // activates/deactivates behaviour.
	void setContur(bool contur, uint32 _contur_color = 0x00000000);
	void setID(size_t _id);

	// controls
	bool handleInput(Controls key, bool key_down, Geometry::Point scrolled_parents_pos, bool in_valid_zone); // returns true if something changed for drawing render should be called
	bool handleInput(char c, bool key_down); // returns true if something changed for drawing render should be called

	static bool initGUI(const char* cimg_resources_file_name); // initializes all data related to GUIObject class
	static bool isBasicControl(Controls key); // returns true if key is for basic 'controlHandler'
protected:
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
	void needRedraw() override;
	virtual bool controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone); // returns true if object or cub-objects needs redraw
	virtual bool charHandler(char c, bool key_down); // returns true if object or sub-objects needs redraw

	bool isImageBackground() const; // true if mode is any of modes with user image on background
	Drawers imageDrawMethod() const; // drawing method for Img32::drawRect
	static Composition::Surface* surface(GUIObject* object) { return object; }; // method to simply access Surface level from other object

	static SharedMask getMaskFor(DrawType type, Geometry::Size size); // returns mask for specified type and size. appends (Masks()) cache with new one, if needed one not exist.
};

}; // end GUI

}; // end Graphic32
