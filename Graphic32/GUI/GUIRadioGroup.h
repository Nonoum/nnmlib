#pragma once
#include "GUIButton.h" // includes all needed classes

namespace Graphic32 {

namespace GUI {

/*
	Radio Group implementation. Uses default radio images.
	Callback is being called for any action as it's default behaviour,
	in this basic call parameter 'length' is 0 (default).

	On KEY_MOUSE_LEFT down event callback is being called
	with patameters:
		'hit_area' == true;
		'index' is index of current active radio if it has changed (equal to result of 'getActiveItem' method);
		'length' is count of items in radio group (equal to result of 'getItemsCount' method);
	For comfortable using object id can be specified on constructing GUIRadioGroup
	and checked in callback:
		size_t id = params.caller->getID();
*/
class GUIRadioGroup : public GUIObject {
	typedef std::vector<GUIButton*> ButtonsType;
	typedef std::vector<GUITextZone*> TextsType;
	ButtonsType buttons;
	TextsType texts;

	size_t i_active; // index of active item (index in array)
	size_t i_pretender; // index of radio that is going to be set
	const size_t items_count;
public:
	/*
	Creates group with specified amount of radio items with specified text font and alignments.
	Recalculates offered (rect) according to (radios_count) parameter:
		- cuts extra height to fit (radios_count) of radio items with equal height;
		- button width and height in radio item being set to be equal;
		- reduces height more, if calculated radio item height is less than free width;
	*/
	GUIRadioGroup(const Geometry::Rect& rect, const FontInfo& info, size_t radios_count, DrawType t = DRAW_NONE, size_t _id = 0,
			Geometry::Alignment x_text_align = Geometry::ALIGN_BEGIN, Geometry::Alignment y_text_align = Geometry::ALIGN_CENTER);
	void setActiveItem(size_t i_active_radio); // sets the active radio item (counting from zero from top to bottom)
	size_t getActiveItem() const; // returns the active radio item index (counting from zero from top to bottom)
	size_t getItemsCount() const; // returns the total number of radio items
	bool setText(size_t i_item, const char* src); // sets the text for i-th radio (counting from zero from top to bottom)
	void setTextColor(uint32 color, State s); // sets text color for all text
	void setColor(uint32 color, State s); // sets color for all buttons
private:
	bool setActiveItem(size_t i_active_radio, bool begin_pretend);
	static Geometry::Rect getCorrectedRect(const Geometry::Rect& total_rect, size_t items_count);
	static Geometry::Rect getButtonRect(const Geometry::Rect& total_rect, size_t items_count, size_t i_item);
	static Geometry::Rect getTextRect(const Geometry::Rect& total_rect, size_t items_count, size_t i_item);
	// internal button callback -------------------------------------------------------------------------------
	class ButtonCallback : public CallbackInterface {
		GUIRadioGroup& owner;
	public:
		ButtonCallback(GUIRadioGroup& _owner);
		bool action(CallbackParams& params) override;
	};
};

}; // end GUI

}; // end Graphic32
