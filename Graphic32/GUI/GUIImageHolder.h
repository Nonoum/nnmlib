#pragma once
#include "GUIObject.h"

namespace Graphic32 {

namespace GUI {

/*
	Class for displaying aligned image without repeating.
*/
class GUIImageHolder : public GUIObject {
	GUIObject* area_controller;
	Geometry::Alignment x_align, y_align;
public:
	GUIImageHolder(const Geometry::Rect& rect, size_t _id, bool is_solid,
		Geometry::Alignment _x_align = Geometry::ALIGN_CENTER, Geometry::Alignment _y_align = Geometry::ALIGN_CENTER);
	void setImage(const SharedImage& image); // overloads GUIObject's method
	void setCachedImage(const char* cached_image_name); // overloads GUIObject's method
	void setXAlignment(Geometry::Alignment _x_align);
	void setYAlignment(Geometry::Alignment _y_align); // ALIGN_BEGIN corresponds to top of available area
};

}; // end GUI

}; // end Graphic32
