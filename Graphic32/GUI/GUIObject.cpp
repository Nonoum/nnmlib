#include "GUIObject.h"
#include "../../Helpers/ScopedVarLock.h"

namespace Graphic32 {

namespace GUI {

Img32 GUIObject :: original_masks[DRAW_MASKS_COUNT];

GUIObject :: GUIObject(const Geometry::Rect& rect, DrawType t, size_t _id, const char* image_cached_name)
		: Surface(rect, 0, false), type(t), id(_id), auto_sized(false), active(true), skip_need_redraw(false) {

	const Geometry::Size& size = getPlacement().getSize();
	if(type < DRAW_MASKS_COUNT) { // using mask
		if(! original_masks[type]) { // no appropriated resource has been loaded (initGUI() hasn't been done)
			type = DRAW_SOLID_COLOR; // to draw something at least
			return; // out
		}
		setMask( getMaskFor(type, size) );
	} else if(isImageBackground()) { // custom image for background
		auto_sized = ! size;
		if(image_cached_name) { // look for the cached image name in images manager and set if appropriated one found
			setCachedImage(image_cached_name);
		} // else - user have to call setImage later
	}
};

bool GUIObject :: setMask(const SharedMask& _mask) {
	if(type >= DRAW_MASKS_COUNT) { // incorrect set. out
		return false;
	}
	if(mask.get() != _mask.get()) { // needs redraw if mask pointer is different
		needRedraw();
	}
	mask = _mask;
	return mask.get() != NULL;
};

bool GUIObject :: setImage(const SharedImage& _image) {
	if(! isImageBackground()) { // incorrect set. out
		return false;
	}
	if(image.get() != _image.get()) { // needs redraw if image pointer is different
		needRedraw();
	}
	image = _image;
	if(image.get() && auto_sized) { // auto size setting by image size
		setSize(image.get()->size());
	}
	return image.get() != NULL;
};

bool GUIObject :: setCachedImage(const char* cached_name) {
	if(! isImageBackground()) { // incorrect set. out
		return false;
	}
	return setImage( Images().find(ImageDescription(cached_name)) );
};

void GUIObject :: activate(bool _active) {
	active = _active;
};

bool GUIObject :: isActive() const {
	return active;
};

void GUIObject :: setContur(bool contur, uint32 _contur_color) {
	if(contur) {
		setBorders(1, 1, 1, 1);
		setBordersColor(_contur_color);
	} else {
		setBorders(0, 0, 0, 0);
	}
};

bool GUIObject :: handleInput(Controls key, bool key_down, Geometry::Point scrolled_parents_pos, bool in_valid_zone) {
	if(! isActive()) {
		return false;
	}
	Geometry::Point local_pos = scrolled_parents_pos - getPlacement().getPoint();
	bool result = controlHandler(key, key_down, local_pos, in_valid_zone);

	in_valid_zone &= containsPoint(local_pos);
	scrollLocalPoint(local_pos);

	const Surface::SubsType& subs = getSubs();
	if(isBasicControl(key)) {
		bool any_non_default = false;
		for(size_t i = 0; i < subs.size(); ++i) {
			GUIObject* const obj = static_cast<GUIObject*>(subs[i]);
			result |= obj->handleInput(key, key_down, local_pos, in_valid_zone);
			if(obj->getState() == STATE_SELECTED) {
				setFocusedIndex(i); // some object is focused now
			}
			any_non_default |= (obj->getState() != STATE_DEFAULT);
		}
		if((getState() == STATE_SELECTED) && (! any_non_default)) { // click on this surface, missing all sub-surfaces
			setFocusedIndex(-1);
		}
	} else { // control for focused object only
		if(getFocusedIndex() != size_t(-1)) { // have focused object
			result |= static_cast<GUIObject*>(subs[getFocusedIndex()])->handleInput(key, key_down, local_pos, in_valid_zone);
		}
	}
	return result;
};

bool GUIObject :: handleInput(char c, bool key_down) {
	if(! isActive()) {
		return false;
	}
	if(getFocusedIndex() != size_t(-1)) {
		return static_cast<GUIObject*>(getSubs()[getFocusedIndex()])->handleInput(c, key_down);
	}
	return charHandler(c, key_down);
};

bool GUIObject :: controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) {
	const State prev_state = getState();
	// check whether local_pos within *this surface area. local_pos is left non-scrolled within *this surface
	const bool hit = containsPoint(local_pos) & in_valid_zone;
	Helpers::ScopedVarLock<bool> svl(skip_need_redraw, true, false);
	bool result = true;

	switch (key) {
	case KEY_MOUSE_LEFT:
	case KEY_MOUSE_DOUBLE:
		if(hit) {
			setState(key_down ? STATE_SELECTED : STATE_HOVER);
		} else { // outside of this object
			if((! key_down) && (getState() != STATE_DEFAULT)) {
				setState(STATE_DEFAULT);
			} else {
				return false; // no changes here
			}
		}
		break;
	case KEY_MOUSE_MOVE:
		if(hit) {
			if(getState() != STATE_DEFAULT) {
				result = false; // no changes here, or state is STATE_SELECTED. pass to callback
			} else {
				if(getState() != STATE_HOVER) {
					setState(STATE_HOVER);
				} else {
					return false; // no changes here
				}
			}
		} else { // mouse moved outside of this object
			if(getState() == STATE_HOVER) { // ...and was on this object
				setState(STATE_DEFAULT);
			} else { // ...and wasn't on this object
				return false; // no changes here
			}
		}
		break;
	case KEY_MOUSE_RIGHT:
		if(key_down && (! hit)) { // for key_down passing only when hit the object
			return false;
		} // for key_up - passing both
		result = false;
		break;
	default:
		result = false; // no action match
		if(hit) {
			break; // pass other actions to callback
		}
		return false; // others won't pass
	};
	if(result) {
		uint32 cur_color = getColor(getState());
		uint32 prev_solor = getColor(prev_state);
		if((type == DRAW_IMAGE_SOLID) // state is not affects this mode
			|| (type == DRAW_NONE) // same with this mode
			|| ((type < DRAW_MASKS_COUNT) && (((cur_color ^ prev_solor) & 0x00FFFFFF) == 0)) // RGB value is the same
			|| ((type == DRAW_IMAGE_ALPHA) && (((cur_color ^ prev_solor) & 0xFF000000) == 0))) { // alpha value is the same
			result = false; // optimization. no visual changes here
		}
	}
	if(haveCallback()) {
		CallbackParams params(this, key, key_down);
		params.local_pos = local_pos;
		params.hit_area = hit;
		result |= sendCallback(params);
	}
	if(result) {
		invalidate(); // need redraw
	}
	return result;
};

bool GUIObject :: charHandler(char c, bool key_down) {
	return false; // no reaction on char-input in basic class
};

bool GUIObject :: initGUI(const char* cimg_resources_file_name) {
	static bool init_done = false;
	if(init_done) {
		return true;
	}
	Img32 buffer[DRAW_MASKS_COUNT]; // all masks for mask-mode
	const uint16 num = buffer->readCimgBatch(cimg_resources_file_name, DRAW_MASKS_COUNT);
	for(uint16 i = 0; i < num; ++i) {
		original_masks[i].swap(buffer[i]);
	}
	init_done = true;
	return true;
};

bool GUIObject :: isBasicControl(Controls key) {
	return key <= KEY_MOUSE_DOUBLE;
};

GUIObject::DrawType GUIObject :: getType() const {
	return type;
};

bool GUIObject :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	const ssize x_src_offs = local_area.x();
	const ssize y_src_offs = local_area.y();
	const ssize x = displacement.x() + local_area.x();
	const ssize y = displacement.y() + local_area.y();
	const ssize wi = local_area.width();
	const ssize he = local_area.height();
	bool result = false;
	if(type < DRAW_MASKS_COUNT) { // mask mode
		if(mask.get()) { // checking for NULL pointer
			result = dst.drawByMask(x, y, wi, he, *mask.get(), x_src_offs, y_src_offs, getColor(getState()));
		}
	} else if(isImageBackground()) { // image background mode
		if(image.get()) { // checking for NULL pointer
			result = dst.drawRect(x, y, wi, he, *image.get(), x_src_offs, y_src_offs, imageDrawMethod(), getColor(getState()) >> 24);
		}
	} else if(type == DRAW_SOLID_COLOR) {
		result = dst.fillRect(getColor(getState()), x, y, wi, he, false); // not keeping original alpha
	}
	return result;
};

size_t GUIObject :: getID() const {
	return id;
};

void GUIObject :: setID(size_t _id) {
	id = _id;
};

bool GUIObject :: isImageBackground() const {
	return type == DRAW_IMAGE_SOLID || type == DRAW_IMAGE_ALPHA;
};

Drawers GUIObject :: imageDrawMethod() const {
	if(type == DRAW_IMAGE_SOLID) {
		return D_SOLID;
	} else if(type == DRAW_IMAGE_ALPHA) {
		if((getColor(getState()) & 0xFF000000) != 0xFF000000) {
			return D_MULTIPLEDALPHA;
		}
		return D_ALPHA;
	}
	return D_SOLID;
};

void GUIObject :: needRedraw() {
	if(! skip_need_redraw) {
		invalidate();
	}
};

SharedMask GUIObject :: getMaskFor(DrawType type, Geometry::Size size) {
	if(type >= DRAW_MASKS_COUNT) {
		return SharedMask();
	}
	const MaskDescription mask_desc(size);
	if(! Masks(type).has(mask_desc)) { // no appropriate cached mask yet
		// making mask with needed size
		Img32 buf;
		if(! original_masks[type].resizeAnisotropic(buf, size.width(), size.height())) { // anisotropic may not work if output size will be larger than original
			if(! original_masks[type].resizeBilinear(buf, size.width(), size.height())) { // try bilinear
				return SharedMask();
			}
		}
		// needed mask is in buf now, preparing SharedMask
		SharedMask m(new AlphaMask());
		m.get()->loadChannel(buf, 0);
		Masks(type).set(mask_desc, m);
		return m;
	}
	return Masks(type).find(mask_desc);
};

}; // end GUI

}; // end Graphic32
