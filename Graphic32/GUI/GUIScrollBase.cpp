#include "GUIScrollBase.h"
#include <stdexcept>

namespace Graphic32 {

namespace GUI {

GUIScrollBase :: GUIScrollBase(const Geometry::Rect& rect, DrawType _type, GUIObject* _client, Scrolls scroll_type, ssize _divider, bool _need_self_background)
		: GUIObject(rect, _type), client(_client), s_type(scroll_type), divider(_divider), need_self_background(_need_self_background) {

	m_left_down = false;
	m_right_down = false;
	last_pos = 0;
	setDefaultColors(SET_SCROLL);
	if(! _client) {
		throw std::runtime_error("GUIScrollBase :: GUIScrollBase error : NULL pointer as client.");
	}
};

void GUIScrollBase :: setBackgroundColor(uint32 color, State s) {
	self_background_colors.setColor(color, s);
	if(need_self_background && (getState() == s)) {
		needRedraw();
	}
};

bool GUIScrollBase :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	const Geometry::Size size = getPlacement().getSize();
	const usize length = (s_type == SCROLL_X) ? size.width() : size.height();
	bool result = false;
	if(need_self_background) {
		result |= dst.fillRect(self_background_colors.getColor(getState()),
			displacement.x() + local_area.x(),
			displacement.y() + local_area.y(),
			local_area.width(),
			local_area.height(), false); // not keeping original alpha
	}
	result |= GUIObject::renderArea(dst, displacement, local_area);
	// drawing status
	if(getState() != STATE_DEFAULT) { // drawing status over it
		const ssize draw_size = calcScrollMarkerSize(length);
		const ssize max = Helpers::max2<ssize>(surface(client)->getScrollLimit(s_type), 1);

		const ssize pos = (length - draw_size) * surface(client)->getScrollValue(s_type) / max;
		Geometry::Rect area;
		if(s_type == SCROLL_X) {
			area = Geometry::Rect(pos, 0, draw_size, size.height()) & local_area;
		} else { // SCROLL_Y
			area = Geometry::Rect(0, pos, size.width(), draw_size) & local_area;
		}
		result |= dst.fillRect(sliderColor(),
			displacement.x() + area.x(),
			displacement.y() + area.y(),
			area.width(),
			area.height(), false);
	}
	return result;
};

uint32 GUIScrollBase :: sliderColor() const {
	return getColor( getState() ) ^ 0x00FFFFFF; // negative color
};

usize GUIScrollBase :: calcScrollMarkerSize(usize maximum) {
	const usize enough = 2; // size in pixels
	usize sz = maximum / 50; // 2%
	if(sz < enough) {
		sz = (maximum >= enough) ? enough : maximum;
	}
	return sz;
};

bool GUIScrollBase :: controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) {
	const bool result = GUIObject::controlHandler(key, key_down, local_pos, in_valid_zone);
	in_valid_zone &= containsPoint(local_pos);

	if((key == KEY_MOUSE_LEFT) || (key == KEY_MOUSE_DOUBLE)) {
		m_left_down = key_down & in_valid_zone;
		if(m_left_down) {
			return result | autoScroll(local_pos);
		}
	} else if(key == KEY_MOUSE_RIGHT) {
		m_right_down = key_down & in_valid_zone;
		if(m_right_down) {
			return result | manualScroll(key_down, local_pos);
		}
	} else if(key == KEY_MOUSE_MOVE) {
		if(m_right_down) {
			return result | manualScroll(key_down, local_pos);
		} else if(m_left_down) {
			return result | autoScroll(local_pos);
		}
	}
	return result;
};

ssize GUIScrollBase :: moveAndUpdate(ssize new_pos) {
	const ssize distance = (new_pos / divider) - (last_pos / divider);
	if(distance) { // if discrete sector were changed, then apply changes
		last_pos = new_pos;
	}
	return distance;
};

bool GUIScrollBase :: manualScroll(bool key_down, Geometry::Point local_pos) {
	const ssize pos = (s_type == SCROLL_X) ? local_pos.x() : local_pos.y();
	if(key_down) {
		last_pos = pos;
		return false;
	}
	const ssize distance = moveAndUpdate(pos);
	if(! distance) {
		return false;
	}
	const ssize scroll_max = surface(client)->getScrollLimit(s_type);
	if(! scroll_max) {
		return false;
	}
	ssize new_scroll_pos = surface(client)->getScrollValue(s_type) - distance;
	new_scroll_pos = Helpers::max2<ssize>(new_scroll_pos, 0);
	new_scroll_pos = Helpers::min2<ssize>(new_scroll_pos, scroll_max);

	if(new_scroll_pos != surface(client)->getScrollValue(s_type)) { // changed, updating
		surface(client)->setScrollValue(s_type, new_scroll_pos);
		needRedraw();
		return true;
	}
	return false;
};

bool GUIScrollBase :: autoScroll(Geometry::Point local_pos) {
	const ssize pos = (s_type == SCROLL_X) ? local_pos.x() : local_pos.y();
	const ssize field_size = (s_type == SCROLL_X) ? getPlacement().width() : getPlacement().height();

	const ssize scroll_max = surface(client)->getScrollLimit(s_type);
	ssize new_scroll_pos = surface(client)->getScrollValue(s_type);

	if(! scroll_max) {
		return false;
	}
	ssize center_pos = field_size / 2;
	ssize power = pos - center_pos;
	if(! power) { // hit in center, pass
		return false;
	}
	ssize add_offset = 0;
	center_pos *= center_pos;
	if(power > 0) {
		power *= power;
		add_offset = (scroll_max - new_scroll_pos) * power;
		if(add_offset & center_pos) {
			add_offset += center_pos;
		}
	} else { // power < 0
		power *= -power;
		add_offset = new_scroll_pos * power;
		if(add_offset & center_pos) {
			add_offset -= center_pos;
		}
	}
	new_scroll_pos += add_offset / center_pos;
	new_scroll_pos = Helpers::max2<ssize>(new_scroll_pos, 0);
	new_scroll_pos = Helpers::min2<ssize>(new_scroll_pos, scroll_max);

	if(new_scroll_pos != surface(client)->getScrollValue(s_type)) { // changed, updating
		surface(client)->setScrollValue(s_type, new_scroll_pos);
		needRedraw();
		return true;
	}
	return false;
};

}; // end GUI

}; // end Graphic32
