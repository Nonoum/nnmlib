#pragma once
#include "gui_utils.h"

namespace Graphic32 {

namespace GUI {

typedef std::shared_ptr<TextPrinter> SharedPrinter;

/*
	Comparable description for printer (by FontInfo).
*/
class PrinterDescription {
	FontInfo info;
public:
	PrinterDescription(const FontInfo& _info) : info(_info) {
	};
	bool operator <(const PrinterDescription& right) const {
		return info < right.info;
	};
	static bool cleanable(const PrinterDescription& desc, const SharedPrinter& printer) {
		return printer.unique();
	};
};

}; // end GUI

}; // end Graphic32
