#pragma once
#include "GUIObject.h"

namespace Graphic32 {

namespace GUI {

/*
	Class for displaying text in object's area.
	Supports different text alignments.
*/
class GUITextZone : public GUIObject {
	SharedPrinter printer;
	const Geometry::Alignment x_align, y_align; // text alignment (initialization in constructor)
	GUIState textColors; // set of colors for printing text

	Geometry::Point printing_offset; // basic positions to start drawing text (without offsets for scrolling)
	Geometry::Size fit_size; // size in pixels to fit all current text

	GUIObject* fit_controller; // needed to control internal sizes
	const bool auto_sized;
	bool align_on_settext;
protected:
	std::string text; // full text
public:
	GUITextZone(const Geometry::Rect& rect, const FontInfo& info,
		DrawType _type = DRAW_SOLID_COLOR, bool _auto_sized = false, size_t _id = 0,
		const char* cached_image_name = NULL,
		Geometry::Alignment _x_align = Geometry::ALIGN_BEGIN, Geometry::Alignment _y_align = Geometry::ALIGN_BEGIN);

	bool setText(const char* src); // sets text
	const char* getText() const;

	void setTextColor(uint32 color, State s); // sets text color for corresponding state
	Geometry::Size getFitSize() const; // simply returns (this->cached_fit_size)
protected:
	Geometry::Point getPrintingOffset() const;
	const SharedPrinter& getPrinter() const; // just printer accessor for inheritors

	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
	bool controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) override;
	bool printWithParams(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area);
	virtual void beforeTextSet(const char* text_to_set); // EMPTY event for inheritors

	void alignOnSetText(bool auto_align);
private:
	void setPrinter(const SharedPrinter& _printer);
	bool prepareTextPlacement(const char* src); // computes fitSize and offsets for printing with specified alignment
	void processAlignments(Geometry::Size size, Geometry::Size fit_size);
};

}; // end GUI

}; // end Graphic32
