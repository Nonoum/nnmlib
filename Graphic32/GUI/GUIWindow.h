#pragma once
#include "GUIObject.h"
#include "../../Helpers/ObjectLocker.h"
#include "../../Helpers/Requests.h"

namespace Graphic32 {

namespace GUI {

/*
	Completed window class.
	On 'render' and on any changes during processing any of 'handleInput' methods
	it re-renders the window and calls registered callback to display it.
	callback takes CallbackParams& with:
		const Img32* screen: rendered image of screen;
		Controls action: event code (always == KEY_RENDER_ACTION);
		size_t length: count of calls for current render set;
		size_t index: index of call in set;
		Geometry::Rect area: area of screen that need to be refreshed;
*/
class GUIWindow : public GUIObject, protected Helpers::ObjectLocker {
	Img32 screen; // rendering surface
	Helpers::Request<Geometry::Size> resizing_request;
	Helpers::Request<void> cleaning_request;
	Surface::DirtType refreshed_areas;
	bool initially_rendered;
public:
	GUIWindow(Geometry::Size size, DrawType t = DRAW_SOLID_COLOR, size_t _id = 0);
	~GUIWindow();

	void clearObjects(bool redraw = true); // removes all objects from window (correct deleting).
	bool registerObject(GUIObject* new_object); // adds any GUI object to window.
	bool render();

	bool setRenderSize(Geometry::Size size); // sets size of image to render.

	bool handleInput(Controls key, bool key_down, Geometry::Point local_pos); // overloading GUIObject's function to handle need in redrawing
	bool handleInput(char c, bool key_down); // same overloading
protected:
	void onFullUnlock() override; // calls registered requests on full unlock
};

}; // end GUI

}; // end Graphic32
