#include "GUIProgressBar.h"
#include "template_helpers.h"

namespace Graphic32 {

namespace GUI {

GUIProgressBar :: GUIProgressBar(const Geometry::Rect& rect, size_t limit, size_t value, uint32 done_color, uint32 undone_color)
		: GUIObject(rect, DRAW_SOLID_COLOR), lim(limit), val(value), d_color(done_color), u_color(undone_color) {

	assert(value <= limit);
	val = Helpers::min2(value, limit);
	setContur(true, 0xFFFFFFFF);
	const uint32 clrs[] = {u_color, u_color, u_color};
	SetColors(this, clrs);
};

void GUIProgressBar :: setValue(size_t value) {
	assert(value <= lim);
	val = Helpers::min2(value, lim);
	needRedraw();
};

void GUIProgressBar :: setLimit(size_t limit) {
	lim = limit;
	assert(val <= limit);
	val = Helpers::min2(val, lim);
	needRedraw();
};

void GUIProgressBar :: setColors(uint32 done_color, uint32 undone_color) {
	d_color = done_color;
	u_color = undone_color;
	const uint32 clrs[] = {u_color, u_color, u_color};
	SetColors(this, clrs);
	needRedraw();
};

bool GUIProgressBar :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	const Geometry::Size& size = getPlacement().getSize();
	bool result = GUIObject::renderArea(dst, displacement, local_area);
	const ssize x = getBorder(Geometry::SIDE_LEFT);
	const ssize y = getBorder(Geometry::SIDE_DOWN);
	const ssize wi = ((size.width() - (getBorder(Geometry::SIDE_LEFT) + getBorder(Geometry::SIDE_RIGHT)))
						* ssize(val)) / ssize(lim ? lim : 1);
	const ssize he = size.height() - ( getBorder(Geometry::SIDE_UP) + getBorder(Geometry::SIDE_DOWN) );
	const Geometry::Rect area = Geometry::Rect(x, y, wi, he) & local_area;
	return result | dst.fillRect(d_color, displacement.x() + area.x(),
									displacement.y() + area.y(),
									area.width(), area.height(), false);
};

}; // end GUI

}; // end Graphic32
