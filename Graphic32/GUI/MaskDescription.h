#pragma once
#include "gui_utils.h"

namespace Graphic32 {

namespace GUI {

typedef std::shared_ptr<AlphaMask> SharedMask;

/*
	Comparable description for mask (by it's size).
*/
class MaskDescription : public Geometry::Size {
public:
	MaskDescription(Geometry::Size size) : Geometry::Size(size) {
	};
	bool operator <(const MaskDescription& right) const {
		return getSize().lessSorting(right.getSize());
	};
	static bool cleanable(const MaskDescription& desc, const SharedMask& mask) {
		return mask.unique();
	};
};

}; // end GUI

}; // end Graphic32
