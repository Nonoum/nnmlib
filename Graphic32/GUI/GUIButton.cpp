#include "GUIButton.h"

namespace Graphic32 {

namespace GUI {

GUIButton :: GUIButton(const Geometry::Rect& rect, const FontInfo& info, DrawType _type, size_t _id,
	Geometry::Alignment _x_align, Geometry::Alignment _y_align)
		: GUITextZone(rect, info, _type, false, _id, NULL, _x_align, _y_align), drag(1, 1) {
};

GUIButton :: GUIButton(const Geometry::Rect& rect, DrawType _type, size_t _id, const char* image_cached_name)
		: GUITextZone(rect, FontInfo(0, 0, 0), _type, false, _id, image_cached_name), drag(1, 1) {
};

Geometry::Point GUIButton :: getDrag() const {
	return drag;
};

void GUIButton :: setDrag(const Geometry::Point& _drag) {
	drag = _drag;
	if(getState() == STATE_SELECTED) {
		needRedraw();
	}
};

bool GUIButton :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	Geometry::Point pos = displacement;
	Geometry::Rect area = local_area;
	if(getState() == STATE_SELECTED) {
		pos += drag;
		area.setRect(area.x() - Helpers::min2<ssize>(drag.x(), 0),
					area.y() - Helpers::min2<ssize>(drag.y(), 0),
					area.width() - drag.abs().x(),
					area.height() - drag.abs().y());
		if(! area.getSize().canFit( Geometry::Size(1, 1) )) {
			return false;
		}
	}
	bool result = GUITextZone::renderArea(dst, pos, area);
	return result | GUITextZone::printWithParams(dst, pos, area);
};

bool GUIButton :: controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) {
	const bool was_pushed = (getState() == STATE_SELECTED);
	bool result = GUITextZone::controlHandler(key, key_down, local_pos, in_valid_zone);
	const bool is_pushed = (getState() == STATE_SELECTED);
	result |= (was_pushed ^ is_pushed);
	if(result) {
		needRedraw();
	}
	return result;
};

}; // end GUI

}; // end Graphic32
