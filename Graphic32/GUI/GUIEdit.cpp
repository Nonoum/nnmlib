#include "GUIEdit.h"
#include "SystemBuffer.h"
#include <algorithm> // std::equal_range
#include <functional> // std::unary_function

namespace Graphic32 {

namespace GUI {

static inline bool XorRect(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area, uint32 color) {
	return dst.xorWith(color, displacement.x() + local_area.x(),
								displacement.y() + local_area.y(),
								local_area.width(),
								local_area.height());
};

struct SpaceLike : public std::unary_function<char, bool> {
	bool operator ()(char c) const {
		return (c == 32) || (c == 13) || (c == 10) || (c == 9);
	}
};

template <bool forward_direction, class Stopper> size_t FindTextBound(const std::string& text, size_t index, Stopper stopper) {
	if(forward_direction) {
		size_t length = text.length();
		for(size_t i = index; i < length; ++i) {
			if(stopper(text[i])) {
				return i;
			}
		}
		return length;
	} else {
		for(size_t i = index+1; i != 0;) {
			--i;
			if(stopper(text[i])) {
				return size_t(i+1);
			}
		}
		return size_t(0);
	}
	return index;
};

class RectHighlighter {
	Img32& dst;
	const Geometry::Point displacement;
	const Geometry::Rect local_area;
	const uint32 color;
public:
	RectHighlighter(Img32& _dst, const Geometry::Point& _displacement, const Geometry::Rect& _local_area, uint32 _color)
			: dst(_dst), displacement(_displacement), local_area(_local_area), color(_color) {
	};
	bool operator()(const Geometry::Rect& rect) const {
		return XorRect(dst, displacement, rect.checkedIntersection(local_area), color);
	};
};

class RectInvalidator {
	Composition::Surface* const sf;
	const Geometry::Point offset;
public:
	RectInvalidator(Composition::Surface* _sf, Geometry::Point _offset)
			: sf(_sf), offset(_offset) {
	};
	bool operator()(const Geometry::Rect& rect) const {
		sf->invalidate(rect + offset);
		return true;
	};
};


GUIEdit :: GUIEdit(const Geometry::Rect& rect, const FontInfo& info, DrawType _type, size_t _id,
	Geometry::Alignment _x_align, Geometry::Alignment _y_align)
		: GUITextZone(rect, info, _type, false, _id, NULL, _x_align, _y_align) {

	char_areas.reserve(64);
	row_ranges.reserve(16);
	m_left_down = false; // mouse left wasn't pushed
	capture_on = false; // not capturing now
	cursor = 0; // place to insert new characters
	proposed_char_width = info.width > 0 ? info.width : 1;
};

bool GUIEdit :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	bool result = GUITextZone::renderArea(dst, displacement, local_area);
	result |= processRectRange(cur_sel, RectHighlighter(dst, displacement, local_area, 0x00FFFF00));
	result |= drawCursor(dst, displacement, local_area, cursor);
	return result;
};

std::string GUIEdit :: getSelectedText() const {
	if(cur_sel.isEmpty()) {
		return "";
	}
	return std::string(text.begin() + cur_sel.getStart(), text.begin() + cur_sel.getEnd());
};

bool GUIEdit :: controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) {
	bool result = GUITextZone::controlHandler(key, key_down, local_pos, in_valid_zone);
	bool was_keyboard_event;
	result |= keyboardSubHandler(key, key_down, was_keyboard_event);

	if(! was_keyboard_event) {
		result |= mouseSubHandler(key, key_down, local_pos, in_valid_zone);
	}
	return result;
};

bool GUIEdit :: charHandler(char c, bool key_down) {
	bool result = GUITextZone::charHandler(c, key_down);
	if(! key_down) {
		return result;
	}
	size_t other_start = cur_sel.getEnd();
	Range next_sel;
	bool add_char = false;
	switch (c) {
	case 8: // backspace
		if((cursor != 0) && (cur_sel.isEmpty())) {
			--cursor;
		} else if(! cur_sel.isEmpty()) {
			cursor = cur_sel.getStart();
		}
		next_sel = Range(cursor, cursor);
		break;
	default:
		cursor = cur_sel.getStart();
		add_char = true;
		break;
	};
	std::string str(text.begin(), text.begin() + cursor);
	if(add_char) {
		char tmp[2] = {c, 0};
		str.append(tmp);
		++cursor;
		next_sel = Range(cursor, cursor);
	}
	str.append(text.begin() + other_start, text.end());
	cur_sel = next_sel;
	alignOnSetText(false);
	result |= setText(str.c_str());
	alignOnSetText(true);
	if(result) {
		needRedraw();
	}
	return result;
};

bool GUIEdit::RectYComparator :: operator ()(const Geometry::Rect& first, const Geometry::Rect& second) const {
	const ssize yd_diff = first.y() - second.y();
	const ssize yu_diff = first.getYEnd() - second.getYEnd();
	if(yd_diff * yu_diff <= 0) { // (first) inside of (second) or (second) inside of (first) by Y coordinate
		return false; // not less, not greater by Y
	}
	return yu_diff > 0;
};

bool GUIEdit::RectXComparator :: operator ()(const Geometry::Rect& first, const Geometry::Rect& second) const {
	const ssize xl_diff = first.x() - second.x();
	const ssize xr_diff = first.getXEnd() - second.getXEnd();
	if(xl_diff * xr_diff <= 0) { // (first) inside of (second) or (second) inside of (first) by X coordinate
		return false; // not less, not greater by X
	}
	return xl_diff < 0;
};

size_t GUIEdit :: findCharFor(ssize x, ssize y) const {
	const Geometry::Rect matcher(x, y, 1, 1);
	const auto y_range = std::equal_range(char_areas.begin(), char_areas.end(), matcher, RectYComparator());

	if(y_range.first != y_range.second) { // can match characters on some line (Y matches)
		const auto x_range = std::equal_range(y_range.first, y_range.second, matcher, RectXComparator());

		if(x_range.first == x_range.second) {
			if(x_range.first == y_range.first) { // before first on line, return index of first
				return y_range.first - char_areas.begin();
			} else { // after last on line, return index of newline marker
				const char* start = text.c_str();
				const char* end = start + (y_range.first - char_areas.begin()); // start of current line
				end = TextPrinter::reachLineEnd(end); // get position of newline marker
				return end - start;
			}
		} // else - exact match found
		return x_range.first - char_areas.begin();
	}
	return y_range.first - char_areas.begin();
};

bool GUIEdit::RangeComparator :: operator ()(const Range& first, const Range& second) const {
	// TODO: for very big ranges use something smarter than 'long' here
	const long start_diff = first.getStart() - second.getStart();
	const long end_diff = first.getEnd() - second.getEnd();
	if(start_diff * end_diff <= 0) {
		return false;
	}
	return start_diff < 0;
};

size_t GUIEdit :: findRowFor(size_t char_index) const {
	const auto result = std::equal_range(row_ranges.begin(), row_ranges.end(), Range(char_index, char_index+1), RangeComparator());
	// row with returned index may not contain (char_index)
	if(result.first == result.second) { // not found
		if(result.first != row_ranges.begin()) { // and have previous rows
			// returning index of previous row, cause missed (char_index) between rows is pointing
			// to new-line markers, that logically belongs to previous row.
			return (result.first - row_ranges.begin()) - 1;
		}
	}
	return result.first - row_ranges.begin();
};

bool GUIEdit :: updateSelection(ssize x, ssize y, bool key_down) {
	bool result = true;
	const size_t index = findCharFor(x, y);
	if(key_down) { // user starts selecting
		cur_sel.first() = index; // may be hit, may be not
		cur_sel.last() = index;
		if(prev_sel.isEmpty()) { // no selection yet, and wasn't, so - no changes
			result = false;
		}
	} else { // user continuing selecting
		if(cur_sel.last() == index) { // no changes in existed selection
			result = false;
		} else {
			cur_sel.last() = index;
		}
	}
	if(result) {
		needRedraw();
	}
	return true;
};

bool GUIEdit :: keyboardSubHandler(Controls key, bool key_down, bool& was_keyboard_event) {
	bool local_result = false;
	const size_t areas_cnt = char_areas.size();
	prev_sel = cur_sel; // copying selection to optimize redraw later
	was_keyboard_event = true;
	size_t row, next_row;

	switch (key) { // keyboard selection and cursor controls
	case KEY_CAPTURE:
		capture_on = key_down;
		return local_result; // out here
	case KEY_LEFT: // move to previous
		if((! key_down) || (! cur_sel.last())) { // key_up or can't move back
			return local_result; // no changes, out
		}
		--cur_sel.last();
		break;
	case KEY_RIGHT: // move to next
		if((! key_down) || (cur_sel.last() >= areas_cnt)) { // key_up or can't move forward
			return local_result; // no changes, out
		}
		++cur_sel.last();
		break;
	case KEY_YSTART:
		if((! key_down) // key_up or
			|| ((! cur_sel.last()) && (cur_sel.isEmpty()))) { // or already on the beginning
			return local_result; // no changes, out
		}
		cur_sel.last() = 0;
		break;
	case KEY_YEND:
		if((! key_down) // key_up or
			|| ((cur_sel.last() >= areas_cnt) && (cur_sel.isEmpty()))) { // or already on the beginning
			return local_result; // no changes, out
		}
		cur_sel.last() = areas_cnt;
		break;
	case KEY_XSTART:
		if(! key_down) {
			return local_result;
		}
		cur_sel.last() = row_ranges[findRowFor(cursor)].getStart();
		break;
	case KEY_XEND:
		if(! key_down) {
			return local_result;
		}
		cur_sel.last() = row_ranges[findRowFor(cursor)].getEnd();
		break;
	case KEY_UP:
	case KEY_DOWN:
		if(! key_down) {
			return local_result;
		}
		row = findRowFor(cursor);
		if((key == KEY_UP) && (row == 0)) {
			return local_result;
		}
		if((key == KEY_DOWN) && (row >= row_ranges.size()-1)) {
			return local_result;
		}
		next_row = (key == KEY_UP) ? row-1 : row+1;
		if(row_ranges[next_row].length() < row_ranges[row].localOf(cursor)) {
			cur_sel.last() = row_ranges[next_row].length(); // local index for the row to jump
		} else {
			cur_sel.last() = row_ranges[row].localOf(cursor); // local index for the row to jump
		}
		cur_sel.last() = row_ranges[next_row].globalOf(cur_sel.last()); // making global index
		break;
	case KEY_COPY:
		if(! key_down) {
			return local_result;
		}
		SystemBuffer::put( getSelectedText().c_str() );
		return local_result;
	case KEY_PASTE:
		if(! key_down) {
			return local_result;
		}
		{
			std::string center = SystemBuffer::get();
			std::string start(text.begin(), text.begin() + cur_sel.getStart());
			cursor = start.length() + center.length(); // new cursor position
			std::string end(text.begin() + cur_sel.getEnd(), text.end());
			cur_sel = Range(cursor, cursor);
			std::string total = start + center + end;
			alignOnSetText(false);
			setText(total.c_str());
			alignOnSetText(true);
		}
		break;
	default:
		was_keyboard_event = false;
		return local_result;
	};
	if(! capture_on) {
		cur_sel.first() = cur_sel.last();
	}
	local_result = true;
	cursor = cur_sel.last(); // correcting cursor position
	// invalidating changed areas
	Geometry::Point scrolling_offset(0, 0);
	scrollLocalPoint(scrolling_offset);
	scrolling_offset = -scrolling_offset;

	processRectRange( getSelectionChanges(), RectInvalidator(surface(this), scrolling_offset) ); // selection changes
	invalidate(getCursorRect(cursor) + scrolling_offset); // current cursor area
	invalidate(getCursorRect(prev_sel.last()) + scrolling_offset); // previous cursor area
	return local_result;
};

bool GUIEdit :: mouseSubHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) {
	const bool hit = containsPoint(local_pos) & in_valid_zone;
	scrollLocalPoint(local_pos);
	const Geometry::Point text_start_pos = local_pos - getPrintingOffset();
	if(! hit) {
		return false;
	}
	bool local_result = false;
	prev_sel = cur_sel; // copying selection to optimize redraw later
	size_t index;
	switch (key) { // mouse controls
	case KEY_MOUSE_LEFT:
		if(m_left_down == key_down) { // unexpected, probably key_up after doubleclick
			break;
		}
		local_result |= updateSelection(text_start_pos.x(), text_start_pos.y(), key_down);
		m_left_down = key_down;
		break;
	case KEY_MOUSE_MOVE:
		if(m_left_down) {
			local_result |= updateSelection(text_start_pos.x(), text_start_pos.y(), false);
		}
		break;
	case KEY_MOUSE_DOUBLE:
		if(! text.length()) {
			return local_result;
		}
		index = findCharFor(text_start_pos.x(), text_start_pos.y());
		if(index >= text.length()) {
			return local_result; // click after all text
		}
		if(! SpaceLike()(text[index])) { // select range of non-spaces
			cur_sel.first() = FindTextBound<false>(text, index, SpaceLike());
			cur_sel.last() = FindTextBound<true>(text, index, SpaceLike());
		} else { // select range of spaces
			cur_sel.first() = FindTextBound<false>(text, index, std::not1(SpaceLike()));
			cur_sel.last() = FindTextBound<true>(text, index, std::not1(SpaceLike()));
		}
		break;
	default:
		return local_result;
	};
	local_result = true;
	cursor = cur_sel.last(); // correcting cursor position
	needRedraw();
	return local_result;
};

void GUIEdit :: beforeTextSet(const char* text_to_set) {
	assert(getPrinter().get() != NULL);
	if(getPrinter().get() != NULL) {
		getPrinter().get()->getCharAreas(text_to_set, &char_areas, NULL);
	}

	cur_sel = Range(cur_sel.getStart(), cur_sel.getStart());
	prev_sel = cur_sel;
	// filling rows ranges info
	row_ranges.clear();
	const char* text_start = text_to_set;
	const char* start = text_start;
	const char* end = start;
	do {
		end = TextPrinter::reachLineEnd(end);
		row_ranges.push_back( Range(end-text_start, start-text_start) );
		end = TextPrinter::passNewlineMarker(end);
		start = end;
	} while(*end != '\0');
};

GUIEdit::Range GUIEdit :: getSelectionChanges() const {
	if(cur_sel.isEmpty() && (! prev_sel.isEmpty())) { // now empty, but wasn't
		return prev_sel; // revert previous selection
	}
	if(cur_sel.getStart() == prev_sel.getEnd()) { // reverted forwardly
		return Range(prev_sel.getStart(), cur_sel.getEnd()); // revert both areas
	}
	if(cur_sel.getEnd() == prev_sel.getStart()) { // reverted backwardly
		return Range(cur_sel.getStart(), prev_sel.getEnd()); // revert both areas
	}
	if(cur_sel.getStart() == prev_sel.getStart()) { // forward continuation
		return Range(prev_sel.getEnd(), cur_sel.getEnd()); // select added area
	}
	if(cur_sel.getEnd() == prev_sel.getEnd()) { // backward continuation
		return Range(cur_sel.getStart(), prev_sel.getStart()); // select added area
	}
	return Range(0, 0);
};

template <class Functor> bool GUIEdit :: processRectRange(Range range, Functor functor) {
	const Geometry::Point test_start_pos = getPrintingOffset();
	bool result = false;
	if(! range.isEmpty()) {
		size_t first = range.getStart();
		size_t last = range.getEnd();
		for(size_t i = first; i < last; ) {
			// optimization of highlighting. gathering batches of highlights in rows.
			// supposing that characters all mono-height and there is no emptiness between them.
			size_t sub_start = i;
			size_t sub_end = sub_start + 1;
			ssize y_value = char_areas[sub_start].y();
			for( ; sub_end < last ; ++sub_end) {
				if(y_value != char_areas[sub_end].y()) {
					break;
				}
			}
			// range for row is known: [sub_start ; sub_end)
			ssize x = test_start_pos.x() + char_areas[sub_start].x();
			ssize y = test_start_pos.y() + char_areas[sub_start].y();
			ssize w = char_areas[sub_end-1].x() - char_areas[sub_start].x() + char_areas[sub_end-1].width();
			ssize h = char_areas[sub_start].height();
			result |= functor(Geometry::Rect(x, y, w, h));
			i = sub_end; // jumping to end of range which is done
		}
	}
	return result;
};

Geometry::Rect GUIEdit :: getCursorRect(size_t cursor_pos) const {
	const Geometry::Point text_start_pos = getPrintingOffset();
	ssize x, y, w;
	ssize cursor_height = 1;
	if(getPrinter().get()) {
		cursor_height = getPrinter().get()->getCHeight() / 4; // cursor height is 25% of character
	}
	const size_t n_areas = char_areas.size();
	if((cursor_pos == 0) && (n_areas == 0)) { // can't take parameter from char area
		x = 0;
		y = 0;
		w = proposed_char_width;
	} else if(cursor_pos >= n_areas) { // can't take parameter from char area
		// char_areas.size() > 0 here
		// row_ranges.size() is always > 0
		if(row_ranges[row_ranges.size()-1].getEnd() != char_areas.size()) { // ending on newline marker
			x = 0;
			y = char_areas[n_areas-1].y() - char_areas[n_areas-1].height(); // position of newline marker from upper line, shifted on one character height
		} else {
			x = char_areas[n_areas-1].x() + char_areas[n_areas-1].width();
			y = char_areas[n_areas-1].y();
		}
		w = proposed_char_width;
	} else { // can take parameters
		x = char_areas[cursor_pos].x();
		y = char_areas[cursor_pos].y();
		w = char_areas[cursor_pos].width();
		if(! w) { // cursor is on empty-size character ('\n' or '\r')
			w = proposed_char_width;
		}
	}
	return Geometry::Rect(x + text_start_pos.x(), y + text_start_pos.y(), w, cursor_height);
};

bool GUIEdit :: drawCursor(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area, size_t cursor_pos) {
	return XorRect(dst, displacement, getCursorRect(cursor_pos) & local_area, 0x007F7F7F);
};

}; // end GUI

}; // end Graphic32
