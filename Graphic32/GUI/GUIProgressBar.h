#pragma once
#include "GUIObject.h"

namespace Graphic32 {

namespace GUI {

/*
	Simple progress bar object. Draws rectangle with specified color as progress status.
*/
class GUIProgressBar : public GUIObject {
	size_t lim, val;
	uint32 d_color, u_color;
public:
	GUIProgressBar(const Geometry::Rect& rect, size_t limit, size_t value, uint32 done_color, uint32 undone_color);
	void setValue(size_t value);
	void setLimit(size_t limit);
	void setColors(uint32 done_color, uint32 undone_color);
protected:
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
};

}; // end GUI

}; // end Graphic32
