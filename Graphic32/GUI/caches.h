#pragma once
#include "ObjectSet.h"
#include "MaskDescription.h"
#include "PrinterDescription.h"
#include "ImageDescription.h"

namespace Graphic32 {

namespace GUI {

typedef ObjectSet<ImageDescription, SharedImage, &ImageDescription::cleanable> ImagesCacheType;
typedef ObjectSet<PrinterDescription, SharedPrinter, &PrinterDescription::cleanable> PrintersCacheType;
typedef ObjectSet<MaskDescription, SharedMask, &MaskDescription::cleanable> MasksCacheType;

// single cache for images
ImagesCacheType& Images();

// single cache for printers
PrintersCacheType& Printers();

// array of mask caches. with size = GUIObject::DRAW_MASKS_COUNT
MasksCacheType& Masks(size_t index);

// optional function for periodical cleaning of cache (not thread-safe)
void ClearCaches();

}; // end GUI

}; // end Graphic32
