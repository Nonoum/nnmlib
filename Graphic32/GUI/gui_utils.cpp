#include "gui_utils.h"
#include "GUIObject.h"

namespace Graphic32 {

namespace GUI {

CallbackParams :: CallbackParams(GUIObject* _caller, Controls _action, bool _key_down)
		: caller(_caller), action(_action), key_down(_key_down) { // guaranteed parameters

	screen = NULL;
	state = 0;
	length = 0;
	index = 0;
	hit_area = false;
};

CallbackInterface :: CallbackInterface()
		: pressed(false), key_id(0) {
};

CallbackInterface :: ~CallbackInterface() {
};

bool CallbackInterface :: hitCompleted(const CallbackParams& params) {
	const size_t key = params.caller->getID();
	if(! params.hit_area) {
		pressed = false;
		return false;
	}
	const bool res = (! params.key_down) && pressed && (key_id == key);
	key_id = key;
	pressed = params.key_down;
	return res;
};

std::ostream& operator <<(std::ostream& dst, const CallbackParams& params) {
	dst << params.state << " : " << params.index << " : " << params.length << std::endl
		<< params.hit_area << " : " << params.local_pos << std::endl
		<< params.screen << " : " << params.area << std::endl
		<< params.caller << " : " << params.action << " : " << params.key_down << std::endl;
	return dst;
};

}; // end GUI

}; // end Graphic32
