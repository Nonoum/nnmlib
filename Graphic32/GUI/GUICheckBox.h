#pragma once
#include "GUIButton.h" // includes all needed classes

namespace Graphic32 {

namespace GUI {

/*
	Check Box implementation. Uses default check images.
	Callback is being called for any action as it's default behaviour,
	in this basic call parameter 'length' is 0 (default).

	On KEY_MOUSE_LEFT down event callback is being called secondary
	with patameters:
		'hit_area' == true;
		'length' == 1;
		'state' == 1 if state has changed to checked;
		'state' == 0 if state has changed to unchecked;
	For comfortable using object id can be specified on constructing GUICheckBox
	and checked in callback:
		size_t id = params.caller->getID();
*/
class GUICheckBox : public GUIObject {
	GUIButton* button;
	GUITextZone* text;
	bool is_checked;
public:
	GUICheckBox(const Geometry::Rect& rect, const FontInfo& info, DrawType t = DRAW_NONE, size_t _id = 0,
			Geometry::Alignment x_text_align = Geometry::ALIGN_BEGIN, Geometry::Alignment y_text_align = Geometry::ALIGN_CENTER);
	void setChecked(bool checked);
	bool isChecked() const;
	bool setText(const char* src);
	void setTextColor(uint32 color, State s);
	void setColor(uint32 color, State s);
private:
	static Geometry::Rect getButtonRect(const Geometry::Rect& total_rect);
	static Geometry::Rect getTextRect(const Geometry::Rect& total_rect);
	// internal button callback -------------------------------------------------------------------------------
	class ButtonCallback : public CallbackInterface {
		GUICheckBox& owner;
	public:
		ButtonCallback(GUICheckBox& _owner);
		bool action(CallbackParams& params) override;
	};
};

}; // end GUI

}; // end Graphic32
