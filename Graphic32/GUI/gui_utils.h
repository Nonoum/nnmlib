#pragma once
#include "../Img32.h"
#include "../AlphaMask.h"
#include "../TextPrinter.h"
#include "../../Geometry/Rect.h"
#include "../../Algorithms/Extras.h"
#include <set> // main containers
#include <vector> // , that used
#include <string> // , in library
#include <assert.h>
#include <iostream>
#include <memory>

namespace Graphic32 {

namespace GUI {

enum Controls {
	// user implementation, internal/external using
	KEY_MOUSE_LEFT = 0,
	KEY_MOUSE_RIGHT,
	KEY_MOUSE_MOVE,
	KEY_MOUSE_WHEEL_UP,
	KEY_MOUSE_WHEEL_DOWN,
	KEY_MOUSE_DOUBLE,
	// keyboard controls
	KEY_LEFT,
	KEY_RIGHT,
	KEY_UP,
	KEY_DOWN,
	KEY_XSTART,
	KEY_XEND,
	KEY_YSTART,
	KEY_YEND,
	KEY_CAPTURE,
	KEY_COPY,
	KEY_PASTE,
	// internal implementation, external using
	KEY_RENDER_ACTION
};

class GUIObject; // forward declaration here

struct CallbackParams {
	size_t state;
	size_t index;
	size_t length;
	bool hit_area;
	Geometry::Point local_pos;
	const Img32* screen;
	Geometry::Rect area;
	GUIObject* const caller;
	const Controls action;
	const bool key_down;

	CallbackParams(GUIObject* _caller, Controls _action, bool _key_down); // basically guaranteed parameters
};

std::ostream& operator <<(std::ostream& dst, const CallbackParams& params);


class CallbackInterface {
	size_t key_id;
	bool pressed;
public:
	CallbackInterface();
	virtual ~CallbackInterface();
	// helper function for common use-case, process event if it returned true
	bool hitCompleted(const CallbackParams& params);
	// action must return true if caller changes there
	virtual bool action(CallbackParams&) = 0;
};

}; // end GUI

}; // end Graphic32
