#pragma once
#include "gui_utils.h"

namespace Graphic32 {

namespace GUI {

template <class Key, class Value> bool AllCleanable(const Key& key, const Value& value) {
	return true;
};

// set of custom pairs of objects (sorted with 'less' operator)
template <class Key, class Value, bool (*cleanable)(const Key& key, const Value& value) = AllCleanable<Key, Value> >
	class ObjectSet : private NonCopyable {

	typedef std::map<Key, Value> CollectionType;
	CollectionType map;
public:
	bool has(const Key& key) const { // checks whether set has specified key
		return map.find(key) != map.end();
	};
	Value find(const Key& key) const { // returns appropriate Value or dummy Value() if key not found
		auto it = map.find(key);
		return it == map.end() ? Value() : it->second;
	};
	bool set(const Key& key, const Value& value) { // returns true if here already was such key
		const bool result = map.find(key) == map.end();
		map[key] = value;
		return result;
	};
	bool removeCleanable() { // optional method. returns true if any of elements was removed
		bool result = false;
		while(1) {
			bool removed = false;
			for(auto it = map.begin(); it != map.end(); ++it) {
				if(cleanable(it->first, it->second)) {
					map.erase(it);
					removed = true;
					result = true;
					break;
				}
			}
			if(! removed) {
				break;
			}
		}
		return result;
	};
};

}; // end GUI

}; // end Graphic32
