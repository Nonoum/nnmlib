#pragma once
#include "GUITextZone.h"

namespace Graphic32 {

namespace GUI {

/*
	Completed button class.
	Supports logic for displaced drawing on pushing.
	Supports two constructors:
		1) button with text;
		2) button without text (for using only cached image on background);
*/

class GUIButton : public GUITextZone {
	Geometry::Point drag;
public:
	GUIButton(const Geometry::Rect& rect, const FontInfo& info, DrawType _type, size_t _id = 0,
		Geometry::Alignment _x_align = Geometry::ALIGN_CENTER, Geometry::Alignment _y_align = Geometry::ALIGN_CENTER);
	GUIButton(const Geometry::Rect& rect, DrawType _type, size_t _id = 0, const char* image_cached_name = NULL);

	Geometry::Point getDrag() const; // returns offset for drawing on STATE_SELECTED
	void setDrag(const Geometry::Point& _drag);
protected:
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
	bool controlHandler(Controls key, bool key_down, Geometry::Point local_pos, bool in_valid_zone) override;
};

}; // end GUI

}; // end Graphic32
