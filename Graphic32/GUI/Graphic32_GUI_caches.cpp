#include "caches.h"
#include "GUIObject.h"
#include <stdexcept>

namespace Graphic32 {

namespace GUI {

ImagesCacheType& Images() {
	static ImagesCacheType cache;
	return cache;
};

PrintersCacheType& Printers() {
	static PrintersCacheType cache;
	return cache;
};

MasksCacheType& Masks(size_t index) {
	static MasksCacheType cache[GUIObject::DRAW_MASKS_COUNT];
	assert(index < GUIObject::DRAW_MASKS_COUNT);
	if(index >= GUIObject::DRAW_MASKS_COUNT) {
		throw std::out_of_range("Attempted to access non existent GUI masks cache");
	}
	return cache[index];
};

void ClearCaches() {
	for(size_t i = 0; i < GUIObject::DRAW_MASKS_COUNT; ++i) {
		Masks(i).removeCleanable();
	}
	Images().removeCleanable();
	Printers().removeCleanable();
};

}; // end GUI

}; // end Graphic32
