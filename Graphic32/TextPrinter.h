#pragma once
#include "Img32.h"
#include "AlphaMask.h"
#include "../Geometry/Rect.h"
#include <vector>

namespace Graphic32 {

/*
	Start parameters of font for TextPrinter object.
*/

struct FontInfo {
	ssize width;
	ssize height;
	ubyte power;
	FontInfo(ssize _width, ssize _height, ubyte _power);
	bool operator <(const FontInfo& right) const;
};

/*
	Completed printer of text. Supports:
		1. printing text on Img32 output;
		2. printing with output zone restriction;
		3. making different font configurations (see FontInfo above);
		4. taking fit size for string (size in pixels for full text printed);
		5. taking characters areas to vector of rects;
		
	Usage:
		TextPrinter::initFonts("nnmlib1.0_fonts.cimgs"); // single time resources initialization
		TextPrinter pr;
		FontInfo font(8, 16, 120);
		pr.create(font);
		Img32 output;
		// ... some output initialization
		Geometry::Rect zone(100, 200, 300, 400);
		size_t n_printed_chars = pr.print(output, 50, 50, 0x00FF0000, "Hello\nhere", &zone);
*/
class TextPrinter {
	static Img32 font_original;
	AlphaMask font;
	ssize c_width, c_height; // character sizes
public:
	// no width getter for future possibility of non-mono-width fonts
	ssize getCHeight() const;
	size_t print(Img32& dst, ssize x, ssize y, uint32 color, const char* str,
		const Geometry::Rect* zone = NULL) const; // prints text to (dst) from (str) on (x, y) with decreasing (y) by passing lines.
	bool create(const FontInfo& info); // initializes printer object.
	Geometry::Size getFitSize(const char* str) const; // returns size in pixels that exactly fits the printed (str).
	size_t getCharAreas(const char* str, std::vector<Geometry::Rect>* char_areas,
		const Geometry::Rect* zone = NULL) const; // returns count of areas, and areas array in (*char_areas).
	static size_t nLines(const char* str); // count of lines in (str).
	static size_t nColumns(const char* str); // count of columns in (str).
	static const char* passNewlineMarker(const char* str); // returns pointer to next character in (str) after one new_line markers sequence.
	static const char* reachLineEnd(const char* str); // returns pointer to first character of next new_line markers sequence.
	static bool initFonts(const char* cimg_fonts_file_name); // must be called before any call of ::create()
private:
	bool printChar(Img32& dst, ssize x, ssize y, ssize width, ssize height,
		ssize xoffs, ssize yoffs, uint32 color, char c) const;
	static size_t processString(Img32* dst, ssize x, ssize y, uint32 color, const char* str,
		size_t* nl, size_t* nc, const TextPrinter* pr,
		const Geometry::Rect* zone = NULL, std::vector<Geometry::Rect>* char_areas = NULL);
};

}; // end Graphic32
