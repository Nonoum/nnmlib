#include "functions.h"
#include "Pixel/head.h"
#include "Math/head.h"
#include <math.h>

namespace Graphic32 {

#define OPTIMIZE_DRAWRECT_HARDLY

#ifdef OPTIMIZE_DRAWRECT_HARDLY

#include "../Helpers/MinMax.h"
using Helpers::min2;

template <class T> inline T GridDivide(T start, T end, T divider) {
	T start_area = start / divider;
	T end_area = (end + divider - 1) / divider;
	return end_area - start_area;
};

bool DrawRect(uint32* dst, ssize dst_w, ssize dst_h, ssize x, ssize y, usize cx, usize cy,
		const uint32* src, ssize src_w, ssize src_h, ssize toffsx, ssize toffsy, Drawers drawer, ubyte level) {

	if((! dst) || (! src)) {
		return false;
	}
	ssize xs, ys, xe, ye, xofs, yofs, i, j, src_y, xstart;
	union {
		uint32 value;
		ubyte bytes[];
	} pixel;
	xs = x;
	xe = x + cx;
	if((xe <= 0) || (xs >= dst_w)) {
		return false; // out of range
	}
	ys = y;
	ye = y + cy;
	if((ye <= 0) || (ys >= dst_h)) {
		return false; // out of range
	}
	xofs = yofs = 0;
	if(xs < 0) {
		xofs = -xs;
		xs = 0;
	}
	if(ys < 0) {
		yofs = -ys;
		ys = 0;
	}
	if(xe > dst_w) {
		xe = dst_w;
	}
	if(ye > dst_h) {
		ye = dst_h;
	}
	//--//
	src_y = (yofs + toffsy) % src_h;
	if(src_y < 0) {
		src_y += src_h;
	}
	xstart = (xofs + toffsx) % src_w;
	if(xstart < 0) {
		xstart += src_w;
	}
	const ssize ytimes = GridDivide(src_y, src_y + ye - ys, src_h);
	const ssize xtimes = GridDivide(xstart, xstart + xe - xs, src_w);
	// all case-labels have copy-pasted code for optimization
	switch (drawer) {
	case D_SOLID: {
			// WARNING: the following HARDCORE approved for appropriate audiences only.
			uint32* dst_ptr = dst + ys * dst_w;
			y = ys; // initial Y destination
			ssize ysrc = src_y; // Y source for first cycle
			for(i = 0; i < ytimes; ++i) {
				const uint32* src_ptr = src + ysrc * src_w;
				const ssize ylimit = min2(src_h, ysrc + ye - y); // limit for Y source on this iteration
				y += ylimit - ysrc; // Y destination will move on this value after cycle
				for(; ysrc < ylimit; ++ysrc) {
					x = xs;
					ssize xsrc = xstart; // X source for first cycle
					for(j = 0; j < xtimes; ++j) {
						const ssize xlength = min2(src_w, xsrc + xe - x) - xsrc;
						uint32* dst_row = dst_ptr + x;
						const uint32* src_row = src_ptr + xsrc;
						x += xlength;
						xsrc = 0;
						// unpacking cycle
						const ssize singles_cnt = xlength & 0x03;
						const ssize last_block_offset = xlength - singles_cnt;
						ssize block_offset = 0;
						for(; block_offset < last_block_offset; block_offset += 4) {
							dst_row[block_offset] = src_row[block_offset];
							dst_row[block_offset+1] = src_row[block_offset+1];
							dst_row[block_offset+2] = src_row[block_offset+2];
							dst_row[block_offset+3] = src_row[block_offset+3];
						}
						dst_row += block_offset;
						src_row += block_offset;
						for(ssize index = 0; index < singles_cnt; ++index) {
							dst_row[index] = src_row[index];
						}
					}
					dst_ptr += dst_w;
					src_ptr += src_w;
				}
				ysrc = 0; // Y source for other cycles
			}
			break;
		}
	case D_ALPHA: {
			// WARNING: the following HARDCORE approved for appropriate audiences only.
			uint32* dst_ptr = dst + ys * dst_w;
			y = ys; // initial Y destination
			ssize ysrc = src_y; // Y source for first cycle
			for(i = 0; i < ytimes; ++i) {
				const uint32* src_ptr = src + ysrc * src_w;
				const ssize ylimit = min2(src_h, ysrc + ye - y); // limit for Y source on this iteration
				y += ylimit - ysrc; // Y destination will move on this value after cycle
				for(; ysrc < ylimit; ++ysrc) {
					x = xs;
					ssize xsrc = xstart; // X source for first cycle
					for(j = 0; j < xtimes; ++j) {
						const ssize xlength = min2(src_w, xsrc + xe - x) - xsrc;
						uint32* dst_row = dst_ptr + x;
						const uint32* src_row = src_ptr + xsrc;
						x += xlength;
						xsrc = 0;
						// unpacking cycle
						const ssize singles_cnt = xlength & 0x03;
						const ssize last_block_offset = xlength - singles_cnt;
						ssize block_offset = 0;
						for(; block_offset < last_block_offset; block_offset += 4) {
							AlphaMerge(dst_row[block_offset], src_row[block_offset]);
							AlphaMerge(dst_row[block_offset+1], src_row[block_offset+1]);
							AlphaMerge(dst_row[block_offset+2], src_row[block_offset+2]);
							AlphaMerge(dst_row[block_offset+3], src_row[block_offset+3]);
						}
						dst_row += block_offset;
						src_row += block_offset;
						for(ssize index = 0; index < singles_cnt; ++index) {
							AlphaMerge(dst_row[index], src_row[index]);
						}
					}
					dst_ptr += dst_w;
					src_ptr += src_w;
				}
				ysrc = 0; // Y source for other cycles
			}
			break;
		}
	case D_ALPHAKEEP: {
			// WARNING: the following HARDCORE approved for appropriate audiences only.
			uint32* dst_ptr = dst + ys * dst_w;
			y = ys; // initial Y destination
			ssize ysrc = src_y; // Y source for first cycle
			for(i = 0; i < ytimes; ++i) {
				const uint32* src_ptr = src + ysrc * src_w;
				const ssize ylimit = min2(src_h, ysrc + ye - y); // limit for Y source on this iteration
				y += ylimit - ysrc; // Y destination will move on this value after cycle
				for(; ysrc < ylimit; ++ysrc) {
					x = xs;
					ssize xsrc = xstart; // X source for first cycle
					for(j = 0; j < xtimes; ++j) {
						const ssize xlength = min2(src_w, xsrc + xe - x) - xsrc;
						uint32* dst_row = dst_ptr + x;
						const uint32* src_row = src_ptr + xsrc;
						x += xlength;
						for(ssize index = 0; index < xlength; ++index) {
							AlphaMergeKeep(dst_row[index], src_row[index]);
						}
						xsrc = 0;
					}
					dst_ptr += dst_w;
					src_ptr += src_w;
				}
				ysrc = 0; // Y source for other cycles
			}
			break;
		}
	case D_THRESHOLD: {
			// WARNING: the following HARDCORE approved for appropriate audiences only.
			uint32* dst_ptr = dst + ys * dst_w;
			y = ys; // initial Y destination
			ssize ysrc = src_y; // Y source for first cycle
			for(i = 0; i < ytimes; ++i) {
				const uint32* src_ptr = src + ysrc * src_w;
				const ssize ylimit = min2(src_h, ysrc + ye - y); // limit for Y source on this iteration
				y += ylimit - ysrc; // Y destination will move on this value after cycle
				for(; ysrc < ylimit; ++ysrc) {
					x = xs;
					ssize xsrc = xstart; // X source for first cycle
					for(j = 0; j < xtimes; ++j) {
						const ssize xlength = min2(src_w, xsrc + xe - x) - xsrc;
						uint32* dst_row = dst_ptr + x;
						const uint32* src_row = src_ptr + xsrc;
						x += xlength;
						for(ssize index = 0; index < xlength; ++index) {
							pixel.value = src_row[index];
							if(pixel.bytes[3] >= level) { // alpha >= threshold level
								dst_row[index] = pixel.value;
							}
						}
						xsrc = 0;
					}
					dst_ptr += dst_w;
					src_ptr += src_w;
				}
				ysrc = 0; // Y source for other cycles
			}
			break;
		}
	case D_MULTIPLEDALPHA: {
			// WARNING: the following HARDCORE approved for appropriate audiences only.
			register union {
				uint16 result;
				ubyte bytes[];
			} optimizer;
			uint32* dst_ptr = dst + ys * dst_w;
			y = ys; // initial Y destination
			ssize ysrc = src_y; // Y source for first cycle
			for(i = 0; i < ytimes; ++i) {
				const uint32* src_ptr = src + ysrc * src_w;
				const ssize ylimit = min2(src_h, ysrc + ye - y); // limit for Y source on this iteration
				y += ylimit - ysrc; // Y destination will move on this value after cycle
				for(; ysrc < ylimit; ++ysrc) {
					x = xs;
					ssize xsrc = xstart; // X source for first cycle
					for(j = 0; j < xtimes; ++j) {
						const ssize xlength = min2(src_w, xsrc + xe - x) - xsrc;
						uint32* dst_row = dst_ptr + x;
						const uint32* src_row = src_ptr + xsrc;
						x += xlength;
						for(ssize index = 0; index < xlength; ++index) {
							pixel.value = src_row[index]; // get src
							optimizer.result = pixel.bytes[3] * level; // multiple alpha
							pixel.bytes[3] = optimizer.bytes[1]; // write high part to alpha
							AlphaMerge(dst_row[index], pixel.value); // merge
						}
						xsrc = 0;
					}
					dst_ptr += dst_w;
					src_ptr += src_w;
				}
				ysrc = 0; // Y source for other cycles
			}
			break;
		}
	default:
		return false; // unknown mode
	};
	return true;
}; // end draw rect

#else

bool DrawRect(uint32* dst, ssize dst_w, ssize dst_h, ssize x, ssize y, usize cx, usize cy,
		const uint32* src, ssize src_w, ssize src_h, ssize toffsx, ssize toffsy, Drawers drawer, ubyte level) {

	if((! dst) || (! src)) {
		return false;
	}
	ssize xs, ys, xe, ye, xofs, yofs, i, j, src_x, src_y, ofs, ofs2, xstart;
	union {
		uint32 value;
		ubyte bytes[];
	} pixel;
	xs = x;
	xe = x + cx;
	if((xe <= 0) || (xs >= dst_w)) {
		return false; // out of range
	}
	ys = y;
	ye = y + cy;
	if((ye <= 0) || (ys >= dst_h)) {
		return false; // out of range
	}
	xofs = yofs = 0;
	if(xs < 0) {
		xofs = -xs;
		xs = 0;
	}
	if(ys < 0) {
		yofs = -ys;
		ys = 0;
	}
	if(xe > dst_w) {
		xe = dst_w;
	}
	if(ye > dst_h) {
		ye = dst_h;
	}
	//--//
	src_y = (yofs + toffsy) % src_h;
	if(src_y < 0) {
		src_y += src_h;
	}
	xstart = (xofs + toffsx) % src_w;
	if(xstart < 0) {
		xstart += src_w;
	}
	switch (drawer) {
	case D_SOLID:
		for(j = ys; j < ye; ++j, ++src_y) {
			if(src_y >= src_h) {
				src_y = 0;
			}
			ofs = j * dst_w;
			ofs2 = src_y * src_w;
			src_x = xstart;
			for(i = xs; i < xe; ++i, ++src_x) {
				if(src_x >= src_w) {
					src_x = 0;
				}
				dst[ofs + i] = src[ofs2 + src_x];
			}
		}
		break;
	case D_ALPHA:
		for(j = ys; j < ye; ++j, ++src_y) {
			if(src_y >= src_h) {
				src_y = 0;
			}
			ofs = j * dst_w;
			ofs2 = src_y * src_w;
			src_x = xstart;
			for(i = xs; i < xe; ++i, ++src_x) {
				if(src_x >= src_w) {
					src_x = 0;
				}
				AlphaMerge(dst[ofs + i], src[ofs2 + src_x]);
			}
		}
		break;
	case D_ALPHAKEEP:
		for(j = ys; j < ye; ++j, ++src_y) {
			if(src_y >= src_h) {
				src_y = 0;
			}
			ofs = j * dst_w;
			ofs2 = src_y * src_w;
			src_x = xstart;
			for(i = xs; i < xe; ++i, ++src_x) {
				if(src_x >= src_w) {
					src_x = 0;
				}
				AlphaMergeKeep(dst[ofs + i], src[ofs2 + src_x]);
			}
		}
		break;
	case D_THRESHOLD:
		for(j = ys; j < ye; ++j, ++src_y) {
			if(src_y >= src_h) {
				src_y = 0;
			}
			ofs = j * dst_w;
			ofs2 = src_y * src_w;
			src_x = xstart;
			for(i = xs; i < xe; ++i, ++src_x) {
				if(src_x >= src_w) {
					src_x = 0;
				}
				pixel.value = src[ofs2 + src_x];
				if(pixel.bytes[3] >= level) { // alpha >= threshold level
					dst[ofs + i] = pixel.value;
				}
			}
		}
		break;
	case D_MULTIPLEDALPHA:
		register union {
			uint16 result;
			ubyte bytes[];
		} optimizer;
		for(j = ys; j < ye; ++j, ++src_y) {
			if(src_y >= src_h) {
				src_y = 0;
			}
			ofs = j * dst_w;
			ofs2 = src_y * src_w;
			src_x = xstart;
			for(i = xs; i < xe; ++i, ++src_x) {
				if(src_x >= src_w) {
					src_x = 0;
				}
				pixel.value = src[ofs2 + src_x]; // get src
				optimizer.result = pixel.bytes[3] * level; // multiple alpha
				pixel.bytes[3] = optimizer.bytes[1]; // write high part to alpha
				AlphaMerge(dst[ofs + i], pixel.value); // merge
			}
		}
		break;
	default:
		return false; // unknown mode
	};
	return true;
}; // end draw rect

#endif

bool FillPolygon4p(uint32* dst, ssize width, ssize height, float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD, uint32 color) {
	if(! dst) {
		return false;
	}
	Polygon4p2d <float> polygon(xA, yA, xB, yB, xC, yC, xD, yD);
	ssize xs, xe, ys, ye, i, j, cx, cy;
	float xbuf1, xbuf2;
	ys = (ssize)floor(min4(yA, yB, yC, yD));
	ye = (ssize)ceil(max4(yA, yB, yC, yD));
	ys = max2(ys, (ssize)(0));
	ye = min2(ye, height);
	cy = ye - ys;
	uint32 *dout, *doutbuf;
	doutbuf = dst;
	doutbuf += ys * width;
	for(i = 0; i < cy; ++i, doutbuf += width) {
		if(! polygon.getXRange(float(ys + i), &xbuf1, &xbuf2)) {
			continue;
		}
		xs = (ssize)floor(xbuf1);
		xe = (ssize)ceil(xbuf2);
		xs = max2(xs, ssize(0));
		xe = min2(xe, width);
		cx = xe - xs;
		dout = doutbuf + xs;
		for(j = 0; j < cx; ++j) {
			dout[j] = color;
		}
	}
	return true;
}; // end Fill polygon 4 points

bool DrawTexture(uint32* dst, ssize width, ssize height, float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD,
		const uint32* src, ssize src_width, ssize src_height, bool full_square, Resamplers method, uint32 background_color) {

	if((! dst) || (! src)) {
		return false;
	}
	// 1. calc draw area
	ssize xs, xe, ys, ye;
	xs = ssize(floor(min4(xA, xB, xC, xD)) - 1); // x start
	xe = ssize(ceil(max4(xA, xB, xC, xD)) + 1); // x end
	ys = ssize(floor(min4(yA, yB, yC, yD)) - 1); // y start
	ye = ssize(ceil(max4(yA, yB, yC, yD)) + 1); // y end
	xe -= xs; // now cx
	ye -= ys; // now cy
	if(! BoundsCheck(width, height, xs, ys, xe, ye)) { // now xs, ys, xe, ye
		return true; // out of picture
	}
	Polygon4p2d <float> polygon(xA, yA, xB, yB, xC, yC, xD, yD);
	// 2. rotation equations (setting D-point on X-AXIS)
	float dx, dy, angle, cosa, sina, fbufx, fbufy;
	float xAsave, yAsave;
	xAsave = xA;
	yAsave = yA;
	dx = xD - xA;
	dy = yD - yA;
	if(dx == 0) {
		dx = float(0.0001);
	}
	angle = -atan(dy / dx); // angle known (radians)
	if(dx < 0) {
		angle = float(3.14159265358979323846) + angle;
	}
	cosa = cos(angle);
	sina = sin(angle);
	// x' = x * cosa - y * sina;
	// y' = x * sina + y * cosa;
	// shifting all points
	xB -= xA; xC -= xA; xD -= xA; // x
	yB -= yA; yC -= yA; yD -= yA; // y
	xA = 0; yA = 0; // A-point is center now
	// rotating all points
	fbufx = xB * cosa - yB * sina;
	fbufy = xB * sina + yB * cosa;
	xB = fbufx;
	yB = fbufy;
	fbufx = xC * cosa - yC * sina;
	fbufy = xC * sina + yC * cosa;
	xC = fbufx;
	yC = fbufy;
	fbufx = xD * cosa - yD * sina;
	fbufy = xD * sina + yD * cosa;
	xD = fbufx;
	yD = fbufy;
	// D-point is on X-AXIS now
	// 3. shift equations (setting B-point on Y-AXIS)
	float coefa; // shifting A-coefficient (only X)
	dx = xB;// - xA; (xA is 0)
	dy = yB;// - yA; (yA is 0)
	coefa = -dx / dy;
	// x'' = x' + coefa * y' , = x * cosa - y * sina + coefa * (x * sina + y * cosa)
	// y'' = y'
	// x'' = x * cosa - y * sina + x * sina * coefa + y * cosa * coefa
	// x'' = x * (cosa + sina * coefa) + y * (-sina + cosa * coefa);
	// y'' = x * (sina) + y * (cosa);
	xB = 0;
	xC = xC + coefa * yC;
	// B-point is on Y-AXIS now
	/*
	now it's like:
						|         *C
						*B
						|
						*A-----*D-----
	*/
	// 4. zoom equations (to [src_width, src_height])
	float zoomx,zoomy;
	zoomx = float(src_width - 1) / xD;
	zoomy = float(src_height - 1) / yB;
	xD = float(src_width - 1);
	yB = float(src_height - 1);
	xC *= zoomx;
	yC *= zoomy;
	// x''' = x'' * zoomx
	// y''' = y'' * zoomy
	// x''' = x * (cosa + sina * coefa) * zoomx + y * (-sina + cosa * coefa) * zoomx;
	// y''' = x * (sina * zoomy) + y * (cosa * zoomy);
	// 5. stretch equations ( setting C.x = D.x , C.y = B.y )
	float cfx, cfy; // stretching coefficients for x,y
	dx = xD - xC;
	dy = yC;
	cfx = dx / dy;
	dy = yB - yC;
	dx = xC;
	cfy = dy / dx;
	// x'''' = x''' + x''' * y''' * cfx
	// y'''' = y''' + x''' * y''' * cfy
	/*
	now it's like:
						|
						*B-----*C
						|       |
						*A-----*D-----
	*/
	// 6. buffering coefficients
	float cx1, cx2, cy1, cy2;
	cx1 = (cosa + sina * coefa) * zoomx;
	cx2 = (-sina + cosa * coefa) * zoomx; // x''' = x * cx1 + y * cx2
	cy1 = (sina * zoomy);
	cy2 = (cosa * zoomy); // y''' = x * cy1 + y * cy2
	// 7. processing
	float x, y, outx, outy, xD8, yB8, xD16, yB16;
	ssize i, j, cx, cy;
	uint32 *dout, *doutbuf;
	ssize x8, y8;
	doutbuf = dst;
	doutbuf += ys * width;
	cx = xe - xs;
	cy = ye - ys;
	xD8 = xD * 256.0f;
	yB8 = yB * 256.0f;
	xD16 = xD * 65536.0f;
	yB16 = yB * 65536.0f;
	// anisotropic part
	float fsize_x, fsize_y;
	ssize x16, y16, size_x16, size_y16;
	//--//
	float xbuf1, xbuf2;
	y = ys - yAsave;
	for(i = 0; i < cy; ++i, y += 1, doutbuf += width) {
		if(! full_square) {
			if(! polygon.getXRange(float(ys + i), &xbuf1, &xbuf2)) {
				continue;
			}
			xs = (ssize)floor(xbuf1);
			xe = (ssize)ceil(xbuf2);
			xs = max2(xs, (ssize)(0));
			xe = min2(xe, width);
			cx = xe - xs;
		}
		dout = doutbuf + xs;
		x = xs - xAsave;
		outx = x * cx1 + y * cx2;
		outy = x * cy1 + y * cy2;
		if(method == R_BILINEAR) {
			for(j = 0; j < cx; ++j) {
				fbufx = outx * xD8 / (xD - outy * cfx);
				fbufy = outy * yB8 / (yB - outx * cfy);
				x8 = usize(fbufx);
				y8 = usize(fbufy);
				AlphaMergeKeep(dout[j], BilinearPixel(src, src_width, src_height, x8, y8));
				outx += cx1; //
				outy += cy1; // x increasing by 1, y don't changing here
			}
		} else if(method == R_ANISOTROPIC) {
			for(j = 0; j < cx; ++j) {
				outx += cx1; //
				outy += cy1; // x increasing by 1, y don't changing here
				fsize_x = xD16 / (xD - outy * cfx);
				fsize_y = yB16 / (yB - outx * cfy);
				fbufx = outx * fsize_x;
				fbufy = outy * fsize_y;
				x16 = ssize(fbufx);
				y16 = ssize(fbufy);
				size_x16 = ssize(fsize_x * zoomx);
				size_y16 = ssize(fsize_y * zoomy);
				AlphaMergeKeep(dout[j], AnisotropicPixel(src, src_width, src_height, x16, y16, size_x16, size_y16));
			}
		}
	}
	return true;
}; // end draw Texture

}; // end Graphic32
