#pragma once
#include "Surface.h"

namespace Graphic32 {

namespace Composition {

/*
	Surface implementation for custom images (with/without alpha).
	Surface object doesn't own specified image objects,
	so they have to be cleaned externally after destruction of corresponding surface.

	For simple work with animations e.t.c either could be done:
	- overload 'getFrame' method and call 'invalidate' method to notify that frame has changed,
	- use 'setImage' method to set new frame to draw.
*/
class ImageSurface : public Surface {
	const Img32* image;
public:
	ImageSurface(const Geometry::Rect& rect, int drawing_level, bool is_solid, const Img32* img);
	const Img32* getImage() const;
	void setImage(const Img32* img);
protected:
	virtual const Img32* getFrame() const;
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
};

}; // end Composition

}; // end Graphic32
