#pragma once
#include "Surface.h"

namespace Graphic32 {

namespace Composition {

/*
	Surface implementation with solid color background.
*/
class SolidColorSurface : public Surface {
	uint32 bg_color;
public:
	SolidColorSurface(const Geometry::Rect& rect, int drawing_level, uint32 color);
	uint32 getColor() const;
	void setColor(uint32 color);
protected:
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
};

}; // end Composition

}; // end Graphic32
