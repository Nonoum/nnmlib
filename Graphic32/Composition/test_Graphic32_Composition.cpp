#include "head.h"
#include "../../TestsUtils/head.h"

using Graphic32::Img32;
using namespace Graphic32::Composition;

static Img32 txt1, txt2, txt3;

void LoadTextures() {
	static bool loaded = false;
	if(! loaded) {
		loaded = true;
		SRC_FNAME(name1, "tank.tga");
		SRC_FNAME(name2, "barrels.tga");
		SRC_FNAME(name3, "dynamite.tga");
		txt1.readTGA(name1.c_str());
		txt2.readTGA(name2.c_str());
		txt3.readTGA(name3.c_str());
	}
};

NTEST(Graphic32_Composition_Surface_common) {
	Img32 dst(100, 100, 0), ethalon;
	SolidColorSurface sf(Geometry::Rect(0,0,100,110), 0, 0x00FFFFFF);
	SolidColorSurface* sf1 = new SolidColorSurface(Geometry::Rect(0,0,40,30), 0, 0x000000FF);
	SolidColorSurface* sf2 = new SolidColorSurface(Geometry::Rect(20,10,50,40), 2, 0x0000FF00);
	SolidColorSurface* sf3 = new SolidColorSurface(Geometry::Rect(30,20,60,50), 1, 0x00FF0000);
	SolidColorSurface* sf31 = new SolidColorSurface(Geometry::Rect(2,5,50,20), 0, 0x00007F00);
	sf2->setBorders(1,2,3,4);
	sf.push(sf1);
	sf.push(sf2);
	sf.push(sf3);
	sf3->push(sf31);
	sf.render(dst);
	dst.clone(ethalon);

	for(int i = 0; i < 32; ++i) {
		if(i & 1) sf.invalidate();
		if(i & 2) sf1->invalidate();
		if(i & 4) sf2->invalidate();
		if(i & 8) sf3->invalidate();
		if(i & 16) sf31->invalidate();
		sf.render(dst);
		ASSERT( dst.isEqual(ethalon) );
	}
};

#define NNMLIB_NTEST_INVALIDATE_CHECK(sf_ptr) \
	(sf_ptr)->invalidate(); \
	sf.render(dst); \
	ASSERT( dst.isEqual(ethalon) )

NTEST(Graphic32_Composition_Surface_scrolls) {
	Img32 dst(100, 100, 0), ethalon;

	SolidColorSurface sf(Geometry::Rect(0,10,100,100), 0, 0xFFFFFF);
	Surface* sf1 = new SolidColorSurface(Geometry::Rect(20,40,50,50), 0, 0x0000FF);
	sf1->setBorders(1,1,1,1);
	sf.push(sf1);

	Surface* sf2 = new SolidColorSurface(Geometry::Rect(-100,-70,35,35), 0, 0x00FF00);
	sf2->setBorders(1,1,1,1);
	sf1->push(sf2);
	sf1->setScrollValue(Surface::SCROLL_X, 7);
	sf1->setScrollValue(Surface::SCROLL_Y, 3);

	sf.render(dst);
	dst.clone(ethalon);

	NNMLIB_NTEST_INVALIDATE_CHECK(sf2);
	NNMLIB_NTEST_INVALIDATE_CHECK(sf1);

	Surface* sf3 = new SolidColorSurface(Geometry::Rect(-10, 0,20,20), 0, 0x00FFFF);
	sf2->push(sf3);
	sf.render(dst);
	dst.clone(ethalon);

	NNMLIB_NTEST_INVALIDATE_CHECK(sf3);
	NNMLIB_NTEST_INVALIDATE_CHECK(sf2);

	sf3->invalidate();
	NNMLIB_NTEST_INVALIDATE_CHECK(sf2);
};

NTEST(Graphic32_Composition_Surface_scrolls_textured) {
	LoadTextures();
	Img32 dst(100, 100, 0), ethalon, ethalon1, ethalon2;
	SRC_FNAME(name_result1, "surface-rendered-1.tga");
	SRC_FNAME(name_result2, "surface-rendered-2.tga");
	ethalon1.readTGA(name_result1.c_str());
	ethalon2.readTGA(name_result2.c_str());

	SolidColorSurface sf(Geometry::Rect(0,10,100,100), 0, 0xFFFFFF);
	Surface* sf1 = new ImageSurface(Geometry::Rect(20,40,50,50), 0, false, &txt1);
	sf1->setBorders(1,1,1,1);
	sf.push(sf1);

	Surface* sf2 = new ImageSurface(Geometry::Rect(-100,-70,35,35), 0, false, &txt2);
	sf2->setBorders(1,1,1,1);
	sf1->push(sf2);
	sf1->setScrollValue(Surface::SCROLL_X, 7);
	sf1->setScrollValue(Surface::SCROLL_Y, 3);

	sf.render(dst);
	ASSERT( dst.isEqual(ethalon1) );
	dst.clone(ethalon);

	NNMLIB_NTEST_INVALIDATE_CHECK(sf2);
	NNMLIB_NTEST_INVALIDATE_CHECK(sf1);

	Surface* sf3 = new ImageSurface(Geometry::Rect(-10, 0,20,20), 0, false, &txt3);
	sf2->push(sf3);
	sf.render(dst);
	ASSERT( dst.isEqual(ethalon2) );
	dst.clone(ethalon);

	NNMLIB_NTEST_INVALIDATE_CHECK(sf3);
	NNMLIB_NTEST_INVALIDATE_CHECK(sf2);
	sf3->invalidate();
	NNMLIB_NTEST_INVALIDATE_CHECK(sf2);
};

NTEST(Graphic32_Composition_Surface_scrolls_movement) {
	LoadTextures();
	Img32 dst(100, 100, 0), ethalon1, ethalon2, ethalon3;
	SRC_FNAME(name_result1, "surface-rendered-11.tga");
	SRC_FNAME(name_result2, "surface-rendered-12.tga");
	SRC_FNAME(name_result3, "surface-rendered-13.tga");
	ethalon1.readTGA(name_result1.c_str());
	ethalon2.readTGA(name_result2.c_str());
	ethalon3.readTGA(name_result3.c_str());

	SolidColorSurface sf(Geometry::Rect(1,1,100,100), 0, 0xFFFFFF);
	Surface* sf1 = new ImageSurface(Geometry::Rect(13,13,80,90), 0, false, &txt1);
	sf1->setBorders(1,1,1,1);
	sf.push(sf1);

	Surface* sf2 = new ImageSurface(Geometry::Rect(-45,-17,53,69), 0, false, &txt2);
	sf2->setBorders(1,1,1,1);
	sf1->push(sf2);

	Surface* sf3 = new ImageSurface(Geometry::Rect(-8,-9,21,23), 0, false, &txt3);
	sf2->push(sf3);

	sf.render(dst);
	ASSERT( dst.isEqual(ethalon1) );

	sf2->setScrollValue(Surface::SCROLL_X, 7);
	dst.fillRect(0xFF808080);
	sf.render(dst);
	ASSERT( dst.isEqual(ethalon2) );

	sf2->setScrollValue(Surface::SCROLL_X, 9);
	sf2->setScrollValue(Surface::SCROLL_Y, 6);
	dst.fillRect(0xFF808080);
	sf.render(dst);
	ASSERT( dst.isEqual(ethalon3) );
};

NTEST(Graphic32_Composition_Surface_invalidating_invisible) {
	Img32 dst(100, 100, 0), ethalon;

	SolidColorSurface sf(Geometry::Rect(1,3,100,100), 0, 0xFFFFFF);
	Surface* sf1 = new SolidColorSurface(Geometry::Rect(8,13,40,50), 0, 0x0000FF);
	sf.push(sf1);

	Surface* sf2 = new SolidColorSurface(Geometry::Rect(-100,-70,35,35), 0, 0x00FF00);
	sf1->push(sf2);

	Surface* sf3 = new SolidColorSurface(Geometry::Rect(-30,-10,60,70), 0, 0x00FFFF);
	sf1->push(sf3);

	sf1->setScrollValue(Surface::SCROLL_X, 20);
	sf1->setScrollValue(Surface::SCROLL_Y, 15);

	sf.render(dst);
	dst.fillRect(0); // clean. diff will be rendered next
	dst.clone(ethalon); // ethalon is empty image, as rendered diff should be

	NNMLIB_NTEST_INVALIDATE_CHECK(sf3);
};