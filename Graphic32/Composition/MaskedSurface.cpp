#include "MaskedSurface.h"

namespace Graphic32 {

namespace Composition {

MaskedSurface :: MaskedSurface(const Geometry::Rect& rect, int drawing_level, const AlphaMask* mask, uint32 color)
		: Surface(rect, drawing_level, false), s_mask(mask), mask_color(color) {
};

const AlphaMask* MaskedSurface :: getMask() const {
	return s_mask;
};

void MaskedSurface :: setMask(const AlphaMask* mask) {
	if(mask!= s_mask) { // somewhat check
		invalidate();
	}
	s_mask = mask;
};

uint32 MaskedSurface :: getColor() const {
	return mask_color;
};

void MaskedSurface :: setColor(uint32 color) {
	if(color != mask_color) {
		invalidate();
	}
	mask_color = color;
};

bool MaskedSurface :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	bool result = false;
	if(s_mask != NULL) {
		const ssize x = displacement.x() + local_area.x();
		const ssize y = displacement.y() + local_area.y();
		const ssize ox = local_area.x();
		const ssize oy = local_area.y();
		result = dst.drawByMask(x, y, local_area.width(), local_area.height(), *s_mask, ox, oy, mask_color);
	}
	return result;
};

}; // end Composition

}; // end Graphic32
