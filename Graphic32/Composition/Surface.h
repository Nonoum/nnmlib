#pragma once
#include "../Img32.h"
#include "../../Geometry/Rect.h"

namespace Graphic32 {

namespace Composition {

/*
	Base class for constructing graphical scenes with optimization of redrawing
	after small changing of scene.
	Contains:
	- independent optimization algorithm;
	- pure virtual method 'renderArea' to overload with drawing implementation;
*/
class Surface {
public:
	enum Scrolls {
		SCROLL_X = 0,
		SCROLL_Y = 1
	};
protected:
	typedef std::vector<Surface*> SubsType;
	typedef std::vector<Geometry::Rect> DirtType;
private:
	enum { SCROLLS_COUNT = SCROLL_Y+1 };
	// basic params
	const bool solid;
	const int level;
	Surface* parent;
	SubsType subs; // sub-surfaces
	DirtType dirt; // rects in internal coordinates
	// dynamic params
	Geometry::Rect placement;
	bool need_redraw;
	usize borders[4];
	uint32 borders_color;
	// secondary params
	size_t i_focused;

	// scrolls data
	Geometry::Rect full_covering_area; // area that covers all sub-surfaces
	usize s_limits[SCROLLS_COUNT]; // scroll limits
	usize s_values[SCROLLS_COUNT]; // scroll values [0..limit]
public:
	/*
		Constructor params:
		- (rect) is area inside of parent surface, for root surface coordinates of rect determines common drawing offset;
		- (drawing_level) is number of layer to draw, higher ones lays above the others;
		- (is_solid) whether drawing of this Surface two times in a row for same area gives valid result.
		Corresponding parameters in derived classes have equal purpose.
	*/
	Surface(const Geometry::Rect& rect, int drawing_level, bool is_solid);
	virtual ~Surface();

	bool isSolid() const;
	int getLevel() const;
	inline const Geometry::Rect& getPlacement() const { return placement; }; // non-scrolled placement in parent
	void scrollLocalPoint(Geometry::Point& local_point) const; // scrolls point given in internal coordinates.
	bool containsPoint(const Geometry::Point& local_point) const; // returns whether (local_point) is in local placement rect ([0..width); [0..height))

	void setSize(const Geometry::Size& size); // updates all related data (scroll etc) for this surface and for parent
	void setPlacementPos(const Geometry::Point& pos); // same here
	void setPlacement(const Geometry::Rect& rect); // does the job of previous two, but better

	usize getBorder(Geometry::Side side) const;
	void setBorders(usize left, usize top, usize right, usize bottom);
	void setBordersColor(uint32 color);

	void push(Surface* new_surface); // adds sub-surface allocated with (operator new) and owns it. updates all related data
	void exclude(Surface* sub_surface); // surface is only being removed from list, no memory releasing here. all related data (scrolls etc) is being updated
	void invalidate(); // invalidates surface, all sub-surfaces, and parent if needed
	void invalidate(Geometry::Rect local_area); // same with 'invalidate' but for partial invalidation
	void move(ssize dx, ssize dy); // similar to 'setPlacementPos' but parameters are offsets to add
	bool render(Img32& dst); // renders current state of scene to (dst). supposes that (dst) is not being changed outside.
	void clear(); // deletes all sub-surfaces and updates all related data

	usize getScrollLimit(Scrolls s) const; // returns maximum value (including) that corresponds to specified scroll
	usize getScrollValue(Scrolls s) const; // returns current value for specified scroll
	void setScrollValue(Scrolls s, usize value); // sets current value for specified scroll with pre-check
protected:
	/*
		'renderArea' method params:
		- (dst) is image to draw to;
		- (displacement) is offsets on (dst) to add to (local_area) to get real drawing area;
		- (local_area) is area to draw (in local coordinates of this Surface object).
	*/
	virtual bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) = 0;

	const SubsType& getSubs() const;
	const DirtType& getDirt() const;
	virtual void onScrollOffsetChanged(Scrolls s);
	size_t getFocusedIndex() const { return i_focused; }; // returns size_t(-1) if there is no focused sub-surface
	void setFocusedIndex(size_t index); // sets focused index, that will be tracked during changes of sub-surfaces list
private:
	Geometry::Rect getLegalZone() const;
	Geometry::Rect getLocalLegalZone() const;

	void invalidateArea(Geometry::Rect external_area);
	bool renderSubs(Img32& dst, Geometry::Point displacement, const Geometry::Rect& screen_area);
	bool renderInternal(Img32& dst, Geometry::Point pre_displacement, const Geometry::Rect& screen_area);
	bool renderBorders(Img32& dst, Geometry::Point displacement, Geometry::Rect local_area);

	Geometry::Point getScrollingOffset() const;
	void updateScrollsByAddedSub(const Geometry::Rect& added_sub_rect);
	void updateScrollsByChangedSub(const Geometry::Rect& changed_sub_rect);
	void updateScrolls(const Geometry::Point& prev_offset);
};

}; // end Composition

}; // end Graphic32
