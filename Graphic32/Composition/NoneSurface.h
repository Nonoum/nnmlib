#pragma once
#include "Surface.h"

namespace Graphic32 {

namespace Composition {

/*
	Transparent surface implementation (only borders can be drawed).
*/
class NoneSurface : public Surface {
public:
	NoneSurface(const Geometry::Rect& rect, int drawing_level);
protected:
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
};

}; // end Composition

}; // end Graphic32
