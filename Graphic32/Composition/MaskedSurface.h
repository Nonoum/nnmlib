#pragma once
#include "Surface.h"
#include "../AlphaMask.h"

namespace Graphic32 {

namespace Composition {

/*
	Surface implementation for 8bit alpha masks with independent custom color.
	Surface object doesn't own specified mask objects,
	so they have to be cleaned externally after destruction of corresponding surface.
*/
class MaskedSurface : public Surface {
	const AlphaMask* s_mask;
	uint32 mask_color;
public:
	MaskedSurface(const Geometry::Rect& rect, int drawing_level, const AlphaMask* mask, uint32 color);
	const AlphaMask* getMask() const;
	void setMask(const AlphaMask* mask);
	uint32 getColor() const;
	void setColor(uint32 color);
protected:
	bool renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) override;
};

}; // end Composition

}; // end Graphic32
