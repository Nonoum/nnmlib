#include "ImageSurface.h"

namespace Graphic32 {

namespace Composition {

ImageSurface :: ImageSurface(const Geometry::Rect& rect, int drawing_level, bool is_solid, const Img32* img)
		: Surface(rect, drawing_level, is_solid), image(img) {
};

const Img32* ImageSurface :: getImage() const {
	return image;
};

void ImageSurface :: setImage(const Img32* img) {
	if(img != image) { // somewhat check
		invalidate();
	}
	image = img;
};

const Img32* ImageSurface :: getFrame() const {
	return image;
};

bool ImageSurface :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	bool result = false;
	const Img32* frame = getFrame();
	if(frame != NULL) {
		const ssize x = displacement.x() + local_area.x();
		const ssize y = displacement.y() + local_area.y();
		const ssize ox = local_area.x();
		const ssize oy = local_area.y();
		result = dst.drawRect(x, y, local_area.width(), local_area.height(), *frame, ox, oy, isSolid() ? D_SOLID : D_ALPHA);
	}
	return result;
};

}; // end Composition

}; // end Graphic32
