#include "Surface.h"
#include "../../Helpers/ClearNewPointersCollection.h"
#include "../../Algorithms/Extras.h"
#include <assert.h>

namespace Graphic32 {

namespace Composition {

// helpers
inline bool IsValidBorders(const Geometry::Size& size, usize borders[4]) {
	return (! size) || // size could be zero
			((ssize(borders[Geometry::SIDE_LEFT] + borders[Geometry::SIDE_RIGHT]) < size.width()) &&
				(ssize(borders[Geometry::SIDE_UP] + borders[Geometry::SIDE_DOWN]) < size.height()));
};

inline bool HaveBorders(usize borders[4]) {
	return (borders[0] | borders[1] | borders[2] | borders[3]) ? true : false; // not too pretty, but optimal check
};


Surface :: Surface(const Geometry::Rect& rect, int drawing_level, bool is_solid)
		: placement(rect), solid(is_solid), level(drawing_level), parent(NULL), i_focused(-1) {

	Algorithms::fill(s_limits, 0);
	Algorithms::fill(s_values, 0);
	need_redraw = true;
	Algorithms::fill(borders, 0);
	borders_color = 0;
};

Surface :: ~Surface() {
	parent = NULL;
	i_focused = -1;
	Helpers::ClearNewPointersCollection(subs);
};

bool Surface :: isSolid() const {
	return solid;
};

int Surface :: getLevel() const {
	return level;
};

void Surface :: scrollLocalPoint(Geometry::Point& local_point) const {
	local_point += getScrollingOffset();
};

bool Surface :: containsPoint(const Geometry::Point& local_point) const {
	return Geometry::Rect(placement.getSize()) & local_point;
};

void Surface :: setSize(const Geometry::Size& size) {
	if(placement.getSize() != size) {
		// invalidating for old size
		invalidate();
		placement.getSize() = size;
		assert( IsValidBorders(placement.getSize(), borders) && "Too big borders after resizing" );
		// invalidating for new size
		need_redraw = false;
		invalidate();
		// updating scrolls
		updateScrolls(getScrollingOffset());
		if(parent) {
			parent->updateScrollsByChangedSub(placement);
		}
	}
};

void Surface :: setPlacementPos(const Geometry::Point& pos) {
	if(placement.getPoint() != pos) {
		// invalidating for old position
		invalidate();
		placement.getPoint() = pos;
		// invalidating for new position
		need_redraw = false;
		invalidate();
		// updating scrolls
		if(parent) {
			parent->updateScrollsByChangedSub(placement);
		}
	}
};

void Surface :: setPlacement(const Geometry::Rect& rect) {
	if(placement != rect) {
		// invalidating for old placement
		invalidate();
		placement = rect;
		assert( IsValidBorders(placement.getSize(), borders) && "Too big borders after resizing" );
		// invalidating for new placement
		need_redraw = false;
		invalidate();
		// updating scrolls
		updateScrolls(getScrollingOffset());
		if(parent) {
			parent->updateScrollsByChangedSub(placement);
		}
	}
};

void Surface :: setBorders(usize left, usize top, usize right, usize bottom) {
	const bool had = HaveBorders(borders);
	borders[Geometry::SIDE_LEFT] = left;
	borders[Geometry::SIDE_UP] = top;
	borders[Geometry::SIDE_RIGHT] = right;
	borders[Geometry::SIDE_DOWN] = bottom;
	if(HaveBorders(borders) || had) {
		invalidate();
	}
	assert( IsValidBorders(placement.getSize(), borders) && "Too big borders" );
};

usize Surface :: getBorder(Geometry::Side side) const {
	assert(side < Geometry::SIDE_NONE);
	if(side < Geometry::SIDE_NONE) {
		return borders[side];
	}
	return 0;
};

void Surface :: setBordersColor(uint32 color) {
	borders_color = color;
	if(HaveBorders(borders)) {
		invalidate();
	}
};

struct SurfacePtrComparator {
	bool operator()(const Surface* left, const Surface* right) const {
		return left->getLevel() < right->getLevel();
	};
};

void Surface :: push(Surface* new_surface) {
	assert( new_surface != NULL && "Attempted to push a NULL surface" );
	if(! new_surface) {
		return;
	}
	assert( Algorithms::noneIs(subs, new_surface) && "Attempted to add one sub-surface two times" );
	new_surface->parent = this;
	const auto found = std::upper_bound(subs.begin(), subs.end(), new_surface, SurfacePtrComparator());
	if(i_focused != -1) {
		const size_t idx = found - subs.begin();
		if(idx <= i_focused) {
			++i_focused;
		}
	}
	subs.insert(found, new_surface);
	invalidate();
	updateScrollsByAddedSub(new_surface->placement);
};

void Surface :: invalidate() {
	if(need_redraw) {
		return; // already invalid
	}
	need_redraw = true;
	std::for_each(subs.begin(), subs.end(), std::mem_fun(static_cast<void (Surface::*)()>(&Surface::invalidate)));
	if(parent != NULL) {
		Geometry::Rect legal_zone = getLegalZone();
		if(legal_zone.getSize()) {
			parent->invalidateArea(legal_zone += parent->placement.getPoint());
		}
	}
	dirt.clear();
};

void Surface :: invalidate(Geometry::Rect local_area) {
	const Geometry::Rect this_area = Geometry::Rect(placement.getSize());
	if(need_redraw || (! local_area.crossing( this_area ))) {
		return; // already invalid, or area is out of visible bounds
	}
	invalidateArea((local_area & this_area) + placement.getPoint());
};

void Surface :: move(ssize dx, ssize dy) {
	setPlacementPos(placement.getPoint() + Geometry::Point(dx, dy));
};

bool Surface :: render(Img32& dst) {
	assert( parent == NULL && "Render was called from non-root surface" );
	Surface* runner = this;
	while(runner->parent != NULL) {
		runner = runner->parent;
	}
	return runner->renderInternal(dst, Geometry::Point(), placement);
};

bool Surface :: renderBorders(Img32& dst, Geometry::Point displacement, Geometry::Rect local_area) {
	bool result = false;
	local_area.getPoint() += displacement;
	const Geometry::Rect global_full_rect(displacement, placement.getSize());
	const Geometry::Rect rects[4] = {
		global_full_rect.getPlacedRect(0, 0, borders[Geometry::SIDE_LEFT], placement.height(), Geometry::ALIGN_BEGIN, Geometry::ALIGN_BEGIN),
		global_full_rect.getPlacedRect(0, 0, borders[Geometry::SIDE_RIGHT], placement.height(), Geometry::ALIGN_END, Geometry::ALIGN_BEGIN),
		global_full_rect.getPlacedRect(borders[Geometry::SIDE_LEFT], 0, placement.width() - borders[Geometry::SIDE_LEFT] - borders[Geometry::SIDE_RIGHT],
			borders[Geometry::SIDE_DOWN], Geometry::ALIGN_BEGIN, Geometry::ALIGN_BEGIN),
		global_full_rect.getPlacedRect(borders[Geometry::SIDE_LEFT], 0, placement.width() - borders[Geometry::SIDE_LEFT] - borders[Geometry::SIDE_RIGHT],
			borders[Geometry::SIDE_UP], Geometry::ALIGN_BEGIN, Geometry::ALIGN_END)
	};
	for(const auto& r : rects) {
		if(r.crossing(local_area)) {
			const Geometry::Rect rect = r & local_area;
			result |= dst.fillRect(borders_color, rect.x(), rect.y(), rect.width(), rect.height());
		}
	}
	return result;
};

void Surface :: invalidateArea(Geometry::Rect external_area) {
	// (external_area) is in same with (placement) coordinate space (parent's)
	// (external_area) could be fully outside of (placement)
	if(need_redraw) {
		return; // already fully invalid
	}
	// CAUTION: BLOOD, HELL, BIDIRECTIONAL RECURSION.
	// TODO: simplify related code somehow

	if(placement.insideOf(external_area)) {
		invalidate();
		return;
	}
	const Geometry::Point parent_scrolls = parent ? parent->getScrollingOffset() : Geometry::Point();
	const Geometry::Point this_scrolls = getScrollingOffset();

	Geometry::Rect internal_area = external_area - placement.getPoint(); // local unscrolled coordinates

	const Geometry::Rect local_zone = getLocalLegalZone() + parent_scrolls; // local unscrolled coordinates
	if(! internal_area.crossing(local_zone)) {
		return;
	}

	const Geometry::Rect internal_area_cut = internal_area & local_zone;
	for(const auto& dirt_rect : dirt) {
		if(internal_area_cut.insideOf(dirt_rect)) {
			return; // this area is already invalid
		}
	}
	for(size_t i = 0; i < dirt.size(); ++i) {
		if(dirt[i].crossing(internal_area)) {
			internal_area = dirt[i].unionWith(internal_area);
			dirt.erase(dirt.begin() + i);
			// for solid drawing in case of uniting rects there is no need to re-check previously checked rects again
			// for non-solid drawing re-check is needed to prevent double-drawing of same non-solid area
			i = isSolid() ? i - 1 : size_t(-1);
		}
	}
	dirt.push_back(internal_area & local_zone);
	// updating sub-surfaces
	for(auto sub : subs) {
		sub->invalidateArea((sub->placement - this_scrolls).checkedIntersection(internal_area) + this_scrolls);
	}
	// updating parent surface
	if(parent) {
		// calculating external area for parent (filtering by (*this) area limitations)
		external_area = (internal_area & local_zone) + placement.getPoint();
		// applying parent's scrolls for external area and filtering by parent's size
		external_area = (external_area - parent_scrolls).checkedIntersection(Geometry::Rect(parent->placement.getSize()));
		// converting to parent->parent's coordinates
		external_area += parent->placement.getPoint();
		parent->invalidateArea(external_area);
	}
};

inline bool Surface :: renderSubs(Img32& dst, Geometry::Point displacement, const Geometry::Rect& screen_area) {
	bool result = false;
	for(auto sub : subs) {
		result |= sub->renderInternal(dst, displacement, screen_area);
	}
	return result;
};

bool Surface :: renderInternal(Img32& dst, Geometry::Point pre_displacement, const Geometry::Rect& screen_area) {
	const Geometry::Point displacement = pre_displacement + placement.getPoint(); // screen offset for beginning of (*this)
	const Geometry::Point offset_compensation = parent ? parent->getScrollingOffset() : Geometry::Point(); // to avoid subtracting extra scrolling offset
	const Geometry::Rect zone = getLocalLegalZone() + displacement + offset_compensation; // zone of (*this) in screen coordinates
	if(! screen_area.crossing(zone)) {
		need_redraw = false;
		dirt.clear();
		return false;
	}
	const Geometry::Point scrolling_offset = getScrollingOffset();
	const Geometry::Point scrolled_disp = displacement - scrolling_offset;
	const Geometry::Rect checked_zone = zone & screen_area;
	bool result = false;
	if(need_redraw) {
		// full redraw
		result |= renderArea(dst, scrolled_disp, checked_zone - scrolled_disp);

		result |= renderSubs(dst, scrolled_disp, checked_zone);
		if(HaveBorders(borders)) {
			result |= renderBorders(dst, displacement, checked_zone - displacement);
		}
		need_redraw = false;
	} else {
		for(const auto& dirt_rect : dirt) {
			const Geometry::Rect area = (dirt_rect + scrolling_offset + scrolled_disp).checkedIntersection(checked_zone) - scrolled_disp;
			result |= renderArea(dst, scrolled_disp, area);
		}
		result |= renderSubs(dst, scrolled_disp, checked_zone);
		if(HaveBorders(borders)) {
			for(const auto& dirt_rect : dirt) {
				const Geometry::Rect area = (dirt_rect + displacement).checkedIntersection(checked_zone) - displacement;
				result |= renderBorders(dst, displacement, area);
			}
		}
		dirt.clear();
	}
	return result;
};

inline Geometry::Rect Surface :: getLegalZone() const {
	if(parent) {
		const Geometry::Rect rect(parent->placement.getSize());
		return rect.checkedIntersection(placement - parent->getScrollingOffset());
	}
	return placement;
};

inline Geometry::Rect Surface :: getLocalLegalZone() const {
	return getLegalZone() - placement.getPoint();
};

// scrolls -------------------------------------------------------------------------------

usize Surface :: getScrollLimit(Scrolls s) const {
	assert(s < int(SCROLLS_COUNT) && "Invalid scroll type");
	return s < int(SCROLLS_COUNT) ? s_limits[s] : 0;
};

usize Surface :: getScrollValue(Scrolls s) const {
	assert(s < int(SCROLLS_COUNT) && "Invalid scroll type");
	return s < int(SCROLLS_COUNT) ? s_values[s] : 0;
};

void Surface :: setScrollValue(Scrolls s, usize value) {
	assert(s < int(SCROLLS_COUNT) && "Invalid scroll type");
	if((s < int(SCROLLS_COUNT)) && (s_values[s] != value)) {
		s_values[s] = Helpers::min2<usize>(value, s_limits[s]);
		invalidate();
	}
};

void Surface :: onScrollOffsetChanged(Scrolls s) { // empty
};

Geometry::Point Surface :: getScrollingOffset() const {
	const ssize x = Helpers::min2<ssize>(full_covering_area.x(), 0) + s_values[SCROLL_X];
	const ssize y = Helpers::min2<ssize>(full_covering_area.y(), 0) + s_values[SCROLL_Y];
	return Geometry::Point(x, y);
};

void Surface :: updateScrollsByAddedSub(const Geometry::Rect& added_sub_rect) {
	const Geometry::Point prev_offset = getScrollingOffset();
	full_covering_area = full_covering_area.unionWith(added_sub_rect);
	updateScrolls(prev_offset);
};

void Surface :: updateScrollsByChangedSub(const Geometry::Rect& changed_sub_rect) {
	const Geometry::Point prev_offset = getScrollingOffset();
	// updating lower bounds
	ssize x_min = changed_sub_rect.x();
	ssize y_min = changed_sub_rect.y();
	ssize x_max = changed_sub_rect.getXEnd();
	ssize y_max = changed_sub_rect.getYEnd();

	for(const auto sub : subs) {
		x_min = Helpers::min2<ssize>(x_min, sub->placement.x());
		y_min = Helpers::min2<ssize>(y_min, sub->placement.y());
		x_max = Helpers::max2<ssize>(x_max, sub->placement.getXEnd());
		y_max = Helpers::max2<ssize>(y_max, sub->placement.getYEnd());
	}
	full_covering_area.setRect(x_min, y_min, x_max - x_min, y_max - y_min);
	updateScrolls(prev_offset);
};

void Surface :: updateScrolls(const Geometry::Point& prev_offset) {
	const ssize x_lower_space = Helpers::max2<ssize>(-full_covering_area.x(), 0);
	const ssize x_higher_space = Helpers::max2<ssize>(full_covering_area.getXEnd() - placement.width(), 0);
	const ssize y_lower_space = Helpers::max2<ssize>(-full_covering_area.y(), 0);
	const ssize y_higher_space = Helpers::max2<ssize>(full_covering_area.getYEnd() - placement.height(), 0);
	s_limits[SCROLL_X] = x_higher_space + x_lower_space;
	s_limits[SCROLL_Y] = y_higher_space + y_lower_space;
	// updating values
	for(int i = 0; i < SCROLLS_COUNT; ++i) {
		s_values[i] = Helpers::min2<usize>(s_values[i], s_limits[i]);
	}

	const Geometry::Point curr_offset = getScrollingOffset();
	if(curr_offset != prev_offset) {
		invalidate();
	}
	if(curr_offset.x() != prev_offset.x()) {
		onScrollOffsetChanged(SCROLL_X);
	}
	if(curr_offset.y() != prev_offset.y()) {
		onScrollOffsetChanged(SCROLL_Y);
	}
};

// others -------------------------------------------------------------------------------

void Surface :: clear() {
	i_focused = -1;
	Helpers::ClearNewPointersCollection(subs);
	invalidate();
	updateScrolls(getScrollingOffset());
};

const Surface::SubsType& Surface :: getSubs() const {
	return subs;
};

const Surface::DirtType& Surface :: getDirt() const {
	return dirt;
};

void Surface :: setFocusedIndex(size_t index) {
	assert((index == size_t(-1)) || (index < subs.size()));
	if(index >= subs.size()) {
		i_focused = -1;
	} else {
		i_focused = index;
	}
};

void Surface :: exclude(Surface* sub_surface) {
	assert(sub_surface != NULL);
	auto found = std::find(subs.begin(), subs.end(), sub_surface);
	if(found != subs.end()) {
		if(i_focused != -1) {
			const size_t idx = found - subs.begin();
			if(idx == i_focused) {
				i_focused = -1;
			} else if(idx < i_focused) {
				--i_focused;
			}
		}
		subs.erase(found);
	}
	updateScrollsByChangedSub(subs.size() ? subs[0]->placement : Geometry::Rect());
};

}; // end Composition

}; // end Graphic32
