#include "NoneSurface.h"

namespace Graphic32 {

namespace Composition {

NoneSurface :: NoneSurface(const Geometry::Rect& rect, int drawing_level)
		: Surface(rect, drawing_level, false) {
};

bool NoneSurface :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	return false;
};

}; // end Composition

}; // end Graphic32
