#include "SolidColorSurface.h"

namespace Graphic32 {

namespace Composition {

SolidColorSurface :: SolidColorSurface(const Geometry::Rect& rect, int drawing_level, uint32 color)
		: Surface(rect, drawing_level, true), bg_color(color) {
};

uint32 SolidColorSurface :: getColor() const {
	return bg_color;
};

void SolidColorSurface :: setColor(uint32 color) {
	if(color != bg_color) {
		invalidate();
	}
	bg_color = color;
};

bool SolidColorSurface :: renderArea(Img32& dst, Geometry::Point displacement, const Geometry::Rect& local_area) {
	return dst.fillRect(bg_color, displacement.x() + local_area.x(), displacement.y() + local_area.y(), local_area.width(), local_area.height());
};

}; // end Composition

}; // end Graphic32
