#pragma once
#include "Utils.h"
#include "../CompressedImageCodec/head.h"
#include "../Geometry/Rect.h"
#include <vector>

namespace Graphic32 {

class BitMask;
class AlphaMask;

class Img32 {
	uint32* data;
	usize _width;
	usize _height;
	ubyte nchannels;
public:
	Img32();
	Img32(const Img32& other);
	Img32(Img32& other, bool to_swap);
	Img32(usize width, usize height);
	Img32(usize width, usize height, uint32 color);
	Img32& operator=(const Img32& other);

	virtual ~Img32();

	// accessors
	inline usize width() const { return _width; };
	inline usize height() const { return _height; };
	inline Geometry::Size size() const { return Geometry::Size(_width, _height); };
	inline const uint32* getData() const { return data; };
	inline operator bool() const { return data != NULL; };
	inline uint32 pixelUnchecked(usize x, usize y) const {
		return data[y*_width + x];
	};

	// setters
	void setActiveChannels(ubyte channels_count); // count of channels to write in CIMG

	// internal processing
	bool moveAlphaToRGB();
	bool moveAverageRGBToAlpha(bool keep_rgb = false);
	bool channelPower(ubyte power, ubyte ichannel);
	void processChannels(ubyte rgba_flags, Operation op, uint32 pixel = 0); // (rgba_flags): 8=r, 4=g, 2=b, 1=alpha
	bool mirrorMerge(bool by_x, bool by_y, usize x_start, usize y_start);

	// simple processing with operands
	void subtract(const Img32& right, ubyte rgba_flags);
	bool setXorOf(const Img32& src1, const Img32& src2, uint32 background_color = 0x00000000);
	bool fillRect(uint32 color = 0xFF000000,
		ssize x = 0, ssize y = 0, ssize cx = size_t(-1) >> 1, ssize cy = size_t(-1) >> 1, bool keep_alpha = false);
	bool xorWith(uint32 color = 0xFFFFFFFF,
		ssize x = 0, ssize y = 0, ssize cx = size_t(-1) >> 1, ssize cy = size_t(-1) >> 1);

	// converters
	bool resizeBilinear(Img32& dst, usize new_width, usize new_height) const;
	bool resizeAnisotropic(Img32& dst, usize new_width, usize new_height) const;
	bool cycle(Img32& dst, usize new_width, usize new_height) const;
	bool rotate(Img32& dst, double degrees, bool same_size = true, uint32 background_color = 0x00000000) const;

	// complicated processing ----------------------------------------------------------------------------------------------
	// drawing geometry
	bool drawLine(ssize x1, ssize y1, ssize x2, ssize y2, uint32 color);
	bool drawRectContur(ssize x, ssize y, ssize width_included, ssize height_included, uint32 color);
	bool drawGrid(ssize x_disp, ssize y_disp, ssize vline_width, ssize hline_height, ssize x_step, ssize y_step, uint32 color, const Geometry::Rect* allowed_area = NULL);
	bool drawPolygon(float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD, uint32 color);

	// drawing from sources
	bool drawRect(ssize x, ssize y, usize cx, usize cy, const Img32& src,
		ssize xoffs = 0, ssize yoffs = 0, Drawers mode = D_SOLID, ubyte level = 0);
	bool drawByMask(ssize x, ssize y, usize cx, usize cy, const BitMask& src, ssize xoffs, ssize yoffs, uint32 color);
	bool drawByMask(ssize x, ssize y, usize cx, usize cy, const AlphaMask& src, ssize xoffs, ssize yoffs, uint32 color);
	bool drawTexture(const Img32& src, float xA, float yA, float xB, float yB, float xC, float yC, float xD, float yD,
		bool full_square, Resamplers method = R_BILINEAR);

	// TGA ----------------------------------------------------------------------------------------------
	uint32 readTGA(const char* file_name);
	uint32 writeTGA(const char* file_name, bool rle = false) const;

	// CIMG ----------------------------------------------------------------------------------------------
	usize readCimg(const char* file_name, CompressedImageCodec::CimgInfo* dst_info = NULL);
	usize readCimg(std::istream& src_file, CompressedImageCodec::CimgInfo* dst_info = NULL);
	uint16 readCimgBatch(const char* file_name, uint16 count_from_this,
		std::vector<CompressedImageCodec::CimgInfo>* dst_info = NULL); // returns 'count_from_this' > 0 ? count_of_readed : count_in_file;

	bool writeCimg(const char* file_name,
		const CompressedImageCodec::Codec& configured = CompressedImageCodec::Codec::getDefault()) const;
	bool writeCimg(std::ostream& dst_file,
		const CompressedImageCodec::Codec& configured = CompressedImageCodec::Codec::getDefault()) const;
	uint16 writeCimgBatch(const char* file_name, uint16 count_from_this,
		const CompressedImageCodec::Codec& configured = CompressedImageCodec::Codec::getDefault()) const;

	// basic stuff ----------------------------------------------------------------------------------------------
	void alloc(usize new_width, usize new_height);
	void dealloc();
	void swap(Img32& img);
	void clone(Img32& dst) const;
	bool isEqual(const Img32& img, ubyte max_allowed_diff = 0) const;
	void rotate(int side, Img32& dst) const; // (side) = times of 90* rotation by clock arrow
private:
	void reset();
};

}; // end Graphic32
