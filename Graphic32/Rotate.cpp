#include "functions.h"
#include "Pixel/head.h"
#include <math.h>

namespace Graphic32 {

// code uses +12 bits for calculations

void CalculateNewSizeRotating(double radians_rotation, usize width, usize height, usize* new_w, usize* new_h) {
	ssize _cos, _sin;
	_cos = (ssize)(cos(radians_rotation) * 4096.0);
	_sin = (ssize)(sin(radians_rotation) * 4096.0);
	ssize xin[4], yin[4], xout[4], yout[4], i, xmin, xmax, ymin, ymax;
	xin[0] = 0;
	xin[1] = width - 1;
	xin[2] = width - 1;
	xin[3] = 0;
	yin[0] = 0;
	yin[1] = height - 1;
	yin[2] = 0;
	yin[3] = height - 1;
	for(i = 0; i < 4; ++i) {
		xout[i] = xin[i] * _cos - yin[i] * _sin;
		yout[i] = xin[i] * _sin + yin[i] * _cos;
	}
	xmin = min4(xout[0], xout[1], xout[2], xout[3]);
	xmax = max4(xout[0], xout[1], xout[2], xout[3]);
	ymin = min4(yout[0], yout[1], yout[2], yout[3]);
	ymax = max4(yout[0], yout[1], yout[2], yout[3]);
	xmin &= ssize(-4096); // ff..ff0000
	ymin &= ssize(-4096);
	xmax -= xmin;
	ymax -= ymin;
	if(xmax & 0x0fff) {
		xmax += 0x1000; // round
	}
	if(ymax & 0x0fff) {
		ymax += 0x1000; // round
	}
	*new_w = usize(xmax >> 12);
	*new_h = usize(ymax >> 12);
}; // end calculate new size rotating

bool Rotate(const uint32* src, usize width, usize height, double degrees, bool same_size,
		uint32** dst, usize* dst_width, usize* dst_height, uint32 background_color) {

	if((! src) || (! dst)) {
		return false;
	}
	degrees *= (-3.14159265358979323846 / 180.0); // -> radians
	ssize cos10, sin10; // 10 bits after point
	ssize xi8, yi8, xc8, yc8, x2, y2, offs; // x in, y in, center X8, buffer x,y X2 (+2 bits)
	usize xo, yo; // x out, y out
	cos10 = (ssize)(cos(degrees) * 1024.0); // cosinus X10 (+10 bits)
	sin10 = (ssize)(sin(degrees) * 1024.0); // sinus X10
	xc8 = (width << 7); // x center X8
	yc8 = (height << 7); // y center X8
	usize new_w = width;
	usize new_h = height;
	if((sizeof(usize) == 4) && (width > 360000) && (height > 360000))  {
		return false; // using +12 bits +sign (32-12-1 == 19 bits left) + rotating may increase size in sqrt(2) times.. Too large image
	}
	if(! same_size) {
		CalculateNewSizeRotating(degrees, width, height, &new_w, &new_h);
	}
	Allocate(dst, dst_width, dst_height, new_w, new_h);
	uint32* dst_pixel;
	ssize xoc2, yoc2;
	xoc2 = (new_w << 1); // x out center X2
	yoc2 = (new_h << 1); // y out center X2
	offs = 0;
	dst_pixel = *dst;
	for(yo = 0; yo < new_h; ++yo) {
		y2 = (yo << 2) - yoc2;
		x2 = (   0   ) - xoc2;
		for(xo = 0; xo < new_w; ++xo) {
			xi8 = ((x2*cos10 - y2*sin10) >> 4) + xc8;
			yi8 = ((x2*sin10 + y2*cos10) >> 4) + yc8;
			dst_pixel[offs] = BilinearPixel(src, width, height, xi8, yi8, background_color);
			++offs;
			x2 += 4;
		} // x out
	} // y out
	return true;
}; // end rotate

}; // end Graphic32
