#include "AlphaMask.h"
#include <assert.h>

namespace Graphic32 {

AlphaMask :: AlphaMask()
		: data(NULL), _width(0), _height(0) {
};

void AlphaMask :: loadChannel(const Img32& src, ubyte ichannel) {
	assert(ichannel <= 3);
	if((! src) || (ichannel > 3)) { // 0,1,2,3 channels are allowed
		return;
	}
	union {
		uint32 value;
		ubyte channels[4];
	} pixel;
	alloc(src.width(), src.height());
	for(usize y = 0; y < _height; ++y) {
		for(usize x = 0; x < _width; ++x) {
			pixel.value = src.pixelUnchecked(x, y);
			getUnchecked(x, y) = pixel.channels[ichannel];
		}
	}
};

void AlphaMask :: alloc(usize new_width, usize new_height) {
	if((!data) || (_width != new_width) || (_height != new_height)) {
		delete [] data;
		data = NULL;
		_width = 0;
		_height = 0;
		data = new ubyte[new_width * new_height];
		_width = new_width;
		_height = new_height;
	}
};

}; // end Graphic32
