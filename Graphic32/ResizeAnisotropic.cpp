#include "functions.h"

namespace Graphic32 {

bool ResizeAnisotropic(const uint32* src, usize width, usize height, uint32** dst, usize* dst_width, usize* dst_height, usize new_w, usize new_h) {
	// max resize (decreasing) : 256 times
	uint32 *buffer, *bactptr, *bactptr2;
	const uint32* src_pos;
	union {
		uint32 value;
		struct {
			ubyte b, g, r, a;
		};
	} pixel;
	if((! src) || (! dst)) {
		return false; // NULL pointer
	}
	if((new_w < 2) || (new_h < 2)) {
		return false; // BAD sizes
	}
	if((new_w > width) || (new_h > height)) {
		return false; // WRONG sizes
	}
	if(sizeof(usize) == 4) {
		if((width > 32000) || (height > 32000)) {
			return false; // too large
		}
	}
	Allocate(dst, dst_width, dst_height, new_w, new_h);
	//
	usize dx, dy, square; // dx, dy, square - x16 bit
	usize i, absy, x, y, xx, yy, min, max, left, right, cleft, cright, new_w_x4;
	uint32 rbuf, gbuf, bbuf, abuf;
	dx = (width << 16) / new_w;
	--dx; //
	dy = (height << 16) / new_h;
	--dy; //
	square = usize( double(dx) * double(dy) / 65536.0 );
	new_w_x4 = new_w * 4;
	try {
		buffer = new uint32 [new_w_x4 * height]; // 4 channels
	} catch (std::bad_alloc) { // cleaning allocated image..
		delete [] *dst;
		dst = NULL;
		*dst_width = 0;
		*dst_height = 0;
		throw;
	}

	for(y = 0; y < height ; ++y) { // making rows of pixels, progressed by X
		bactptr = (buffer + (y * new_w_x4));
		src_pos = (src + (y * width));
		left = 0;
		for(xx = 0; xx < new_w; ++xx) {
			right = left + dx;
			min = left >> 16; // left Index
			max = (right - 1) >> 16; // right Index
			rbuf = gbuf = bbuf = abuf = 0;
			for(x = min + 1; x < max; ++x) { // main contribution ( * 1 )
				pixel.value = src_pos[x];
				rbuf += pixel.r;
				gbuf += pixel.g;
				bbuf += pixel.b;
				abuf += pixel.a;
			} // end x
			cleft = 0x10000 - (left & 0x0ffff); // left coefficient
			cright = right & 0xffff; // right coefficient
			rbuf <<= 16;
			gbuf <<= 16;
			bbuf <<= 16;
			abuf <<= 16;
			pixel.value = src_pos[min];
			rbuf += pixel.r * cleft;
			gbuf += pixel.g * cleft;
			bbuf += pixel.b * cleft;
			abuf += pixel.a * cleft;
			pixel.value = src_pos[max];
			rbuf += pixel.r * cright;
			gbuf += pixel.g * cright;
			bbuf += pixel.b * cright;
			abuf += pixel.a * cright;
			// rbuf, gbuf, bbuf, abuf - x16, not divided
			usize offset = xx << 2;
			bactptr[offset  ] = rbuf;
			bactptr[offset+1] = gbuf;
			bactptr[offset+2] = bbuf;
			bactptr[offset+3] = abuf;
			// --
			left = right;
		} // end xx
	} // end y
	absy = 0;
	for(yy = 0; yy < new_h; ++yy) {
		// rows done
		left = absy;
		absy += dy;
		right = absy;
		min = left >> 16; // left Index (down)
		max = (right - 1) >> 16; // right Index (up)
		cleft = 0x10000 - (left & 0x0ffff); // left coefficient (down)
		cleft >>= 8;
		cright = right & 0xffff; // right coefficient (up)
		cright >>= 8;
		bactptr = (buffer + (min * new_w_x4));
		for(xx = 0; xx < new_w_x4; xx += 4) {
			bactptr[xx  ] >>= 8;
			bactptr[xx+1] >>= 8;
			bactptr[xx+2] >>= 8;
			bactptr[xx+3] >>= 8;
			bactptr[xx  ] *= cleft;
			bactptr[xx+1] *= cleft;
			bactptr[xx+2] *= cleft;
			bactptr[xx+3] *= cleft;
		}
		for(i = min + 1; i < max; ++i) {
			bactptr2 = (buffer + (i * new_w_x4));
			for(xx = 0; xx < new_w_x4; xx+= 4) {
				bactptr[xx  ] += bactptr2[xx  ];
				bactptr[xx+1] += bactptr2[xx+1];
				bactptr[xx+2] += bactptr2[xx+2];
				bactptr[xx+3] += bactptr2[xx+3];
			}
		}
		bactptr2 = (buffer + (max * new_w_x4));
		uint32* dst_pos = (*dst + (yy * new_w));
		for(xx = 0; xx < new_w_x4; xx+= 4) {
			bactptr[xx  ] += ((bactptr2[xx  ] >> 8) * cright);
			bactptr[xx+1] += ((bactptr2[xx+1] >> 8) * cright);
			bactptr[xx+2] += ((bactptr2[xx+2] >> 8) * cright);
			bactptr[xx+3] += ((bactptr2[xx+3] >> 8) * cright);
			bactptr[xx  ] /= square;
			bactptr[xx+1] /= square;
			bactptr[xx+2] /= square;
			bactptr[xx+3] /= square;
			pixel.r = (ubyte)bactptr[xx  ];
			pixel.g = (ubyte)bactptr[xx+1];
			pixel.b = (ubyte)bactptr[xx+2];
			pixel.a = (ubyte)bactptr[xx+3];
			dst_pos[xx>>2] = pixel.value;
		}
		/// !!
	} // end yy
	delete [] buffer;
	return true;
}; // end resize anisotropic

}; // end Graphic32
