#include "Img32.h"
#include "../TestsUtils/head.h"
#include <string>

using Graphic32::Img32;

int testTGA(bool rle) {
	SRC_FNAME(inp, "tga32.tga");
	TMP_FNAME(tmp, "tga32.tga");
	Img32 img, img2;
	int errors = 0;
	errors += img.readTGA(inp.c_str()) == 0 ? 1 : 0;
	errors += img.writeTGA(tmp.c_str(), rle) == 0 ? 1 : 0;
	errors += img2.readTGA(tmp.c_str()) == 0 ? 1 : 0;
	errors += img.isEqual(img2) == 0 ? 1 : 0;
	return errors;
};

NTEST(Graphic32_Img32_TGA_rle) {
	ASSERT( testTGA(true) == 0 )
};

NTEST(Graphic32_Img32_TGA_non_rle) {
	ASSERT( testTGA(false) == 0 )
};

NTEST(Graphic32_Img32_rotate) {
	SRC_FNAME(inp, "tga32.tga");
	Img32 img, img2, img3, img4;
	img.readTGA(inp.c_str());
	img.rotate(1, img2);
	img2.rotate(1, img3);
	img3.rotate(2, img4);
	ASSERT( img.isEqual(img4) )
};

NTEST(Graphic32_Img32_CIMG_common) {
	SRC_FNAME(inp, "cimg32.cimg");
	TMP_FNAME(tmp, "cimg32.cimg");
	Img32 img, img2;
	ASSERT( img.readCimg(inp.c_str()) )
	ASSERT( img.writeCimg(tmp.c_str()) )
	ASSERT( img2.readCimg(tmp.c_str()) )
	ASSERT( img.isEqual(img2) )
};

NTEST(Graphic32_Img32_drawRect) {
	SRC_FNAME(inp1, "cimg32.cimg");
	SRC_FNAME(inp2, "tga32.tga");
	SRC_FNAME(ethalon, "test0000.cimg");
	Img32 img1, img2, img3;
	ASSERT( img1.readCimg(inp1.c_str()) )
	ASSERT( img2.readTGA(inp2.c_str()) )
	ASSERT( img1.drawRect(0, 0, img2.width(), img2.height(), img2, 10, 10, Graphic32::D_ALPHA) )
	ASSERT( img3.readCimg(ethalon.c_str()) )
	ASSERT( img1.isEqual(img3) )
};

NTEST(Graphic32_Img32_drawRect_ranges) {
	SRC_FNAME(inp, "cimg32.cimg");
	SRC_FNAME(ethalon, "drawRect_2.cimg");
	Img32 dst, src;
	ASSERT( src.readCimg(inp.c_str()) )
	{
		Img32 tmp;
		ASSERT( src.cycle(tmp, 16, 12) )
		src.swap(tmp);
	}
	dst.alloc(300, 120);
	dst.fillRect(0x80000000);
	for(int y = 0; y < 40; y += 30) {
		ASSERT( dst.drawRect(-2, -1+y, 16, 12, src, 0, 0, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(20, -1+y, 16, 12, src, 0, 0, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(40, -1+y, 10, 10, src, 2, 1, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(60, -1+y, 10, 14, src, -2, -1, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(80, -1+y, 19, 9, src, 2, -1, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(100, -1+y, 17, 16, src, 2, 1, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(120, -1+y, 30, 16, src, -2, 1, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(160, -1+y, 30, 10, src, 2, 1, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(200, -1+y, 30, 16, src, -2, -1, Graphic32::D_SOLID) )
		ASSERT( dst.drawRect(240, -1+y, 55, 11, src, -2, 1, Graphic32::D_SOLID) )
	}
	ASSERT( dst.drawRect(2, 60, 17, 30, src, -2, -1, Graphic32::D_SOLID) )
	ASSERT( dst.drawRect(20, 60, 17, 30, src, -2, 1, Graphic32::D_SOLID) )
	ASSERT( dst.drawRect(40, 60, 17, 40, src, 0, -1, Graphic32::D_SOLID) )
	ASSERT( dst.drawRect(60, 60, 17, 40, src, 0, 1, Graphic32::D_SOLID) )
	ASSERT( dst.drawRect(80, 60, 81, 45, src, 1, -1, Graphic32::D_SOLID) )
	ASSERT( src.readCimg(ethalon.c_str()) )
	ASSERT( src.isEqual(dst) )
};

NTEST(Graphic32_Img32_setXorOf) {
	Img32 im1, im2, im3;
	im1.alloc(500, 400); // random data
	im1.clone(im2);
	im1.clone(im3);
	im1.dealloc();
	ASSERT( im1.setXorOf(im2, im3) )
	ASSERT( im2.fillRect(0) )
	ASSERT( im1.isEqual(im2) )
};
