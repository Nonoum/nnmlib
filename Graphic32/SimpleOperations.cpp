#include "functions.h"
#include "../Helpers/MinMax.h"

namespace Graphic32 {

using Helpers::max2;

bool FillWithPixel(uint32* dst, usize width, usize height, ssize x, ssize y, ssize cx, ssize cy, uint32 pixel, bool keep_alpha) {
	if(! dst) {
		return false;
	}
	if(! BoundsCheck(width, height, x, y, cx, cy)) {
		return true; // out of bounds
	}
	// now cx is x_end, cy is y_end
	uint32* ptr;
	ssize i, j;
	cx -= x; // correcting
	ptr = dst + (width * y + x);
	if(! keep_alpha) {
		for(i = y; i < cy; ++i) {
			for(j = cx - 1; j >= 0; --j) {
				ptr[j] = pixel;
			}
			ptr += width;
		}
	} else { // keep alpha
		for(i = y; i < cy; ++i) {
			for(j = cx - 1; j >= 0; --j) {
				ptr[j] &= 0xff000000;
				ptr[j] |= (pixel & 0x00ffffff);
			}
			ptr += width;
		}
	}
	return true;
}; // end fill with pixel

bool XorWithPixel(uint32* dst, usize width, usize height, ssize x, ssize y, ssize cx, ssize cy, uint32 pixel) {
	if(! dst) {
		return false;
	}
	if(! BoundsCheck(width, height, x, y, cx, cy)) {
		return true; // out of bounds
	}
	// now cx is x_end, cy is y_end
	uint32* ptr;
	ssize i, j;
	cx -= x; // correcting
	ptr = dst + (width * y + x);
	for(i = y; i < cy; ++i) {
		for(j = cx - 1; j >= 0; --j) {
			ptr[j] ^= pixel;
		}
		ptr += width;
	}
	return true;
}; // end xor with pixel

bool Cut(const uint32* src, usize width, usize height,
		ssize c_left, ssize c_right, ssize c_up, ssize c_down, uint32** dst, usize* dst_width, usize* dst_height) {

	if((! src) || (! dst)) {
		return false;
	}
	ssize xs, xe, ys, ye;
	ssize x, y, cx, cy;
	c_left = max2(c_left, ssize(0));
	c_right = max2(c_right, ssize(0));
	c_down = max2(c_down, ssize(0));
	c_up = max2(c_up, ssize(0));
	xs = c_left;
	xe = width - c_right;
	if(xs >= xe) {
		return false;
	}
	ys = c_down;
	ye = height - c_up;
	if(ys >= ye) {
		return false;
	}
	Allocate(dst, dst_width, dst_height, xe-xs, ye-ys);
	cx = xe - xs;
	cy = ye - ys;
	const uint32* pin = src;
	uint32* pout = *dst;
	pin += ys * width + xs;
	for(y = 0; y < cy; ++y) {
		for(x = 0; x < cx; ++x) {
			pout[x] = pin[x];
		}
		pin += width;
		pout += cx;
	}
	return true;
}; // end cut

template <class T> T DivideRoundUp(T val, T divider) {
	val += divider - 1;
	return val / divider;
};

bool Cycle(const uint32* src, usize width, usize height,
		uint32** dst, usize* dst_width, usize* dst_height, usize new_w, usize new_h) {

	if((! src) || (! width) || (! height)) {
		return false;
	}
	Allocate(dst, dst_width, dst_height, new_w, new_h);
	const usize ytimes = DivideRoundUp(new_h, height);
	const usize xtimes = DivideRoundUp(new_w, width);
	uint32* dst_ptr = *dst;
	usize yout = 0;
	for(usize i = 0; i < ytimes; ++i) {
		const uint32* src_ptr = src;
		const usize ylimit = Helpers::min2(new_h - yout, height);
		for(usize y = 0; y < ylimit; ++y) {
			usize xout = 0;
			for(usize j = 0; j < xtimes; ++j) {
				const usize xlimit = Helpers::min2(new_w - xout, width);
				for(usize x = 0; x < xlimit; ++x, ++xout) {
					dst_ptr[xout] = src_ptr[x];
				}
			}
			src_ptr += width;
			dst_ptr += new_w;
		}
		yout += height;
	}
	return true;
};

}; // end Graphic32
