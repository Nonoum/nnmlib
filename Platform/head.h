#pragma once
#include "Application.h"
#include "Window.h"
/*
	Platform dependent implementations for main stuff.
	Delivers minimal functionality for simple cross-platform UI applications.

	By default, all functionality is supposed to be used from main thread only, if none other specified.
*/