#include "Application.h"

namespace Platform {

static volatile bool is_quit_requested = false;

const char* Application :: checkedStringFor(const char* key, const char* default_str) {
	const char* str = stringFor(key);
	if(! (str && *str)) {
		str = default_str;
	}
	return str;
};

bool Application :: isQuitRequested() {
	return is_quit_requested;
};

void Application :: requestQuit(bool need_quit) {
	is_quit_requested = need_quit;
	if(need_quit) {
		onQuitRequest();
	}
};

ApplicationData& Application :: appData() {
	static ApplicationData app_data;
	return app_data;
};

}; // end Platform
