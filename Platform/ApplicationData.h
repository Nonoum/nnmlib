#pragma once
#include <stdlib.h>

namespace Platform {

/*
	Some parameters of application.
*/
struct ApplicationData {
	void* icon; // icon for windows (OS data)

	ApplicationData(void* _icon = NULL) : icon(_icon) {
	};
};

}; // end Platform
