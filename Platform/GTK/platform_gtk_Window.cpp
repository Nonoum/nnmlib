#include "platform_gtk_WindowData.h"
#include "platform_gtk_Wins.h"

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <cairo.h>
#include <cairo-xlib.h>

#include <assert.h>

namespace Platform {

// forward declaration
static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data);

WindowsMap& getWindows() {
	static WindowsMap wnds;
	return wnds;
};

static void CloseWindow(WindowData* wdata) {
	gtk_widget_destroy(wdata->window);
	delete_event(wdata->window, NULL, wdata);
};

void CloseAllWindows() {
	while(getWindows().begin() != getWindows().end()) {
		CloseWindow(getWindows().begin()->second);
	}
};

//-----------------------------------------------------------------------------------

// on window closing
static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
	Window* const client = ((WindowData*)data)->client;
	auto iter = getWindows().find( client );
	if(iter != getWindows().end()) {
		getWindows().erase(iter);
		client->onClose();
		client->setCallback(NULL);
		delete (WindowData*)data;
	}

	if(getWindows().size() == 0) {
		gtk_main_quit();
	}
	return FALSE;
};

// keyboard part
static void HandleGUIKey(GdkEventKey* event, WindowData* wdata) {
	Graphic32::GUI::Controls key;
	switch (event->keyval) {
	case GDK_KEY_Left: key = Graphic32::GUI::KEY_LEFT; break;
	case GDK_KEY_Right: key = Graphic32::GUI::KEY_RIGHT; break;
	case GDK_KEY_Up: key = Graphic32::GUI::KEY_UP; break;
	case GDK_KEY_Down: key = Graphic32::GUI::KEY_DOWN; break;
	case GDK_KEY_Shift_L: key = Graphic32::GUI::KEY_CAPTURE; break;
	case GDK_KEY_Shift_R: key = Graphic32::GUI::KEY_CAPTURE; break;
	case GDK_KEY_Page_Up: key = Graphic32::GUI::KEY_YSTART; break;
	case GDK_KEY_Page_Down: key = Graphic32::GUI::KEY_YEND; break;
	case GDK_KEY_Home: key = Graphic32::GUI::KEY_XSTART; break;
	case GDK_KEY_End: key = Graphic32::GUI::KEY_XEND; break;
	case GDK_KEY_Insert:
		if(event->state & GDK_CONTROL_MASK) {
			key = Graphic32::GUI::KEY_COPY;
		} else if(event->state & GDK_SHIFT_MASK) {
			key = Graphic32::GUI::KEY_PASTE;
		} else {
			return;
		}
		break;
	default:
		return;
	};
	wdata->client->handleInput(key, (event->type == GDK_KEY_PRESS), Geometry::Point());
};

static gboolean key_press(GtkWidget* widget, GdkEventKey* event, gpointer data) {
	if(event->type != GDK_KEY_PRESS && event->type != GDK_KEY_RELEASE) {
		return FALSE;
	}
	const bool key_down = (event->type == GDK_KEY_PRESS);
	WindowData* const wdata = ((WindowData*)data);
	HandleGUIKey(event, wdata);

	guint32 ch = gdk_keyval_to_unicode(event->keyval);
	if(event->keyval == GDK_KEY_BackSpace) {
		ch = 8;
	} else if(event->keyval == GDK_KEY_Return) {
		ch = '\n';
	}
	if((ch != 0) && (ch < 128)) { // ascii
		wdata->client->handleInput(char(ch), key_down);
	}

	wdata->client->onKeyboardEvent(event->keyval, key_down);
	wdata->client->onEventComplete();
	return FALSE;
};

// mouse part
static gboolean button_press(GtkWidget* widget, GdkEventButton *event, gpointer data) {
	WindowData* const wdata = ((WindowData*)data);
	const Geometry::Point pos(event->x, wdata->size.height() - 1 - event->y);

	if(event->type == GDK_BUTTON_PRESS || event->type == GDK_BUTTON_RELEASE) {
		const bool key_down = event->type == GDK_BUTTON_PRESS;
		if(event->button == 1) { // left mouse button
			wdata->client->handleInput(Graphic32::GUI::KEY_MOUSE_LEFT, key_down, pos);
			wdata->client->onMouseEvent(Graphic32::GUI::KEY_MOUSE_LEFT, key_down, pos);
			wdata->client->onEventComplete();
		} else if(event->button == 3) { // right mouse button
			wdata->client->handleInput(Graphic32::GUI::KEY_MOUSE_RIGHT, key_down, pos);
			wdata->client->onMouseEvent(Graphic32::GUI::KEY_MOUSE_RIGHT, key_down, pos);
			wdata->client->onEventComplete();
		}
	} else if(event->type == GDK_2BUTTON_PRESS) {
		if(event->button == 1) {
			wdata->client->handleInput(Graphic32::GUI::KEY_MOUSE_DOUBLE, true, pos);
			wdata->client->onMouseEvent(Graphic32::GUI::KEY_MOUSE_DOUBLE, true, pos);
			wdata->client->onEventComplete();
		}
	}
	return FALSE;
};

static gboolean mouse_motion(GtkWidget* widget, GdkEventButton *event, gpointer data) {
	WindowData* const wdata = ((WindowData*)data);
	const Geometry::Point pos(event->x, wdata->size.height() - 1 - event->y);
	wdata->client->handleInput(Graphic32::GUI::KEY_MOUSE_MOVE, false, pos);
	wdata->client->onMouseEvent(Graphic32::GUI::KEY_MOUSE_MOVE, false, pos);
	wdata->client->onEventComplete();
	return FALSE;
};

// drawing part
static gboolean on_drawingarea_event(GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	cairo_t * const cr = gdk_cairo_create(widget->window);
	const GdkPixbuf* pbuf = ((WindowData*)data)->canvas.getPixBuf();
	gdk_cairo_set_source_pixbuf(cr, pbuf, 0, 0);
	cairo_rectangle(cr, 0, 0, gdk_pixbuf_get_width(pbuf), gdk_pixbuf_get_height(pbuf));
	cairo_fill(cr);
	cairo_destroy(cr);
	return FALSE;
};

// callback implementation
class CallbackImpl : public Graphic32::GUI::CallbackInterface {
	WindowData* const wdata;
public:
	CallbackImpl(WindowData* wd) : wdata(wd) {
	};
	bool action(Graphic32::GUI::CallbackParams& params) {
		if(params.screen) {
			wdata->canvas.draw(*params.screen, params.area);
			if(params.index == params.length - 1) { // last segment is updated
				gtk_widget_queue_draw(wdata->window);
			}
		}
		return false;
	};
};

// engine implementations

static void set_window_geometry(WindowData* wdata) {
	GdkGeometry geo;
	geo.min_width = wdata->size.width();
	geo.max_width = wdata->size.width();
	geo.min_height = wdata->size.height();
	geo.max_height = wdata->size.height();
	gtk_window_set_geometry_hints(GTK_WINDOW(wdata->window), NULL, &geo, GdkWindowHints(GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE));
};

void Window :: display() {
	auto iter = getWindows().find(this);
	if(iter != getWindows().end()) {
		const Geometry::Size sz = iter->first->getRenderSize();
		WindowData* const wdata = iter->second;
		if(sz != wdata->size) {
			wdata->size = sz;
			wdata->canvas.alloc(wdata->size.width(), wdata->size.height());
			render();
			set_window_geometry(wdata);
			gtk_window_resize(GTK_WINDOW(wdata->window), wdata->size.width(), wdata->size.height());
		}
		return;
	}
	GtkWidget* const window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	WindowData* const wdata = new WindowData(this, window);
	getWindows()[this] = wdata;
	setCallback(new CallbackImpl(wdata));

	gtk_window_set_title(GTK_WINDOW(window), name.c_str());
	gtk_widget_set_events(window, GDK_BUTTON_PRESS_MASK
				| GDK_BUTTON_RELEASE_MASK
				| GDK_BUTTON_MOTION_MASK
				| GDK_POINTER_MOTION_MASK);

	g_signal_connect(window, "delete-event", G_CALLBACK(delete_event), wdata);

	wdata->size = getRenderSize();
	if(! wdata->size) {
		wdata->size.setSize(320, 240);
	}
	wdata->canvas.alloc(wdata->size.width(), wdata->size.height());
	gtk_window_set_default_size(GTK_WINDOW(window), wdata->size.width(), wdata->size.height());
	set_window_geometry(wdata);
	render();

	g_signal_connect(window, "key_press_event", (GtkSignalFunc)key_press, wdata);
	g_signal_connect(window, "key_release_event", (GtkSignalFunc)key_press, wdata);

	g_signal_connect(window, "button_press_event", (GtkSignalFunc)button_press, wdata);
	g_signal_connect(window, "button_release_event", (GtkSignalFunc)button_press, wdata);
	g_signal_connect(window, "motion_notify_event", (GtkSignalFunc)mouse_motion, wdata);

	GtkWidget* drawing_area = gtk_drawing_area_new();
	g_signal_connect(G_OBJECT(drawing_area), "expose-event", G_CALLBACK(on_drawingarea_event), wdata);
	gtk_container_add(GTK_CONTAINER(window), drawing_area);

	gtk_widget_show_all(window);
};

bool Window :: isDisplayed() const {
	for(auto& pair : getWindows()) {
		if(pair.first == this) {
			return true;
		}
	}
	return false;
};

void Window :: close() {
	if(isDisplayed()) {
		CloseWindow( getWindows().find(this)->second );
	}
};

}; // end Platform

