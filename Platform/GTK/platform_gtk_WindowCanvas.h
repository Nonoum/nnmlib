#pragma once
#include <gtk/gtk.h>
#include "../../Geometry/Rect.h"
#include "../../Graphic32/Img32.h"

namespace Platform {

using namespace LibTypes;

class WindowCanvas {
	uint32* data;
	ssize wi;
	ssize he;
	GdkPixbuf* pixbuf;

	void dealloc();
	void fillAlpha();
public:
	WindowCanvas();
	~WindowCanvas();
	void alloc(ssize width, ssize height);
	void draw(const Graphic32::Img32& img, Geometry::Rect area);
	GdkPixbuf* getPixBuf();
};

}; // end Platform

