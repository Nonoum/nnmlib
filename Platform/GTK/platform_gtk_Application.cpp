#include <gtk/gtk.h>
#include "../Application.h"
#include "platform_gtk_Wins.h"

namespace Platform {

// forward declaration
void InitTimer(size_t period_milliseconds);

// quit-related code
static const int QUIT_DELAY_MILLIS = 10;

static gboolean QuitFunction(gpointer unused) {
	Application::requestQuit(false);
	CloseAllWindows();
	return FALSE;
};


void Application :: onQuitRequest() {
	g_timeout_add(QUIT_DELAY_MILLIS, (GSourceFunc)QuitFunction, NULL);
};

void Application :: showMessage(const char* caption, const char* message) {
	GtkWidget* dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
							"%s\n\n%s", caption, message);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
};

void Application :: setMainTimer(size_t period_milliseconds) {
	InitTimer(period_milliseconds);
};

}; // end Platform
