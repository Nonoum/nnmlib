#include "platform_gtk_WindowCanvas.h"
#include <assert.h>

namespace Platform {

WindowCanvas :: WindowCanvas()
		: data(NULL), wi(0), he(0), pixbuf(NULL) {
};

WindowCanvas :: ~WindowCanvas() {
	dealloc();
};

void WindowCanvas :: dealloc() {
	delete [] data;
	data = NULL;
	wi = 0;
	he = 0;
	if(pixbuf) {
		g_object_unref(pixbuf);
	}
	pixbuf = NULL;
};

void WindowCanvas :: alloc(ssize width, ssize height) {
	if(width != wi || height != he) {
		dealloc();
		data = new uint32[width * height];
		wi = width;
		he = height;
		fillAlpha();
		pixbuf = gdk_pixbuf_new_from_data((guchar*)data, GDK_COLORSPACE_RGB, TRUE, 8, wi, he, wi*sizeof(uint32), NULL, NULL);
	}
};

void WindowCanvas :: fillAlpha() {
	const size_t cnt = wi * he;
	for(size_t i = 0; i < cnt; ++i) {
		((guchar*)(&data[i]))[3] = 0xff;
	}
};

void WindowCanvas :: draw(const Graphic32::Img32& img, Geometry::Rect area) {
	assert(img.size() == Geometry::Size(wi, he));
	area = area.checkedIntersection( Geometry::Size(wi, he) );

	const ssize y_end = area.getYEnd();
	for(ssize y = area.y(); y < y_end; ++y) {
		const guchar* src = (const guchar*)(img.getData() + y * img.width() + area.x());
		guchar* dst = (guchar*) (data + (he - 1 - y) * wi + area.x());
		for(ssize i = 0; i < area.width(); ++i) {
			dst[0] = src[2];
			dst[1] = src[1];
			dst[2] = src[0];
			dst += sizeof(uint32);
			src += sizeof(uint32);
		}
	}
};

GdkPixbuf* WindowCanvas :: getPixBuf() {
	return pixbuf;
};

}; // end Platform

