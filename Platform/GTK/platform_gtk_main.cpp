#include "../Application.h"
#include <gtk/gtk.h>
#include <stdexcept>

static const char* LaunchErrorMsg() {
	return Platform::Application::checkedStringFor("launch_error", "An error has occurred.");
};

static const char* LaunchErrorAdviseMsg() {
	return Platform::Application::checkedStringFor("launch_error_advise", "\nTry to reinstall the package.");
};

int main(int argc, char *argv[]) {
	gtk_init(&argc, &argv);
	int exit_code = 0;
	try {
		Platform::Application::onLaunch(argc, (const char**)argv);
		gtk_main();
	} catch (std::exception& e) {
		std::string s = e.what();
		s.append(LaunchErrorAdviseMsg());
		Platform::Application::showMessage(LaunchErrorMsg(), s.c_str());
		exit_code = 1;
	} catch (...) {
		std::string s = LaunchErrorMsg();
		s.append(LaunchErrorAdviseMsg());
		Platform::Application::showMessage(LaunchErrorMsg(), s.c_str());
		exit_code = 2;
	}
	Platform::Application::onExit(exit_code);
	return exit_code;
};

