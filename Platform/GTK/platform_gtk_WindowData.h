#pragma once
#include "platform_gtk_WindowCanvas.h"
#include "../Window.h"

namespace Platform {

struct WindowData {
	WindowCanvas canvas;
	Window* const client;
	GtkWidget* const window;
	Geometry::Size size;

	WindowData(Window* _client, GtkWidget* gtk_window)
			: client(_client), window(gtk_window) {
	};
};

}; // end Platform

