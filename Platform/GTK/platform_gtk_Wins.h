#pragma once
#include <gtk/gtk.h>
#include <map>
#include "platform_gtk_WindowData.h"

namespace Platform {

typedef std::map<Window*, WindowData*> WindowsMap;

WindowsMap& getWindows();
void CloseAllWindows();

}; // end Platform
