#include "../Application.h"
#include "platform_gtk_Wins.h"
#include <gtk/gtk.h>
#include <sys/time.h>
#include <stdexcept>

namespace Platform {

static gboolean TimerFunction(size_t period);

static size_t GetTimeMillis() {
	struct timeval te;
	gettimeofday(&te, NULL);
	return te.tv_sec*1000 + te.tv_usec/1000;
};

class Timer {
	size_t period;
	bool started;

	Timer() : period(0), started(false) {
	};
	~Timer() {
		started = false;
	};
public:
	void init(size_t period_milliseconds) {
		period = period_milliseconds;
		if(started && (! period)) {
			started = false;
		} else if((! started) && period) {
			started = true;
			g_timeout_add(period, (GSourceFunc)TimerFunction, (gpointer)period);
		}
	};
	bool isStarted() const { return started; };
	size_t getInterval() const { return period; };
	static Timer& get() {
		static Timer timer;
		return timer;
	};
};


static gboolean TimerFunction(size_t period) {
	const size_t start_time = GetTimeMillis();
	for(auto& pair : getWindows()) {
		try {
			pair.first->onTimer(period);
		} catch(std::exception& e) {
			Application::showMessage(Application::checkedStringFor("error_caption", "Error"), e.what());
		}
	}
	if(! Timer::get().isStarted()) { // timer was stopped
		return FALSE;
	}
	period = Timer::get().getInterval();
	const size_t end_time = GetTimeMillis();
	const size_t millis = period - (end_time - start_time);
	if(millis < period) { // normal
		g_timeout_add(millis, (GSourceFunc)TimerFunction, (gpointer)period);
	} else { // overflowed period
		g_timeout_add(period, (GSourceFunc)TimerFunction, (gpointer)period);
	}
	return FALSE;
};

// interface implementation
void InitTimer(size_t period_milliseconds) {
	Timer::get().init(period_milliseconds);
};

}; // end Platform

