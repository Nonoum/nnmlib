#pragma once
#include "../Graphic32/GUI/head.h"

namespace Platform {

class Window : public Graphic32::GUI::GUIWindow {
	std::string name;
	bool want_close;
public:
	Window(Geometry::Size size, const char* _name, DrawType type); // creates window object without creating OS window

	Geometry::Size getRenderSize() const; // returns size of internal GUIWindow object
	bool setRenderSize(Geometry::Size size); // sets render size for GUIWindow, resizes OS window if it's exist, and calls render. if size is same with old size - does nothing.

	void display(); // creates and displays OS window, if it's not created yet
	void close(); // closes window as soon as possible
	bool isDisplayed() const; // whether there is displayed OS window for (*this)
	void requestClose(); // safe request for closing window, causes call of 'close' in 'onEventComplete'. use it within callbacks.

	// events to POST-process
	virtual void onResize(Geometry::Size old_size, Geometry::Size current_size); // called after resizing of current window and before it's re-rendering
	virtual void onMouseEvent(Graphic32::GUI::Controls key, bool key_down, Geometry::Point decart_pos); // called after processing of mouse event
	virtual void onKeyboardEvent(int raw_os_key, bool key_down); // called after processing of keyboard event
	virtual void onClose(); // called when OS window gets closed
	virtual void onTimer(size_t period_milliseconds); // called on main thread from main Application timer

	// events with not empty implementation
	virtual void onEventComplete(); // called after processing of any mouse or keyboard event
};

}; // end Platform
