#include "Window.h"

namespace Platform {

Window :: Window(Geometry::Size size, const char* _name, DrawType type)
		: GUIWindow(size, type), name(_name), want_close(false) {
};

Geometry::Size Window :: getRenderSize() const {
	return getPlacement().getSize();
};

bool Window :: setRenderSize(Geometry::Size size) {
	const Geometry::Size old = getRenderSize();
	if(size == old) {
		return false;
	}
	const bool result = GUIWindow::setRenderSize(size);
	onResize(old, size);
	if(isDisplayed()) {
		display(); // update OS window size
	}
	render();
	return result;
};

void Window :: requestClose() {
	want_close = true;
};

void Window :: onEventComplete() {
	if(want_close) {
		want_close = false;
		close();
	}
};

// event handlers (empty implementations)

void Window :: onResize(Geometry::Size old_size, Geometry::Size current_size) {
};
void Window :: onMouseEvent(Graphic32::GUI::Controls key, bool key_down, Geometry::Point decart_pos) {
};
void Window :: onKeyboardEvent(int raw_os_key, bool key_down) {
};
void Window :: onClose() {
};
void Window :: onTimer(size_t period_milliseconds) {
};

}; // end Platform
