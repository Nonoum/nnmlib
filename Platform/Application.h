#pragma once
#include "ApplicationData.h"
#include <stdlib.h>

namespace Platform {

class Application {
	static void onQuitRequest();
public:
	static bool isQuitRequested(); // returns whether quit was requested
	static void requestQuit(bool need_quit); // leaves request for quit, that will be processed after next event (thread-safe)
	static void showMessage(const char* caption, const char* message); // displays message box
	static void setMainTimer(size_t period_milliseconds); // sets main timer to notify all windows. set 0 to disable main timer
	static const char* checkedStringFor(const char* key, const char* default_str); // returns result of 'stringFor' for (key), or (default_str) if none

	static ApplicationData& appData(); // access application data, set required data during initialization
	// user implementation
	static void onLaunch(int argc, const char* argv[]); // implement entry point here
	static void onExit(int exit_code); // implement some data saving here
	static const char* stringFor(const char* key); // implement for some messages
};

}; // end Platform
