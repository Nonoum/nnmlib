#pragma once
#include "../Window.h"
#include <windows.h>

namespace Platform {

extern HINSTANCE hInstance;
extern DWORD main_thread_id;

typedef std::map<HWND, Window*> WindowsMap;
WindowsMap& getWindows();

}; // end Platform
