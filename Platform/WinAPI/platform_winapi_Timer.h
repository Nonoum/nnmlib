#pragma once
#include <windows.h>
#include <mmsystem.h>

namespace Platform {

static const int TIMER_MESSAGE_ID = WM_APP + 1;

void ProcessTimer(int timer_param); // called from main thread, calls 'onTimer' for every Window

class Timer {
	MMRESULT mmtimer;
	bool created;
public:
	Timer();
	~Timer();
	void create(size_t milliseconds);
	void kill();
};

}; // end Platform
