#include "../Application.h"
#include "platform_winapi_Timer.h"

namespace Platform {

void Application :: onQuitRequest() { // empty
};

void Application :: showMessage(const char* caption, const char* message) {
	MessageBox(0, message, caption, MB_OK | MB_ICONERROR);
};

void Application :: setMainTimer(size_t period_milliseconds) {
	static Timer timer;
	timer.create(period_milliseconds);
};

}; // end Platform
