#include "../Application.h"
#include "platform_winapi_Wins.h"
#include "platform_winapi_Timer.h"
#include <vector>
#include <string>
#include <algorithm>
#include <functional>

namespace Platform {

HINSTANCE hInstance;
DWORD main_thread_id;

void QuitApplication() {
	for(auto& pair : getWindows()) {
		PostMessage(pair.first, WM_DESTROY, 0, 0);
	}
};

}; // end Platform

static const char* LaunchErrorMsg() {
	return Platform::Application::checkedStringFor("launch_error", "An error has occurred.");
};

static const char* LaunchErrorAdviseMsg() {
	return Platform::Application::checkedStringFor("launch_error_advise", "\nTry to reinstall the package.");
};

static std::string GetModulePath() {
	char buffer[2048];
	GetModuleFileName(NULL, buffer, sizeof(buffer));
	return std::string(buffer);
};

static void InitApplicationCommandLine(std::string cmd_line) {
	std::vector<std::string> argv_strings;
	argv_strings.push_back(GetModulePath());
	size_t i = 0;
	for( ; i < cmd_line.length() ; ) {
		size_t lower = cmd_line.find_first_not_of(" \t", i);
		size_t upper;
		if(cmd_line[lower] == '"' && ((! lower) || (lower > 0 && cmd_line[lower-1] != '\\'))) {
			upper = lower;
			while(1) {
				upper = cmd_line.find_first_of('"', upper + 1);
				if((upper + 1 == 0) || (cmd_line[upper-1] != '\\')) {
					break;
				}
			}
			++lower;
		} else {
			upper = cmd_line.find_first_of(" \t", lower + 1);
		}
		argv_strings.push_back(std::string(cmd_line, lower, upper - lower));
		i = (upper + 1 == 0) ? cmd_line.length() : upper + 1;
	}

	std::vector<const char*> argv(argv_strings.size());
	std::transform(argv_strings.begin(), argv_strings.end(), argv.begin(), std::mem_fun_ref(&std::string::c_str));
	Platform::Application::onLaunch(argv.size(), &*argv.begin());
};

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow) {
	Platform::hInstance = hInstance;
	Platform::main_thread_id = GetCurrentThreadId();
	int exit_code = 0;
	try {
		InitApplicationCommandLine(lpCmdLine);
		MSG msg;
		while(Platform::getWindows().size() > 0) {
			while(GetMessage(&msg, NULL, 0, 0)) {
				if(msg.message == Platform::TIMER_MESSAGE_ID) {
					Platform::ProcessTimer(msg.wParam);
					continue;
				}
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				if(Platform::Application::isQuitRequested()) {
					Platform::Application::requestQuit(false);
					Platform::QuitApplication();
				}
			}
		}
		exit_code = msg.wParam;
	} catch (std::exception& e) {
		std::string s = e.what();
		s.append(LaunchErrorAdviseMsg());
		Platform::Application::showMessage(LaunchErrorMsg(), s.c_str());
		exit_code = 1;
	} catch (...) {
		std::string s = LaunchErrorMsg();
		s.append(LaunchErrorAdviseMsg());
		Platform::Application::showMessage(LaunchErrorMsg(), s.c_str());
		exit_code = 2;
	}
	Platform::Application::onExit(exit_code);
	return exit_code;
};
