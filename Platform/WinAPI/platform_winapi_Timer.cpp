#include "platform_winapi_Timer.h"
#include "platform_winapi_Wins.h"
#include "../Application.h"

#pragma comment (lib, "winmm.lib")

namespace Platform {

static volatile bool timer_processing = false;

void CALLBACK TimerProc(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2) {
	// this will be processed on separate thread
	if(timer_processing) {
		return;
	}
	PostThreadMessage(main_thread_id, TIMER_MESSAGE_ID, dwUser, 0);
};

void ProcessTimer(int timer_param) {
	timer_processing = true;
	for(auto& pair : getWindows()) {
		try {
			pair.second->onTimer(timer_param);
		} catch (std::exception& e) {
			Application::showMessage(Application::checkedStringFor("error_caption", "Error"), e.what());
		}
	}
	timer_processing = false;
};

Timer :: Timer() : created(false) {
};

Timer :: ~Timer() {
	kill();
};

void Timer :: create(size_t milliseconds) {
	kill();
	if(! milliseconds) {
		return;
	}

	TIMECAPS tc;
	timeGetDevCaps(&tc, sizeof(TIMECAPS));
	if( tc.wPeriodMin > milliseconds*2 ) {
		throw std::runtime_error(Application::checkedStringFor("error_init_timer", "Error on initializing timer"));
	}
	const int period = tc.wPeriodMin <= milliseconds ? milliseconds : tc.wPeriodMin;
	timeBeginPeriod(1);
	mmtimer = timeSetEvent(period, 1, TimerProc, (DWORD_PTR)period, TIME_PERIODIC);
	created = true;
};

void Timer :: kill() {
	if(created) {
		created = false;
		timeEndPeriod(1);
		timeKillEvent(mmtimer);
	}
};

}; // end Platform
