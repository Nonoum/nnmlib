#include "../Application.h"
#include "platform_winapi_Wins.h"

namespace Platform {

WindowsMap& getWindows() {
	static WindowsMap windows_map;
	return windows_map;
};

static HWND GetHwndFor(const Window* wnd) {
	for(auto& pair : getWindows()) {
		if(pair.second == wnd) {
			return pair.first;
		}
	}
	return NULL;
};

void DrawInHDC(HDC hdc, Geometry::Point wnd_pos, Geometry::Rect area, const Graphic32::Img32& img) {
	if(! img) {
		return;
	}
	BITMAPINFO bm_info;
	bm_info.bmiHeader.biSize = 40;
	bm_info.bmiHeader.biSizeImage = 0;
	bm_info.bmiHeader.biWidth = img.width();
	bm_info.bmiHeader.biHeight = img.height();
	bm_info.bmiHeader.biPlanes = 1;
	bm_info.bmiHeader.biBitCount = 32;
	bm_info.bmiHeader.biCompression = BI_RGB;
	int x = wnd_pos.x() + area.x();
	int y = wnd_pos.y() + img.height() - area.y() - area.height();
	int xr = x + area.width();
	int yu = y + area.height();
	SetDIBitsToDevice(hdc, x, y, area.width(), area.height(),
		area.x(), 0, 0, area.height(), img.getData(), &bm_info, DIB_RGB_COLORS);
};

class DrawingCallback : public Graphic32::GUI::CallbackInterface {
	HDC hdc;
public:
	DrawingCallback(HDC _hdc) : hdc(_hdc) {
	};
	bool action(Graphic32::GUI::CallbackParams& params) {
		if(params.screen) {
			if(params.index == 0) {
				Geometry::Rect rect = params.area; // ... TODO: find how to use SetDIBitsToDevice to draw part of image
				rect.setRect(0, 0, params.screen->width(), params.screen->height()); // drawing full image
				DrawInHDC(hdc, Geometry::Point(), rect, *params.screen);
			}
		}
		return false;
	};
};

static void HandleButtonInput(WPARAM wParam, bool key_down, Window* window) {
	Graphic32::GUI::Controls key;
	bool valid_key = true;

	switch (wParam) {
	case VK_LEFT: key = Graphic32::GUI::KEY_LEFT; break;
	case VK_RIGHT: key = Graphic32::GUI::KEY_RIGHT; break;
	case VK_UP: key = Graphic32::GUI::KEY_UP; break;
	case VK_DOWN: key = Graphic32::GUI::KEY_DOWN; break;
	case VK_SHIFT: key = Graphic32::GUI::KEY_CAPTURE; break;
	case VK_PRIOR: key = Graphic32::GUI::KEY_YSTART; break;
	case VK_NEXT: key = Graphic32::GUI::KEY_YEND; break;
	case VK_HOME: key = Graphic32::GUI::KEY_XSTART; break;
	case VK_END: key = Graphic32::GUI::KEY_XEND; break;
	case VK_INSERT:
		if(GetKeyState(VK_SHIFT) & (~1)) {
			key = Graphic32::GUI::KEY_PASTE;
		} else if(GetKeyState(VK_CONTROL) & (~1)) {
			key = Graphic32::GUI::KEY_COPY;
		} else {
			valid_key = false;
		}
		break;
	default:
		valid_key = false;
		break;
	};
	if(valid_key) {
		window->handleInput(key, key_down, Geometry::Point());
	}
	window->onKeyboardEvent(wParam, key_down);
	window->onEventComplete();
};

static void HandleMouseInput(WPARAM wParam, LPARAM lParam, UINT message, Window* window) {
	Graphic32::GUI::Controls key;
	const int x = LOWORD(lParam);
	const int y = HIWORD(lParam);
	const int height = window->getRenderSize().height();
	const Geometry::Point pos = Geometry::Point(x, height - y);
	bool key_down = false;

	switch (message) {
	case WM_LBUTTONDOWN: key = Graphic32::GUI::KEY_MOUSE_LEFT; key_down = true; break;
	case WM_LBUTTONUP: key = Graphic32::GUI::KEY_MOUSE_LEFT; key_down = false; break;
	case WM_RBUTTONDOWN: key = Graphic32::GUI::KEY_MOUSE_RIGHT; key_down = true; break;
	case WM_RBUTTONUP: key = Graphic32::GUI::KEY_MOUSE_RIGHT; key_down = false; break;
	case WM_MOUSEMOVE: key = Graphic32::GUI::KEY_MOUSE_MOVE; key_down = false; break;
	case WM_LBUTTONDBLCLK: key = Graphic32::GUI::KEY_MOUSE_DOUBLE; key_down = true; break;
	default:
		return;
	};
	window->handleInput(key, key_down, pos);
	window->onMouseEvent(key, key_down, pos);
	window->onEventComplete();
};

static void SetClientSize(HWND hwnd, int client_width, int client_height) {
	if(IsWindow(hwnd)) {
		DWORD dwStyle = GetWindowLongPtr(hwnd, GWL_STYLE);
		DWORD dwExStyle = GetWindowLongPtr(hwnd, GWL_EXSTYLE);
		RECT rc = {0, 0, client_width, client_height};

		AdjustWindowRectEx(&rc, dwStyle, FALSE, dwExStyle);
		SetWindowPos(hwnd, NULL, 0, 0, rc.right - rc.left, rc.bottom - rc.top, SWP_NOZORDER | SWP_NOMOVE);
	}
};

// Window Proc
static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	Window* window = NULL;

	auto it = getWindows().find(hwnd);
	if(it == getWindows().end()) {
		// on window creating
		it = getWindows().find(0); // initial storage
		if(it == getWindows().end()) {
			return 0;
		}
		window = it->second;
		getWindows().erase(0);
		getWindows()[hwnd] = window;
	} else {
		window = it->second;
	}

	switch (message)
	{
	case WM_CREATE:
		if(window) {
			window->setCallback( new DrawingCallback(GetDC(hwnd)) );
		}
		return 0;

	case WM_SIZE:
		if(window) {
			const Geometry::Size os_size(LOWORD(lParam), HIWORD(lParam));
			const Geometry::Size render_size = window->getRenderSize();
			if(render_size == os_size) {
				SetFocus(hwnd);
				return 0;
			}
			SetClientSize(hwnd, render_size.width(), render_size.height());
		}
		return 0;

	case WM_KEYUP:
		if(window) {
			HandleButtonInput(wParam, false, window);
		}
		return 0;

	case WM_KEYDOWN:
		if(window) {
			HandleButtonInput(wParam, true, window);
		}
		return 0;

	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
	case WM_LBUTTONDBLCLK:
		if(window) {
			HandleMouseInput(wParam, lParam, message, window);
		}
		return 0;

	case WM_CHAR:
		if(window) {
			window->handleInput(wParam, true);
			window->onEventComplete();
		}
		return 0;

	case WM_PAINT:
		if(window) {
			window->render();
		}
		break;

	case WM_ERASEBKGND: // must be empty
		return 0;

	case WM_DESTROY:
		getWindows().erase(hwnd); // designating window
		if(window) {
			window->setCallback(NULL);
			window->onClose();
		}
		PostQuitMessage(0);
		return 0;

	default:
		break;
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
};

static void RegisterWindowClass(const char* class_name) {
	static bool registered = false;
	if(! registered) {
		WNDCLASSEX wndclass;
		HICON icon = NULL;
		if(Application::appData().icon != NULL) {
			// for WinAPI appData().icon is expected to be resource id of icon in HINSTANCE.
			icon = LoadIcon(hInstance, (LPSTR)Application::appData().icon);
		}

		wndclass.cbSize        = sizeof(wndclass);
		wndclass.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wndclass.lpfnWndProc   = WndProc;
		wndclass.cbClsExtra    = 0;
		wndclass.cbWndExtra    = 0;
		wndclass.hInstance     = hInstance;
		wndclass.hIcon         = icon;
		wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		wndclass.lpszMenuName  = NULL;
		wndclass.lpszClassName = class_name;
		wndclass.hIconSm       = icon;

		RegisterClassEx(&wndclass);
		registered = true;
	}
};

// interface methods implementation

void Window :: display() {
	// checking if OS window for this object is already exist
	for(const auto& pair : getWindows()) {
		if(pair.second == this) { // OS window for this object is already exist
			RECT rc;
			GetClientRect(pair.first, &rc);
			const int width = rc.right - rc.left;
			const int height = rc.bottom - rc.top;
			SendMessage(pair.first, WM_SIZE, 0, MAKELONG(width, height));
			return;
		}
	}
	const long window_style = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
	static const char* class_name = "NnmLib 2.0 Window class";

	if(! getRenderSize()) {
		assert(false && "non-initialized window size");
		setRenderSize(Geometry::Size(640, 480));
	}
	RegisterWindowClass(class_name);

	// creating OS window and assigning client to it
	getWindows()[0] = this; // saving in pending place
	HWND hwnd = CreateWindow(class_name, name.c_str(),
		window_style,
		CW_USEDEFAULT, CW_USEDEFAULT,
		getRenderSize().width(), getRenderSize().height(),
		NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, SW_NORMAL);
	UpdateWindow(hwnd);
};

void Window :: close() {
	HWND hwnd = GetHwndFor(this);
	if(hwnd != NULL) {
		DestroyWindow(hwnd);
	}
};

bool Window :: isDisplayed() const {
	return GetHwndFor(this) != NULL;
};

}; // end Platform
