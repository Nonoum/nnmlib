#include "head.h"
#include "../Graphic32/functions.h" // Graphic32::Allocate
#include <string.h> // memset
#include <memory>

namespace TargaImageCodec {

uint32 ReadTGA(std::istream& src_file, uint32** dst_data, usize* dst_width, usize* dst_height, TARGA_HEADER* output_info) {
	// reads 32bit OR 24bit image to 32bit (*dst_data) and fills alpha with 0xFF if needed
	TARGA_HEADER hdr;
	if(! src_file.read((char*)&hdr, sizeof(hdr))) {
		return 0; // reading error
	}
	usize i, n;
	ubyte info, compressed;
	uint32 *p, pixel;
	if((hdr.bpp == 32)||(hdr.bpp == 24)) {
		Graphic32::Allocate(dst_data, dst_width, dst_height, hdr.width, hdr.height);
		p = *dst_data;
		if(output_info) {
			*output_info = hdr;
		}
	} else {
		return 0; // wrong bpp
	}
	const usize count = hdr.width * hdr.height;
	src_file.seekg(hdr.id_length, std::ios::cur);
	if(hdr.bpp == 32) { // 32 bit
		if(hdr.data_type == 2) { // not packed
			if(! src_file.read((char*)p, count*sizeof(uint32))) {
				return 0; // reading error
			}
			return 1; // success
		} else if(hdr.data_type == 10) { // packed
			for(i = 0; i < count; ) {
				if(! src_file.read((char*)&info, 1)) {
					return 0; // reading error
				}
				compressed = info & 0x80;
				n = (usize(info) & 0x7f) + 1;
				if(compressed) { // compressed block
					if(! src_file.read((char*)&pixel, sizeof(uint32))) {
						return 0; // reading error
					}
					for( ; n ; --n, ++i) {
						p[i] = pixel;
					}
				} else { // not compressed block
					if(! src_file.read((char*)&p[i], sizeof(uint32) * n)) {
						return 0; // reading error
					}
					i += n;
				}
			} // i
		} else {
			return 0; // unknown format
		}
		return 1; // success
	} else if(hdr.bpp == 24) { // 24 bit, alpha will be 0xff
		pixel = 0xff000000; // alpha = 0xff
		if(hdr.data_type == 2) { // not packed
			for(i = 0; i < count; ++i) {
				if(! src_file.read((char*)&pixel, sizeof(uint32) - 1)) {
					return 0; // reading error
				}
				p[i] = pixel;
			}
			return 1; // success
		} else if(hdr.data_type == 10) { // packed
			for(i = 0; i < count; ) {
				if(! src_file.read((char*)&info, 1)) {
					return 0; // reading error
				}
				compressed = info & 0x80;
				n = (usize(info) & 0x7f) + 1;
				if(compressed) { // compressed block
					if(! src_file.read((char*)&pixel, sizeof(uint32) - 1)) {
						return 0; // reading error
					}
					for( ; n ; --n, ++i) {
						p[i] = pixel;
					}
				} else { // not compressed block
					for( ; n ; --n, ++i) {
						if(! src_file.read((char*)&pixel, sizeof(uint32) - 1)) {
							return 0; // reading error
						}
						p[i] = pixel;
					}
				}
			}
			return 1; // success
		}
		return 0; // unknown format
	}
	return 1; // success
}; // end Read TGA (one full image)

uint32 WriteTGA32(std::ostream& dst_file, bool rle, const uint32* src, usize width, usize height) {
	if(! src) {
		return 0; // error: null pointer
	}
	ubyte info;
	TARGA_HEADER *hdr;
	uint32 first, pixel;
	const usize count = width * height;
	if(count < 3) {
		return 0; // BADSIZE;
	}
	usize i, j, start, num;
	uint32 result = 1;
	const size_t buffer_size = sizeof(TARGA_HEADER) + (rle ? (count * sizeof(uint32)) + (count/128) + 1 : 0);
	std::unique_ptr<ubyte[]> ptr(new ubyte[buffer_size]);
	//
	hdr = (TARGA_HEADER*)ptr.get();
	memset(ptr.get(), 0, sizeof(TARGA_HEADER));
	hdr->width = (uint16)width;
	hdr->height = (uint16)height;
	hdr->bpp = 32;
	if(! rle) // not compressed format
	{
		hdr->data_type = 2;
		if(! dst_file.write((char*)ptr.get(), sizeof(TARGA_HEADER))) {
			result = 0;
		} else {
			if(! dst_file.write((char*)src, count * sizeof(uint32))) {
				result = 0;
			}
		}
	} else { // compressed format
		hdr->data_type = 10;
		i = sizeof(TARGA_HEADER); // output bytes index
		start = 0; // start index for block
		first = src[0]; // previous pixel
		bool cond = (first == src[1]);
		num = cond ? 0 : 1; // block length
		for(j = 2; j < count; ++j) {
			pixel = src[j];
			if(first == pixel) { // compressable
				if(cond) { // and was compressable
					if(num < 126) {
						++num;
					} else {
						++num;
						info = (ubyte)((num & 0x07f) | 0x80);
						ptr.get()[i] = info;
						++i;
						*((uint32*)(ptr.get() + i)) = first;
						i += 4;
						start = j;
						num = 0;
						cond = false;
					}
				} else { // compressable, but wasn't
					if(num) { // !
						--num; // !
						info = (ubyte)(num & 0x07f);
						ptr.get()[i] = info;
						++i;
						for(usize k = 0; k < num + 1; ++k, i += 4) {
							*((uint32*)(ptr.get() + i)) = src[start + k];
						}
					}
					cond = true;
					start = j - 1;
					num = 0;
				}
			} else { // not compressable
				if(cond) { // but was compressable
					info = (ubyte)(((num + 1) & 0x07f) | 0x80);
					ptr.get()[i] = info;
					++i;
					*((uint32*)(ptr.get() + i)) = first;
					i += 4;
					start = j;
					num = 0;
					cond = false;
				} else { // not compressable and wasn't
					if(num < 127) {
						++num;
					} else {
						info = (ubyte)(num & 0x07f);
						ptr.get()[i] = info;
						++i;
						for(usize k = 0; k < num + 1; ++k, i += 4) {
							*((uint32*)(ptr.get() + i)) = src[start + k];
						}
						start = j;
						num = 0;
					}
				}
			}
			first = pixel;
		}
		num = count - start - 1;
		info = (ubyte)(num & 0x07f);
		ptr.get()[i] = info;
		++i;
		for(usize k = 0; k < num + 1; ++k, i += 4) {
			*((uint32*)(ptr.get() + i)) = src[start + k];
		}
		if(! dst_file.write((char*)ptr.get(), i)) {
			result = 0;
		}
	}
	return result;
}; // end write TGA 32 (one full image)

}; // end TargaImageCodec
