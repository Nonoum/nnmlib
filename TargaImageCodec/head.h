#pragma once
#include "../LibTypes/head.h"
#include <iostream>

/*************************************************************************************
*************************************************************************************/

namespace TargaImageCodec {

using namespace LibTypes;

#pragma pack (push, 1)
typedef struct
{
	ubyte id_length;
	ubyte color_map;
	ubyte data_type;
	ubyte color_map_info[5];
	uint16 x_origin;
	uint16 y_origin;
	uint16 width;
	uint16 height;
	ubyte bpp;
	ubyte description;
} TARGA_HEADER;
#pragma pack (pop)

uint32 ReadTGA(std::istream& src_file, uint32** dst_data, usize* dst_width, usize* dst_height, TARGA_HEADER* output_info = NULL);
/*	'ReadTGA' reads 24 or 32 bit image from (src_file) with TARGA format [*.tga] to
	(*dst_data) with sizes (*dst_width, *dst_height), deletes frevious data first and then
	allocates new.
	Writes tga file header into (*output_info) if it's not NULL.
	returns 1 if succeeds, 0 if fails.
*/

uint32 WriteTGA32(std::ostream& dst_file, bool rle, const uint32* src, usize width, usize height);
/*	'WriteTGA32' writes 32 bit image (src) with sizes (width, height) to (dst_file) with
	TARGA format.
	returns 1 if succeeds, 0 if fails.
*/

}; // end TargaImageCodec
