This is somewhat private 'library of everything' containing:
- GUI framework - Graphic32::GUI namespace
- some 2d graphics - Graphic32 namespace (caution, naked work with raw memory)
- my own compressor that appeared surprisingly strong for 2d graphic (my own - doesn't mean It's unique,
	I just don't know such one but I'm sure I'm not first who came up with it's idea) - SequenceLevelsAnalyzer namespace
- my own image codec - CompressedImageCodec namespace
- TGA reader/writer partial implementations - TargaImageCodec namespace
- Huffman codec implementation (ugly but should be optimized as hell) - HuffmanCodec namespace
- platform dependent stuff for WinAPI and Gtk+ for supplying OS functionality to GUI framework - Platform namespace
- simple local incremental build system on python scripts - Build/ directory
- some less major stuff

Most of this code was written pretty long time ago (and almost doesn't use C++11),
so some of it is ugly, some is C-like, some isn't modern, some comments contain grammar mistakes etc, and if I wanted to continue working on this,
I'd rewrite almost everything, but this doesn't make sense for me, as well as keeping code in private repository.

It's opened for a case if someone is curious, or some parts is useful etc.

There is no licenses or anything like that but let's say it's intended as free software. Have fun.
