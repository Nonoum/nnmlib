#include "Utils.h"

namespace CompressedImageCodec {

void SpatialDifferencePack(const ubyte* src, usize interval, usize width, usize x, usize y, usize cx, usize cy, ubyte* dst) {
	usize i, j, ofs1, ofs2, nout, ofsbuf;
	nout = 0;
	ofs1 = (y*width + x + 1) * interval;
	for(i = 1; i < cx; ++i) {
		dst[nout] = src[ofs1] - src[ofs1-interval];
		++nout;
		ofs1 += interval;
	} // horizontal done
	ofs2 = width * interval;
	ofsbuf = ((y + 1)*width + x) * interval;
	for(i = 0; i < cx; ++i) {
		ofs1 = ofsbuf;
		for(j = 1; j < cy; ++j) {
			dst[nout] = src[ofs1] - src[ofs1-ofs2];
			++nout;
			ofs1 += ofs2;
		}
		ofsbuf += interval;
	}
}; // end spatial difference pack

void SpatialDifferenceUnpack(ubyte* dst, const ubyte* diffs, ubyte init_value, usize interval, usize width, usize x, usize y, usize cx, usize cy) {
	usize ofs1, ofs2, i, j, ofsbuf;
	usize k = 0;
	ofs2 = width * interval;
	ofs1 = (y*width + x) * interval;
	dst[ofs1] = init_value;
	for(j = 1; j < cx; ++j) {
		ofs1 += interval;
		init_value = init_value + diffs[k];
		dst[ofs1] = init_value;
		++k;
	}
	ofsbuf = (y*width + x) * interval;
	for(j = 0; j < cx; ++j) {
		ofs1 = ofsbuf;
		init_value = dst[ofs1];
		for(i = 1; i < cy; ++i) {
			ofs1 += ofs2;
			init_value = init_value + diffs[k];
			dst[ofs1] = init_value;
			++k;
		}
		ofsbuf += interval;
	}
}; // end spatial difference unpack

void SpatialAndBearingDifferencesUnpack(ubyte* dst, ubyte* diffs, ubyte init_value, usize interval, ubyte bchannel, usize width, usize x, usize y, usize cx, usize cy) {
// function converts (diffs) differences in same array to neighbour pixels difference if needed, then
// (dst) is filling with recovered data
	usize ofs1, ofs2, i, j, n, ofsbuf;
	if(bchannel) { // bearing channel differences first
		ubyte* bearing = dst - bchannel;
		ubyte diff;
		ofs1 = (y*width + x + 1) * interval;
		n = 0;
		for(i = 1; i < cx; ++i) {
			diff = bearing[ofs1] - bearing[ofs1-interval];
			diffs[n] = diff - diffs[n];
			++n;
			ofs1 += interval;
		}
		ofs2 = width * interval;
		ofsbuf = ((y + 1)*width + x) * interval;
		for(i = 0; i < cx; ++i) {
			ofs1 = ofsbuf;
			for(j = 1; j < cy; ++j) {
				diff = bearing[ofs1] - bearing[ofs1-ofs2];
				diffs[n] = diff - diffs[n];
				++n;
				ofs1 += ofs2;
			}
			ofsbuf += interval;
		}
	}
	// differences inside current channel
	usize k = 0;
	ofs2 = width * interval;
	ofs1 = (y*width + x) * interval;
	dst[ofs1] = init_value;
	for(j = 1; j < cx; ++j) {
		ofs1 += interval;
		init_value = init_value + diffs[k];
		dst[ofs1] = init_value;
		++k;
	}
	ofsbuf = (y*width + x) * interval;
	for(j = 0; j < cx; ++j) {
		ofs1 = ofsbuf;
		init_value = dst[ofs1];
		for(i = 1; i < cy; ++i) {
			ofs1 += ofs2;
			init_value = init_value + diffs[k];
			dst[ofs1] = init_value;
			++k;
		}
		ofsbuf += interval;
	}
}; // end spatial and bearing difference unpack

}; // end CompressedImageCodec
