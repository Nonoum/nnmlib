#include "head.h"
#include "Methods/head.h"
#include "Config.h"
#include "../Graphic32/functions.h" // Graphic32::Allocate
#include <stdexcept>
#include <string.h> // memset, memcpy
#include <sstream>
#include <memory>

namespace CompressedImageCodec {

static void FillChannel(ubyte* dst, usize cnt, ubyte interval, ubyte value) {
	for(usize i = 0; i < cnt; ++i) {
		*dst = value;
		dst += interval;
	}
};

Codec :: Codec() {
	// next free method code is 0x2D
	methods.add(new MethodMultichannelSLA(0x2C, 8, 8), true);
	methods.add(new MethodMultichannelHuffman(0x29, 16, 16), true);
	methods.add(new MethodMultichannelHuffman(0x2A, 32, 32), true);
	methods.add(new MethodMultichannelHuffman(0x2B, 64, 64), true);
	methods.add(new MethodDirectHuffman(0x23), true);
};

Codec :: ~Codec() {
};

uint16 Codec :: getMethodsCount() const {
	return methods.size();
};

usize Codec :: getMethodDescription(uint16 imethod, char* dst, usize limit) {
	std::ostringstream oss;
	methods.getByIndex(imethod)->description(oss);
	--limit;
	const size_t slen = oss.str().length();
	const size_t n = slen < limit ? slen : limit;
	strncpy(dst, oss.str().c_str(), n);
	if(n == limit) {
		dst[limit] = 0;
	}
	return n;
};

void Codec :: allowMethodWithIndex(uint16 imethod, bool allow) {
	methods.allow(imethod, allow);
};

bool Codec :: writeBatchHeader(std::ostream& dst_file, uint16 images_count) const {
	CIMG_BATCH_OLD_HEADER hdr;
	hdr.signature = CIMG_BATCH_HEADER_OLD_SIGNATURE;
	hdr.count = images_count;
	if(! dst_file.write((char*)&hdr, sizeof(hdr))) {
		return false;
	}
	return true;
};

bool Codec :: compress(const uint32* src, usize width, usize height, ubyte nchannels, std::ostream& dst_file) const {
	if(! src) {
		return false;
	}
	usize size;
	std::unique_ptr<char[]> data((char*)compress(src, width, height, nchannels, &size));
	if(! data.get()) {
		return false;
	}
	return ! (! dst_file.write(data.get(), size));
};

void* Codec :: compress(const uint32* src, usize width, usize height, ubyte nchannels, usize* result_size) const {
	if(nchannels > 4) {
		throw std::runtime_error("CompressedImageCodec::Codec::compress: too many channels!!!");
	}
	CIMG_OLD_HEADER hdr;
	usize best_size, cur_size;
	usize sum_size = 0;
	uint16 best_method;
	for(ubyte ichannel = 0; ichannel < nchannels; ++ichannel) {
		best_size = MAX_SIZE;
		MethodParamsCompress params(((const ubyte*)src) + ichannel, width, height, sizeof(uint32), ichannel,
				NULL, NULL, NULL);
		for(MethodsStorage::Iterator i(methods); ! i.isDone() ; ++i) {
			const MethodBase* method = *i;
			cur_size = method->compress(params);
			if(cur_size < best_size) {
				best_size = cur_size;
				best_method = method->getId();
			}
		}
		if(best_size == MAX_SIZE) {
			return NULL; // FAIL
		}
		hdr.methods[ichannel] = (ubyte)best_method;
		sum_size += best_size;
	}
	sum_size /= 8; // bits count to bytes count
	sum_size += sizeof(uint32); // bound safe zone
	sum_size += sizeof(hdr);
	std::unique_ptr<ubyte[]> dst(new ubyte[sum_size]);
	memset(dst.get(), 0, sum_size);
	usize dst_byte = sizeof(hdr);
	ubyte dst_bit = 0;

	for(ubyte ichannel = 0; ichannel < nchannels; ++ichannel) {
		MethodParamsCompress params(((const ubyte*)src) + ichannel, width, height, sizeof(uint32), ichannel,
				dst.get(), &dst_byte, &dst_bit);
		const MethodBase* method = methods.get( hdr.methods[ichannel] );
		method->compress(params);
	}

	if(dst_bit) {
		++dst_byte;
	}
	hdr.signature = CIMG_HEADER_OLD_SIGNATURE;
	hdr.width = (uint16)width;
	hdr.height = (uint16)height;
	hdr.nchannels = nchannels;
	hdr.data_size = uint32(dst_byte - sizeof(hdr));
	memcpy(dst.get(), &hdr, sizeof(hdr));
	*result_size = dst_byte; // returning compressed size in parameters
	return dst.release();
};

usize Codec :: readBatchHeader(std::istream& src_file, uint16* images_count) const {
	// if succeeds - returns count of bytes passed, and writes in (*images_count) real count of images; or returns 0 if fails.
	CIMG_BATCH_OLD_HEADER hdr;
	if(! src_file.read((char*)&hdr, sizeof(hdr))) {
		return 0;
	}
	if(hdr.signature != CIMG_BATCH_HEADER_OLD_SIGNATURE) {
		return 0;
	}
	*images_count = hdr.count;
	return sizeof(hdr);
};

usize Codec :: decompress(std::istream& src_file, uint32** dst, usize* dst_width, usize* dst_height, CimgInfo* dst_info) const {
	CIMG_OLD_HEADER hdr;
	if(! src_file.read((char*)&hdr, sizeof(hdr.signature))) {
		return 0;
	}
	if(hdr.signature != CIMG_HEADER_OLD_SIGNATURE) {
		return 0;
	}
	if(! src_file.read( ((char*)&hdr) + sizeof(hdr.signature), sizeof(hdr) - sizeof(hdr.signature) )) {
		return 0;
	}
	std::unique_ptr<char[]> src(new char[hdr.data_size + sizeof(uint32) + sizeof(hdr)]);
	if(! (! src_file.read(src.get() + sizeof(hdr), hdr.data_size))) {
		memcpy(src.get(), &hdr, sizeof(hdr));
		return decompress(src.get(), dst, dst_width, dst_height, dst_info);
	}
	return 0;
};

usize Codec :: decompress(void* src, uint32** dst, usize* dst_width, usize* dst_height, CimgInfo* dst_info) const {
	if(! src) {
		return 0;
	}
	usize src_byte = 0;
	ubyte src_bit = 0;
	CIMG_OLD_HEADER* hdr = (CIMG_OLD_HEADER*)src;
	if(hdr->signature != CIMG_HEADER_OLD_SIGNATURE) {
		return 0;
	}
	src_byte += sizeof(CIMG_OLD_HEADER);
	Graphic32::Allocate(dst, dst_width, dst_height, hdr->width, hdr->height);
	const usize pixels_cnt = hdr->width * hdr->height;

	// filling info
	if(dst_info) {
		dst_info->nchannels = hdr->nchannels;
		dst_info->compression = float(pixels_cnt * hdr->nchannels) / float(hdr->data_size);
		if(dst_info->need_string_info) {
			std::ostringstream oss;
			oss << int(dst_info->nchannels) << "-channels image." << std::endl;
			oss << "Resolution: " << hdr->width << " x " << hdr->height << std::endl;
			oss << "Compression: " << dst_info->compression << std::endl;
			oss << "Data size: " << hdr->data_size;
			for(ubyte ichannel = 0; ichannel < hdr->nchannels; ++ichannel) {
				oss << std::endl << int(ichannel) << ": ";
				const MethodBase* method = methods.get( hdr->methods[ichannel] );
				if(! method) {
					oss << "Unknown compression method: " << int(hdr->methods[ichannel]);
				} else {
					method->description(oss);
				}
			}
			dst_info->info = oss.str();
		}
	}

	// decompressing
	for(ubyte ichannel = 0; ichannel < hdr->nchannels; ++ichannel) { // decompressing channels
		MethodParamsDecompress params(src, &src_byte, &src_bit,
			((ubyte*)*dst) + ichannel, hdr->width, hdr->height, sizeof(uint32), ichannel);
		const MethodBase* method = methods.get( hdr->methods[ichannel] );
		if(! method) {
			return 0; // OOPS, method is not supported
		}
		method->decompress(params);
	}
	for(ubyte ichannel = hdr->nchannels; ichannel < sizeof(uint32); ++ichannel) { // filling rest channels
		FillChannel(((ubyte*)*dst) + ichannel, pixels_cnt, sizeof(uint32), 0); // filled with 0 (black) values
	}
	if(src_bit) {
		++src_byte;
	}
	return src_byte;
};

const Codec& Codec :: getDefault() {
	static Codec def;
	return def;
};

}; // end CompressedImageCodec
