#pragma once
#include "../BitProcessing/head.h"

/*************************************************************************************
*************************************************************************************/

namespace CompressedImageCodec {

using namespace BitProcessing;

const usize MAX_SIZE = -1;
const usize CODER_BLOCK_BUFFER_SIZE = 64*64; // maximum width * height of coder block (except FULL-size block)

const uint32 CIMG_HEADER_OLD_SIGNATURE = 0x0000630C; // old image signature
const uint32 CIMG_BATCH_HEADER_OLD_SIGNATURE = 0x1000630C; // old image signature

#pragma pack (push, 1)
typedef struct {
	uint32 signature;
	uint16 width;
	uint16 height;
	ubyte nchannels;
	ubyte methods[4];
	uint32 data_size; // count of bytes for further data
} CIMG_OLD_HEADER; // one image
#pragma pack (pop)

#pragma pack (push, 1)
typedef struct {
	uint32 signature;
	uint16 count; // count of images
} CIMG_BATCH_OLD_HEADER; // batch of images
#pragma pack (pop)

}; // end CompressedImageCodec
