#pragma once
#include "../LibTypes/head.h"

namespace CompressedImageCodec {

using namespace LibTypes;

inline void Sub(ubyte* srcdst, const ubyte* subtractors, usize cnt) {
	for(usize i = 0; i < cnt; ++i) {
		srcdst[i] -= subtractors[i];
	}
}; // end sub

void SpatialDifferencePack(const ubyte* src,
	usize interval, usize width, usize x, usize y, usize cx, usize cy, ubyte* dst);

void SpatialDifferenceUnpack(ubyte* dst, const ubyte* diffs, ubyte init_value,
	usize interval, usize width, usize x, usize y, usize cx, usize cy);

void SpatialAndBearingDifferencesUnpack(ubyte* dst, ubyte* diffs, ubyte init_value,
	usize interval, ubyte bchannel, usize width, usize x, usize y, usize cx, usize cy);

}; // end CompressedImageCodec
