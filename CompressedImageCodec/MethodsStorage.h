#pragma once
#include "Methods/MethodBase.h"
#include <map>

namespace CompressedImageCodec {

using Methods::MethodBase;

class MethodsStorage : private NonCopyable {
	typedef uint16 KeyType;
	typedef std::pair<const MethodBase*, bool> ValueType;
	typedef std::map<KeyType, ValueType> CollectionType;
	CollectionType methods;
	//
	KeyType keyByIndex(uint16 index) const;
public:
	~MethodsStorage();
	uint16 size() const;
	void add(const MethodBase* new_method, bool allowed_for_iterator);
	const MethodBase* get(uint16 id) const;
	void allow(uint16 index, bool allowed_for_iterator);
	const MethodBase* getByIndex(uint16 index);
	//-------------------------------------------------
	class Iterator : private NonCopyable {
		CollectionType::const_iterator i;
		const MethodsStorage& storage;
		//
		void reachAllowed();
	public:
		Iterator(const MethodsStorage& mets);
		bool isDone() const;
		void operator ++();
		const MethodBase* operator *() const;
	};
};

}; // end CompressedImageCodec
