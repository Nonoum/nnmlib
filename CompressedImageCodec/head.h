#pragma once
#include "MethodsStorage.h"
#include <iostream>
#include <string>

/*************************************************************************************
*************************************************************************************/

namespace CompressedImageCodec {

using namespace Methods;

struct CimgInfo {
	ubyte nchannels;
	float compression;
	std::string info;
	bool need_string_info;

	CimgInfo(bool _need_string_info = true) : need_string_info(_need_string_info) {
	};
};

class Codec {
	MethodsStorage methods;
public:
	Codec();
	~Codec();
	static const Codec& getDefault();

	uint16 getMethodsCount() const; // returns count of registered methods.
	usize getMethodDescription(uint16 imethod, char* dst, usize limit); // writes description of method with index=(imethod) to (dst) with max size=(limit); returns count of symbols in description.
	void allowMethodWithIndex(uint16 imethod, bool allow = true); // allows or disallows method with index=(imethod).
	// compressing part
	bool writeBatchHeader(std::ostream& dst_file, uint16 images_count) const; // writes a 'batch-of-images' header in (dst_file).
	bool compress(const uint32* src, usize width, usize height, ubyte nchannels, std::ostream& dst_file) const; // returns true if succeeds.
	void* compress(const uint32* src, usize width, usize height, ubyte nchannels, usize* result_size) const; // returns pointer to compressed data, or NULL if fails.
	// decompressing part
	usize readBatchHeader(std::istream& src_file, uint16* images_count) const; // reads 'batch-of-images' header from (src_file).
	usize decompress(std::istream& src_file, uint32** dst, usize* dst_width, usize* dst_height, CimgInfo* dst_info = NULL) const; // returns count of bytes progressed, or 0 if fails.
	usize decompress(void* src, uint32** dst, usize* dst_width, usize* dst_height, CimgInfo* dst_info = NULL) const; // returns count of bytes progressed, or 0 if fails.
};

}; // end CompressedImageCodec
