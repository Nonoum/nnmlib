#pragma once
#include "MethodMultichannelBase.h"

namespace CompressedImageCodec {

namespace Methods {

class MethodMultichannelSLA : public MethodMultichannelBase {
public:
	MethodMultichannelSLA(uint16 id, usize block_w, usize block_h)
			: MethodMultichannelBase(id, block_w, block_h, "Multichannel SLA") {

		compr.set(CompressorInterface::METHOD_SLA);
	};
};

}; // end Methods

}; // end CompressedImageCodec
