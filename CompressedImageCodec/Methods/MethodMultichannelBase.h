#pragma once
#include "MethodBase.h"
#include "CompressorInterface.h"

namespace CompressedImageCodec {

namespace Methods {

class MethodMultichannelBase : public MethodBase {
protected:
	CompressorInterface compr;

	MethodMultichannelBase(uint16 id, usize block_w, usize block_h, const char* method_name);
public:
	usize compressBlock(MethodParamsCompress ps, usize xs, usize xe, usize ys, usize ye) const override;
	void decompressBlock(MethodParamsDecompress ps, usize xs, usize ys, usize cx, usize cy) const override;
};

}; // end Methods

}; // end CompressedImageCodec
