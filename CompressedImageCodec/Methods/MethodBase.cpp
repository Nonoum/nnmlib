#include "MethodBase.h"
#include <stdexcept>

namespace CompressedImageCodec {

namespace Methods {

MethodBase :: MethodBase(uint16 method_id, usize block_w, usize block_h, const char* method_name)
		: id(method_id), b_width(block_w), b_height(block_h), name(method_name), full_screen(false) {

	if((block_w < 1) || (block_h < 1) || (block_w * block_h > CODER_BLOCK_BUFFER_SIZE) || (! method_name)) {
		throw std::runtime_error("MethodBase constructing error");
	}
};

MethodBase :: MethodBase(uint16 method_id, const char* method_name)
		: id(method_id), b_width(0), b_height(0), name(method_name), full_screen(true) {

	if(! method_name) {
		throw std::runtime_error("MethodBase constructing error: method_name == NULL");
	}
};

uint16 MethodBase :: getId() const {
	return id;
};

MethodBase :: ~MethodBase() {
};

void MethodBase :: description(std::ostream& os) const {
	os << name;
	if(full_screen) {
		os << " (FULLxFULL)";
	} else {
		os << "(" << b_width << "x" << b_height << ")";
	}
};

usize MethodBase :: compress(MethodParamsCompress ps) const {
	// ps.dst = NULL to only calculate the size of output
	usize i, j, xs, xe, ys, ye;
	usize bits_cnt = 0;
	for(i = 0; i < ps.height; i += b_height) {
		ys = i;
		ye = (i + b_height) > ps.height ? ps.height : i + b_height;
		for(j = 0; j < ps.width; j += b_width) {
			xs = j;
			xe = (j + b_width) > ps.width ? ps.width : j + b_width;
			bits_cnt += compressBlock(ps, xs, xe, ys, ye);
		}
	}
	return bits_cnt;
};

void MethodBase :: decompress(MethodParamsDecompress ps) const {
	usize i, j, xs, xe, ys, ye;
	for(i = 0; i < ps.height; i += b_height) {
		ys = i;
		ye = (i + b_height) > ps.height ? ps.height : i + b_height;
		for(j = 0; j < ps.width; j += b_width) {
			xs = j;
			xe = (j + b_width) > ps.width ? ps.width : j + b_width;
			decompressBlock(ps, xs, ys, xe-xs, ye-ys);
		}
	}
};

}; // end Methods

}; // end CompressedImageCodec
