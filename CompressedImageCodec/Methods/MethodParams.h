#pragma once
#include "../Config.h"

namespace CompressedImageCodec {

namespace Methods {

struct MethodParamsCompress {
	const ubyte* src;
	usize width;
	usize height;
	usize interval;
	ubyte channel_from_zero;

	void* dst;
	usize* dst_byte;
	ubyte* dst_bit;

	MethodParamsCompress(const ubyte* _src, usize _width, usize _height, usize _interval, ubyte _channel_from_zero,
		void* _dst, usize* _dst_byte, ubyte* _dst_bit)
		: src(_src), width(_width), height(_height), interval(_interval), channel_from_zero(_channel_from_zero),
		dst(_dst), dst_byte(_dst_byte), dst_bit(_dst_bit) {
		};
};

struct MethodParamsDecompress {
	void* src;
	usize* src_byte;
	ubyte* src_bit;

	ubyte* dst;
	usize width;
	usize height;
	usize interval;
	ubyte channel_from_zero;

	MethodParamsDecompress(void* _src, usize* _src_byte, ubyte* _src_bit,
		ubyte* _dst, usize _width, usize _height, usize _interval, ubyte _channel_from_zero)
		: src(_src), src_byte(_src_byte), src_bit(_src_bit),
		dst(_dst), width(_width), height(_height), interval(_interval), channel_from_zero(_channel_from_zero) {
		};
};

}; // end Methods

}; // end CompressedImageCodec