#pragma once
#include "MethodParams.h"
#include <iostream>

namespace CompressedImageCodec {

namespace Methods {

class MethodBase {
private:
	const uint16 id;
	const usize b_width, b_height;
	const char* name;
	const bool full_screen;
protected:
	MethodBase(uint16 method_id, usize block_w, usize block_h, const char* method_name);
	MethodBase(uint16 method_id, const char* method_name);
public:
	uint16 getId() const;
	virtual ~MethodBase();
	virtual void description(std::ostream& os) const;
	virtual usize compress(MethodParamsCompress ps) const; // returns count of bits in result stream
	virtual void decompress(MethodParamsDecompress ps) const;
protected:
	static inline void extractUncompressedBlock(void* src, usize* src_byte, ubyte* src_bit,
													ubyte* dst, const usize& width, const usize& interval,
													const usize& xs, const usize& ys, const usize& cx, const usize& cy) {
		usize x, y, ofs1, ofs2, ofsbuf;
		ofs2 = width * interval;
		ofsbuf = (ys*width + xs) * interval;
		for(y = 0; y < cy; ++y) {
			ofs1 = ofsbuf;
			for(x = 0; x < cx; ++x) {
				dst[ofs1] = (ubyte)ReadBits(src, src_byte, src_bit, 8);
				ofs1 += interval;
			}
			ofsbuf += ofs2;
		}
	};
	virtual usize compressBlock(MethodParamsCompress ps, usize xs, usize xe, usize ys, usize ye) const {
		return MAX_SIZE;
	};
	virtual void decompressBlock(MethodParamsDecompress ps, usize xs, usize ys, usize cx, usize cy) const {
	};
};

}; // end Methods

}; // end CompressedImageCodec
