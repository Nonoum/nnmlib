#pragma once
#include "../../HuffmanCodec/head.h"
#include "../../SequenceLevelsAnalyzer/head.h"
#include <cstdlib> // 'NULL'

namespace CompressedImageCodec {

namespace Methods {

class CompressorInterface {
	uint32 method;
public:
	enum {
		METHOD_HUFFMAN,
		METHOD_SLA
	};
	CompressorInterface() : method(METHOD_SLA) {
	};
	void set(uint32 _method) {
		method = _method;
	};
	inline usize analyze(const ubyte* src, usize size, uint32* result_description) const {
		if(method == METHOD_SLA) {
			char header;
			usize res = SequenceLevelsAnalyzer::Analyze(src, size, &header);
			*result_description = header;
			return res;
		} else { // METHOD_HUFFMAN
			return HuffmanCodec::Compress(src, size, 1, NULL, NULL, NULL, false);
		}
	};
	inline void compress(const ubyte* src, usize size, void* dst, usize* dst_byte, ubyte* dst_bit, uint32 description) const {
		if(method == METHOD_SLA) {
			SequenceLevelsAnalyzer::Compress(src, size, description, dst, dst_byte, dst_bit);
		} else { // METHOD_HUFFMAN
			HuffmanCodec::Compress(src, size, 1, dst, dst_byte, dst_bit, true);
		}
	};
	inline void decompress(void* src, usize* src_byte, ubyte* src_bit, ubyte* dst, usize size) const {
		if(method == METHOD_SLA) {
			SequenceLevelsAnalyzer::Decompress(src, src_byte, src_bit, dst, size);
		} else { // METHOD_HUFFMAN
			HuffmanCodec::Decompress(src, src_byte, src_bit, dst, size, 1);
		}
	};
};

}; // end Methods

}; // end CompressedImageCodec
