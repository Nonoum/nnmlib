#pragma once
#include "MethodMultichannelBase.h"

namespace CompressedImageCodec {

namespace Methods {

class MethodMultichannelHuffman : public MethodMultichannelBase {
public:
	MethodMultichannelHuffman(uint16 id, usize block_w, usize block_h)
			: MethodMultichannelBase(id, block_w, block_h, "Multichannel Huffman") {

		compr.set(CompressorInterface::METHOD_HUFFMAN);
	};
};

}; // end Methods

}; // end CompressedImageCodec
