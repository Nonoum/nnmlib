#include "MethodMultichannelBase.h"
#include "../Utils.h"

namespace CompressedImageCodec {

namespace Methods {

MethodMultichannelBase :: MethodMultichannelBase(uint16 id, usize block_w, usize block_h, const char* method_name)
		: MethodBase(id, block_w, block_h, method_name) {
};

usize MethodMultichannelBase :: compressBlock(MethodParamsCompress ps, usize xs, usize xe, usize ys, usize ye) const {
	usize bits_cnt, best_size, sect_size, uncompressed_size;
	usize y, wins, sarea, ofs2, ofsbuf;
	ubyte ichannel, chosen_b_channel; // chosen bearing channel
	uint32 header, best_header; // current and best headers of compression method
	ubyte diffs[CODER_BLOCK_BUFFER_SIZE];
	ubyte s_diffs[CODER_BLOCK_BUFFER_SIZE];
	ubyte* sequence;
	// making
	bits_cnt = 0;
	usize cx = xe - xs;
	usize cy = ye - ys;
	sarea = cx * cy;
	//
	SpatialDifferencePack(ps.src, ps.interval, ps.width, xs, ys, cx, cy, s_diffs);
	sequence = s_diffs; // only spatial difference
	best_size = MAX_SIZE;
	best_header = SequenceLevelsAnalyzer::SLA8_FLAWLESS_HEADER;
	for(ichannel = 0; ichannel <= ps.channel_from_zero; ++ichannel) { // looking for best bearing channel to compress
		if(ichannel) {
			SpatialDifferencePack(ps.src - ichannel, ps.interval, ps.width, xs, ys, cx, cy, diffs);
			Sub(diffs, s_diffs, sarea - 1);
			sequence = diffs; // double differenced (spatial + channel)
		}
		sect_size = compr.analyze(sequence, sarea-1, &header);
		sect_size += (ichannel + 1); // data for consistent code of bearing channel number
		if((sect_size < best_size) || (! ichannel)) {
			best_header = header;
			best_size = sect_size;
			chosen_b_channel = ichannel;
		}
	}
	best_size += 8 + 1;
	uncompressed_size = (sarea * 8) + 1;
	wins = uncompressed_size - best_size;
	if(uncompressed_size <= best_size) { // better to make unsompressed block
		wins = MAX_SIZE; // uncompressed block
		bits_cnt += uncompressed_size;
	} else {
		bits_cnt += best_size;
	}
	if(ps.dst) {
		if(wins == MAX_SIZE) { // uncompressed block
			WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 1, 0); // CODED[1] = 0.
			ofs2 = ps.width * ps.interval;
			ofsbuf = (ys * ps.width + xs) * ps.interval;
			for(y = ys; y < ye; ++y) {
				const ubyte* src_row = ps.src + ofsbuf;
				const usize total_length = xe - xs;
				const usize blocks_by_4 = total_length >> 2;
				const usize single_pixels = total_length & 0x03;
				for(usize block_index = 0; block_index < blocks_by_4; ++block_index) {
					WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 8, *src_row);
					src_row += ps.interval;
					WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 8, *src_row);
					src_row += ps.interval;
					WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 8, *src_row);
					src_row += ps.interval;
					WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 8, *src_row);
					src_row += ps.interval;
				}
				for(usize single_index = 0; single_index < single_pixels; ++single_index) {
					WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 8, *src_row);
					src_row += ps.interval;
				}
				ofsbuf += ofs2;
			}
		} else { // compressed block
			WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 1, 1); // CODED[1] = 1.
			WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 8, ps.src[(ys * ps.width + xs) * ps.interval]); // start[8]
			// making consistent code of bearing channel number
			WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, chosen_b_channel, 0); // zeroes-sequence (consistent code)
			WriteBits(ps.dst, ps.dst_byte, ps.dst_bit, 1, 1); // [1] = 1
			// done
			sequence = s_diffs; // if only spatial difference was choosed
			if(chosen_b_channel) { // not only spatial difference
				if(chosen_b_channel != ps.channel_from_zero) { // wrong data in [diffs], refreshing
					SpatialDifferencePack(ps.src - chosen_b_channel, ps.interval, ps.width, xs, ys, cx, cy, diffs);
					Sub(diffs, s_diffs, sarea - 1);
				}
				sequence = diffs;
			}
			compr.compress(sequence, sarea-1, ps.dst, ps.dst_byte, ps.dst_bit, best_header);
		}
	}
	return bits_cnt;
}; // end compress_block

void MethodMultichannelBase :: decompressBlock(MethodParamsDecompress ps, usize xs, usize ys, usize cx, usize cy) const {
	ubyte init_value, nchannel;
	ubyte diffs[CODER_BLOCK_BUFFER_SIZE];

	if(! ReadBits(ps.src, ps.src_byte, ps.src_bit, 1)) { // uncompressed block
		extractUncompressedBlock(ps.src, ps.src_byte, ps.src_bit, ps.dst, ps.width, ps.interval, xs, ys, cx, cy);
		return; // out
	}
	// else - compressed block
	init_value = (ubyte)ReadBits(ps.src, ps.src_byte, ps.src_bit, 8); // start[8]
	// reading consistent code of channel
	nchannel = 0;
	while(! ReadBits(ps.src, ps.src_byte, ps.src_bit, 1)) {
		++nchannel;
	}
	// decompressing differencial sequence
	compr.decompress(ps.src, ps.src_byte, ps.src_bit, diffs, cx * cy - 1);
	// repacking differences
	SpatialAndBearingDifferencesUnpack(ps.dst, diffs, init_value, ps.interval, nchannel, ps.width, xs, ys, cx, cy);
}; // end decompress_block

}; // end Methods

}; // end CompressedImageCodec
