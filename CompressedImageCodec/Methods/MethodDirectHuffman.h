#pragma once
#include "MethodParams.h"
#include "MethodBase.h"
#include "../../HuffmanCodec/head.h"

namespace CompressedImageCodec {

namespace Methods {

class MethodDirectHuffman : public MethodBase {
public:
	MethodDirectHuffman(uint16 method_id) : MethodBase(method_id, "Direct Huffman") {
	};
	usize compress(MethodParamsCompress ps) const override {
		return HuffmanCodec::Compress(ps.src, ps.width * ps.height, ps.interval, ps.dst, ps.dst_byte, ps.dst_bit, ps.dst != NULL);
	};
	void decompress(MethodParamsDecompress ps) const override {
		HuffmanCodec::Decompress(ps.src, ps.src_byte, ps.src_bit, ps.dst, ps.width * ps.height, ps.interval);
	};
};

}; // end Methods

}; // end CompressedImageCodec
