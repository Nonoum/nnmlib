#include "MethodsStorage.h"
#include <stdexcept>

namespace CompressedImageCodec {

MethodsStorage::KeyType MethodsStorage :: keyByIndex(uint16 index) const {
	if(index >= size()) {
		throw std::runtime_error("CompressedImageCodec::MethodsStorage::keyByIndex error: wrong index");
	}
	auto iter = methods.cbegin();
	std::advance(iter, index);
	return iter->first;
};

MethodsStorage :: ~MethodsStorage() {
	for(const auto& pair : methods) {
		delete (pair.second.first);
	}
	methods.clear();
};

uint16 MethodsStorage :: size() const {
	return methods.size();
};

void MethodsStorage :: add(const MethodBase* new_method, bool allowed_for_iterator) {
	uint16 id = new_method->getId();
	if(methods.find(id) != methods.end()) {
		throw std::runtime_error("CompressedImageCodec::MethodsStorage::add error: method with this ID is already registered");
	}
	if(size() == KeyType(-1)) {
		throw std::runtime_error("CompressedImageCodec::MethodsStorage::add error: sorry, list is full");
	}
	methods[id] = ValueType(new_method, allowed_for_iterator);
};

const MethodBase* MethodsStorage :: get(uint16 id) const {
	auto res = methods.find(id);
	return (res == methods.end()) ? NULL : res->second.first;
};

void MethodsStorage :: allow(uint16 index, bool allowed_for_iterator) {
	KeyType key = keyByIndex(index);
	ValueType val = methods[key];
	val.second = allowed_for_iterator;
	methods[key] = val;
};

const MethodBase* MethodsStorage :: getByIndex(uint16 index) {
	KeyType key = keyByIndex(index);
	return methods[key].first;
};

// nested

void MethodsStorage::Iterator :: reachAllowed() {
	while(i != storage.methods.end()) {
		if(i->second.second) { // allowed
			break; // stop
		}
		++i;
	}
};

MethodsStorage::Iterator :: Iterator(const MethodsStorage& mets)
		: storage(mets) {

	i = storage.methods.begin();
	reachAllowed();
};

bool MethodsStorage::Iterator :: isDone() const {
	return i == storage.methods.end();
};

void MethodsStorage::Iterator :: operator ++() {
	++i;
	reachAllowed();
};

const MethodBase* MethodsStorage::Iterator :: operator *() const {
	return i->second.first;
};

}; // end CompressedImageCodec
