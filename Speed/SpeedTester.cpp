#include "SpeedTester.h"
#include <iomanip>

namespace Speed {

void SpeedTester :: getTime(uint32& low, uint32& high) {
	uint32 l, h;
#ifdef _MSC_VER // ms visual studio compiler
	__asm {
		rdtsc;
		mov l, eax;
		mov h, edx;
       }
#else // gcc compiler
	__asm ("rdtsc" : : "a" (l), "d" (h));
#endif
	low = l;
	high = h;
};

double SpeedTester :: calcTacts() const {
	uint32 _low, _high;
	_high = after_h - before_h;
	if(after_l < before_l) {
		--_high;
	}
	_low = after_l - before_l;
	double res = (double)_high;
	res *= 4294967296.0;
	res += double(_low);
	return res;
};

SpeedTester :: SpeedTester(double _proc_freq)
		: proc_freq(_proc_freq) {
};

void SpeedTester :: begin() {
	getTime(before_l, before_h);
};

void SpeedTester :: end() {
	getTime(after_l, after_h);
};

std::ostream& operator <<(std::ostream& os, const SpeedTester& st) {
	return os << std::fixed << std::setprecision(7) << (st.calcTacts() / st.proc_freq) << " seconds";
};

}; // end Speed
