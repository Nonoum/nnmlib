#pragma once
#include "../LibTypes/head.h"
#include <iostream>

namespace Speed {

using namespace LibTypes;

class SpeedTester {
	uint32 before_l, before_h;
	uint32 after_l, after_h;
	const double proc_freq;
	// functions
	static void getTime(uint32& low, uint32& high);
public:
	SpeedTester(double _proc_freq = 3400000000.0);
	void begin();
	void end();
	double calcTacts() const;
	friend std::ostream& operator <<(std::ostream& os, const SpeedTester& st);
};

}; // end Speed
