#pragma once
#include "SpeedTester.h"

namespace Speed {

class ScopeTester : private SpeedTester {
	std::ostream& dst;
public:
	ScopeTester(std::ostream& _dst);
	ScopeTester(std::ostream& _dst, double _proc_freq);
	~ScopeTester();
};

}; // end Speed
