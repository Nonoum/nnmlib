#include "ScopeTester.h"

namespace Speed {

ScopeTester :: ScopeTester(std::ostream& _dst)
		: dst(_dst) {
	begin();
};

ScopeTester :: ScopeTester(std::ostream& _dst, double _proc_freq)
		: dst(_dst), SpeedTester(_proc_freq) {
	begin();
};

ScopeTester :: ~ScopeTester() {
	end();
	dst << "Scope passed in " << *static_cast<SpeedTester*>(this) << "\n";
};

}; // end Speed
