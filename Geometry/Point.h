#pragma once
#include "Utils.h"

namespace Geometry {

class Point {
protected:
	ssize _x;
	ssize _y;
public:
	Point(const char** source);
	inline Point() : _x(0), _y(0) {
	};
	inline Point(ssize x, ssize y) : _x(x), _y(y) {
	};
	inline Point& getPoint() {
		return *this;
	};
	inline const Point& getPoint() const {
		return *this;
	};
	inline void setPoint(ssize x, ssize y) {
		_x = x;
		_y = y;
	};
	inline bool operator ==(const Point& other) const {
		return (_x == other._x) && (_y == other._y);
	};
	inline bool operator !=(const Point& other) const {
		return ! operator==(other);
	};
	inline Point operator +(const Point& right) const {
		return Point(_x + right._x, _y + right._y);
	};
	inline Point operator -(const Point& right) const {
		return Point(_x - right._x, _y - right._y);
	};
	inline const Point& operator +=(const Point& right) {
		_x += right._x;
		_y += right._y;
		return *this;
	};
	inline const Point& operator -=(const Point& right) {
		_x -= right._x;
		_y -= right._y;
		return *this;
	};
	inline Point operator -() const {
		return Point(-_x, -_y);
	};
	inline ssize x() const {
		return _x;
	};
	inline ssize y() const {
		return _y;
	};
	Point abs() const; // return point with absolute values of current coordinates
	usize absDist(const Point& other) const; // returns absolute value of distance between points (x difference + y difference)
};

std::ostream& operator <<(std::ostream& os, const Point& point);

}; // end Geometry
