#include "Side.h"

namespace Geometry {

Alignment XDirection(Side side) {
	if(side == SIDE_LEFT) {
		return ALIGN_BEGIN;
	} else if(side == SIDE_RIGHT) {
		return ALIGN_END;
	}
	return ALIGN_CENTER;
};

Alignment YDirection(Side side) {
	if(side == SIDE_DOWN) {
		return ALIGN_BEGIN;
	} else if(side == SIDE_UP) {
		return ALIGN_END;
	}
	return ALIGN_CENTER;
};

Side Reverse(Side side) {
	// static mapping here. check array size if add smth in enum
	static const Side map[SIDE_NONE+1] = {
		SIDE_RIGHT, // reverse of LEFT
		SIDE_DOWN, // reverse of UP
		SIDE_LEFT, // reverse of RIGHT
		SIDE_UP, // reverse of DOWN
		SIDE_NONE
	};
	return map[side];
};

Side GetSideOrDefault(Side side, Side default_side) {
	return (side == SIDE_NONE) ? default_side : side;
};

std::ostream& operator <<(std::ostream& os, Side side) {
	static const char *map[] = {
		"side(LEFT)", "side(UP)", "side(RIGHT)", "side(DOWN)", "side(NONE)"
	};
	os << map[side];
	return os;
};

}; // end Geometry
