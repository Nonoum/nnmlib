#include "Rect.h"
#include "../Helpers/ParseEmbeddedInts.h"
#include <assert.h>

namespace Geometry {

Rect :: Rect(const char** source) {
	int a, b, c, d;
	void* outputs[] = {&a, &b, &c, &d};
	*source = Helpers::ParseEmbeddedInts(*source, "rect(0:0;0:0)", outputs);
	if(*source) {
		_x = a;
		_width = c - a;
		_y = b;
		_height = d - b;
	}
};

void Rect :: fixToPositive() {
	if(_width < 0) {
		_x += _width;
		_width = -_width;
	}
	if(_height < 0) {
		_y += _height;
		_height = -_height;
	}
};

bool Rect :: crossing(const Rect& area) const {
	assert((_width >= 0) && (_height >= 0) && "(*this) must be positive rect");
	assert((area._width >= 0) && (area._height >= 0) && "(area) must be positive rect");
	// returns true if *this crossing (area)
	return getXRange().crossing(area.getXRange()) && getYRange().crossing(area.getYRange());
};

Rect Rect :: operator &(const Rect& rt) const {
	assert((_width >= 0) && (_height >= 0) && "(*this) must be positive rect");
	assert((rt._width >= 0) && (rt._height >= 0) && "(rt) must be positive rect");
	// returns Rect of crossing area
	Point pt(max2(_x, rt._x), max2(_y, rt._y));
	Size sz(min2(getXEnd(), rt.getXEnd()) - pt.x(), min2(getYEnd(), rt.getYEnd()) - pt.y());
	return Rect(pt, sz);
};

bool Rect :: operator &(const Point& pt) const {
	assert((_width >= 0) && (_height >= 0) && "(*this) must be positive rect");
	// returns true if (pt) is in the Rect
	return getXRange().includes(pt.x()) && getYRange().includes(pt.y());
};

Rect Rect :: checkedIntersection(const Rect& rt) const {
	if(! crossing(rt)) {
		return Rect();
	}
	return operator &(rt);
};

usize Rect :: write(std::ostream& dst) const {
	sint32 data[4]; // fixed binary size
	data[0] = _x;
	data[1] = _y;
	data[2] = _width;
	data[3] = _height;
	dst.write((char*)data, sizeof(data));
	return sizeof(data);
};

usize Rect :: read(std::istream& src) {
	sint32 data[4]; // fixed binary size
	src.read((char*)data, sizeof(data));
	_x = data[0];
	_y = data[1];
	_width = data[2];
	_height = data[3];
	return sizeof(data);
};

Rect Rect :: positiveRect(Point p1, Point p2) {
	Point pt(min2(p1.x(), p2.x()),
			min2(p1.y(), p2.y()));
	Size sz(max2(p1.x(), p2.x()) - pt.x(),
			max2(p1.y(), p2.y()) - pt.y());
	return Rect(pt, sz);
};

size_t Rect :: splitCrossedRect(Rect splitter, Rect output[4], Side side_to_skip) const {
	assert((_width >= 0) && (_height >= 0) && "(*this) must be positive rect");
	assert((splitter._width >= 0) && (splitter._height >= 0) && "(splitter) must be positive rect");
	size_t count = 0;
	const bool expand_longer_y = ((side_to_skip != SIDE_LEFT) && (side_to_skip != SIDE_RIGHT));
	const bool expand_longer_x = (! expand_longer_y)
						&& (side_to_skip != SIDE_UP) && (side_to_skip != SIDE_DOWN);
	for(int sd = SIDE_ITERATOR; sd < SIDE_NONE; ++sd) {
		const Side side = Side(sd);
		if(side == side_to_skip) {
			continue;
		}
		Rect tmp;
		const bool vertical_side = YDirection(side) ? true : false;
		//----------------------- first coordinate
		ssize start; // start for current coordinate/direction
		ssize length = 0; // length for current direction
		if(side == SIDE_LEFT) {
			start = _x;
			length = splitter._x - start;
		} else if(side == SIDE_RIGHT) {
			start = splitter.getXEnd();
			length = getXEnd() - start;
		} else if(side == SIDE_DOWN) {
			start = _y;
			length = splitter._y - start;
		} else if(side == SIDE_UP) {
			start = splitter.getYEnd();
			length = getYEnd() - start;
		}
		if(length <= 0) {
			continue; // full cut
		}
		//----------------------- second coordinate
		if(vertical_side) { // up or down splitter
			tmp._y = start;
			tmp._height = length;
			if(expand_longer_x) {
				tmp._x = _x;
				tmp._width = _width;
			} else {
				ssize xmin = max2(_x, splitter._x);
				ssize xmax = min2(getXEnd(), splitter.getXEnd());
				tmp._x = xmin;
				tmp._width = xmax - xmin;
			}
		} else { // left or right splitter
			tmp._x = start;
			tmp._width = length;
			if(expand_longer_y) {
				tmp._y = _y;
				tmp._height = _height;
			} else {
				ssize ymin = max2(_y, splitter._y);
				ssize ymax = min2(getYEnd(), splitter.getYEnd());
				tmp._y = ymin;
				tmp._height = ymax - ymin;
			}
		}
		output[count] = tmp;
		++count;
	}
	return count;
};

Rect Rect :: getResizedRect(Size new_size, int x_same_side, int y_same_side) const {
	assert((_width >= 0) && (_height >= 0) && "(*this) must be positive rect");
	assert((new_size.width() >= 0) && (new_size.height() >= 0) && "(size) must be positive");
	Rect result;
	result.getSize() = new_size;
	ssize new_x = _x, new_y = _y;

	if(x_same_side > 0) { // to right
		new_x += _width - new_size.width();
	} else if(x_same_side == 0) { // to middle
		new_x += (_width - new_size.width()) >> 1;
	}

	if(y_same_side > 0) { // to top
		new_y += _height - new_size.height();
	} else if(y_same_side == 0) { // to middle
		new_y += (_height - new_size.height()) >> 1;
	}

	result.setPoint(new_x, new_y);
	return result;
};

Rect Rect :: cut(Rect splitter, Side side_to_keep) const {
	assert((_width >= 0) && (_height >= 0) && "(*this) must be positive rect");
	assert((splitter.width() >= 0) && (splitter.height() >= 0) && "(splitter) must be positive rect");
	assert((side_to_keep != Geometry::SIDE_NONE) && "Rect::cut expects an exact side to keep, not NONE");
	Geometry::Rect inv_splitter;
	ssize min_limit = NNMLIB_SSIZE_MIN>>1; // limits that allows to make correct ssize difference
	ssize max_limit = NNMLIB_SSIZE_MAX>>1;

	if(side_to_keep == SIDE_LEFT) {
		inv_splitter.setRect(min_limit, min_limit, splitter.x()-min_limit, max_limit-min_limit);
	} else if(side_to_keep == SIDE_RIGHT) {
		inv_splitter.setRect(splitter.getXEnd(), min_limit, max_limit-splitter.getXEnd(), max_limit-min_limit);
	} else if(side_to_keep == SIDE_UP) {
		inv_splitter.setRect(min_limit, splitter.getYEnd(), max_limit-min_limit, max_limit-splitter.getYEnd());
	} else if(side_to_keep == SIDE_DOWN) {
		inv_splitter.setRect(min_limit, min_limit, max_limit-min_limit, splitter.y()-min_limit);
	}
	return (*this) & inv_splitter;
};

bool Rect :: insideOf(const Geometry::Rect& rect) const {
	return _x >= rect._x && _y >= rect._y && rect.getXEnd() >= getXEnd() && rect.getYEnd() >= getYEnd();
};

bool Rect :: operator ==(const Rect& rect) const {
	return _x == rect._x && _y == rect._y && _width == rect._width && _height == rect._height;
};

bool Rect :: operator !=(const Rect& rect) const {
	return _x != rect._x || _y != rect._y || _width != rect._width || _height != rect._height;
};

Rect Rect :: getPlacedRect(ssize dx, ssize dy, ssize wi, ssize he, Alignment xalign, Alignment yalign) const {
	ssize pt[2] = {dx, dy};
	const ssize sz[2] = {wi, he};
	const ssize dsz[2] = {width(), height()};
	const Alignment al[2] = {xalign, yalign};
	for(int i = 0; i < 2; ++i) {
		if(al[i] == ALIGN_CENTER) {
			if(! pt[i]) {
				pt[i] = (dsz[i] - sz[i]) / 2;
			}
		} else if(al[i] == ALIGN_END) {
			pt[i] = dsz[i] - sz[i] - pt[i];
		}
	}
	pt[0] += x();
	pt[1] += y();
	return Rect(pt[0], pt[1], wi, he);
};

Rect Rect :: unionWith(const Rect& rect) const {
	assert((_width >= 0) && (_height >= 0) && "(*this) must be positive rect");
	assert((rect._width >= 0) && (rect._height >= 0) && "(rect) must be positive rect");
	const ssize nx = min2(x(), rect.x());
	const ssize ny = min2(y(), rect.y());
	const ssize nxend = max2(getXEnd(), rect.getXEnd());
	const ssize nyend = max2(getYEnd(), rect.getYEnd());
	return Rect(nx, ny, nxend-nx, nyend-ny);
};


std::ostream& operator <<(std::ostream& os, const Rect& rect) {
	os << "rect(" << rect.x() << ":" << rect.getXEnd() << "; " <<
		rect.y() << ":" << rect.getYEnd() << ")";
	return os;
};

}; // end Geometry
