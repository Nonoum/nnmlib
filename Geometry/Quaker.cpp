#include "Quaker.h"
#include <math.h>

namespace Geometry {

Quaker :: Quaker(double rotations, double radius, double degrees_per_time, double max_on_percent)
		: degrees(rotations * 360.0), power(radius), speed(degrees_per_time), top(max_on_percent), offset(0, 0) {

	pos = 0;
};

bool Quaker :: operator ()(ssize* offs_x, ssize* offs_y) {
	*offs_x -= offset.x(); // moving back
	*offs_y -= offset.y(); //
	pos += speed;
	if(pos < degrees) { // in process
		const double angle = pos * (3.14159265358979323846 / 180.0); // to radians
		double x = cos(angle);
		double y = sin(angle);
		correctByPower(x, y);
		offset.setPoint(ssize(x), ssize(y));
		*offs_x += offset.x();
		*offs_y += offset.y();
	} else {
		return true;
	}
	return false;
};

void Quaker :: correctByPower(double& x, double& y) const {
	const double percent = 1.0 - ((degrees - pos) / degrees);
	double cf = power;
	if(percent <= top) {
		cf *= percent;
		cf /= top;
	} else {
		cf *= 1.0 - percent;
		cf /= (1.0 - top);
	}
	x *= cf;
	y *= cf;
};

}; // end Geometry
