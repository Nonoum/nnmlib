#include "Range.h"
#include "../TestsUtils/head.h"

typedef Geometry::Range<int, false> Range;

Range r1(20, 27);
Range r2(29, 24);
Range r3(-7, -12);
Range r4(-3, 20);
Range r5(20, 22);
Range r6(24, 27);

NTEST(Geometry_Range_union) {
	ASSERT( (r1+r2) == Range(20,29) )
	ASSERT( (r1+r2) == (r2+r1) )
	ASSERT( (r3+r4) == Range() )
	ASSERT( (r1+r4) == Range(27, -3) )
};

NTEST(Geometry_Range_intersection) {
	ASSERT( (r1*r2) == Range(24,27) )
	ASSERT( (r1*r2) == (r2*r1) )
	ASSERT( (r1*r3) == Range() )
};

NTEST(Geometry_Range_includes) {
	ASSERT( ! r1.includes(19) )
	ASSERT( r1.includes(20) )
	ASSERT( r1.includes(23) )
	ASSERT( r1.includes(26) )
	ASSERT( ! r1.includes(27) )
};

NTEST(Geometry_Range_includes_range) {
	ASSERT( r1.includes(r5) )
	ASSERT( ! r5.includes(r1) )
	ASSERT( r1.includes(r6) )
	ASSERT( ! r6.includes(r1) )
	ASSERT( ! r1.includes(r2) )
	ASSERT( ! r2.includes(r1) )
};

NTEST(Geometry_Range_unionable) {
	ASSERT( r1.unionable(r2) )
	ASSERT( r2.unionable(r1) )
	ASSERT( r1.unionable(r4) )
	ASSERT( r4.unionable(r1) )
	ASSERT( ! r2.unionable(r4) )
	ASSERT( ! r4.unionable(r2) )
};

NTEST(Geometry_Range_crossing) {
	ASSERT( r1.crossing(r2) )
	ASSERT( r2.crossing(r1) )
	ASSERT( ! r1.crossing(r4) )
	ASSERT( ! r4.crossing(r1) )
};
