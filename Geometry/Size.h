#pragma once
#include "Utils.h"

namespace Geometry {

class Size {
protected:
	ssize _width;
	ssize _height;
public:
	Size(const char** source);
	inline Size() : _width(0), _height(0) {
	};
	inline Size(ssize wi, ssize he) : _width(wi), _height(he) {
	};
	inline Size& getSize() {
		return *this;
	};
	inline const Size& getSize() const {
		return *this;
	};
	inline void setSize(ssize wi, ssize he) {
		_width = wi;
		_height = he;
	};
	inline bool operator ==(const Size& other) const {
		return (_width == other._width) && (_height == other._height);
	};
	inline bool operator !=(const Size& other) const {
		return ! operator==(other);
	};
	inline operator bool() const {
		// true if both _width and _height are non-zero
		return _width && _height;
	};
	inline ssize width() const {
		return _width;
	};
	inline ssize height() const {
		return _height;
	};
	bool canFit(const Size& subsize) const; // returns true if both sizes of *this is not less than (subsize)'s sizes
	bool lessSorting(const Size& right) const;
	Size getMaximums(const Size& other) const; // returns size with maximum width and height
	Size getMinimums(const Size& other) const; // returns size with minimum width and height
};

std::ostream& operator <<(std::ostream& os, const Size& size);

}; // end Geometry
