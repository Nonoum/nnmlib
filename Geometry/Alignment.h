#pragma once

namespace Geometry {

// hardcoded values are guaranteed and must not be changed ever.
enum Alignment {
	ALIGN_BEGIN = -1,
	ALIGN_CENTER = 0,
	ALIGN_END = 1
};

}; // end Geometry
