#include "Point.h"
#include "../Helpers/ParseEmbeddedInts.h"

namespace Geometry {

Point :: Point(const char** source)
		: _x(0), _y(0) {
	int a, b;
	void* outputs[] = {&a, &b};
	*source = Helpers::ParseEmbeddedInts(*source, "point(0:0)", outputs);
	if(*source) {
		_x = a;
		_y = b;
	}
};

usize Point :: absDist(const Point& other) const {
	ssize x_res = _x - other._x;
	if(x_res < 0) {
		x_res = -x_res;
	}
	ssize y_res = _y - other._y;
	if(y_res < 0) {
		y_res = -y_res;
	}
	return x_res + y_res;
};

Point Point :: abs() const {
	return Point(_x < 0 ? -_x : _x,
				_y < 0 ? -_y : _y);
};

std::ostream& operator <<(std::ostream& os, const Point& point) {
	os << "point(" << point.x() << ":" << point.y() << ")";
	return os;
};

}; // end Geometry
