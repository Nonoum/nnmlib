#pragma once
#include "Utils.h"
#include "Point.h"
#include "Size.h"
#include "Side.h"
#include "Range.h"

namespace Geometry {

/*
	Some methods suppose that the rect is positive and may work incorrect if it's not.
*/
struct Rect : public Point, public Size {
	typedef Range<ssize, true> RangeType;

	Rect(const char** source);
	inline Rect() {
	};
	inline Rect(const Size& sz) : Point(0, 0), Size(sz) {
	};
	inline Rect(const Point& pt, const Size& sz) : Point(pt), Size(sz) {
	};
	inline Rect(ssize x, ssize y, ssize w, ssize h) : Point(x, y), Size(w, h) {
	};
	// accessors
	inline Rect& getRect() {
		return *this;
	};
	inline const Rect& getRect() const {
		return *this;
	};
	inline void setRect(ssize x, ssize y, ssize w, ssize h) {
		setPoint(x, y);
		setSize(w, h);
	};
	inline operator bool() const {
		return Size::operator bool();
	};
	// movers
	inline Rect operator +(const Point& right) const { // returns rect, moved in forward on specified distance
		return Rect(getPoint() + right, getSize());
	};
	inline Rect operator -(const Point& right) const { // returns rect, moved in backward on specified distance
		return Rect(getPoint() - right, getSize());
	};
	inline const Rect& operator +=(const Point& right) {
		getPoint() += right;
		return *this;
	};
	inline const Rect& operator -=(const Point& right) {
		getPoint() -= right;
		return *this;
	};
	// artificial getters
	inline ssize getXEnd() const {
		return _x + _width;
	};
	inline ssize getYEnd() const {
		return _y + _height;
	};
	inline RangeType getXRange() const {
		return RangeType(x(), getXEnd());
	};
	inline RangeType getYRange() const {
		return RangeType(y(), getYEnd());
	};
	inline Point getLD() const { // left down point
		return getPoint();
	};
	inline Point getLU() const { // left up point
		return Point(_x, _y + _height);
	};
	inline Point getRD() const { // right up point
		return Point(_x + _width, _y);
	};
	inline Point getRU() const { // right up point
		return Point(_x + _width, _y + _height);
	};
	// more complicated methods
	void fixToPositive(); // changes rect to have positive size
	bool crossing(const Rect& area) const; // returns true if rects are crossing
	Rect operator &(const Rect& rt) const; // returns crossing result of rects (valid only if rects are crossing)
	bool operator &(const Point& pt) const; // hittest for point. returns true if point inside of rect
	Rect checkedIntersection(const Rect& rt) const; // like operator &, but returns empty Rect(), if operands are not crossing
	usize write(std::ostream& dst) const; // writes rect to binary stream (FIXED BINARY SIZE, possible cut of data)
	usize read(std::istream& src); // reads rect from binary stream (FIXED BINARY SIZE, possible cut of data)
	static Rect positiveRect(Point p1, Point p2); // create rect with positive size from specified points
	size_t splitCrossedRect(Rect splitter, Rect output[4], Side side_to_skip = SIDE_NONE) const; // returns real count of rects after splitting
	Rect getResizedRect(Size new_size, int x_same_side = 0, int y_same_side = 0) const; // returns resized rect with specified alignment by x, y (-1/0/1 is min/center/max)
	Rect cut(Rect splitter, Side side_to_keep) const; // returns rect which is cut of original with keeping specified side (could lead to empty result)
	bool insideOf(const Rect& rect) const; // returns true if *this inside of (rect)
	bool operator ==(const Rect& rect) const; // checks if rects are equal
	bool operator !=(const Rect& rect) const; // checks if rects are not equal
	Rect getPlacedRect(ssize dx, ssize dy, ssize wi, ssize he, Alignment xalign, Alignment yalign) const; // returns rect placed in *this with (dx),(dy) offsets according to (xalign), (yalign). for center alignment (dx)//(dy) should be 0
	Rect unionWith(const Rect& rect) const; // returns minimal rect that contains both of (*this) and (rect)
};

std::ostream& operator <<(std::ostream& os, const Rect& rect);

}; // end Geometry
