#pragma once
#include "../LibTypes/head.h"
#include "../Helpers/MinMax.h"
#include <iostream> // multiple entities have overloaded IO operators

namespace Geometry {

using namespace LibTypes;

using Helpers::min2;
using Helpers::max2;
using Helpers::min4;
using Helpers::max4;

}; // end Geometry
