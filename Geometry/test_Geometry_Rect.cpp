#include "Rect.h"
#include "../TestsUtils/head.h"

using Geometry::Rect;
using Geometry::Side;
using Geometry::Alignment;

NTEST(Geometry_Rect_crossing) {
	Rect r1(11, 22,  33, 44);
	Rect r2(22, 33,  44, 55);
	Rect r3(1, 2,  3, 4);
	Rect r4(2, 3,  4, 5);
	ASSERT( r1.crossing(r2) )
	ASSERT( r3.crossing(r4) )
	ASSERT( ! r1.crossing(r3) )
	ASSERT( ! r1.crossing(r4) )
	ASSERT( ! r2.crossing(r3) )
	ASSERT( ! r2.crossing(r4) )
};

NTEST(Geometry_Rect_intersection) {
	Rect r1(111, 222,  333, 444);
	Rect r2(222, 333,  444, 555);
	Rect ethalon(222, 333,  222, 333);
	Rect intersect = r1 & r2;
	ASSERT( intersect == ethalon )
};

NTEST(Geometry_Rect_insideOf) {
	Rect r1(100, 200,  20, 50);
	Rect r2(10, 10,  5, 10);
	Rect r3(110, 210,  5, 10);
	ASSERT( ! r2.insideOf(r1) )
	ASSERT( ! r1.insideOf(r2) )
	ASSERT( r3.insideOf(r1) )
	ASSERT( ! r1.insideOf(r3) )
};

NTEST(Geometry_Rect_splitCrossedRect) {
	Rect origin(100, 200,  333, 444);
	Rect splitter(212, 223,  123, 45);
	const Side sides[5] = {Geometry::SIDE_LEFT,
					Geometry::SIDE_RIGHT,
					Geometry::SIDE_UP,
					Geometry::SIDE_DOWN,
					Geometry::SIDE_NONE};
	const Rect results[5][4] = {{Rect(100, 268,  333, 376), Rect(335, 223,  98, 45), Rect(100, 200,  333, 23)},
					{Rect(100, 223,  112, 45), Rect(100, 268,  333, 376), Rect(100, 200,  333, 23)},
					{Rect(100, 200,  112, 444), Rect(335, 200,  98, 444), Rect(212, 200,  123, 23)},
					{Rect(100, 200,  112, 444), Rect(212, 268,  123, 376), Rect(335, 200,  98, 444)},
					{Rect(100, 200,  112, 444), Rect(212, 268,  123, 376), Rect(335, 200,  98, 444), Rect(212, 200,  123, 23)}};
	const int counts[5] = {3,3,3,3,4};
	for(int i = 0; i < 5; ++i) {
		Rect output[4];
		const int cnt = origin.splitCrossedRect(splitter, output, sides[i]);
		ASSERT( cnt == counts[i] )
		for(int j = 0; j < cnt; ++j) {
			ASSERT( results[i][j] == output[j] )
		}
	}
};

NTEST(Geometry_Rect_cut) {
	Rect origin(-100, -200,  500, 300);
	Rect splitter(40, 10,  27, 19);
	const Side sides[4] = {Geometry::SIDE_LEFT,
					Geometry::SIDE_RIGHT,
					Geometry::SIDE_UP,
					Geometry::SIDE_DOWN};
	const Rect results[4] = {Rect(-100, -200,  140, 300),
					Rect(67, -200,  333, 300),
					Rect(-100, 29,  500, 71),
					Rect(-100, -200,  500, 210)};
	for(int i = 0; i < 4; ++i) {
		Rect res = origin.cut(splitter, sides[i]);
		ASSERT( res == results[i] )
	}
};

NTEST(Geometry_Rect_getResizedRect) {
	Rect r1(100, 200,  300, 400);
	Geometry::Size sz(123, 456);
	const Rect results[] = {Rect(100, 200,  123, 456),
					Rect(188, 200,  123, 456),
					Rect(277, 200,  123, 456),
					Rect(100, 172,  123, 456),
					Rect(188, 172,  123, 456),
					Rect(277, 172,  123, 456),
					Rect(100, 144,  123, 456),
					Rect(188, 144,  123, 456),
					Rect(277, 144,  123, 456)};
	int i = 0;
	for(int y_align = -1; y_align < 2; ++y_align) {
		for(int x_align = -1; x_align < 2; ++x_align) {
			Rect res = r1.getResizedRect(sz, x_align, y_align);
			ASSERT( res == results[i] )
			++i;
		}
	}
};

NTEST(Geometry_Rect_getPlacedRect) {
	Rect r1(100, 200,  300, 400);
	const Rect results[] = {Rect(117, 231,  86, 54),
							Rect(117, 231,  86, 54),
							Rect(220, 200,  60, 50),
							Rect(297, 231,  86, 54),
							Rect(117, 231,  86, 54),
							Rect(100, 375,  60, 50),
							Rect(117, 231,  86, 54),
							Rect(220, 375,  60, 50),
							Rect(297, 231,  86, 54),
							Rect(340, 375,  60, 50),
							Rect(117, 515,  86, 54),
							Rect(117, 515,  86, 54),
							Rect(220, 550,  60, 50),
							Rect(297, 515,  86, 54)};
	int i = 0;
	for(int y_align = -1; y_align < 2; ++y_align) {
		for(int x_align = -1; x_align < 2; ++x_align) {
			Rect res = r1.getPlacedRect(17, 31, 86, 54, Alignment(x_align), Alignment(y_align));
			ASSERT( res == results[i] );
			++i;
			if(x_align == 0 || y_align == 0) {
				res = r1.getPlacedRect(0, 0, 60, 50, Alignment(x_align), Alignment(y_align));
				ASSERT( res == results[i] );
				++i;
			}
		}
	}
};

NTEST(Geometry_Rect_unionWith) {
	const Rect r1s[] = {Rect(-10, -20,  100, 200),
						Rect(-20, -10,  50, 30),
						Rect(0, 0,  1000, 2000)};
	const Rect r2s[] = {Rect(23, 45,  17, 99),
						Rect(-23, -45,  50, 30),
						Rect(-300, -400,  900, 600)};
	const Rect res[] = {Rect(-10, -20,  100, 200),
						Rect(-23, -45,  53, 65),
						Rect(-300, -400,  1300, 2400)};
	for(int i = 0; i < 3; ++i) {
		ASSERT( r1s[i].unionWith(r2s[i]) == res[i] );
	}
};
