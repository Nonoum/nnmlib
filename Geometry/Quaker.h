#pragma once
#include "Utils.h"
#include "Point.h"

namespace Geometry {

class Quaker {
	double degrees; // degrees per all time
	double power; // power of quake
	double speed; // degrees to pass in one moment
	double top; // percent of way when power is maximum
	Point offset; // previous offset for quake (x, y)
	double pos; // current degrees
public:
	Quaker(double rotations, double radius, double degrees_per_time, double max_on_percent);
	bool operator ()(ssize* offs_x, ssize* offs_y);
private:
	void correctByPower(double& x, double& y) const;
};

}; // end Geometry
