#include "Size.h"
#include "../Helpers/ParseEmbeddedInts.h"
#include "../Algorithms/LexicographicalCompare.h"

namespace Geometry {

Size :: Size(const char** source)
		: _width(0), _height(0) {
	int a, b;
	void* outputs[] = {&a, &b};
	*source = Helpers::ParseEmbeddedInts(*source, "size(0:0)", outputs);
	if(*source) {
		_width = a;
		_height = b;
	}
};

bool Size :: canFit(const Size& subsize) const {
	// returns true if both sizes of *this is not less than (subsize)'s sizes
	return (_width >= subsize._width) && (_height >= subsize._height);
};

bool Size :: lessSorting(const Size& right) const {
	return Algorithms::LexicographicalCompare(_width, right._width, _height, right._height);
};

Size Size :: getMaximums(const Size& other) const {
	return Size(max2(_width, other._width), max2(_height, other._height));
};

Size Size :: getMinimums(const Size& other) const {
	return Size(min2(_width, other._width), min2(_height, other._height));
};

std::ostream& operator <<(std::ostream& os, const Size& size) {
	os << "size(" << size.width() << ":" << size.height() << ")";
	return os;
};

}; // end Geometry
