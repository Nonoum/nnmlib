#pragma once
#include "Utils.h"
#include "Alignment.h"

namespace Geometry {

enum Side {
	SIDE_ITERATOR = 0,
	SIDE_LEFT = SIDE_ITERATOR,
	SIDE_UP = 1,
	SIDE_RIGHT = 2,
	SIDE_DOWN = 3,
	SIDE_NONE = 4
};

// the following functions convert side to it's alignment according to decart coordinates
Alignment XDirection(Side side); // SIDE_LEFT == ALIGN_BEGIN / SIDE_RIGHT == ALIGN_END / other == ALIGN_CENTER
Alignment YDirection(Side side); // SIDE_DOWN == ALIGN_BEGIN / SIDE_UP == ALIGN_END / other == ALIGN_CENTER

Side Reverse(Side side);

Side GetSideOrDefault(Side side, Side default_side);

std::ostream& operator <<(std::ostream& os, Side side);

}; // end Geometry
