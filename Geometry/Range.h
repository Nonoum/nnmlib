#pragma once
#include "Utils.h"

namespace Geometry {

template <class T, bool sure_last_not_less_than_first = false> class Range {
	T _first;
	T _last;
public:
	Range() : _first(0), _last(0) {
	};
	Range(T last) : _first(0), _last(last) {
	};
	Range(T first, T last) : _first(first), _last(last) {
	};
	bool isEmpty() const {
		return (_first == _last);
	};
	T& first() {
		return _first;
	};
	T& last() {
		return _last;
	};
	const T& first() const {
		return _first;
	};
	const T& last() const {
		return _last;
	};
	T getStart() const {
		if(sure_last_not_less_than_first) {
			return _first;
		}
		return _first < _last ? _first : _last;
	};
	T getEnd() const {
		if(sure_last_not_less_than_first) {
			return _last;
		}
		return _first < _last ? _last : _first;
	};
	T length() const {
		return getEnd() - getStart();
	};
	bool operator ==(const Range& other) const {
		return getStart() == other.getStart() && getEnd() == other.getEnd();
	};
	bool operator !=(const Range& other) const {
		return ! operator ==(other);
	};
	bool includes(T index) const {
		return (index >= getStart()) && (index < getEnd());
	};
	bool includes(const Range& other) const {
		if(other.first() == other.last()) {
			return false;
		}
		return includes(other.getStart()) && includes(other.getEnd()-1);
	};
	bool crossing(const Range& other) const {
		return includes(other.getStart()) || other.includes(getStart());
	};
	bool unionable(Range other) const {
		return includes(other.getEnd()) || other.includes(getEnd());
	};
	Range operator *(const Range& other) const { // intersection of ranges (or empty range if can't make so)
		if(includes(other.getStart())) {
			return Range(other.getStart(), getEnd());
		} else if(other.includes(getStart())) {
			return Range(getStart(), other.getEnd());
		}
		return Range();
	};
	Range operator +(const Range& other) const { // union of ranges (or empty range if can't make so)
		if(! unionable(other)) {
			return Range();
		}
		T start = min4(_first, _last, other._first, other._last);
		T end = max4(_first, _last, other._first, other._last);
		return Range(start, end);
	};
	T globalOf(T local_index) const {
		return getStart() + local_index;
	};
	T localOf(T global_index) const {
		return global_index - getStart();
	};
};

}; // end Geometry
