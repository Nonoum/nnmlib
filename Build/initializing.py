from os import makedirs
from os.path import join, exists

# script supposed to be launched from <root>/Build/ folder

_tests_temp_dir = join('..', '_TestData', 'tmp')
_release_dir = 'Release'
_debug_dir = 'Debug'

def createFolders():
	for dir in (_tests_temp_dir, _release_dir, _debug_dir):
		if not exists(dir):
			makedirs(dir)

createFolders() # initializing required folders