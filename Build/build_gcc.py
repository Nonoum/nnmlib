import os
import src_collector
import initializing
from config import * # filters and constants

_common_settings = ' -std=c++11 '
_release_settings = _common_settings + '-Ofast -DNDEBUG'
_debug_settings = _common_settings

_gtk_settings = ' `pkg-config --cflags --libs gtk+-2.0` '

modes = [{'name': 'release', 'obj_ext': '.o', 'obj_dir': release_objs_dir, 'target': os.path.join('..', 'release.a'), 'file_filter': isMainSorce, 'set': _release_settings},
		{'name': 'pl-release', 'obj_ext': '.o', 'obj_dir': release_objs_dir, 'target': os.path.join('..', 'platform_release.a'), 'file_filter': isGTKSource, 'set': _release_settings + _gtk_settings},
		{'name': 'test-release', 'obj_ext': '.o', 'obj_dir': release_objs_dir, 'target': 'tests_release', 'file_filter': isTestSource, 'set': _release_settings},
		{'name': 'debug', 'obj_ext': '.o', 'obj_dir': debug_objs_dir, 'target': os.path.join('..', 'debug.a'), 'file_filter': isMainSorce, 'set': _debug_settings},
		{'name': 'pl-debug', 'obj_ext': '.o', 'obj_dir': debug_objs_dir, 'target': os.path.join('..', 'platform_debug.a'), 'file_filter': isGTKSource, 'set': _debug_settings + _gtk_settings},
		{'name': 'test-debug', 'obj_ext': '.o', 'obj_dir': debug_objs_dir, 'target': 'tests_debug', 'file_filter': isTestSource, 'set': _debug_settings}]

batches = ['main: debug release',
			'test: test-debug test-release',
			'platform: pl-debug pl-release',
			'all: main platform']

def prepareBuild():
	result = src_collector.getUpdatedSources('..', modes)
	#### writing makefile

	fout = open(makefile_name, 'w')
	fout.write("# This file is auto-generated with script\n\n")
	fout.write("all: release\n\n")
	fout.write("cleanup:\n")
	fout.write("\trm -f makefile\n\n")

	## release part
	for m in modes:
		name = m['name']
		is_test = name.startswith('test')
		r = result[name]
		# link-target
		fout.write("{0}: {0}_objs cleanup\n".format(name))
		fout.write("\t@echo linking for {0}..\n".format(name))
		if len(r['obj_files']) > 0:
			fout.write("\t{0}{1} {2}".format("g++ -o " if is_test else "ar cr ", m['target'], r['obj_files']))
		else:
			fout.write("\t@echo no changes found for linking")
		fout.write("\n\n")
		# compile-target
		fout.write("{0}_objs:\n".format(name))
		fout.write("\t@echo compiling for {0}..\n".format(name))
		if len(r['src_files']) > 0:
			fout.write("\tg++ -c {0} {1}\n".format(m['set'], r['src_files'])) # code files compiling
			fout.write("\tmv *{0} {1}".format(m['obj_ext'], m['obj_dir']))
		else:
			fout.write("\t@echo no changes found for compiling")
		fout.write("\n\n")

	## cleanup part
	fout.write("clean:\n")
	to_remove = {}
	for m in modes:
		to_remove['-rf ' + m['obj_dir']] = True
		to_remove['-f ' + m['target']] = True
	for k in to_remove.keys():
		fout.write("\trm {0}\n".format(k))

	for batch in batches:
		fout.write("\n" + batch + "\n")
	fout.close()

	# running command prompt
	cmds = '  '.join(mode['name'] for mode in modes)
	os.system("echo Use 'make' + command to build library")
	os.system("echo Commands are: " + cmds)
	os.system("echo makefile will be removed automatically after executing.")

prepareBuild() # run
