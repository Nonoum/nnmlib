import os
import re
from os.path import join, normpath, exists

_sources_info = {} # static dictionary for collected sources
_include_pattern = re.compile('^[ \t\f\v]*?#include\s+\".*?"', re.MULTILINE)

_cur_working_dir = os.getcwd()

# returns array of file names with full path from specified file - from strings STARTS with (#include) and have file name in double apostraph
# like: (#include "name")
# examples:
#	src = "C:\\.\\project\\common\\cpp.cpp":
#	#include "..\\utils\\head.h"
#	-> "C:/project/utils/head.h"
#	#include   "D:\\other\\hpp.hpp"
#	-> "D:/other/hpp.hpp"
#	#include <string.h>
#	-> nothing
def findIncludes(src):
	src_dir = os.path.dirname( normpath(src) )
	results = []
	for match in _include_pattern.finditer( open(src, 'r').read() ):
		str = match.group()
		include = str[str.find('"')+1 : str.rfind('"')]
		results.append( include if os.path.isabs(include) else join(src_dir, include) )
	return results

# returns date of last changes for specified source file (maximum modification date of current file, all included and sub-included files)
def lastChangesDate(fname):
	fname = normpath(fname)
	date = max(os.path.getmtime(fname), os.path.getctime(fname)) # in case of recursive inclusion we will return date of this file
	if fname in _sources_info:
		return _sources_info[fname]
	includes = findIncludes(fname)

	_sources_info[fname] = date # to break recursion
	if len(includes):
		_sources_info[fname] = max(date, max(lastChangesDate(fn) for fn in includes))
	return _sources_info[fname]

# returns true if target_filename is behind source_date or not exist
def needUpdate(target_filename, source_date):
	return (not exists(target_filename)) or (os.path.getmtime(target_filename) < source_date)

# Modes: array with records:
#	'name' - mode name;
#	'obj_ext' - object files extension;
#	'obj_dir' - object files directory;
#	'file_filter' - file filter function ( foo(src_fname): return if_accept );
#	'target' - target file name.
# Returns: dictionary with records (by key == 'name'):
#	'src_files' - line with src file names with path from root;
#	'obj_files' - line with object file names with path from 'obj_dir';
#	'latest_changes' - date of latest changes in sources hierarchy.
def getUpdatedSources(root, modes):
	_sources_info = {} # reseting sources info
	result = {}
	#collecting all needed filenames in line
	for m in modes:
		result[m['name']] = {'src_files': '',
							'obj_files': '',
							'latest_changes': None}

	for path, directories, files in os.walk('..'): # search from parent dir
		for fname in files:
			ext = os.path.splitext(fname)[1]
			cut_name = fname[:(len(fname) - len(ext))] # file name without extension
			full_fname = join(path, fname)
			current_file_changes_date = lastChangesDate(full_fname)

			for m in modes:
				if m['file_filter'](fname) == True:
					res = result[m['name']]

					if (res['latest_changes'] == None) or (res['latest_changes'] < current_file_changes_date):
						res['latest_changes'] = current_file_changes_date

					obj_fname = join(m['obj_dir'], cut_name + m['obj_ext'])
					if needUpdate(obj_fname, current_file_changes_date):
						res['src_files'] += '"{0}" '.format(full_fname)
					res['obj_files'] += '"{0}" '.format(obj_fname)

	# cleaning targets outputs if everything is up to date
	for m in modes:
		res = result[m['name']]
		target_path = normpath( join(_cur_working_dir, m['target']) )
		if not needUpdate(target_path, res['latest_changes']):
			res['obj_files'] = ''
	return result
