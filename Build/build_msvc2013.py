import os
import src_collector
import initializing
from config import * # filters and constants

_debug_settings = '/MTd /D "_DEBUG" /EHsc'
_release_settings = '/O2 /Ot /MT /EHsc /D "NDEBUG"'

modes = [{'name': 'debug', 'obj_ext': '.obj', 'obj_dir': debug_objs_dir, 'target': os.path.join('..', 'debug.lib'), 'file_filter': isMainSorce, 'set': _debug_settings},
		{'name': 'release', 'obj_ext': '.obj', 'obj_dir': release_objs_dir, 'target': os.path.join('..', 'release.lib'), 'file_filter': isMainSorce, 'set': _release_settings},
		{'name': 'pl-debug', 'obj_ext': '.obj', 'obj_dir': debug_objs_dir, 'target': os.path.join('..', 'platform_debug.lib'), 'file_filter': isWinAPISource, 'set': _debug_settings},
		{'name': 'pl-release', 'obj_ext': '.obj', 'obj_dir': release_objs_dir, 'target': os.path.join('..', 'platform_release.lib'), 'file_filter': isWinAPISource, 'set': _release_settings},
		{'name': 'test-debug', 'obj_ext': '.obj', 'obj_dir': debug_objs_dir, 'target': 'tests_debug.exe', 'file_filter': isTestSource, 'set': _debug_settings},
		{'name': 'test-release', 'obj_ext': '.obj', 'obj_dir': release_objs_dir, 'target': 'tests_release.exe', 'file_filter': isTestSource, 'set': _release_settings}]

batches = ['main: debug release',
			'test: test-debug test-release',
			'platform: pl-debug pl-release',
			'all: main platform']

def prepareBuild():
	result = src_collector.getUpdatedSources('..', modes)
	#### writing makefile

	fout = open(makefile_name, 'w')
	fout.write("# This file is auto-generated with script\n\n")
	fout.write("all: release\n\n")

	## release part
	for m in modes:
		name = m['name']
		is_test = name.startswith('test')
		r = result[name]
		# link-target
		fout.write("{0}: {0}_objs\n".format(name))
		fout.write("  @echo linking for {0}..\n".format(name))
		if len(r['obj_files']) > 0:
			fout.write("  {0}{1} {2}".format("link /OUT:" if is_test else "lib /OUT:", m['target'], r['obj_files']))
		else:
			fout.write("  @echo no changes found for linking")
		fout.write("\n\n")
		# compile-target
		fout.write("{0}_objs:\n".format(name))
		fout.write("  @echo compiling for {0}..\n".format(name))
		if len(r['src_files']) > 0:
			fout.write("  cl /c /MP8 {0} {1}\n".format(m['set'], r['src_files'])) # code files compiling
			fout.write("  move /y *{0} {1}/".format(m['obj_ext'], m['obj_dir']))
		else:
			fout.write("  @echo no changes found for compiling")
		fout.write("\n\n")

	## cleanup part
	fout.write("clean:\n")
	to_remove = {}
	for m in modes:
		to_remove[m['obj_dir']] = True
		to_remove[m['target']] = True
	for k in to_remove.keys():
		fout.write("  del {0} /q\n".format(k))

	for batch in batches:
		fout.write("\n" + batch + "\n")
	fout.close()

	# running command prompt
	cmds = '  '.join(mode['name'] for mode in modes)
	os.system("echo Use 'NMAKE' + command to build library")
	os.system("echo Commands are: " + cmds)
	os.system("echo For build after cleaning - script MUST be run again to generate new makefile")
	os.system("msvc_command_prompt.bat")
	# cleanup
	os.system("del " + makefile_name)

prepareBuild() # run
