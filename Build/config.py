import os

def isSource(fname): # any source file
	return os.path.splitext(fname)[1].lower() in [".cpp", ".c"]

def isTestSource(fname):
	return isMainSorce(fname) or (isSource(fname) and fname.lower().startswith("test_"))

def isMainSorce(fname): # source for main part of library (not test and not platform)
	return isSource(fname) and not fname.lower().startswith("test_") and not fname.lower().startswith("platform_")

def isWinAPISource(fname):
	return isSource(fname) and (fname.lower().startswith("platform_all") or fname.lower().startswith("platform_winapi"))

def isGTKSource(fname):
	return isSource(fname) and (fname.lower().startswith("platform_all") or fname.lower().startswith("platform_gtk"))

makefile_name = "makefile"
debug_objs_dir = "Debug"
release_objs_dir = "Release"
