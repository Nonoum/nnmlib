#pragma once
#include "../BitProcessing/head.h"

/*************************************************************************************
// -------------------------------------------------------------------------------- //
// ----------------------- HUFFMAN CODER AND DECODER LIBRARY ---------------------- //
// ------------------------ Implemented by Evstratov Evgeniy ---------------------- //
// -------------------------------------------------------------------------------- //

	Maximum ~50 gb data, program DOESN'T check it,
	and makes WRONG stream if condition not observed.
*************************************************************************************/

namespace HuffmanCodec {

using namespace BitProcessing;

usize Compress(const ubyte* src, usize bytes_cnt, usize interval, void* dst, usize* dst_byte, ubyte* dst_bit, bool write);
/*	'Compress' encodes (bytes_cnt) bytes from (src) with (interval) bytes between neighbours, with huffman
	method, to (dst) from position (byte = *dst_byte, bit = *dst_bit) IF (write) is (true), and returns bits count
	of resulting data in ANY way.
	stream DOESN'T contain information about sizes of input or output.
	if (write) is (false), (dst, dst_byte, dst_bit) it unused (can be NULL), if (write) is (true) then function
	changes (dst_byte, dst_bit) respectively.
	IMPORTANT: (dst) HAVE TO BE pre-filled with zeroes.
*/

void Decompress(void* src, usize* src_byte, ubyte* src_bit, ubyte* dst, usize bytes_cnt, usize interval, usize uint32_bound = -1);
/*	'Decompress' decodes encoded huffman stream from (src) from position (byte = *src_byte, bit = *src_bit)
	to (dst) with (interval) bytes between neighbours, supposing that stream contain (bytes_cnt) encoded bytes.
	(uint32_bound) is offset of last uint32 in (src), if it's specified not as (-1) then additional checks
	provided on decompressing: throws std::out_of_range on exceeding specified bound.
*/

}; // end HuffmanCodec
