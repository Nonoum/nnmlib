#pragma once
#include "head.h"
#include <cstdlib> // 'NULL'

namespace HuffmanCodec {

class HTree : private NonCopyable {
	HTree *right;
	HTree *left;
	HTree *parent;
	ubyte code;
	usize value;
	// methods
	inline bool isSymbol() const {
		return left == NULL; // supposes that tree is completed and both Left and Right are NULL
	};
	inline bool isRoot() const {
		return parent == NULL;
	};
	inline void unpackMake(HTree* L, HTree* R) {
		L->parent = this;
		R->parent = this;
		left = L;
		right = R;
	};
	inline void setCode(ubyte _code) {
		code = _code;
		right = left = NULL;
	};
	inline void insertionSortDown(int size) {
		int i, j;
		register ubyte db;
		register usize dw;
		for(i = 1; i < size; ++i) {
			db = this[i].code;
			dw = this[i].value;
			for(j = i-1; j >= 0 && this[j].value < dw; --j) {
				this[j+1].code = this[j].code;
				this[j+1].value = this[j].value;
			}
			this[j+1].value = dw;
			this[j+1].code = db;
		}
	};
public:
	inline void set(ubyte _code, usize _value) {
		value = _value;
		setCode(_code);
	};
	inline ubyte getCode() const {
		return code;
	};
	inline void make(HTree* L, HTree* R) {
		parent = NULL;
		L->parent = this;
		R->parent = this;
		left = L;
		right = R;
		value = L->value + R->value;
	}; // end make
	inline bool operator >=(HTree& h2) {
		return value >= h2.value;
	};
	// more complicated methods
	void quickSortDown(int size);
	ubyte wayFromTop(uint32* res); // returns count of bits per code
	void compressTree(void* dst, usize* dst_byte, ubyte* dst_bit); // compresses the tree from this node
	void unpackTree(HTree* memory, short* next, void* src, usize* src_byte, ubyte* src_bit); // decompresses tree to this node
	ubyte find(void* src, usize* src_byte, ubyte* src_bit); // decodes a byte from compressed stream
	// secured method versions, throws std::out_of_range on range errors. (uint32_bound) is offset of last uint32 in source
	void unpackTreeSecured(HTree* memory, short* next, void* src, usize* src_byte, ubyte* src_bit, usize uint32_bound);
	ubyte findSecured(void* src, usize* src_byte, ubyte* src_bit, usize uint32_bound);
}; // end class HTree

}; // end HuffmanCodec
