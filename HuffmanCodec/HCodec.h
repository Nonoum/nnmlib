#pragma once
#include "HTree.h"

namespace HuffmanCodec {

class HCodec : private NonCopyable {
	HTree parts[511]; // huffman tree
	short indexes[511]; // sorted indexes, by reduction of parts[indexes[i]].value
	short links[256]; // indexes of appropriate codes
	short next; // next index in memory
	short isize; // count of free indexes
	short root; // root index
public:
	HCodec();
	usize compress(const ubyte* src, usize bytes_cnt, usize interval, void* dst, usize* dst_byte, ubyte* dst_bit, bool write);
	void decompress(void* src, usize* src_byte, ubyte* src_bit, ubyte* dst, usize bytes_cnt, usize interval, usize uint32_bound = -1);
private:
	void loadStats(usize freqs[256]);
	void makeIter();
	void make();
}; // end class HCodec

}; // end HuffmanCodec
