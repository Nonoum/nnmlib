#include "head.h"
#include "HCodec.h"

namespace HuffmanCodec {

usize Compress(const ubyte* src, usize bytes_cnt, usize interval, void* dst, usize* dst_byte, ubyte* dst_bit, bool write) {
	HCodec hfnn;
	return hfnn.compress(src, bytes_cnt, interval, dst, dst_byte, dst_bit, write);
}; // end compress

void Decompress(void* src, usize* src_byte, ubyte* src_bit, ubyte* dst, usize bytes_cnt, usize interval, usize uint32_bound) {
	HCodec hfnn;
	hfnn.decompress(src, src_byte, src_bit, dst, bytes_cnt, interval, uint32_bound);
}; // end decompress

}; // end HuffmanCodec
