#include "HCodec.h"
#include <stdexcept>
#include <algorithm>
#include <assert.h>

namespace HuffmanCodec {

HCodec :: HCodec()
		: next(0), root(0) {
};

void HCodec :: loadStats(usize freqs[256]) {
	for(int i = 0; i < 256; ++i) {
		if(freqs[i] != 0) {
			parts[next].set((ubyte)i, freqs[i]);
			++next;
		}
	}
	parts->quickSortDown(next); // fast version
	isize = next;
	for(int i = 0; i < next; ++i) {
		indexes[i] = i;
		links[parts[i].getCode()] = i;
	}
}; // end Load Stats

void HCodec :: makeIter() {
	short i;
	parts[next].make(&parts[indexes[isize-2]], &parts[indexes[isize-1]]);
	isize -= 2;
	if(isize == 0) {
		isize = 1;
		indexes[0] = next;
		return;
	}
	for(i = isize - 1; i >= 0; --i) {
		if(parts[indexes[i]] >= parts[next]) {
			break;
		}
	}
	i < 0 ? i = 0 : ++i;
	for(short j = isize; j > i; --j) {
		indexes[j] = indexes[j-1];
	}
	indexes[i] = next;
	++isize;
	++next;
}; // end make iter

void HCodec :: make() {
	while(isize >= 2) { // while have a pair of nodes, create a new parent node
		makeIter();
	}
	root = indexes[0];
}; // end make

usize HCodec :: compress(const ubyte* src, usize bytes_cnt, usize interval, void* dst, usize* dst_byte, ubyte* dst_bit, bool write) {
	if(! bytes_cnt) {
		assert(false && "Huffman block can't be zero-length!");
		return 0;
	}
	usize freqs[256];
	uint32 codes[256*2]; // 256 codes by 2 dwords per code maximum
	ubyte cbits[256]; // bit counts
	usize totalbits, ndifferent;
	ubyte value;
	// calc frequences
	std::fill(freqs, freqs+256, 0);
	for(usize i = 0, ofs = 0; i < bytes_cnt; ++i) {
		++freqs[src[ofs]];
		ofs += interval;
	}
	// making tree
	ndifferent = 0;
	for(usize i = 0; i < 256; ++i) {
		if(freqs[i]) {
			++ndifferent;
			value = (ubyte)i; // save code
		}
	}
	totalbits = (ndifferent * 10) - 1; // size of tree
	if(ndifferent == 1) { // only one different code
		// optimization - avoid making tree, e.t.c.
		if(write) {
			WriteBits(dst, dst_byte, dst_bit, 8+1, (uint32(value) << 1) | 1);
		}
	} else { // > 1 different codes
		loadStats(freqs);
		make();
		// compressing tree
		if(write) {
			parts[root].compressTree(dst, dst_byte, dst_bit);
		}
		// caching codes
		for(usize i = 0; i < 256; ++i) {
			if(freqs[i]) {
				cbits[i] = parts[links[i]].wayFromTop(&codes[i]);
				totalbits += freqs[i] * cbits[i];
			}
		}
		// compressing data
		if(write) {
			for(usize i = 0, ofs = 0; i < bytes_cnt; ++i) {
				value = src[ofs];
				if(cbits[value] < 26) { // one part
					WriteBits(dst, dst_byte, dst_bit, cbits[value], codes[value]);
				} else { // 2 parts
					WriteBits(dst, dst_byte, dst_bit, cbits[value]-25, codes[value]); // lower part
					WriteBits(dst, dst_byte, dst_bit, 25, codes[value+256]); // higher part
				}
				ofs += interval;
			}
		}
	}
	return totalbits;
}; // end compress

void HCodec :: decompress(void* src, usize* src_byte, ubyte* src_bit, ubyte* dst, usize bytes_cnt, usize interval, usize uint32_bound) {
	if(! bytes_cnt) {
		assert(false && "Compressed Huffman block can't be zero-length!");
		return;
	}
	if((uint32_bound != usize(-1)) && (*src_byte > uint32_bound)) {
		throw std::out_of_range("Huffman decompressing out of range (1)");
	}
	if(ReadBits(src, src_byte, src_bit, 1)) {
		if((uint32_bound != usize(-1)) && (*src_byte > uint32_bound)) {
			throw std::out_of_range("Huffman decompressing out of range (2)");
		}
		ubyte buf = (ubyte)ReadBits(src, src_byte, src_bit, 8);
		for(usize i = 0, ofs = 0; i < bytes_cnt; ++i, ofs += interval) {
			dst[ofs] = buf;
		}
		return;
	}
	next = 1;
	if(uint32_bound == usize(-1)) {
		parts[0].unpackTree(parts, &next, src, src_byte, src_bit);
		// unpacked!, decoding
		for(usize i = 0, ofs = 0; i < bytes_cnt; ++i, ofs += interval) {
			dst[ofs] = parts[0].find(src, src_byte, src_bit);
		}
	} else {
		parts[0].unpackTreeSecured(parts, &next, src, src_byte, src_bit, uint32_bound);
		for(usize i = 0, ofs = 0; i < bytes_cnt; ++i, ofs += interval) {
			dst[ofs] = parts[0].findSecured(src, src_byte, src_bit, uint32_bound);
		}
	}
	return;
}; // end decompress

}; // end HuffmanCodec
