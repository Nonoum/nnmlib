#include "HTree.h"
#include <stdexcept>

namespace HuffmanCodec {

void HTree :: quickSortDown(int size) {
	if(size < 32) {
		insertionSortDown(size);
		return;
	}
	const usize candidate = this[size >> 1].value;
	int i = 0, j = size - 1;
	while(i <= j)
	{
		while(this[i].value > candidate) {
			++i;
		}
		while(this[j].value < candidate) {
			--j;
		}
		if(i > j){
			break;
		}
		register ubyte db = this[i].code;
		this[i].code = this[j].code;
		this[j].code = db;
		register usize dw = this[i].value;
		this[i].value = this[j].value;
		this[j].value = dw;
		++i;
		--j;
	}
	if(j > 0) {
		quickSortDown(j + 1);
	}
	if(i < size) {
		this[i].quickSortDown(size - i);
	}
};

ubyte HTree :: wayFromTop(uint32* res) { // returns count of bits
	// new (50 bits) code.
	// features:
	// 1. res[256] is high part
	// 2. if returning 'len' > 25 then high dword has 25, and low dword has the rest (1..25)
	HTree *ht, *hprev;
	ubyte len = 0;
	uint32 val;
	val = 0;
	hprev = this;
	ht = parent;
	while(1) {
		len++;
		if(len == 26) { // part done
			res[256] = val;
			val = 0;
		}
		val <<= 1;
		if(hprev == ht->right) {
			val |= 1;
		}
		if(ht->isRoot()) {
			break; // exit while
		}
		hprev = ht;
		ht = ht->parent;
	}; // end while
	*res = val;
	return len;
}; // end way from top

void HTree :: compressTree(void* dst, usize* dst_byte, ubyte* dst_bit) {
	if(isSymbol()) {
		WriteBits(dst, dst_byte, dst_bit, 8+1, (uint32(code) << 1) | 1); // bit [1] , then code [8]
	} else {
		WriteBits(dst, dst_byte, dst_bit, 1, 0);
		left->compressTree(dst, dst_byte, dst_bit);
		right->compressTree(dst, dst_byte, dst_bit);
	}
}; // end compress tree

void HTree :: unpackTree(HTree* memory, short* next, void* src, usize* src_byte, ubyte* src_bit) {
	if(*next == 1) { // start
		unpackMake(&memory[*next], &memory[(*next)+1]);
		(*next) += 2;
		left->unpackTree(memory, next, src, src_byte, src_bit);
		right->unpackTree(memory, next, src, src_byte, src_bit);
		return;
	}
	if(ReadBits(src, src_byte, src_bit, 1) == 1) { // value (leaf)
		setCode((ubyte)ReadBits(src, src_byte, src_bit, 8));
		return;
	}
	unpackMake(&memory[*next], &memory[(*next)+1]);
	(*next) += 2;
	left->unpackTree(memory, next, src, src_byte, src_bit);
	right->unpackTree(memory, next, src, src_byte, src_bit);
	return;
}; // end unpack HTree

void HTree :: unpackTreeSecured(HTree* memory, short* next, void* src, usize* src_byte, ubyte* src_bit, usize uint32_bound) {
	if(*next == 1) { // start
		unpackMake(&memory[*next], &memory[(*next)+1]);
		(*next) += 2;
		left->unpackTreeSecured(memory, next, src, src_byte, src_bit, uint32_bound);
		right->unpackTreeSecured(memory, next, src, src_byte, src_bit, uint32_bound);
		return;
	}
	if(*src_byte > uint32_bound) {
		throw std::out_of_range("Huffman decompressing out of range (3)");
	}
	if(ReadBits(src, src_byte, src_bit, 1) == 1) { // value (leaf)
		if(*src_byte > uint32_bound) {
			throw std::out_of_range("Huffman decompressing out of range (4)");
		}
		setCode((ubyte)ReadBits(src, src_byte, src_bit, 8));
		return;
	}
	if(*next >= 511) { // hardcoded tree memory limit
		throw std::out_of_range("Huffman decompressing out of range (0)");
	}
	unpackMake(&memory[*next], &memory[(*next)+1]);
	(*next) += 2;
	left->unpackTreeSecured(memory, next, src, src_byte, src_bit, uint32_bound);
	right->unpackTreeSecured(memory, next, src, src_byte, src_bit, uint32_bound);
	return;
}; // end unpack HTree

ubyte HTree :: find(void* src, usize* src_byte, ubyte* src_bit) { // any bit count supported
	// optimized code with bit conveyor
	HTree* ht;
	uint32 bpassed, bmask, val, dwpassed;
	dwpassed = 0;
	bpassed = 0;
	bmask = uint32(1L << (*src_bit));
	val = *((uint32*)((ubyte*)src+(*src_byte)));
	ht = this;
	while(1) {
		if(ht->isSymbol()) {
			break; // found!
		}
		if(val & bmask) {
			ht = ht->right;
		} else {
			ht = ht->left;
		}
		++bpassed; // bits passed
		bmask <<= 1;
		if(! bmask) {
			bmask = 1;
			++dwpassed;
			val = ((uint32*)((ubyte*)src+(*src_byte)))[dwpassed];
		}
	} // end while
	bpassed += (*src_bit);
	(*src_byte) += (bpassed >> 3);
	(*src_bit) = (ubyte)(bpassed & 0x07);
	return ht->code;
}; // end find code

ubyte HTree :: findSecured(void* src, usize* src_byte, ubyte* src_bit, usize uint32_bound) { // any bit count supported
	// optimized code with bit conveyor
	HTree* ht;
	uint32 bpassed, bmask, val, dwpassed;
	dwpassed = 0;
	bpassed = 0;
	bmask = uint32(1L << (*src_bit));
	if(*src_byte > uint32_bound) {
		throw std::out_of_range("Huffman decompressing out of range (5)");
	}
	val = *((uint32*)((ubyte*)src+(*src_byte)));
	ht = this;
	while(1) {
		if(ht->isSymbol()) {
			break; // found!
		}
		if(val & bmask) {
			ht = ht->right;
		} else {
			ht = ht->left;
		}
		++bpassed; // bits passed
		bmask <<= 1;
		if(! bmask) {
			bmask = 1;
			++dwpassed;
			if(*src_byte > uint32_bound) {
				throw std::out_of_range("Huffman decompressing out of range (6)");
			}
			val = ((uint32*)((ubyte*)src+(*src_byte)))[dwpassed];
		}
	} // end while
	bpassed += (*src_bit);
	(*src_byte) += (bpassed >> 3);
	(*src_bit) = (ubyte)(bpassed & 0x07);
	return ht->code;
}; // end find code

}; // end HuffmanCodec
